-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `gigskyNotificationGateway`.`Device` 
ADD COLUMN `preferences` LONGTEXT NULL DEFAULT NULL AFTER `updateTime`;


DELIMITER $$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`VersionInformation_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`VersionInformation_BEFORE_UPDATE` BEFORE UPDATE ON `VersionInformation` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`HttpError_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`HttpError_BEFORE_UPDATE` BEFORE UPDATE ON `HttpError` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`ErrorString_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`ErrorString_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorString` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`ConfigurationsKeyValue_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`ConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `ConfigurationsKeyValue` FOR EACH ROW
    begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`UrlDetail_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`UrlDetail_BEFORE_UPDATE` BEFORE UPDATE ON `UrlDetail` FOR EACH ROW
    begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`ThirdPartyTool_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`ThirdPartyTool_BEFORE_UPDATE` BEFORE UPDATE ON `ThirdPartyTool` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Customer_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Customer_BEFORE_UPDATE` BEFORE UPDATE ON `Customer` FOR EACH ROW
    begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Device_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Device_BEFORE_UPDATE` BEFORE UPDATE ON `Device` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`CustomerDevice_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerDevice_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerDevice` FOR EACH ROW
    begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Template_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Template_BEFORE_UPDATE` BEFORE UPDATE ON `Template` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`NotificationType_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`NotificationType_BEFORE_UPDATE` BEFORE UPDATE ON `NotificationType` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Policy_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Policy_BEFORE_UPDATE` BEFORE UPDATE ON `Policy` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`EventType_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`EventType_BEFORE_UPDATE` BEFORE UPDATE ON `EventType` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`GroupType_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`GroupType_BEFORE_UPDATE` BEFORE UPDATE ON `GroupType` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`EventPolicy_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`EventPolicy_BEFORE_UPDATE` BEFORE UPDATE ON `EventPolicy` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Event_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Event_BEFORE_UPDATE` BEFORE UPDATE ON `Event` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`CustomerEvent_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerEvent_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerEvent` FOR EACH ROW
 begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Notification_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Notification_BEFORE_UPDATE` BEFORE UPDATE ON `Notification` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`PushNotification_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`PushNotification_BEFORE_UPDATE` BEFORE UPDATE ON `PushNotification` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`NotificationTemplate_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`NotificationTemplate_BEFORE_UPDATE` BEFORE UPDATE ON `NotificationTemplate` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`SupportedClient_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`SupportedClient_BEFORE_UPDATE` BEFORE UPDATE ON `SupportedClient` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`GroupEvent_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`GroupEvent_BEFORE_UPDATE` BEFORE UPDATE ON `GroupEvent` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`PushedDevice_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`PushedDevice_BEFORE_UPDATE` BEFORE UPDATE ON `PushedDevice` FOR EACH ROW
      begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`GroupEventCustomer_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`GroupEventCustomer_BEFORE_UPDATE` BEFORE UPDATE ON `GroupEventCustomer` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`CustomerGroup_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerGroup_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerGroup` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`MockUser_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockUser_BEFORE_UPDATE` BEFORE UPDATE ON `MockUser` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`MockUserToken_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockUserToken_BEFORE_UPDATE` BEFORE UPDATE ON `MockUserToken` FOR EACH ROW
begin
	set new.updateTime = now();
end$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`MockPushNotification_BEFORE_UPDATE` $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockPushNotification_BEFORE_UPDATE` BEFORE UPDATE ON `MockPushNotification` FOR EACH ROW
begin
	set new.updateTime = now();
end$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
