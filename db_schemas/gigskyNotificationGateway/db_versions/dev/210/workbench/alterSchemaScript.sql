SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `gigskyNotificationGateway`.`PushNotification` 
CHANGE COLUMN `location` `location` VARCHAR(250) NULL DEFAULT NULL ;

ALTER TABLE `gigskyNotificationGateway`.`MockEmailNotificationSent` 
ADD COLUMN `Notification_id` INT(11) NOT NULL AFTER `id`,
ADD INDEX `fk_MockEmailNotificationSent_Notification1_idx` (`Notification_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`MockEmailNotificationSent` 
ADD CONSTRAINT `fk_MockEmailNotificationSent_Notification1`
  FOREIGN KEY (`Notification_id`)
  REFERENCES `gigskyNotificationGateway`.`Notification` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
