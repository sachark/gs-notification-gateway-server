SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`CustomerContext` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `gsCustomerId` BIGINT(20) NULL DEFAULT NULL,
  `token` VARCHAR(45) NULL DEFAULT NULL,
  `iccid` VARCHAR(40) NULL DEFAULT NULL,
  `simNickname` VARCHAR(64) NULL DEFAULT NULL,
  `simType` VARCHAR(32) NULL DEFAULT NULL,
  `deviceId` VARCHAR(255) NULL DEFAULT NULL,
  `userId` VARCHAR(255) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_unicode_ci;


DELIMITER $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerContext_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerContext` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
