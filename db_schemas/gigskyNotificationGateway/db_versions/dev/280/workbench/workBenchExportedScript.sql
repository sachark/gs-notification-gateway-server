-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema gigskyNotificationGateway
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `gigskyNotificationGateway` ;

-- -----------------------------------------------------
-- Schema gigskyNotificationGateway
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gigskyNotificationGateway` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `gigskyNotificationGateway` ;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`VersionInformation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`VersionInformation` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`VersionInformation` (
  `major` INT NOT NULL,
  `minor` INT NULL,
  `build` INT NULL,
  `buildType` ENUM('dev','test','live','stage') NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  PACK_KEYS = Default;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`HttpError`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`HttpError` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`HttpError` (
  `id` INT NOT NULL,
  `errorCode` INT NOT NULL,
  `errorString` VARCHAR(500) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `errorCode_UNIQUE` (`errorCode` ASC))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`ErrorString`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`ErrorString` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`ErrorString` (
  `id` INT NOT NULL,
  `errorId` INT NOT NULL,
  `errorString` TEXT NOT NULL,
  `httpError_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `errorId_UNIQUE` (`errorId` ASC),
  INDEX `fk_ErrorString_HttpError1_idx` (`httpError_id` ASC),
  CONSTRAINT `fk_ErrorString_HttpError1`
  FOREIGN KEY (`httpError_id`)
  REFERENCES `gigskyNotificationGateway`.`HttpError` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`ConfigurationsKeyValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`ConfigurationsKeyValue` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`ConfigurationsKeyValue` (
  `ckey` VARCHAR(250) NOT NULL,
  `value` TEXT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  UNIQUE INDEX `key_UNIQUE` (`ckey` ASC),
  PRIMARY KEY (`ckey`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`UrlDetail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`UrlDetail` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`UrlDetail` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `apiBaseUrl` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`ThirdPartyTool`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`ThirdPartyTool` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`ThirdPartyTool` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `version` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`Tenant`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`Tenant` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Tenant` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL,
  `description` LONGTEXT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`Customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `preferences` LONGTEXT NULL,
  `tenant_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `customerUuid` VARCHAR(45) NULL,
  `customerType` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Customer_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_Customer_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`Device`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`Device` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Device` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `deviceType` ENUM('IOS','ANDROID') NULL,
  `deviceOSVersion` VARCHAR(45) NULL,
  `model` VARCHAR(45) NULL,
  `channelId` VARCHAR(45) NULL,
  `installationId` VARCHAR(45) NULL,
  `deviceToken` VARCHAR(100) NULL,
  `locale` VARCHAR(45) NULL,
  `appVersion` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `preferences` LONGTEXT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`CustomerDevice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`CustomerDevice` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`CustomerDevice` (
  `lastAccessTime` TIMESTAMP NULL,
  `device_id` INT NOT NULL,
  `customer_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `isDeleted` TINYINT(1) NULL,
  INDEX `fk_CustomerDevice_Device1_idx` (`device_id` ASC),
  INDEX `fk_CustomerDevice_Customer1_idx` (`customer_id` ASC),
  PRIMARY KEY (`device_id`, `customer_id`),
  CONSTRAINT `fk_CustomerDevice_Device1`
  FOREIGN KEY (`device_id`)
  REFERENCES `gigskyNotificationGateway`.`Device` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CustomerDevice_Customer1`
  FOREIGN KEY (`customer_id`)
  REFERENCES `gigskyNotificationGateway`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`Template`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`Template` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Template` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `content` LONGTEXT NULL,
  `languageCode` VARCHAR(45) NULL,
  `contentType` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`NotificationType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`NotificationType` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`NotificationType` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `type` ENUM('PUSH','SMS','EMAIL') NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`Policy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`Policy` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Policy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `interval` INT NULL,
  `repeatCount` INT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `notificationType_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Policy_NotificationType1_idx` (`notificationType_id` ASC),
  CONSTRAINT `fk_Policy_NotificationType1`
  FOREIGN KEY (`notificationType_id`)
  REFERENCES `gigskyNotificationGateway`.`NotificationType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`EventType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`EventType` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`EventType` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` ENUM('LOW_BALANCE','DATA_USED','SUBSCRIPTION_NEARING_EXPIRY','SUBSCRIPTION_EXPIRED','NEW_LOCATION','EMAIL') NULL,
  `subEventType` VARCHAR(100) NULL,
  `description` VARCHAR(45) NULL,
  `enableNotification` TINYINT(1) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`GroupType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`GroupType` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`GroupType` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`EventPolicy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`EventPolicy` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`EventPolicy` (
  `policy_id` INT NOT NULL,
  `eventType_id` INT NOT NULL,
  `tenant_id` INT NOT NULL,
  `enableEvent` TINYINT(1) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `groupType_id` INT NULL,
  PRIMARY KEY (`policy_id`, `eventType_id`, `tenant_id`),
  INDEX `fk_events_policy_policy1_idx` (`policy_id` ASC),
  INDEX `fk_eventsPolicy_eventType1_idx` (`eventType_id` ASC),
  INDEX `fk_EventPolicy_GroupType1_idx` (`groupType_id` ASC),
  INDEX `fk_EventPolicy_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_events_policy_policy1`
  FOREIGN KEY (`policy_id`)
  REFERENCES `gigskyNotificationGateway`.`Policy` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_eventsPolicy_eventType1`
  FOREIGN KEY (`eventType_id`)
  REFERENCES `gigskyNotificationGateway`.`EventType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_EventPolicy_GroupType1`
  FOREIGN KEY (`groupType_id`)
  REFERENCES `gigskyNotificationGateway`.`GroupType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_EventPolicy_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`Event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`Event` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NULL,
  `receivedTime` TIMESTAMP NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `eventType_id` INT NOT NULL,
  `eventCategory` ENUM('SINGLE_EVENT','GROUP_EVENT') NULL,
  `statusMessage` VARCHAR(45) NULL,
  `failCount` INT NULL,
  `creationTime` VARCHAR(45) NULL,
  `tenant_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Event_EventType1_idx` (`eventType_id` ASC),
  INDEX `fk_Event_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_Event_EventType1`
  FOREIGN KEY (`eventType_id`)
  REFERENCES `gigskyNotificationGateway`.`EventType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Event_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`CustomerEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`CustomerEvent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`CustomerEvent` (
  `data` LONGTEXT NULL,
  `customer_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `event_id` INT NOT NULL,
  `tenant_id` INT NOT NULL,
  INDEX `fk_customer_events_customer1_idx` (`customer_id` ASC),
  INDEX `fk_CustomerEvent_Event1_idx` (`event_id` ASC),
  PRIMARY KEY (`event_id`),
  INDEX `fk_CustomerEvent_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_customer_events_customer1`
  FOREIGN KEY (`customer_id`)
  REFERENCES `gigskyNotificationGateway`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CustomerEvent_Event1`
  FOREIGN KEY (`event_id`)
  REFERENCES `gigskyNotificationGateway`.`Event` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CustomerEvent_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`Notification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`Notification` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Notification` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `notificationType_id` INT NOT NULL,
  `expiry` TIMESTAMP NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `event_id` INT NOT NULL,
  `status` VARCHAR(45) NULL,
  `statusMessage` VARCHAR(45) NULL,
  `policy_id` INT NOT NULL,
  `tenant_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_notification_notificationTypes1_idx` (`notificationType_id` ASC),
  INDEX `fk_Notification_Event1_idx` (`event_id` ASC),
  INDEX `fk_Notification_Policy1_idx` (`policy_id` ASC),
  INDEX `fk_Notification_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_notification_notificationTypes1`
  FOREIGN KEY (`notificationType_id`)
  REFERENCES `gigskyNotificationGateway`.`NotificationType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Notification_Event1`
  FOREIGN KEY (`event_id`)
  REFERENCES `gigskyNotificationGateway`.`Event` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Notification_Policy1`
  FOREIGN KEY (`policy_id`)
  REFERENCES `gigskyNotificationGateway`.`Policy` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Notification_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`PushNotification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`PushNotification` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`PushNotification` (
  `pushId` VARCHAR(45) NULL,
  `openCount` INT NULL,
  `pushTime` TIMESTAMP NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `iccId` VARCHAR(45) NULL,
  `location` VARCHAR(250) NULL,
  `sendCount` INT NULL,
  `languageCode` VARCHAR(45) NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `notification_id` INT NOT NULL,
  `status` VARCHAR(45) NULL,
  `lastAttemptStatus` VARCHAR(45) NULL,
  `lastAttemptTime` TIMESTAMP NULL,
  `attemptCount` INT NULL,
  `statusMessage` VARCHAR(45) NULL,
  `template_id` INT NOT NULL,
  `notificationCategory` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CustomerPushNotification_Notification1_idx` (`notification_id` ASC),
  INDEX `fk_CustomerPushNotification_Template1_idx` (`template_id` ASC),
  CONSTRAINT `fk_CustomerPushNotification_Notification1`
  FOREIGN KEY (`notification_id`)
  REFERENCES `gigskyNotificationGateway`.`Notification` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CustomerPushNotification_Template1`
  FOREIGN KEY (`template_id`)
  REFERENCES `gigskyNotificationGateway`.`Template` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`NotificationTemplate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`NotificationTemplate` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`NotificationTemplate` (
  `template_id` INT NOT NULL,
  `notificationType_id` INT NOT NULL,
  `eventType_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `tenant_id` INT NOT NULL,
  PRIMARY KEY (`template_id`, `notificationType_id`, `eventType_id`),
  INDEX `fk_NotificationTemplate_template1_idx` (`template_id` ASC),
  INDEX `fk_NotificationTemplate_notificationType1_idx` (`notificationType_id` ASC),
  INDEX `fk_NotificationTemplate_eventType1_idx` (`eventType_id` ASC),
  INDEX `fk_NotificationTemplate_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_NotificationTemplate_template1`
  FOREIGN KEY (`template_id`)
  REFERENCES `gigskyNotificationGateway`.`Template` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_NotificationTemplate_notificationType1`
  FOREIGN KEY (`notificationType_id`)
  REFERENCES `gigskyNotificationGateway`.`NotificationType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_NotificationTemplate_eventType1`
  FOREIGN KEY (`eventType_id`)
  REFERENCES `gigskyNotificationGateway`.`EventType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_NotificationTemplate_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`SupportedClient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`SupportedClient` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`SupportedClient` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `clientType` ENUM('IOS','ANDROID') NULL,
  `minClientVersion` VARCHAR(45) NULL,
  `policy_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_SupportedClient_Policy1_idx` (`policy_id` ASC),
  CONSTRAINT `fk_SupportedClient_Policy1`
  FOREIGN KEY (`policy_id`)
  REFERENCES `gigskyNotificationGateway`.`Policy` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`GroupEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`GroupEvent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`GroupEvent` (
  `options` LONGTEXT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `deviceType` ENUM('IOS','ANDROID') NULL,
  `location` VARCHAR(45) NULL,
  `customerType` VARCHAR(45) NULL,
  `title` VARCHAR(45) NULL,
  `message` VARCHAR(45) NULL,
  `event_id` INT NOT NULL,
  PRIMARY KEY (`event_id`),
  INDEX `fk_GroupEvent_Event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_GroupEvent_Event1`
  FOREIGN KEY (`event_id`)
  REFERENCES `gigskyNotificationGateway`.`Event` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`PushedDevice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`PushedDevice` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`PushedDevice` (
  `device_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `pushNotification_id` INT NOT NULL,
  INDEX `fk_PushedDevices_Device1_idx` (`device_id` ASC),
  PRIMARY KEY (`device_id`, `pushNotification_id`),
  INDEX `fk_PushedDevice_CustomerPushNotification1_idx` (`pushNotification_id` ASC),
  CONSTRAINT `fk_PushedDevices_Device1`
  FOREIGN KEY (`device_id`)
  REFERENCES `gigskyNotificationGateway`.`Device` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PushedDevice_CustomerPushNotification1`
  FOREIGN KEY (`pushNotification_id`)
  REFERENCES `gigskyNotificationGateway`.`PushNotification` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`GroupEventCustomer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`GroupEventCustomer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`GroupEventCustomer` (
  `customer_id` INT NOT NULL,
  `groupEvent_event_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  INDEX `fk_GroupEventCustomer_Customer1_idx` (`customer_id` ASC),
  PRIMARY KEY (`customer_id`, `groupEvent_event_id`),
  INDEX `fk_GroupEventCustomer_GroupEvent1_idx` (`groupEvent_event_id` ASC),
  CONSTRAINT `fk_GroupEventCustomer_Customer1`
  FOREIGN KEY (`customer_id`)
  REFERENCES `gigskyNotificationGateway`.`Customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_GroupEventCustomer_GroupEvent1`
  FOREIGN KEY (`groupEvent_event_id`)
  REFERENCES `gigskyNotificationGateway`.`GroupEvent` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`CustomerGroup`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`CustomerGroup` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`CustomerGroup` (
  `customer_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `groupType_id` INT NOT NULL,
  INDEX `fk_CustomerGroup_Customer1_idx` (`customer_id` ASC),
  PRIMARY KEY (`customer_id`, `groupType_id`),
  INDEX `fk_CustomerGroup_GroupType1_idx` (`groupType_id` ASC),
  CONSTRAINT `fk_CustomerGroup_Customer1`
  FOREIGN KEY (`customer_id`)
  REFERENCES `gigskyNotificationGateway`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CustomerGroup_GroupType1`
  FOREIGN KEY (`groupType_id`)
  REFERENCES `gigskyNotificationGateway`.`GroupType` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`MockUser`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`MockUser` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`MockUser` (
  `userId` INT NOT NULL AUTO_INCREMENT,
  `userUuid` VARCHAR(45) NULL,
  `tenant_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`userId`),
  INDEX `fk_MockUser_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_MockUser_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`MockUserToken`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`MockUserToken` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`MockUserToken` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `basicToken` VARCHAR(225) NULL,
  `mockUser_userId` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_MockUserToken_MockUser1_idx` (`mockUser_userId` ASC),
  CONSTRAINT `fk_MockUserToken_MockUser1`
  FOREIGN KEY (`mockUser_userId`)
  REFERENCES `gigskyNotificationGateway`.`MockUser` (`userId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`MockPushNotification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`MockPushNotification` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`MockPushNotification` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `notificationId` INT NULL,
  `eventId` INT NULL,
  `status` VARCHAR(45) NULL,
  `statusMessage` VARCHAR(45) NULL,
  `iccId` VARCHAR(45) NULL,
  `location` VARCHAR(45) NULL,
  `languageCode` VARCHAR(45) NULL,
  `notificationCategory` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `eventTypeId` VARCHAR(45) NULL,
  `deviceId` INT NULL,
  `userId` VARCHAR(45) NULL,
  `title` VARCHAR(255) NULL,
  `message` LONGTEXT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`PartnerAppDetails`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`PartnerAppDetails` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`PartnerAppDetails` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `deviceType` ENUM('IOS','ANDROID') NULL,
  `ipAddress` VARCHAR(45) NULL,
  `deviceOSVersion` VARCHAR(45) NULL,
  `locale` VARCHAR(45) NULL,
  `registeredDate` TIMESTAMP NULL,
  `iccId` VARCHAR(45) NULL,
  `location` VARCHAR(45) NULL,
  `partnerCode` VARCHAR(45) NULL,
  `sdkVersion` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`AppFeatureInfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`AppFeatureInfo` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`AppFeatureInfo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `clientType` VARCHAR(45) NULL,
  `clientVersion` VARCHAR(45) NULL,
  `userId` VARCHAR(45) NULL,
  `tenant_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `config` LONGTEXT NULL,
  `features` LONGTEXT NULL,
  `baseUrl` TEXT NULL,
  `logoBaseUrl` TEXT NULL,
  `troubleshootingBaseUrl` TEXT NULL,
  `updateMandatory` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_AppFeatureInfo_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_AppFeatureInfo_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`EmailEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`EmailEvent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`EmailEvent` (
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `data` LONGTEXT NULL,
  `email_id` LONGTEXT NOT NULL,
  `event_id` INT NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `tenant_id` INT NOT NULL,
  INDEX `fk_EmailEvent_Event1_idx` (`event_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_EmailEvent_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_EmailEvent_Event1`
  FOREIGN KEY (`event_id`)
  REFERENCES `gigskyNotificationGateway`.`Event` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_EmailEvent_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`EmailNotification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`EmailNotification` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`EmailNotification` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `languageCode` VARCHAR(45) NULL,
  `template_id` INT NOT NULL,
  `notification_id` INT NOT NULL,
  `sender` VARCHAR(100) NULL,
  `recipient` VARCHAR(200) NULL,
  `bccRecipient` VARCHAR(200) NULL,
  `subject` LONGTEXT NULL,
  `content` LONGTEXT NULL,
  `failedCount` INT NULL,
  `status` VARCHAR(45) NULL,
  `statusMessage` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CustomerPushNotification_Template1_idx` (`template_id` ASC),
  INDEX `fk_EmailNotification_Notification1_idx` (`notification_id` ASC),
  CONSTRAINT `fk_CustomerPushNotification_Template10`
  FOREIGN KEY (`template_id`)
  REFERENCES `gigskyNotificationGateway`.`Template` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_EmailNotification_Notification1`
  FOREIGN KEY (`notification_id`)
  REFERENCES `gigskyNotificationGateway`.`Notification` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`MockEmailNotificationSent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`MockEmailNotificationSent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`MockEmailNotificationSent` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Notification_id` INT NOT NULL,
  `languageCode` VARCHAR(45) NULL,
  `sender` VARCHAR(100) NULL,
  `recipient` VARCHAR(200) NULL,
  `bccRecipient` VARCHAR(200) NULL,
  `subject` LONGTEXT NULL,
  `content` LONGTEXT NULL,
  `status` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_MockEmailNotificationSent_Notification1_idx` (`Notification_id` ASC),
  CONSTRAINT `fk_MockEmailNotificationSent_Notification1`
  FOREIGN KEY (`Notification_id`)
  REFERENCES `gigskyNotificationGateway`.`Notification` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`CustomerContext`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`CustomerContext` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`CustomerContext` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gsCustomerId` BIGINT NULL,
  `token` VARCHAR(45) NULL,
  `iccid` VARCHAR(40) NULL,
  `simNickname` VARCHAR(64) NULL,
  `simType` VARCHAR(32) NULL,
  `deviceId` VARCHAR(255) NULL,
  `userId` VARCHAR(255) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyNotificationGateway`.`TenantConfigurationsKeyValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyNotificationGateway`.`TenantConfigurationsKeyValue` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`TenantConfigurationsKeyValue` (
  `ckey` VARCHAR(250) NOT NULL,
  `value` TEXT NOT NULL,
  `tenant_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  UNIQUE INDEX `key_UNIQUE` (`ckey` ASC),
  PRIMARY KEY (`ckey`),
  INDEX `fk_TenantConfigurationsKeyValue_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_TenantConfigurationsKeyValue_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;
USE `gigskyNotificationGateway`;

DELIMITER $$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`VersionInformation_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`VersionInformation_BEFORE_UPDATE` BEFORE UPDATE ON `VersionInformation` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`HttpError_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`HttpError_BEFORE_UPDATE` BEFORE UPDATE ON `HttpError` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`ErrorString_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`ErrorString_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorString` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`ConfigurationsKeyValue_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`ConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `ConfigurationsKeyValue` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`UrlDetail_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`UrlDetail_BEFORE_UPDATE` BEFORE UPDATE ON `UrlDetail` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`ThirdPartyTool_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`ThirdPartyTool_BEFORE_UPDATE` BEFORE UPDATE ON `ThirdPartyTool` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Tenant_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Tenant_BEFORE_UPDATE` BEFORE UPDATE ON `Tenant` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Customer_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Customer_BEFORE_UPDATE` BEFORE UPDATE ON `Customer` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Device_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Device_BEFORE_UPDATE` BEFORE UPDATE ON `Device` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`CustomerDevice_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerDevice_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerDevice` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Template_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Template_BEFORE_UPDATE` BEFORE UPDATE ON `Template` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`NotificationType_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`NotificationType_BEFORE_UPDATE` BEFORE UPDATE ON `NotificationType` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Policy_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Policy_BEFORE_UPDATE` BEFORE UPDATE ON `Policy` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`EventType_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`EventType_BEFORE_UPDATE` BEFORE UPDATE ON `EventType` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`GroupType_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`GroupType_BEFORE_UPDATE` BEFORE UPDATE ON `GroupType` FOR EACH ROW
  begin
    set new.updateTime = now();
  end            $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`EventPolicy_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`EventPolicy_BEFORE_UPDATE` BEFORE UPDATE ON `EventPolicy` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Event_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Event_BEFORE_UPDATE` BEFORE UPDATE ON `Event` FOR EACH ROW
  begin
    set new.updateTime = now();
  end        $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`CustomerEvent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerEvent_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerEvent` FOR EACH ROW
  begin
    set new.updateTime = now();
  end   $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`Notification_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Notification_BEFORE_UPDATE` BEFORE UPDATE ON `Notification` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`PushNotification_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`PushNotification_BEFORE_UPDATE` BEFORE UPDATE ON `PushNotification` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`NotificationTemplate_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`NotificationTemplate_BEFORE_UPDATE` BEFORE UPDATE ON `NotificationTemplate` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`SupportedClient_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`SupportedClient_BEFORE_UPDATE` BEFORE UPDATE ON `SupportedClient` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`GroupEvent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`GroupEvent_BEFORE_UPDATE` BEFORE UPDATE ON `GroupEvent` FOR EACH ROW
  begin
    set new.updateTime = now();
  end   $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`PushedDevice_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`PushedDevice_BEFORE_UPDATE` BEFORE UPDATE ON `PushedDevice` FOR EACH ROW
  begin
    set new.updateTime = now();
  end     $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`GroupEventCustomer_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`GroupEventCustomer_BEFORE_UPDATE` BEFORE UPDATE ON `GroupEventCustomer` FOR EACH ROW
  begin
    set new.updateTime = now();
  end        $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`CustomerGroup_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerGroup_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerGroup` FOR EACH ROW
  begin
    set new.updateTime = now();
  end        $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`MockUser_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockUser_BEFORE_UPDATE` BEFORE UPDATE ON `MockUser` FOR EACH ROW
  begin
    set new.updateTime = now();
  end        $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`MockUserToken_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockUserToken_BEFORE_UPDATE` BEFORE UPDATE ON `MockUserToken` FOR EACH ROW
  begin
    set new.updateTime = now();
  end        $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`MockPushNotification_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockPushNotification_BEFORE_UPDATE` BEFORE UPDATE ON `MockPushNotification` FOR EACH ROW
  begin
    set new.updateTime = now();
  end        $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`AppFeatureInfo_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`AppFeatureInfo_BEFORE_UPDATE` BEFORE UPDATE ON `AppFeatureInfo` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`EmailEvent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`EmailEvent_BEFORE_UPDATE` BEFORE UPDATE ON `EmailEvent` FOR EACH ROW
  BEGIN
    set new.updateTime = now();
  END
$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`EmailNotification_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`EmailNotification_BEFORE_UPDATE` BEFORE UPDATE ON `EmailNotification` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`MockEmailNotificationSent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockEmailNotificationSent_BEFORE_UPDATE` BEFORE UPDATE ON `MockEmailNotificationSent` FOR EACH ROW
  begin
    set new.updateTime = now();
  end    $$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`CustomerContext_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`CustomerContext_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerContext` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyNotificationGateway`$$
DROP TRIGGER IF EXISTS `gigskyNotificationGateway`.`TenantConfigurationsKeyValue_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`TenantConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `TenantConfigurationsKeyValue` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
