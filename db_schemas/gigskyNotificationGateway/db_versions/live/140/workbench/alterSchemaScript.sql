-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`PartnerAppDetails` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `deviceType` ENUM('IOS','ANDROID') NULL DEFAULT NULL,
  `ipAddress` VARCHAR(45) NULL DEFAULT NULL,
  `deviceOSVersion` VARCHAR(45) NULL DEFAULT NULL,
  `locale` VARCHAR(45) NULL DEFAULT NULL,
  `registeredDate` TIMESTAMP NULL DEFAULT NULL,
  `iccId` VARCHAR(45) NULL DEFAULT NULL,
  `location` VARCHAR(45) NULL DEFAULT NULL,
  `partnerCode` VARCHAR(45) NULL DEFAULT NULL,
  `sdkVersion` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
