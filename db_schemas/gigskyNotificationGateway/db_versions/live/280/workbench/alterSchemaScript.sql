-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`Tenant` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  `description` LONGTEXT NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

ALTER TABLE `gigskyNotificationGateway`.`Customer` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `preferences`,
ADD INDEX `fk_Customer_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`EventPolicy` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `eventType_id`,
ADD COLUMN `enableEvent` TINYINT(1) NULL DEFAULT NULL AFTER `tenant_id`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`policy_id`, `eventType_id`, `tenant_id`),
ADD INDEX `fk_EventPolicy_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`Event` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `creationTime`,
ADD INDEX `fk_Event_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`CustomerEvent` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `event_id`,
ADD INDEX `fk_CustomerEvent_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`Notification` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `policy_id`,
ADD INDEX `fk_Notification_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`NotificationTemplate` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `updateTime`,
ADD INDEX `fk_NotificationTemplate_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`MockUser` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `userUuid`,
ADD INDEX `fk_MockUser_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`AppFeatureInfo` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `userId`,
ADD INDEX `fk_AppFeatureInfo_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyNotificationGateway`.`EmailEvent` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `id`,
ADD INDEX `fk_EmailEvent_Tenant1_idx` (`tenant_id` ASC);

CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`TenantConfigurationsKeyValue` (
  `ckey` VARCHAR(250) NOT NULL,
  `value` TEXT NOT NULL,
  `tenant_id` INT(11) NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  UNIQUE INDEX `key_UNIQUE` (`ckey` ASC),
  PRIMARY KEY (`ckey`),
  INDEX `fk_TenantConfigurationsKeyValue_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_TenantConfigurationsKeyValue_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

ALTER TABLE `gigskyNotificationGateway`.`Customer` 
ADD CONSTRAINT `fk_Customer_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`EventPolicy` 
ADD CONSTRAINT `fk_EventPolicy_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`Event` 
ADD CONSTRAINT `fk_Event_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`CustomerEvent` 
ADD CONSTRAINT `fk_CustomerEvent_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`Notification` 
ADD CONSTRAINT `fk_Notification_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`NotificationTemplate` 
ADD CONSTRAINT `fk_NotificationTemplate_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`MockUser` 
ADD CONSTRAINT `fk_MockUser_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`AppFeatureInfo` 
ADD CONSTRAINT `fk_AppFeatureInfo_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyNotificationGateway`.`EmailEvent` 
ADD CONSTRAINT `fk_EmailEvent_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyNotificationGateway`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


DELIMITER $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`Tenant_BEFORE_UPDATE` BEFORE UPDATE ON `Tenant` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`TenantConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `TenantConfigurationsKeyValue` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
