#!/bin/bash

args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

#Get sql arguments
SQL_ARGS=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)

#following mysql query doesn't throw error if no database is passed. Hence check it explicitly

if [ $parsed_DatabaseName_Present == "NO" ]
then
    echo "Please provide database name with -d option"
    exit 1;
fi


mysql --protocol=tcp $SQL_ARGS --execute="select table_name, column_name as 'foreign key', referenced_table_name, referenced_column_name as 'references' from information_schema.key_column_usage where referenced_table_name is not null and REFERENCED_TABLE_SCHEMA='$parsed_DatabaseName'" --silent --skip-column-names | 
while read table_name foreign_key referenced_table referenced_column group
do
	#echo $table_name $foreign_key $referenced_table $referenced_column

	child_table=$table_name;
	child_key=$foreign_key;
	parent_table=$referenced_table;
	parent_key=$referenced_column

	#"SELECT customerSims.customerID FROM customerSims LEFT JOIN customers\n ON (customerSims.customerID = customers.customerID)\n WHERE customerSims.customerID IS NOT NULL AND customers.customerID IS NULL"
	#select CONCAT('SELECT ',$child_table ,'.',$child_key,' FROM ',$child_table,' LEFT JOIN ', $parent_table, CONCAT('\n ON (',$child_table,'.',$child_key,' = ',$parent_table,'.',$parent_key,')'), CONCAT('\n ','WHERE ',$child_table,'.',$child_key,' IS NOT NULL AND ',$parent_table,'.',$parent_key,' IS NULL'));"


	sqlQuery="SELECT $child_table.$child_key from $child_table LEFT JOIN $parent_table ON ($child_table.$child_key=$parent_table.$parent_key) WHERE $child_table.$child_key IS NOT NULL AND $parent_table.$parent_key IS NULL"
	mysql --protocol=tcp $SQL_ARGS --execute="$sqlQuery" | 
	while read orphanChildCount
	do
		echo Referential Key Error: $table_name.$foreign_key count:$orphanChildCount referred table:$referenced_table column:$referenced_column
		break;
	done

done

