#!/bin/bash


if [ "$(whoami)" != "root" ]
then
	echo run as sudo, exiting
	exit 1;
fi

args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)
echo $DB_UTILS_PATH

#parse the arguments
. $DB_UTILS_PATH/commandLineParser.sh $args

#Get sql arguments
SQL_ARGS=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)" --default-character-set=utf8 --protocol=tcp"

PASSWORD_HASH_DIR=$DB_UTILS_PATH/"/passwordHashes"
if [ "$parsed_DatabaseType" == "dev" ] 
then
	passwordHashFile=$PASSWORD_HASH_DIR/devDbPassword.hash
elif [ "$parsed_DatabaseType" == "live" ] 
then
	passwordHashFile=$PASSWORD_HASH_DIR/liveDbPassword.hash
elif [ "$parsed_DatabaseType" == "stage" ]
then
    passwordHashFile=$PASSWORD_HASH_DIR/stageDbPassword.hash
else
	echo "Error: Invalid arguments: choose dev / live /stage for -e option"
	exit 1
fi


$DB_UTILS_PATH/generateHash.sh $parsed_DatabaseType_Password | cmp -b $passwordHashFile > /dev/null 2>&1
if [ "$?" != "0" ] 
then
	echo "Authentication failed"
	exit 1
fi


#SQL_DIR=$(which mysql | sed 's/mysql$//')/../
#echo SQL Installed directory traced:$SQL_DIR
TEMP_FILE=/tmp/tempfile.csv


#check if host is reachable
echo "mysql $SQL_ARGS --execute="SELECT 1" --silent --skip-column-names > /dev/null 2>&1"
mysql $SQL_ARGS --execute="SELECT 1" --silent --skip-column-names > /dev/null 2>&1
if [ "$?" != "0" ]
then
	echo "Database unreachable; exiting.."
	exit 1
fi



#Get all tables names from database
TABLE_NAMES_LIST= 
TABLE_NAMES_LIST=$(mysql $SQL_ARGS -B -e "SELECT TABLE_NAME FROM information_schema.TABLES C WHERE table_schema = '$parsed_DatabaseName';" | awk '{print $1}' | grep -iv ^COLUMN_NAME$ | tr '\n' ',')
#echo $TABLE_NAMES_LIST

TABLE_NAMES_LIST=${TABLE_NAMES_LIST//,/$'\n'}  # change the semicolons to white space
TABLE_NAMES_LIST=${TABLE_NAMES_LIST//TABLE_NAME/}  # remove TABLE_NAME that gets added to string
echo "Dumping tables:"
echo $TABLE_NAMES_LIST
echo "" ""

rm -rf $parsed_Snapshot_Directory/*.csv
mkdir -p $parsed_Snapshot_Directory > /dev/null 2>&1

#following setting done to resolve sed: invalid byte sequence errors
export LC_CTYPE='C'


for TABLE_NAME in $TABLE_NAMES_LIST
do
	FNAME=$parsed_Snapshot_Directory/$TABLE_NAME.csv

    useSelectIntoOutFileToDump=0
    if [ $useSelectIntoOutFileToDump == 1 ]
    then

    
        #(1)creates empty file and sets up column names using the information_schema
        TABLE_ROWS=$(mysql $SQL_ARGS -B -e "SELECT COLUMN_NAME FROM information_schema.COLUMNS C WHERE table_name = '$TABLE_NAME';" | awk   '{print $1}' | grep -iv ^COLUMN_NAME$ | tr '\n' ',')
        #echo "Logging following columns" $TABLE_ROWS
        echo $TABLE_ROWS >> $FNAME

        if [ "$?" != "0" ]
        then
            echo "Failed to write to file $FNAME"
            exit 1
        fi

        #(3)dumps data from DB into tempfile
        sudo rm -f $TEMP_FILE
        mysql $SQL_ARGS -B -e "SELECT * INTO OUTFILE '$TEMP_FILE' FIELDS TERMINATED BY '\t' TERMINATED BY ',' ENCLOSED BY '\"'  LINES TERMINATED BY '\n' FROM $TABLE_NAME;"
        if [ "$?" != "0" ]
        then
            echo "Database dump failed; exiting.."
            exit 1
        fi

        #(4)merges data file and file w/ column names
        cat $TEMP_FILE >> $FNAME

    else
        #Use basic select along with sed command
        mysql $SQL_ARGS -e "select * from $TABLE_NAME;" | sed 's/	/","/g;s/^/"/;s/$/"/' | sed 's/"NULL"/\\N/g'> $FNAME

        if [ "$?" != "0" ]
        then
            echo "Database dump failed; exiting.."
            exit 1
        fi

    fi

done
sudo rm -f $TEMP_FILE

echo Snapshot taken at path: $parsed_Snapshot_Directory

