package com.gigsky;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.gigsky.database.bean.ErrorStringDBBean;
import com.gigsky.database.dao.ErrorStringDao;
import com.gigsky.exception.NotificationException;
import com.gigsky.rest.bean.ErrorResponse;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.GigskyNotificationGatewayServerException;
import javassist.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Component
public class GigskyNotificationGatewayExceptionMapper implements ExceptionMapper<Throwable> {
	private static final Logger logger = LoggerFactory.getLogger(GigskyNotificationGatewayExceptionMapper.class);
	@Autowired
	private ErrorStringDao errorStringDao;

	public static ErrorResponse createErrorResponse(ErrorStringDBBean errorStringBean, String paramToReplaceInMessage) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorInt(errorStringBean.getErrorId());
		String errorString = errorStringBean.getErrorString();
		errorString = StringUtils.isEmpty(errorString) ? StringUtils.EMPTY : String.format(errorString, paramToReplaceInMessage);
		errorResponse.setErrorStr(errorString);
		return errorResponse;
	}

	@Override
	public Response toResponse(Throwable ex) {

		ErrorStringDBBean errorStringBean;
		String param = StringUtils.EMPTY;

		if (ex instanceof GigskyNotificationGatewayServerException) {
			logger.error("GigskyNotificationGatewayServerException exception occurred");
			GigskyNotificationGatewayServerException lGsNGWServerException = (GigskyNotificationGatewayServerException) ex;
			errorStringBean = errorStringDao.getErrorStringByErrorId(lGsNGWServerException.getErrorCode().statusCode);
			param = lGsNGWServerException.getMessage();

		}
		else if(ex instanceof NotificationException)
		{
			logger.error(ex.getClass().getName() +" occurred");
			NotificationException notificationException = (NotificationException) ex;
			errorStringBean = errorStringDao.getErrorStringByErrorId(notificationException.getErrorCode().statusCode);
			param = notificationException.getErrorMessage();

		}
		else if (ex instanceof NotFoundException)
		{

			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.RESOURCE_NOT_FOUND.statusCode);
			logger.error("URL not found " + ex.getMessage());

		}
		else if (ex instanceof WebApplicationException)
		{
			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.RESOURCE_NOT_FOUND.statusCode);

			int httpErrorCode = ((WebApplicationException) ex).getResponse().getStatus();
			if (httpErrorCode == ErrorCode.HTTP_METHOD_NOT_SUPPORTED_CODE.statusCode)
			{
				errorStringBean.getHttpErrorId().setErrorCode(ErrorCode.HTTP_RESOURCE_NOT_SUPPORTED_CODE.statusCode);
			}
			else
			{
				errorStringBean.getHttpErrorId().setErrorCode(((WebApplicationException) ex).getResponse().getStatus());
			}

			logger.error("resource not found " + ex.getMessage());

		}
		else if (ex instanceof IllegalArgumentException)
		{
			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.INVALID_ARGUMENTS.statusCode);
			logger.error("Illegal arguments " + ex.getMessage());

		}
		else if (ex instanceof HttpClientErrorException)
		{
			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.RESOURCE_NOT_FOUND.statusCode);
			logger.error("HttpClientErrorException due to invalid arguments " + ex.getMessage());

		}
		else if (ex instanceof JsonMappingException)
		{
			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.INVALID_ARGUMENTS.statusCode);
			String errorMessage = ex.getMessage();
			if (errorMessage != null && (errorMessage.indexOf("(") > 0)) {
				errorMessage = errorMessage.substring(0, errorMessage.indexOf("("));
				errorStringBean.setErrorString(errorMessage);
			}
			logger.error("JsonMappingException " + errorMessage);

		}
		else {
			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.UNKNOWN_ERROR.statusCode);
			//errorStringBean.setErrorString(ex.getMessage());
			logger.error("Unexpected " + ex.getClass().getName() + " occurred due to " + ex.getMessage(), ex);

		}

		// construct error response using error string bean
		if (errorStringBean != null) {
			return Response.status(errorStringBean.getHttpErrorId().getErrorCode())
					.entity(createErrorResponse(errorStringBean, param))
					.type(MediaType.APPLICATION_JSON).build();
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
	}
}
