package com.gigsky.auth;

import com.gigsky.rest.bean.UserInfo;

import java.util.Map;

/**
 * Created by anant on 29/10/15.
 */
public interface UserManagementInterface {

    // used only for testing purpose.
    public void initUserInfo(Map<String,Integer> userList);

    // used only for testing purpose.
    public void clearUserInfo();

    public UserInfo getUserInfo(String token, int tenantId) throws BackendRequestException;

    public void updateUserInfo(UserInfo info, String token, int tenantId) throws BackendRequestException;
}
