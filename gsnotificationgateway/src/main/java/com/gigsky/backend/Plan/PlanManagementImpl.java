package com.gigsky.backend.Plan;

import com.gigsky.backend.beans.*;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerContextDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.CustomerContextDao;
import com.gigsky.rest.bean.PlanPurchase;
import com.gigsky.rest.exception.ErrorCode;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import util.HttpClientUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by prsubbareddy on 11/01/17.
 */
public class PlanManagementImpl implements PlanManagementInterface {

    private static final Logger logger = LoggerFactory.getLogger(PlanManagementImpl.class);

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValueDao;

    @Autowired
    Configurations configurations;

    @Autowired
    HttpClientUtil httpClientUtil;

    @Autowired
    CustomerContextDao customerContextDao;

    @Override
    public Subscription getSubscriptionDetails(String countryCode, CustomerContextDBBean customerContextDBBean) throws ServiceException, BackendRequestException {

        List<Subscription> subscriptionList = getSubscriptionResponse(customerContextDBBean);

        if(subscriptionList != null && subscriptionList.size() > 0)
        {

            if(countryCode == null || countryCode.length() == 0)
            {
                // Go through loop and return IN_USE subscription
                for(int i = 0; i < subscriptionList.size(); i++)
                {
                    Subscription subscriptionRes = subscriptionList.get(i);
                    if("IN_USE".equals(subscriptionRes.getSubscriptionStatus())) {
                        return prepareResponse(customerContextDBBean, subscriptionRes);
                    }
                }
            } else {

                // Go through loop and return country code matched subscription
                for(int i = 0; i < subscriptionList.size(); i++) {
                    Subscription subscriptionRes = subscriptionList.get(i);

                    List<String> countryCodes = subscriptionRes.getNetworkGroupInfo().getCountryCodes();

                    for(int j = 0; j < countryCodes.size(); j ++){

                        String cCode = countryCodes.get(j);
                        if(countryCode.toLowerCase().equals(cCode.toLowerCase())) {
                            return prepareResponse(customerContextDBBean, subscriptionRes);
                        }
                    }
                }
            }
        }
        // throw exception for no subscription
        throw new BackendRequestException(ErrorCode.NO_ACTIVE_SUBSCRIPTIONS);
    }

    private Subscription prepareResponse(CustomerContextDBBean customerContextDBBean, Subscription subscriptionRes) {
        Subscription subscription = new Subscription(subscriptionRes.getExpiryDate(), subscriptionRes.getBalanceInDays(), subscriptionRes.getBalanceTimeInDay(), subscriptionRes.getBalanceDataInKB(), subscriptionRes.getSubscriptionStatus());
        subscription.setType("BalanceDetails");
        subscription.setSimName(customerContextDBBean.getSimNickname());
        subscription.setIccid(customerContextDBBean.getIccid());
        return subscription;
    }

    @Override
    public NetworkGroupResponse getNetworkGroupForCountry(CustomerContextDBBean customerContextDBBean, String countryCode) throws ServiceException, BackendRequestException {

        List<NetworkGroupResponse> networkList = getNetworkGroupResponse(customerContextDBBean, countryCode);

        if(networkList != null && networkList.size() > 0){
            return networkList.get(0);
        } else {
            throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
        }
    }

    @Override
    public NetworkGroupPlans getPlansForNetwork(CustomerContextDBBean customerContextDBBean, NetworkGroupResponse networkGroupResponse, String countryCode) throws ServiceException, BackendRequestException {

        NetworkGroupPlans networkGroupPlans = getPlanResponse(customerContextDBBean, networkGroupResponse.getNetworkGroupId(), countryCode);
        networkGroupPlans.setNetworkGroupInfo(networkGroupResponse);
        return networkGroupPlans;
    }

    @Override
    public PaymentStatusResponse getPaymentStatus(CustomerContextDBBean customerContextDBBean) throws ServiceException, BackendRequestException {
        return getPaymentStatusResponse(customerContextDBBean);
    }

    @Override
    public PlanPurchaseResponse makePlanPurchase(CustomerContextDBBean customerContextDBBean, PlanPurchase planInfo, PaymentStatusResponse paymentStatusResponse) throws ServiceException, BackendRequestException {
        return makePlanPurchaseTransaction(customerContextDBBean, planInfo, paymentStatusResponse);
    }

    @Override
    public SIMStatusResponse getSIMStatus(CustomerContextDBBean customerContextDBBean) throws ServiceException, BackendRequestException {
        return getSIMStatusResponse(customerContextDBBean);
    }

    private List<Subscription> getSubscriptionResponse(CustomerContextDBBean customerContextDBBean) throws BackendRequestException {

        //Get url required for get subscription api
        String url = configurations.getBackendBaseUrl() + "account/"+customerContextDBBean.getGsCustomerId()+"/SIM/"+customerContextDBBean.getIccid()+"/subscriptions?type=ACTIVE";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +customerContextDBBean.getToken());

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                SubscriptionList subscriptionList =
                        (SubscriptionList)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), SubscriptionList.class);

                // If backend not return any response, returning as backend request failure
                if(subscriptionList == null) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);

                    // if response has type error
                } else if("error".equals(subscriptionList.getType())) {

                    // If response has errorCode as 2000 return invalid credentials
                    //TODO: additional SIM error codes

                    if(subscriptionList.getErrorInt() == 1501) {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.INVALID_SIM_ID);
                    } else {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                    }
                }

                return subscriptionList.getList();
            }
        }
        catch (Exception e){

            logger.error("Subscription api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }

    private List<NetworkGroupResponse> getNetworkGroupResponse(CustomerContextDBBean customerContextDBBean, String countryCode) throws BackendRequestException {

        //Get url required for get Network Group api
        String url = configurations.getBackendBaseUrl() + "countries/" + countryCode+"?simType="+customerContextDBBean.getSimType()+"&simId="+customerContextDBBean.getIccid();

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        //headers.put("Authorization", "Basic " +customerContextDBBean.getToken());

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                NetworkGroupList networkList =
                        (NetworkGroupList)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), NetworkGroupList.class);

                // If backend not return any response, returning as backend request failure
                if(networkList == null || "error".equals(networkList.getType())) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                }
                return networkList.getList();
            }
        }
        catch (Exception e){

            logger.error("NetworkGroup api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }

    private NetworkGroupPlans getPlanResponse(CustomerContextDBBean customerContextDBBean, int networkGroupId, String countryCode) throws BackendRequestException {

        //Get url required for get Plan api
        String url = configurations.getBackendBaseUrl() + "networkGroups/" + networkGroupId + "/plansExt?includeFreePlan=false"+
                            "&simId="+ customerContextDBBean.getIccid()+"&countryCode=" + countryCode;

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        //headers.put("Authorization", "Basic " +customerContextDBBean.getToken());

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                NetworkGroupPlans plansList =
                        (NetworkGroupPlans)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), NetworkGroupPlans.class);

                // If backend not return any response, returning as backend request failure
                if(plansList == null || "error".equals(plansList.getType())) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                }
                return plansList;
            }
        }
        catch (Exception e){

            logger.error("NetworkGroup Plans api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }

    private PaymentStatusResponse getPaymentStatusResponse(CustomerContextDBBean customerContextDBBean) throws BackendRequestException {
        //Get url required for get Plan api
        String url = configurations.getBackendBaseUrl() + "account/"+customerContextDBBean.getGsCustomerId()+"/paymentStatus";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +customerContextDBBean.getToken());

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){
                //Deserialize the response
                PaymentStatusResponse paymentStatusResponse =
                        (PaymentStatusResponse)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), PaymentStatusResponse.class);

                // If backend not return any response, returning as backend request failure
                if(paymentStatusResponse == null || "error".equals(paymentStatusResponse.getType())) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                }
                return paymentStatusResponse;
            }
        }
        catch (Exception e){

            logger.error("Payment Status api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }



    private PlanPurchaseResponse makePlanPurchaseTransaction(CustomerContextDBBean customerContextDBBean, PlanPurchase planInfo, PaymentStatusResponse paymentStatusResponse) throws BackendRequestException {
        //Get url required for get Plan api
        String url = configurations.getBackendBaseUrl() + "account/"+customerContextDBBean.getGsCustomerId()+"/SIM/" + customerContextDBBean.getIccid() + "/transactions";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +customerContextDBBean.getToken());

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            ArrayList<String> countryCodes = new ArrayList<String>();
            countryCodes.add(planInfo.getCountryCode());

            PlanPurchaseInput planPurchaseInput = new PlanPurchaseInput(planInfo.getPlanId(),
                    paymentStatusResponse.getGigskyCreditAmt(), paymentStatusResponse.getBillingId(), countryCodes);

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("POST", url, null, planPurchaseInput, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                PlanPurchaseResponse planPurchaseResponse =
                        (PlanPurchaseResponse)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), PlanPurchaseResponse.class);

                // If backend not return any response, returning as backend request failure
                if(planPurchaseResponse == null) {

                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);

                } else if("error".equals(planPurchaseResponse.getType())){
                    // If response has errorCode as 7043 return no billing info added
                    if(planPurchaseResponse.getErrorInt() == 7043) {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.PURCHASE_PLAN_REQUIRES_VALID_BILLING_INFO);
                    }
                    else if(planPurchaseResponse.getErrorInt() == 6506){
                        throw new BackendRequestException(ErrorCode.INVALID_ARGUMENTS);
                    }
                    else if(planPurchaseResponse.getErrorInt() == 6512) {
                        throw new BackendRequestException(ErrorCode.SIM_IS_BLOCKED_CURRENTLY);
                    }
                    else if(planPurchaseResponse.getErrorInt() == 4500) {
                        throw new BackendRequestException(ErrorCode.PURCHASE_PLAN_REQUIRES_VALID_PLAN_ID);
                    }
                    else if(planPurchaseResponse.getErrorInt() == 3503) {
                        throw new BackendRequestException(ErrorCode.PURCHASE_PLAN_REQUIRES_VALID_PLAN_ID);
                    }
                    else {
                        throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                    }
                }
                return planPurchaseResponse;
            }
        }
        catch (Exception e){

            logger.error("Make Transaction api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }

    private SIMStatusResponse getSIMStatusResponse(CustomerContextDBBean customerContextDBBean) throws BackendRequestException {
        //Get url required for get Plan api
        String url = configurations.getBackendBaseUrl() + "account/"+customerContextDBBean.getGsCustomerId()+"/SIM/"+customerContextDBBean.getIccid()+"/simStatus?detail=availableNetworkGroupInfo,location";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +customerContextDBBean.getToken());

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){
                //Deserialize the response
                SIMStatusResponse simStatusResponse =
                        (SIMStatusResponse)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), SIMStatusResponse.class);

                // If backend not return any response, returning as backend request failure
                if(simStatusResponse == null || "error".equals(simStatusResponse.getType())) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                }
                return simStatusResponse;
            }
        }
        catch (Exception e){

            logger.error("SIM Status api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }

    @Override
    public ForbiddenCountryResponse getForbiddenCountries() throws ServiceException, BackendRequestException {

        //Get url required for get Plan api
        String url = configurations.getBackendBaseUrl() + "countries/forbidden/buySameCountry?type=GS_SIM_50";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){
                //Deserialize the response
                ForbiddenCountryResponse forbiddenCountriesResponse =
                        (ForbiddenCountryResponse)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), ForbiddenCountryResponse.class);

                // If backend not return any response, returning as backend request failure
                if(forbiddenCountriesResponse == null || "error".equals(forbiddenCountriesResponse.getType())) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                }
                return forbiddenCountriesResponse;
            }
        }
        catch (Exception e){

            logger.error("Forbidden Countries Response api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }
}
