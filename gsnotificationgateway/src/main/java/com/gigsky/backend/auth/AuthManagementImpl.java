package com.gigsky.backend.auth;

import com.gigsky.auth.UserManagementInterface;
import com.gigsky.backend.beans.Login;
import com.gigsky.backend.beans.LoginResponse;
import com.gigsky.backend.beans.RolesResponse;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.rest.exception.ErrorCode;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import util.GSUtil;
import util.HttpClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vinayr on 11/01/17.
 */
public class AuthManagementImpl implements AuthManagementInterface {

    private static final Logger logger = LoggerFactory.getLogger(AuthManagementImpl.class);

    //Response of successful request of Get roles on user API
    private static final String ROLES_SUCCESS = "RolesResponse";

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValueDao;

    @Autowired
    Configurations configurations;

    @Autowired
    HttpClientUtil httpClientUtil;

    @Autowired
    UserManagementInterface userManagementInterface;

    public boolean validateCustomerToken(String token, int tenantId) throws ServiceException, com.gigsky.backend.exception.BackendRequestException, com.gigsky.auth.BackendRequestException
    {

        String gateWayMode = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE,"TEST");

        if(gateWayMode != null && gateWayMode.equals("TEST"))
        {
            return userManagementInterface.getUserInfo(token, tenantId) != null;
        } else {
            //Call "Get User Account Role" api
            RolesResponse rolesResponse = getResponse(token, tenantId);

            //If we get the response, then the token is valid
            if(rolesResponse != null &&
                    ROLES_SUCCESS.equals(rolesResponse.getType()))
                return true;
        }
        return false;
    }

    @Override
    public LoginResponse loginUsingToken(String token, int tenantId) throws ServiceException, BackendRequestException {

        String baseToken = GSUtil.decodeBaseToken(token);

        if(baseToken.contains(":"))
        {
            String[] credentials = baseToken.split(":");
            LoginResponse loginResponse = getLoginResponse(credentials[0], credentials[1], tenantId);

            if(loginResponse != null){
                return loginResponse;
            }
        } else {
            throw new BackendRequestException(ErrorCode.INVALID_TOKEN);
        }

        return null;
    }

    //Get response for the api get user roles
    private RolesResponse getResponse(String token, int tenantId) throws BackendRequestException {

        //Get url required for get user roles api
        String url = configurations.getBackendBaseUrl() + "account/roles";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +token);
        //Adding tenant id header
        headers.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                RolesResponse rolesResponse =
                        (RolesResponse) HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), RolesResponse.class);

                // If backend not return any response, returning as backend request failure
                if(rolesResponse == null) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);

                // if response has type error
                } else if("error".equals(rolesResponse.getType())) {

                    // If response has errorCode as 2001 should return null,
                    // it will take care of handling invalid token
                    if(rolesResponse.getErrorInt() == 2001) {
                        return null;
                    } else {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                    }
                }

                return rolesResponse;
            }
        }
        catch (Exception e){

            logger.error(" User roles api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }

    private LoginResponse getLoginResponse(String username, String password, int tenantId) throws BackendRequestException {

        //Get url required for get login api
        String url = configurations.getBackendBaseUrl() + "account/login";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        //Adding tenant id header
        headers.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            Login loginInput = new Login();
            loginInput.setType("AccountLogin");
            loginInput.setLoginType("NORMAL");
            loginInput.setEmailId(username);
            loginInput.setPassword(password);

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("POST", url, null, loginInput, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                LoginResponse loginResponse =
                        (LoginResponse) HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), LoginResponse.class);

                // If backend not return any response, returning as backend request failure
                if(loginResponse == null) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);

                    // if response has type error
                } else if("error".equals(loginResponse.getType())) {

                    // If response has errorCode as 2000 return invalid credentials
                    if(loginResponse.getErrorInt() == 2000) {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.INVALID_CREDENTIALS);
                    } else {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                    }
                }

                return loginResponse;
            }
        }
        catch (Exception e){

            logger.error("Login api exception " + e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }
}
