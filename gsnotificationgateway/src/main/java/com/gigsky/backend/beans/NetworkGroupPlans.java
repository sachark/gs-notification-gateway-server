package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by prsubbareddy on 10/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkGroupPlans {

    private String type = "NetworkGroupPlans";
    private int startIndex;
    private int count;
    private int totalCount;
    private List<PlanResponse> list;
    private NetworkGroupResponse networkGroupInfo;

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<PlanResponse> getList() {
        return list;
    }

    public void setList(List<PlanResponse> list) {
        this.list = list;
    }

    public NetworkGroupResponse getNetworkGroupInfo() {
        return networkGroupInfo;
    }

    public void setNetworkGroupInfo(NetworkGroupResponse networkGroupInfo) {
        this.networkGroupInfo = networkGroupInfo;
    }

}
