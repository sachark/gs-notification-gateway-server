package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by prsubbareddy on 09/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkGroupResponse {

    private String type;
    private int networkGroupId;
    private String region;
    private String networkGroupType;
    private ArrayList<String> countryCodes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNetworkGroupId() {
        return networkGroupId;
    }

    public void setNetworkGroupId(int networkGroupId) {
        this.networkGroupId = networkGroupId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getNetworkGroupType() {
        return networkGroupType;
    }

    public void setNetworkGroupType(String networkGroupType) {
        this.networkGroupType = networkGroupType;
    }

    public ArrayList<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(ArrayList<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

}
