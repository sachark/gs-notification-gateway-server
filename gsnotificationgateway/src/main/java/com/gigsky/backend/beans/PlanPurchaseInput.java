package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by prsubbareddy on 09/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanPurchaseInput {

    private String type = "TransactionRequest";
    private Integer planId;
    private float gigskyCreditAmt;

    public String getBillingID() {
        return billingID;
    }

    public void setBillingID(String billingID) {
        this.billingID = billingID;
    }

    private String billingID;
    private ArrayList<String> purchaseIntentCountries;

    public PlanPurchaseInput(Integer planId, float creditAmount, Integer bId, ArrayList<String> intentCountries) {

        this.planId = planId;
        this.gigskyCreditAmt = creditAmount;

        if(bId != null) {
            this.billingID = bId.toString();
        }

        this.purchaseIntentCountries = intentCountries;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public float getGigskyCreditAmt() {
        return gigskyCreditAmt;
    }

    public void setGigskyCreditAmt(float gigskyCreditAmt) {
        this.gigskyCreditAmt = gigskyCreditAmt;
    }

    public ArrayList<String> getPurchaseIntentCountries() {
        return purchaseIntentCountries;
    }

    public void setPurchaseIntentCountries(ArrayList<String> purchaseIntentCountries) {
        this.purchaseIntentCountries = purchaseIntentCountries;
    }

}
