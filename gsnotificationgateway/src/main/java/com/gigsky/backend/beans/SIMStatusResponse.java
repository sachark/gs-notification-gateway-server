package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by prsubbareddy on 17/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SIMStatusResponse {

    private String type = "SimStatus";
    private ConnectionInfo connectionInfo;
    private List<NetworkGroupResponse> availableNetworkGroupList;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ConnectionInfo getConnectionInfo() {
        return connectionInfo;
    }

    public void setConnectionInfo(ConnectionInfo connectionInfo) {
        this.connectionInfo = connectionInfo;
    }

    public List<NetworkGroupResponse> getAvailableNetworkGroupList() {
        return availableNetworkGroupList;
    }

    public void setAvailableNetworkGroupList(List<NetworkGroupResponse> availableNetworkGroupList) {
        this.availableNetworkGroupList = availableNetworkGroupList;
    }


}
