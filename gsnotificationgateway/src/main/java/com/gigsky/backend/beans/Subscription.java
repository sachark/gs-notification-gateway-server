package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by prsubbareddy on 08/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Subscription {

    private String type;
    private String expiryDate;
    private int balanceInDays;
    private String balanceTimeInDay;
    private int balanceDataInKB;
    private String subscriptionStatus;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private NetworkGroupResponse networkGroupInfo;
    private String simName;
    private String iccid;

    public Subscription(){

    }

    public Subscription(String expDate, int balanceDays, String timeInDay, int balanceData, String status) {

        this.expiryDate = expDate;
        this.balanceInDays = balanceDays;
        this.balanceTimeInDay = timeInDay;
        this.balanceDataInKB = balanceData;
        this.subscriptionStatus = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getBalanceInDays() {
        return balanceInDays;
    }

    public void setBalanceInDays(int balanceInDays) {
        this.balanceInDays = balanceInDays;
    }

    public String getBalanceTimeInDay() {
        return balanceTimeInDay;
    }

    public void setBalanceTimeInDay(String balanceTimeInDay) {
        this.balanceTimeInDay = balanceTimeInDay;
    }

    public int getBalanceDataInKB() {
        return balanceDataInKB;
    }

    public void setBalanceDataInKB(int balanceDataInKB) {
        this.balanceDataInKB = balanceDataInKB;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getSimName() {
        return simName;
    }

    public void setSimName(String simName) {
        this.simName = simName;
    }

    public NetworkGroupResponse getNetworkGroupInfo() {
        return networkGroupInfo;
    }

    public void setNetworkGroupInfo(NetworkGroupResponse networkGroupInfo) {
        this.networkGroupInfo = networkGroupInfo;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }
}
