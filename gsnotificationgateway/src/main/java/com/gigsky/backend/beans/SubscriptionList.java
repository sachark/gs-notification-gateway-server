package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by prsubbareddy on 08/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionList {

    private String type = "Subscriptions";
    private List<Subscription> list;
    private int errorInt;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Subscription> getList() {
        return list;
    }

    public void setList(List<Subscription> list) {
        this.list = list;
    }

    public int getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(int errorInt) {
        this.errorInt = errorInt;
    }

}
