package com.gigsky.backend.exception;

import com.gigsky.exception.NotificationException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by pradeepragav on 22/02/17.
 */
public class BackendRequestException extends NotificationException {

    public BackendRequestException(ErrorCode code)
    {
        super(code);
    }

    public BackendRequestException(String message)
    {
        super(message);
    }

    public BackendRequestException(String message, Exception e)
    {
        super(message, e);
    }

    public BackendRequestException(Exception e)
    {
        super(e);
    }
}
