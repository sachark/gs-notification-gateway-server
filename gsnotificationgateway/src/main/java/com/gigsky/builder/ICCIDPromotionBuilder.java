package com.gigsky.builder;

import com.gigsky.composer.MessageComposerException;
import com.gigsky.database.bean.TemplateDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.EmailEvent;
import com.gigsky.message.EmailMessage;
import com.gigsky.notification.BaseNotification;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.GSUtil;

import java.text.MessageFormat;

/**
 * Created by prsubbareddy on 04/12/17.
 */
public class ICCIDPromotionBuilder implements EmailBuilder {

    private static final Logger logger = LoggerFactory.getLogger(ICCIDPromotionBuilder.class);

    @Override
    public EmailMessage buildMessage(BaseEvent event, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException {

        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setEventType(event.getEventType());
        emailMessage.setSubEventType(event.getSubEventType());
        emailMessage.setNotificationType(notification.getNotificationType());
        try {
            JSONObject object = new JSONObject(templateDBBean.getContent());

            String title  = object.getString("title");
            String message = object.getString("message");

            String optionsData = ((EmailEvent) event).getEventData().getString("options");

            JSONObject optionsObj = new JSONObject(optionsData);
            String customerName = optionsObj.optString("customerName");
            String customerEmail = optionsObj.optString("customerEmail");
            int promoAmount = optionsObj.optInt("promoAmount");
            String currency = optionsObj.optString("currency");
            int gsCustomerId = optionsObj.optInt("gsCustomerId");

            // Support is there for options csn
            //String csn = optionsObj.optString("CSN");


            if(StringUtils.isEmpty(customerName)){
                customerName = customerEmail;
            }

            String promoCredit = GSUtil.GSSDK_formatPrice(String.valueOf(promoAmount), currency);

            title = MessageFormat.format(title, promoCredit);
            message = MessageFormat.format(message, customerName, promoCredit, String.valueOf(gsCustomerId));

            emailMessage.setEmailTitle(title);
            emailMessage.setEmailMessage(message);
            return emailMessage;

        } catch (Exception e){
            logger.error("EmailMessageComposer messageForType_PromotionBuilder JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+event.getEventId()+"has JSON Exception while creating message for SDS Promotion Builder", e);
        }
    }
}
