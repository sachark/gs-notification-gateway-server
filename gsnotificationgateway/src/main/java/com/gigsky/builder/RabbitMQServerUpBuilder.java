package com.gigsky.builder;

import com.gigsky.composer.MessageComposerException;
import com.gigsky.database.bean.TemplateDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.message.EmailMessage;
import com.gigsky.notification.BaseNotification;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by prsubbareddy on 04/12/17.
 */
public class RabbitMQServerUpBuilder implements EmailBuilder {

    private static final Logger logger = LoggerFactory.getLogger(RabbitMQServerUpBuilder.class);

    @Override
    public EmailMessage buildMessage(BaseEvent event, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException {

        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setEventType(event.getEventType());
        emailMessage.setSubEventType(event.getSubEventType());
        emailMessage.setNotificationType(notification.getNotificationType());
        try {
            JSONObject object = new JSONObject(templateDBBean.getContent());

            String title  = object.getString("title");
            String message = object.getString("message");

            emailMessage.setEmailTitle(title);
            emailMessage.setEmailMessage(message);
            return emailMessage;

        } catch (Exception e){
            logger.error("EmailMessageComposer messageForType_RabbitMQServerUpBuilder JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+event.getEventId()+"has JSON Exception while creating message for RabbitMQ Server Up Builder", e);
        }
    }
}
