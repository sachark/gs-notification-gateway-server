package com.gigsky.common;

import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by vinayr on 17/11/15.
 */
public class Configurations {

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    //Constants
    public static final long DEFAULT_REQUEST_TIMEOUT_MS = 5000;

    public enum ErrorMsgType {
        SYSTEM_ERROR("SYSTEM_ERROR :");

        String string = null;
        ErrorMsgType(String str) {
            string = str;
        }

        @Override
        public String toString() {
            return string;
        }
    }


    //Constants
    public static final String IOS_TYPE = "IOS";
    public static final String ANDROID_TYPE = "ANDROID";
    public static final String WEB_TYPE = "WEB";
    public static final String CAYMAN_TYPE = "CAYMAN";

    public static final int MAX_OS_VERSION_LENGTH = 20;
    public static final int MAX_MODEL_LENGTH = 45;
    public static final int MAX_CHANNEL_ID_LENGTH = 45;
    public static final int MAX_INSTALLATION_ID_LENGTH = 45;
    public static final int MAX_DEVICE_TOKEN_LENGTH = 100;
    public static final int MAX_LOCALE_LENGTH = 10;
    public static final int MAX_APP_VERSION_LENGTH = 10;
    public static final int MAX_IP_ADDRESS_LENGTH = 45;
    public static final int MAX_ICCID_LENGTH = 40;
    public static final int MAX_EXPIRY_LENGTH = 20;
    public static final int MAX_SIM_NAME_LENGTH = 255;
    public static final int MAX_LOCATION_LENGTH = 255;
    public static final int MAX_PARTNER_CODE_LENGTH = 255;
    public static final int MAX_SDK_VERSION_LENGTH = 255;


    public enum LanguageType {
        ENG_LANG("en");

        String string = null;
        LanguageType(String str) {
            string = str;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public static class EventType {

        public static final String TYPE_LOW_BALANCE = "LOW_BALANCE";
        public static final String TYPE_DATA_USED = "DATA_USED";
        public static final String TYPE_SUBSCRIPTION_NEARING_EXPIRY = "SUBSCRIPTION_NEARING_EXPIRY";
        public static final String TYPE_SUBSCRIPTION_EXPIRED = "SUBSCRIPTION_EXPIRED";
        public static final String TYPE_NEW_LOCATION = "NEW_LOCATION";
        public static final String TYPE_EMAIL = "EMAIL";
    }

    public static class SubEventType {

        public static final String TYPE_REFERRAL_INVITE = "REFERRAL_INITIATION";
        public static final String TYPE_NEW_CUSTOMER_SIGNUP = "NEW_CUSTOMER_SIGNUP";
        public static final String TYPE_CREDIT_ADDED = "ADVOCATE_CREDIT_ADDED";
        public static final String TYPE_CREDIT_ALERT = "INTERNAL_CREDIT_ALERT";
        public static final String TYPE_CREDIT_LIMIT = "CREDIT_LIMIT";
        public static final String TYPE_CREDIT_WARNING_INTERNAL = "INTERNAL_CREDIT_WARNING";
        public static final String TYPE_CREDIT_DEDUCT = "INTERNAL_CREDIT_DEDUCT";
        public static final String TYPE_ICCID_PROMOTION_CREDIT = "ICCID_PROMOTION_CREDIT";
        public static final String TYPE_RABBIT_MQ_SERVER_UP = "RABBIT_MQ_SERVER_UP";
        public static final String TYPE_RABBIT_MQ_SERVER_DOWN = "RABBIT_MQ_SERVER_DOWN";
        public static final String TYPE_DELETED_ACCOUNTS_THRESHOLD = "DELETED_ACCOUNTS_THRESHOLD";
    }

    public static class CategoryType {

        public static final String CATEGORY_CUSTOMER = "SINGLE_EVENT";
        public static final String CATEGORY_GROUP = "GROUP_EVENT";
    }

    public static class CommonType {

        public static final String OS_VERSION = "OS_VERSION";
        public static final String APP_VERSION = "APP_VERSION";
        public static final String LOCALE = "LOCALE";
        public static final String MODEL_LENGTH = "MODEL_LENGTH";
        public static final String INSTALLATION_ID = "INSTALLATION_ID";
        public static final String DEVICE_TOKEN = "DEVICE_TOKEN";
        public static final String CHANNEL_ID = "CHANNEL_ID";
        public static final String ICCID_LENGTH = "ICCID_LENGTH";
        public static final String EXPIRY_LENGTH = "EXPIRY_LENGTH";
        public static final String CREATION_LENGTH = "CREATION_LENGTH";
        public static final String ALLOW = "ALLOW";
        public static final String DENY = "DENY";

        //Partner App related
        public static final String IP_ADDRESSS_LENGTH = "IP_ADDRESSS_LENGTH";
        public static final String PARTNER_APP_OS_VERSION = "PARTNER_APP_OS_VERSION";
        public static final String PARTNER_APP_LOCALE = "PARTNER_APP_LOCALE";
        public static final String PARTNER_APP_DATE = "PARTNER_APP_DATE";
        public static final String PARTNER_APP_ICCID_LENGTH = "PARTNER_APP_ICCID_LENGTH";
        public static final String PARTNER_APP_LOCATION = "PARTNER_APP_LOCATION";
        public static final String PARTNER_APP_CODE = "PARTNER_APP_CODE";
        public static final String PARTNER_APP_SDK = "PARTNER_APP_SDK";
    }

    public static class ConfigurationKeyValueType {

        public static final String DEVICE_COUNT = "DEVICE_COUNT";
        public static final String BACKEND_BASE_URL = "BACKEND_BASE_URL";
        public static final String EVENT_EXEC_TIME_MS = "EVENT_EXEC_TIME_MS";
        public static final String EVENT_FAIL_COUNT = "EVENT_FAIL_COUNT";
        public static final String EVENTS_PROCESSING_COUNT = "EVENTS_PROCESSING_COUNT";
        public static final String GATEWAY_MODE = "GATEWAY_MODE";
        public static final String NOTIFICATION_PROCESSING_THREADS = "NOTIFICATION_PROCESSING_THREADS";
        public static final String PROCESS_EVENTS = "PROCESS_EVENTS";
        public static final String SHUTDOWN_WAIT_TIME_MS = "SHUTDOWN_WAIT_TIME_MS";
        public static final String UA_FAILURE_MODE = "UA_FAILURE_MODE";
        public static final String RATE_CONTROL_TIME_MS = "RATE_CONTROL_TIME_MS";
        public static final String MOCK_NOTIFICATION_DELAY = "MOCK_NOTIFICATION_DELAY";
        public static final String NOTIFICATION_EXPIRY = "NOTIFICATION_EXPIRY";
        public static final String PARTNER_APP_DETAILS_EXPIRY = "PARTNER_APP_DETAILS_EXPIRY";
        public static final String GATEWAY_UTILITY_EXEC_TIME = "GATEWAY_UTILITY_EXEC_TIME";


        public static final String REQUEST_TIMEOUT_MS = "REQUEST_TIMEOUT_MS";
        public static final String GSB_BASE_URL = "GSB_BASE_URL";
        public static final String PURCHASE_PLAN_MODE = "PURCHASE_PLAN_MODE";

        public static final String DEFAULT_TENANT_ID = "DEFAULT_TENANT_ID";
        public static final String UA_BASE_URL = "UA_BASE_URL";

        // Email host and email credentials for email support
        public static final String EMAIL_HOST = "EMAIL_HOST";
        public static final String SMTP_HOST = "SMTP_HOST";
        public static final String EMAIL_USER = "EMAIL_USER";
        public static final String EMAIL_PWD = "EMAIL_PASSWORD";
        public static final String EMAIL_FAIL_COUNT = "EMAIL_FAIL_COUNT";
    }

    public static class TenantConfigurationKeyValueType {

        // Urban airship config keys for push notification support
        public static final String UA_APP_KEY = "UA_APP_KEY";
        public static final String UA_APP_SECRET = "UA_APP_SECRET";
        public static final String UA_AUTHENTICATION = "UA_AUTHENTICATION";
    }




    public static class StatusType {

        public static final String STATUS_NEW = "NEW";
        public static final String STATUS_COMPLETE = "COMPLETE";
        public static final String STATUS_IN_PROCESS = "IN_PROCESS";
        public static final String STATUS_FAILED = "FAILED";
        public static final String STATUS_FAILED_AFTER_RETRIES = "FAILED_AFTER_RETRIES";
        public static final String STATUS_COMPLETE_WITH_NOTIFICATION = "COMPLETE_WITH_NOTIFICATION";
        public static final String STATUS_COMPLETE_WITHOUT_NOTIFICATION = "COMPLETE_WITHOUT_NOTIFICATION";
        public static final String STATUS_FAILED_PUSH_PROCESS = "FAILED_PUSH_PROCESS";
        public static final String STATUS_COMPLETE_SENT = "COMPLETE_SENT";
        public static final String STATUS_COMPLETE_NOT_SENT = "COMPLETE_NOT_SENT";
        public static final String STATUS_EXPIRED = "EXPIRED";
        public static final String STATUS_NOT_DELIVERED = "NOT_DELIVERED";
        public static final String STATUS_CUSTOMER_NOT_PART_OF_GROUP = "CUSTOMER_NOT_PART_OF_GROUP";
        public static final String STATUS_OUT_OF_ORDER = "OUT_OF_ORDER";
        public static final String STATUS_RATE_CONTROLLED = "RATE_CONTROLLED";
        public static final String STATUS_FAILED_EMAIL_SEND_PROCESS = "FAILED_EMAIL_SEND_PROCESS";
        public static final String STATUS_FAILED_EMAIL_CREATION_PROCESS = "FAILED_EMAIL_CREATION_PROCESS";
    }

    public static class UrbanAirshipCategories {

        //Push Notifications
        public static final String DATA_PLAN_REMAINING = "CategoryDataPlanRemaining";  //Low balance event
        public static final String DATA_PLAN_USED = "CategoryDataPlanUsed"; //Data used event
        public static final String DATA_PLAN_EXPIRING_IN_HOURS = "CategoryDataPlanExpiringInHours"; //Subscription nearing expiry event
        public static final String DATA_PLAN_EXPIRED = "CategoryDataPlanExpired"; //Subscription Expired event
        public static final String DATA_PLAN_AVAILABLE = "CategoryDataPlanAvailable"; //Location event

        //In App Messages
        public static final String IN_APP_GENERIC_MESSAGE = "CategoryInAppGenericMessage"; //In App Generic Message
    }

    public static class UrbanAirshipMessages {

        //Types of UA Messages
        public static final String UA_PUSH = "UA_PUSH";  //Push notification
        public static final String UA_INAPP = "UA_INAPP";  //In App Message
        public static final String UA_RICH_PUSH = "UA_RICH_PUSH";  //Rich Message
    }


    public String getBackendBaseUrl() {
        return configurationKeyValuesDao.getConfigurationValue(ConfigurationKeyValueType.GSB_BASE_URL);
    }

    public boolean isPlanPurchaseMode(){
        String purchasePlanMode = configurationKeyValuesDao.getConfigurationValue(ConfigurationKeyValueType.PURCHASE_PLAN_MODE);
        return ("YES".equals(purchasePlanMode));
    }

    public long getRequestTimeOut(){

        //Use default value
        long timeOutInMs = DEFAULT_REQUEST_TIMEOUT_MS;

        //Get timeout from db
        String timeOutInMsStr = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REQUEST_TIMEOUT_MS);

        if(StringUtils.isNotEmpty(timeOutInMsStr)){

            //Convert string to long
            timeOutInMs = new GigskyDuration(timeOutInMsStr).convertTimeInMilliSeconds();
        }

        return timeOutInMs;
    }

}
