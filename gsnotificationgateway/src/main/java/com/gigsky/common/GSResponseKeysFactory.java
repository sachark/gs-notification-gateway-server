package com.gigsky.common;

/**
 * Created by prsubbareddy on 24/11/15.
 */
public class GSResponseKeysFactory {

    public static class pushNotificationKeys {

        public static final String PUSH_NOTIFICATION_ID = "pushNotificationId";
        public static final String NOTIFICATION_ID = "notificationId";
        public static final String PUSH_ID = "PushID";
        public static final String DEVICE_ID = "DeviceID";
    }

    public static class EmailNotificationKeys {
        public static final String EMAIL_NOTIFICATION_ID = "EmailNotificationId";
    }

}
