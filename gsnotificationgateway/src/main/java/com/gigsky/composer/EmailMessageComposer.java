package com.gigsky.composer;

import com.gigsky.builder.*;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TemplateDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;
import com.gigsky.template.NotificationTemplateFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by prsubbareddy on 28/12/16.
 */
public class EmailMessageComposer {

    @Autowired
    NotificationTemplateFactory notificationTemplateFactory;

    @Autowired
    ReferralInviteBuilder referralInviteBuilder;

    @Autowired
    CreditWarningInternalBuilder creditWarningInternalBuilder;

    @Autowired
    CreditAddedBuilder creditAddedBuilder;

    @Autowired
    NewCustomerSignupBuilder newCustomerSignupBuilder;

    @Autowired
    CreditAlertBuilder creditAlertBuilder;

    @Autowired
    CreditLimitBuilder creditLimitBuilder;

    @Autowired
    CreditDeductBuilder creditDeductBuilder;

    @Autowired
    ICCIDPromotionBuilder iccidPromotionBuilder;

    @Autowired
    RabbitMQServerUpBuilder rabbitMQServerUpBuilder;

    @Autowired
    RabbitMQServerDownBuilder rabbitMQServerDownBuilder;

    @Autowired
    DeletedAccountsThresholdBuilder deletedAccountsThresholdBuilder;


    public NotificationMessage composeMessage(BaseEvent event, BaseNotification notification, String locale) throws MessageComposerException {

            TemplateDBBean templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                    notification.getNotificationTypeId(), event.getTenantId(), locale);

            if(templateDBBean == null && !locale.equals("en"))
            {
                templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                        notification.getNotificationTypeId(), event.getTenantId(),"en");
            }

            if (Configurations.SubEventType.TYPE_REFERRAL_INVITE.equals(event.getSubEventType())) {
                return referralInviteBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_NEW_CUSTOMER_SIGNUP.equals(event.getSubEventType())) {
                return newCustomerSignupBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_CREDIT_ADDED.equals(event.getSubEventType())) {
                return creditAddedBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_CREDIT_ALERT.equals(event.getSubEventType())) {
                return creditAlertBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_CREDIT_LIMIT.equals(event.getSubEventType())) {
                return creditLimitBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_CREDIT_WARNING_INTERNAL.equals(event.getSubEventType())) {
                return creditWarningInternalBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_CREDIT_DEDUCT.equals(event.getSubEventType())) {
                return creditDeductBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_ICCID_PROMOTION_CREDIT.equals(event.getSubEventType())) {
                return iccidPromotionBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_RABBIT_MQ_SERVER_UP.equals(event.getSubEventType())) {
                return rabbitMQServerUpBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_RABBIT_MQ_SERVER_DOWN.equals(event.getSubEventType())) {
                return rabbitMQServerDownBuilder.buildMessage(event, notification, templateDBBean);

            } else if(Configurations.SubEventType.TYPE_DELETED_ACCOUNTS_THRESHOLD.equals(event.getSubEventType())) {
                return deletedAccountsThresholdBuilder.buildMessage(event, notification, templateDBBean);
            }

        return null;
    }

}
