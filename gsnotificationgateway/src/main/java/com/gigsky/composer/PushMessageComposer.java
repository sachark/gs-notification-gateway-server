package com.gigsky.composer;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TemplateDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.CustomerEvent;
import com.gigsky.event.GroupEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.message.PushNotificationMessage;
import com.gigsky.notification.BaseNotification;
import com.gigsky.template.NotificationTemplateFactory;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DecimalFormat;

/**
 * Created by anant on 28/10/15.
 */
public class PushMessageComposer {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PushMessageComposer.class);

    @Autowired
    NotificationTemplateFactory notificationTemplateFactory;

    public NotificationMessage composeMessage(BaseEvent event, BaseNotification notification, String locale) throws MessageComposerException {

        if(Configurations.CategoryType.CATEGORY_CUSTOMER.equals(event.getEventCategory())) {
            CustomerEvent customerEvent = (CustomerEvent) event;

            TemplateDBBean templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                    notification.getNotificationTypeId(), event.getTenantId(), locale);

            if(templateDBBean == null && !locale.equals("en"))
            {
                templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                        notification.getNotificationTypeId(), event.getTenantId(), "en");
            }

            if (Configurations.EventType.TYPE_LOW_BALANCE.equals(customerEvent.getEventType())) {
                return messageForType_Low_Balance(customerEvent, notification, templateDBBean);

            } else if(Configurations.EventType.TYPE_DATA_USED.equals(customerEvent.getEventType())) {
                return messageForType_Data_Used(customerEvent, notification, templateDBBean);

            } else if (Configurations.EventType.TYPE_SUBSCRIPTION_NEARING_EXPIRY.equals(customerEvent.getEventType())) {
                return messageForType_Sub_Near_Expiry(customerEvent, notification, templateDBBean);

            } else if(Configurations.EventType.TYPE_SUBSCRIPTION_EXPIRED.equals(customerEvent.getEventType())) {
                return messageForType_Sub_Expired(customerEvent, notification, templateDBBean);

            } else if (Configurations.EventType.TYPE_NEW_LOCATION.equals(customerEvent.getEventType())) {
                return messageForType_New_Location(customerEvent, notification, templateDBBean);
            }
        } else if (Configurations.CategoryType.CATEGORY_GROUP.equals(event.getEventCategory())){
            return messageForGroupEvent(event, notification);
        }
        return null;
    }


    private PushNotificationMessage messageForType_Low_Balance(CustomerEvent customerEvent, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException
    {
        PushNotificationMessage pushNotificationMessage = new PushNotificationMessage();
        pushNotificationMessage.setEventType(customerEvent.getEventType());
        pushNotificationMessage.setNotificationType(notification.getNotificationType());

        try
        {
            JSONObject obj = new JSONObject(templateDBBean.getContent());

            String title = obj.optString("title");
            String eventOptionsData = customerEvent.getEventData().getString("options");

            if(eventOptionsData != null)
            {
                JSONObject optionsData = new JSONObject(eventOptionsData);
                if(optionsData != null && optionsData.opt("simName") != null)
                {
                    title = (String) optionsData.opt("simName");
                }
            }
            String message = obj.optString("message");
            pushNotificationMessage.setNotificationTitle(title);
            pushNotificationMessage.setNotificationMessage(message);
            logger.info("Event event_id "+customerEvent.getEventId()+" has Low Balance push notification with template_id " + templateDBBean.getId());
            return pushNotificationMessage;

        } catch (Exception e)
        {
            logger.error("PushMessageComposer messageForType_Low_Balance JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+customerEvent.getEventId()+"has JSON Exception while creating message for Low Balance", e);
        }
    }

    private PushNotificationMessage messageForType_Data_Used(CustomerEvent customerEvent, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException
    {
        PushNotificationMessage pushNotificationMessage = new PushNotificationMessage();
        pushNotificationMessage.setEventType(customerEvent.getEventType());
        pushNotificationMessage.setNotificationType(notification.getNotificationType());

        try
        {
            JSONObject obj = new JSONObject(templateDBBean.getContent());

            String title = obj.optString("title");
            String eventOptionsData = customerEvent.getEventData().getString("options");

            if(eventOptionsData != null)
            {
                JSONObject optionsData = new JSONObject(eventOptionsData);
                if(optionsData != null && optionsData.opt("simName") != null)
                {
                    title = (String) optionsData.opt("simName");
                }
            }
            String message = obj.optString("message");
            pushNotificationMessage.setNotificationTitle(title);
            pushNotificationMessage.setNotificationMessage(message);
            logger.info("Event event_id " + customerEvent.getEventId() + " has Data usage push notification with template_id " + templateDBBean.getId());
            return pushNotificationMessage;
        }
        catch (Exception e)
        {
            logger.error("PushMessageComposer messageForType_Data_Used JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+customerEvent.getEventId()+"has JSON Exception while creating message for Data Used", e);
        }

    }

    private PushNotificationMessage messageForType_Sub_Near_Expiry(CustomerEvent customerEvent, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException
    {
        PushNotificationMessage pushNotificationMessage = new PushNotificationMessage();
        pushNotificationMessage.setEventType(customerEvent.getEventType());
        pushNotificationMessage.setNotificationType(notification.getNotificationType());

        try {
            JSONObject obj = new JSONObject(templateDBBean.getContent());

            String title = obj.optString("title");
            String eventOptionsData = customerEvent.getEventData().getString("options");

            if(eventOptionsData != null)
            {
                JSONObject optionsData = new JSONObject(eventOptionsData);
                if(optionsData != null && optionsData.opt("simName") != null)
                {
                    title = (String) optionsData.opt("simName");
                }
            }
            String message = obj.optString("message");
            pushNotificationMessage.setNotificationTitle(title);
            pushNotificationMessage.setNotificationMessage(message);
            logger.info("Event event_id "+customerEvent.getEventId()+" has Subscription Near Expiry push notification with template_id " + templateDBBean.getId());
            return pushNotificationMessage;
        } catch (Exception e) {
            logger.error("PushMessageComposer messageForType_Sub_Near_Expiry JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+customerEvent.getEventId()+"has JSON Exception while creating message for Subscription Near Expiry", e);
        }

    }

    private PushNotificationMessage messageForType_Sub_Expired(CustomerEvent customerEvent, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException
    {
        PushNotificationMessage pushNotificationMessage = new PushNotificationMessage();
        pushNotificationMessage.setEventType(customerEvent.getEventType());
        pushNotificationMessage.setNotificationType(notification.getNotificationType());

        try
        {
            JSONObject obj = new JSONObject(templateDBBean.getContent());

            String title = obj.optString("title");
            String eventOptionsData = customerEvent.getEventData().getString("options");

            if(eventOptionsData != null)
            {
                JSONObject optionsData = new JSONObject(eventOptionsData);
                if(optionsData != null && optionsData.opt("simName") != null)
                {
                    title = (String) optionsData.opt("simName");
                }
            }
            String message = obj.optString("message");
            pushNotificationMessage.setNotificationTitle(title);
            pushNotificationMessage.setNotificationMessage(message);
            logger.info("Event event_id "+customerEvent.getEventId()+" has Subscription Expired push notification with template_id " + templateDBBean.getId());
            return pushNotificationMessage;
        }
        catch (Exception e) {
            logger.error("PushMessageComposer messageForType_Sub_Expired JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+customerEvent.getEventId()+"has JSON Exception while creating message for Subscription Expired", e);
        }
    }

    private PushNotificationMessage messageForType_New_Location(CustomerEvent customerEvent, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException
    {
        PushNotificationMessage pushNotificationMessage = new PushNotificationMessage();
        pushNotificationMessage.setEventType(customerEvent.getEventType());
        pushNotificationMessage.setNotificationType(notification.getNotificationType());

        try {
            JSONObject obj = new JSONObject(templateDBBean.getContent());

            String title = obj.optString("title");
            String eventOptionsData = customerEvent.getEventData().getString("options");

            if(eventOptionsData != null)
            {
                JSONObject optionsData = new JSONObject(eventOptionsData);
                if(optionsData != null && optionsData.opt("simName") != null)
                {
                    title = (String) optionsData.opt("simName");
                }
            }
            String message = obj.optString("message");
            pushNotificationMessage.setNotificationTitle(title);
            pushNotificationMessage.setNotificationMessage(message);
            logger.info("Event event_id "+customerEvent.getEventId()+" has New Location push notification with template_id " + templateDBBean.getId());
            return pushNotificationMessage;
        } catch (Exception e) {
            logger.error("PushMessageComposer messageForType_New_Location JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+customerEvent.getEventId()+"has JSON Exception while creating message for New Location", e);
        }
    }

    private PushNotificationMessage messageForGroupEvent(BaseEvent event, BaseNotification notification)
    {
        PushNotificationMessage pushNotificationMessage = new PushNotificationMessage();
        pushNotificationMessage.setEventType(event.getEventType());
        pushNotificationMessage.setNotificationType(notification.getNotificationType());

        pushNotificationMessage.setNotificationTitle(((GroupEvent) event).getTitle());
        pushNotificationMessage.setNotificationMessage(((GroupEvent) event).getMessage());
        //logger.info("Event event_id "+event.getEventId()+" has Group push notification title " + ((GroupEvent) event).getTitle() + " message " + ((GroupEvent) event).getMessage());
        return pushNotificationMessage;
    }

    // Formatting data according to plan size in KB/MB/GB
    public static String formatDataSize(int dataInKB) {
        String measure = " KB";
        float data = dataInKB;

        if ((dataInKB / (1024.0 * 1024.0)) >= 1.0) {
            measure = " GB";
            data = (float) (dataInKB / ((1024.0) * (1024.0)));
        } else if (dataInKB / 1024.0 >= 1.0) {
            measure = " MB";
            data =  (float) (dataInKB / 1024.0);
        }

        data = Math.round(data * 100);
        data = data / 100;

        String d = new DecimalFormat("##.##").format(data);
        return d + measure;
    }
}
