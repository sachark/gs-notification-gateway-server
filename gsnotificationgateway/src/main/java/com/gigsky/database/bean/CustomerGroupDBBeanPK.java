package com.gigsky.database.bean;

import java.io.Serializable;

/**
 * Created by vinayr on 19/10/15.
 */
public class CustomerGroupDBBeanPK implements Serializable {
    private int groupId;
    private int customerId;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerGroupDBBeanPK that = (CustomerGroupDBBeanPK) o;

        if (groupId != that.groupId) return false;
        if (customerId != that.customerId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = groupId;
        result = 31 * result + customerId;
        return result;
    }
}
