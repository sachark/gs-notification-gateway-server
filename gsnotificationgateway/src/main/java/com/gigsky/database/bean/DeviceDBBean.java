package com.gigsky.database.bean;

import com.gigsky.rest.bean.Device;
import com.gigsky.rest.bean.Preference;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Created by vinayr on 19/10/15.
 */
public class DeviceDBBean {

    private static final Logger logger = LoggerFactory.getLogger(Device.class);

    private int id;
    private String deviceType;
    private String deviceOsVersion;
    private String model;
    private String channelId;
    private String installationId;
    private String deviceToken;
    private String locale;
    private String appVersion;
    private Timestamp createTime;
    private Timestamp updateTime;
    private String preferences;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getInstallationId() {
        return installationId;
    }

    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }

    public DeviceDBBean()
    {

    }

    public DeviceDBBean(Device device)
    {
        this.setDeviceType(device.getDeviceType());
        this.setDeviceOsVersion(device.getDeviceOSVersion());
        this.setChannelId(device.getChannelId());
        this.setInstallationId(device.getInstallationId());
        this.setDeviceToken(device.getDeviceToken());
        this.setAppVersion(device.getAppVersion());
        this.setLocale(device.getLocale());
        this.setModel(device.getModel());

        createPreferences(device);
    }

    private void createPreferences(Device device)
    {
        Preference preferences = device.getPreferences();

        if (preferences != null)
        {
            JSONObject obj = new JSONObject();
            if(!preferences.getLocation().isEmpty())
            {
                try
                {
                    obj.put("location", preferences.getLocation());

                    this.setPreferences(obj.toString());
                }
                catch (JSONException e)
                {
                    logger.error("DeviceDBBean json parsing preferences error:" + e);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceDBBean that = (DeviceDBBean) o;

        if (id != that.id) return false;
        if (deviceType != null ? !deviceType.equals(that.deviceType) : that.deviceType != null) return false;
        if (deviceOsVersion != null ? !deviceOsVersion.equals(that.deviceOsVersion) : that.deviceOsVersion != null)
            return false;
        if (model != null ? !model.equals(that.model) : that.model != null) return false;
        if (channelId != null ? !channelId.equals(that.channelId) : that.channelId != null) return false;
        if (installationId != null ? !installationId.equals(that.installationId) : that.installationId != null)
            return false;
        if (deviceToken != null ? !deviceToken.equals(that.deviceToken) : that.deviceToken != null) return false;
        if (locale != null ? !locale.equals(that.locale) : that.locale != null) return false;
        if (appVersion != null ? !appVersion.equals(that.appVersion) : that.appVersion != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (preferences != null ? !preferences.equals(that.preferences) : that.preferences != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (deviceType != null ? deviceType.hashCode() : 0);
        result = 31 * result + (deviceOsVersion != null ? deviceOsVersion.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (channelId != null ? channelId.hashCode() : 0);
        result = 31 * result + (installationId != null ? installationId.hashCode() : 0);
        result = 31 * result + (deviceToken != null ? deviceToken.hashCode() : 0);
        result = 31 * result + (locale != null ? locale.hashCode() : 0);
        result = 31 * result + (appVersion != null ? appVersion.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (preferences != null ? preferences.hashCode() : 0);
        return result;
    }


}
