package com.gigsky.database.bean;

import java.io.Serializable;

/**
 * Created by vinayr on 19/10/15.
 */
public class EventPolicyDBBeanPK implements Serializable {
    private int policyId;
    private int eventTypeId;
    private int tenantId;

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public int getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(int eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventPolicyDBBeanPK that = (EventPolicyDBBeanPK) o;

        if (policyId != that.policyId) return false;
        if (eventTypeId != that.eventTypeId) return false;
        if (tenantId != that.tenantId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = policyId;
        result = 31 * result + eventTypeId;
        result = 31 * result + tenantId;
        return result;
    }
}
