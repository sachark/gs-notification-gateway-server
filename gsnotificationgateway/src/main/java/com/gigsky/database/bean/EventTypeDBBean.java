package com.gigsky.database.bean;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by vinayr on 19/10/15.
 */
public class EventTypeDBBean {
    private int id;
    private String type;
    private String subEventType;
    private String description;
    private Byte enableNotification;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Set<PolicyDBBean> policies;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubEventType() {
        return subEventType;
    }

    public void setSubEventType(String subEventType) {
        this.subEventType = subEventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Byte getEnableNotification() {
        return enableNotification;
    }

    public void setEnableNotification(Byte enableNotification) {
        this.enableNotification = enableNotification;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Set<PolicyDBBean> getPolicies() {
        return policies;
    }

    public void setPolicies(Set<PolicyDBBean> policies) {
        this.policies = policies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventTypeDBBean that = (EventTypeDBBean) o;

        if (id != that.id) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (subEventType != null ? !subEventType.equals(that.subEventType) : that.subEventType != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (enableNotification != null ? !enableNotification.equals(that.enableNotification) : that.enableNotification != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (subEventType != null ? subEventType.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (enableNotification != null ? enableNotification.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
