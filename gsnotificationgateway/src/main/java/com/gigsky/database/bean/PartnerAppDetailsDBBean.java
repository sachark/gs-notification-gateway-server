package com.gigsky.database.bean;

import com.gigsky.common.CommonUtils;
import com.gigsky.rest.bean.PartnerAppDetails;

import java.sql.Timestamp;

/**
 * Created by vinayr on 15/04/16.
 */
public class PartnerAppDetailsDBBean {
    private int id;
    private String deviceType;
    private String ipAddress;
    private String deviceOsVersion;
    private String locale;
    private Timestamp registeredDate;
    private String iccId;
    private String location;
    private String partnerCode;
    private String sdkVersion;

    public String getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Timestamp getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Timestamp registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public PartnerAppDetailsDBBean(PartnerAppDetails partnerAppDetails)
    {
        this.deviceType = partnerAppDetails.getDeviceType();
        this.ipAddress = partnerAppDetails.getIpAddress();
        this.deviceOsVersion = partnerAppDetails.getDeviceOSVersion();
        this.locale = partnerAppDetails.getLocale();
        this.registeredDate = CommonUtils.getCurrentTimestampInUTC();
        this.iccId = partnerAppDetails.getIccId();
        this.location = partnerAppDetails.getLocation();
        this.partnerCode = partnerAppDetails.getPartnerCode();
        this.sdkVersion = partnerAppDetails.getSdkVersion();
    }

    public PartnerAppDetailsDBBean()
    {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PartnerAppDetailsDBBean that = (PartnerAppDetailsDBBean) o;

        if (id != that.id) return false;
        if (deviceType != null ? !deviceType.equals(that.deviceType) : that.deviceType != null) return false;
        if (ipAddress != null ? !ipAddress.equals(that.ipAddress) : that.ipAddress != null) return false;
        if (deviceOsVersion != null ? !deviceOsVersion.equals(that.deviceOsVersion) : that.deviceOsVersion != null)
            return false;
        if (locale != null ? !locale.equals(that.locale) : that.locale != null) return false;
        if (registeredDate != null ? !registeredDate.equals(that.registeredDate) : that.registeredDate != null)
            return false;
        if (iccId != null ? !iccId.equals(that.iccId) : that.iccId != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (partnerCode != null ? !partnerCode.equals(that.partnerCode) : that.partnerCode != null) return false;
        if (sdkVersion != null ? !sdkVersion.equals(that.sdkVersion) : that.sdkVersion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (deviceType != null ? deviceType.hashCode() : 0);
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        result = 31 * result + (deviceOsVersion != null ? deviceOsVersion.hashCode() : 0);
        result = 31 * result + (locale != null ? locale.hashCode() : 0);
        result = 31 * result + (registeredDate != null ? registeredDate.hashCode() : 0);
        result = 31 * result + (iccId != null ? iccId.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (partnerCode != null ? partnerCode.hashCode() : 0);
        result = 31 * result + (sdkVersion != null ? sdkVersion.hashCode() : 0);
        return result;
    }
}
