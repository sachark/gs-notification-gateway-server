package com.gigsky.database.bean;

import java.sql.Timestamp;

/**
 * Created by vinayr on 19/10/15.
 */
public class PushNotificationDBBean {
    private String pushId;
    private Integer openCount;
    private Timestamp pushTime;
    private Timestamp createTime;
    private Timestamp updateTime;
    private String iccId;
    private String location;
    private Integer sendCount;
    private String languageCode;
    private int id;
    private int notificationId;
    private String status;
    private String lastAttemptStatus;
    private Timestamp lastAttemptTime;
    private Integer attemptCount;
    private String statusMessage;
    private int templateId;
    private String notificationCategory;

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public Integer getOpenCount() {
        return openCount;
    }

    public void setOpenCount(Integer openCount) {
        this.openCount = openCount;
    }

    public Timestamp getPushTime() {
        return pushTime;
    }

    public void setPushTime(Timestamp pushTime) {
        this.pushTime = pushTime;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getSendCount() {
        return sendCount;
    }

    public void setSendCount(Integer sendCount) {
        this.sendCount = sendCount;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastAttemptStatus() {
        return lastAttemptStatus;
    }

    public void setLastAttemptStatus(String lastAttemptStatus) {
        this.lastAttemptStatus = lastAttemptStatus;
    }

    public Timestamp getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(Timestamp lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public Integer getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public String getNotificationCategory() {
        return notificationCategory;
    }

    public void setNotificationCategory(String notificationCategory) {
        this.notificationCategory = notificationCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PushNotificationDBBean that = (PushNotificationDBBean) o;

        if (id != that.id) return false;
        if (notificationId != that.notificationId) return false;
        if (templateId != that.templateId) return false;
        if (pushId != null ? !pushId.equals(that.pushId) : that.pushId != null) return false;
        if (openCount != null ? !openCount.equals(that.openCount) : that.openCount != null) return false;
        if (pushTime != null ? !pushTime.equals(that.pushTime) : that.pushTime != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (iccId != null ? !iccId.equals(that.iccId) : that.iccId != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (sendCount != null ? !sendCount.equals(that.sendCount) : that.sendCount != null) return false;
        if (languageCode != null ? !languageCode.equals(that.languageCode) : that.languageCode != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (lastAttemptStatus != null ? !lastAttemptStatus.equals(that.lastAttemptStatus) : that.lastAttemptStatus != null)
            return false;
        if (lastAttemptTime != null ? !lastAttemptTime.equals(that.lastAttemptTime) : that.lastAttemptTime != null)
            return false;
        if (attemptCount != null ? !attemptCount.equals(that.attemptCount) : that.attemptCount != null) return false;
        if (statusMessage != null ? !statusMessage.equals(that.statusMessage) : that.statusMessage != null)
            return false;
        if (notificationCategory != null ? !notificationCategory.equals(that.notificationCategory) : that.notificationCategory != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pushId != null ? pushId.hashCode() : 0;
        result = 31 * result + (openCount != null ? openCount.hashCode() : 0);
        result = 31 * result + (pushTime != null ? pushTime.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (iccId != null ? iccId.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (sendCount != null ? sendCount.hashCode() : 0);
        result = 31 * result + (languageCode != null ? languageCode.hashCode() : 0);
        result = 31 * result + id;
        result = 31 * result + notificationId;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (lastAttemptStatus != null ? lastAttemptStatus.hashCode() : 0);
        result = 31 * result + (lastAttemptTime != null ? lastAttemptTime.hashCode() : 0);
        result = 31 * result + (attemptCount != null ? attemptCount.hashCode() : 0);
        result = 31 * result + (statusMessage != null ? statusMessage.hashCode() : 0);
        result = 31 * result + templateId;
        result = 31 * result + (notificationCategory != null ? notificationCategory.hashCode() : 0);
        return result;
    }
}
