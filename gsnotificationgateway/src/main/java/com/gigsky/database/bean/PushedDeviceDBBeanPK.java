package com.gigsky.database.bean;

import java.io.Serializable;

/**
 * Created by vinayr on 19/10/15.
 */
public class PushedDeviceDBBeanPK implements Serializable {
    private int deviceId;
    private int pushNotificationId;

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public int getPushNotificationId() {
        return pushNotificationId;
    }

    public void setPushNotificationId(int pushNotificationId) {
        this.pushNotificationId = pushNotificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PushedDeviceDBBeanPK that = (PushedDeviceDBBeanPK) o;

        if (deviceId != that.deviceId) return false;
        if (pushNotificationId != that.pushNotificationId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deviceId;
        result = 31 * result + pushNotificationId;
        return result;
    }
}
