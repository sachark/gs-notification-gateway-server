package com.gigsky.database.bean;

import java.sql.Timestamp;

/**
 * Created by vinayr on 19/10/15.
 */
public class SupportedClientDBBean {
    private int id;
    private String clientType;
    private String minClientVersion;
    private int policyId;
    private Timestamp createTime;
    private Timestamp updateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getMinClientVersion() {
        return minClientVersion;
    }

    public void setMinClientVersion(String minClientVersion) {
        this.minClientVersion = minClientVersion;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SupportedClientDBBean that = (SupportedClientDBBean) o;

        if (id != that.id) return false;
        if (policyId != that.policyId) return false;
        if (clientType != null ? !clientType.equals(that.clientType) : that.clientType != null) return false;
        if (minClientVersion != null ? !minClientVersion.equals(that.minClientVersion) : that.minClientVersion != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (!(updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null)) return true;
        else return false;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (clientType != null ? clientType.hashCode() : 0);
        result = 31 * result + (minClientVersion != null ? minClientVersion.hashCode() : 0);
        result = 31 * result + policyId;
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
