package com.gigsky.database.dao;

import com.gigsky.database.bean.CustomerContextDBBean;
import com.gigsky.database.bean.CustomerEventDBBean;
import com.gigsky.database.bean.EventDBBean;
import com.gigsky.rest.bean.SIMInfo;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by prsubbareddy on 07/11/17.
 */
public class CustomerContextDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(CustomerContextDao.class);

    public int addCustomerContext(CustomerContextDBBean customerContextDBBean)
    {

        Session session = openSession();
        try
        {
            session.save(customerContextDBBean);
            return customerContextDBBean.getId();
        }
        catch(Exception e)
        {
            logger.warn("Customer Context creation Exception: "+e);
        }
        finally {
            closeSession(session);
        }
        return -1;
    }

    public void updateCustomerContext(CustomerContextDBBean customerContextDBBean)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(customerContextDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    public CustomerContextDBBean getCustomerContext(long gsCustomerId)
    {
        Session session = openSession();

        try
        {
            CustomerContextDBBean customerContext = (CustomerContextDBBean) session.createCriteria(CustomerContextDBBean.class)
                    .add(Restrictions.eq("gsCustomerId", gsCustomerId))
                    .uniqueResult();

            if (customerContext != null)
            {
                return customerContext;
            }

            logger.warn("CustomerContext entry is missing for gsCustomerId" + gsCustomerId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    public void deleteCustomerInfo(CustomerContextDBBean customerContextDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(customerContextDBBean);
        }
        finally {
            closeSession(session);
        }

    }

}
