package com.gigsky.database.dao;

import com.gigsky.database.bean.CustomerDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vinayr on 20/10/15.
 */
public class CustomerDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(CustomerDao.class);

    public static final String CONSUMER_TYPE = "CONSUMER";
    public static final String ENTERPRISE_TYPE = "ENTERPRISE";

    //Create customer information for the UUID passed for the first time
    public int createCustomerInfo(CustomerDBBean customer)
    {
        Session session = openSession();

        try
        {
            session.save(customer);
            return customer.getId();
        }

        finally {
            closeSession(session);
        }
    }

    //Update customer information for the UUID
    public void updateCustomerInfo(CustomerDBBean customer)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(customer);
        }

        finally {
            closeSession(session);
        }
    }

    //Get customer information for the UUID
    public CustomerDBBean getCustomerInfo(String customerUuid)
    {
        Session session = openSession();
        CustomerDBBean customer = null;
        try
        {
            customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("customerUuid", customerUuid))
                    .uniqueResult();

            if (customer == null)
            {
                logger.warn("Customer entry is missing for " + customerUuid);
            }
        }
        catch (Exception e)
        {
            // handle DB exception and return appropriate response.
        }
        finally {
            closeSession(session);
        }
        return customer;
    }

    //Get customer information for the UUID
    public CustomerDBBean getCustomerInfoByCustomerId(int customerId)
    {
        Session session = openSession();

        try
        {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("id", customerId))
                    .uniqueResult();

            if (customer != null)
            {
                return customer;
            }

            logger.warn("Customer entry is missing for " + customerId);
            return null;
        }

        finally {
            closeSession(session);
        }
    }

    //Delete customer information for the UUID
    public void deleteCustomerInfo(CustomerDBBean customer)
    {
        Session session = openSession();

        try
        {
            session.delete(customer);
        }

        finally {
            closeSession(session);
        }
    }
}
