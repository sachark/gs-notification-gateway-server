package com.gigsky.database.dao;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.CustomerDeviceDBBean;
import com.gigsky.database.bean.DeviceDBBean;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.rest.bean.Device;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by vinayr on 20/10/15.
 */
public class DeviceDao extends AbstractDao {

    @Autowired
    private CustomerDao customerDao;

    private static final Logger logger = LoggerFactory.getLogger(DeviceDao.class);

    //Add device to the device table
    public int addDevice(DeviceDBBean device)
    {
        Session session = openSession();

        try
        {
            session.save(device);
            return device.getId();
        }
        finally {
            closeSession(session);
        }
    }


    //Add the device to Device table and then update the mapping in customerDevice table
    public int addDeviceForCustomer(String customerUuid, DeviceDBBean device) throws DatabaseException {
        Session session = openSession();
        Transaction txn = null;
        try
        {
            txn = session.beginTransaction();

            session.save(device);
            CustomerDBBean customer = customerDao.getCustomerInfo(customerUuid);

            if (customer == null)
            {
                logger.error("failed to fetch the customer for UUID " + customerUuid);
                txn.rollback();

                return Integer.parseInt(null);
            }

            //Get current timestamp in UTC
            Timestamp currentTimestamp = CommonUtils.getCurrentTimestampInUTC();

            byte x = 0;
            SQLQuery sqlQuery = session.createSQLQuery("insert into CustomerDevice (customer_id, device_id, lastAccessTime, isDeleted) values (:customerId, :deviceId, :lastAccessTime, :isDeleted)");
            sqlQuery.setParameter("customerId", customer.getId());
            sqlQuery.setParameter("deviceId", device.getId());
            sqlQuery.setParameter("lastAccessTime", currentTimestamp);
            sqlQuery.setParameter("isDeleted",x);
            sqlQuery.executeUpdate();

            txn.commit();
            txn = null;

            return device.getId();
        }

        catch (Exception ex)
        {
            if (txn != null) {
                txn.rollback();
            }

            logger.error("failed to save the device for " + customerUuid, ex);
        }

        finally {
            closeSession(session);
        }

        return INVALID_INT_VALUE;
    }

    //Add the mapping in customerDevice table as the device already exists
    public void addDeviceForCustomer(String customerUuid, int deviceId) throws DatabaseException {

        Session session = openSession();

        try
        {
            CustomerDBBean customer = customerDao.getCustomerInfo(customerUuid);

            if (customer == null)
            {
                logger.error("failed to fetch the customer for UUID " + customerUuid);
                return;
            }

            //Delete the mappings of the device with other customers
            deletedAllDeviceMappings(deviceId);

            //Get current timestamp in UTC
            Timestamp currentTimestamp = CommonUtils.getCurrentTimestampInUTC();
            byte x = 0;
            SQLQuery sqlQuery = session.createSQLQuery("insert into CustomerDevice (customer_id, device_id, lastAccessTime, isDeleted) values (:customerId, :deviceId, :lastAccessTime, :isDeleted)");
            sqlQuery.setParameter("customerId", customer.getId());
            sqlQuery.setParameter("deviceId", deviceId);
            sqlQuery.setParameter("lastAccessTime", currentTimestamp);
            sqlQuery.setParameter("isDeleted",x);

            sqlQuery.executeUpdate();

        }
        finally {
            closeSession(session);
        }
    }


    //Update the last access time in customerDevice table
    public void updateDeviceForCustomer(String customerUuid, DeviceDBBean deviceDBBean) throws DatabaseException
    {
        Session session = openSession();
        Transaction txn = null;

        try
        {
            txn = session.beginTransaction();

            CustomerDBBean customer = customerDao.getCustomerInfo(customerUuid);

            if (customer == null)
            {
                logger.error("failed to fetch the customer for UUID " + customerUuid);
                txn.rollback();
            }

            //Update the last access time
            SQLQuery updateQuery = session.createSQLQuery("update CustomerDevice set lastAccessTime = :lastAccessTime where device_id = :deviceId AND customer_id =:customerId");
            Timestamp currentTimestamp = CommonUtils.getCurrentTimestampInUTC();
            updateQuery.setParameter("lastAccessTime", currentTimestamp);
            updateQuery.setParameter("deviceId", deviceDBBean.getId());
            updateQuery.setParameter("customerId", customer.getId());
            updateQuery.executeUpdate();

            //Update the device DB bean
            session.saveOrUpdate(deviceDBBean);

            txn.commit();
            txn = null;
        }
        catch (Exception ex)
        {
            if (txn != null) {
                txn.rollback();
            }

            logger.error("failed to update the device for " + deviceDBBean.getId(), ex);
        }

        finally {
            closeSession(session);
        }
    }

    //Returns the device details for a device ID
    public DeviceDBBean getDevice(int deviceId)
    {
        Session session = openSession();

        try
        {
            DeviceDBBean device = (DeviceDBBean) session.createCriteria(DeviceDBBean.class)
                    .add(Restrictions.eq("id", deviceId))
                    .uniqueResult();

            if (device != null)
            {
                return device;
            }

            logger.warn("Device entry is missing for " + deviceId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get device based on installationId or channelId or deviceToken
    public DeviceDBBean getDevice(String installationId, String channelId, String deviceToken)
    {
        Session session = openSession();
        Criterion addRestriction;
        String id = null;

        if ((installationId != null) && (!installationId.isEmpty()))
        {
            addRestriction = Restrictions.eq("installationId", installationId);
            id = installationId;
        }
        else if ((channelId != null) && (!channelId.isEmpty()))
        {
            addRestriction = Restrictions.eq("channelId", channelId);
            id = channelId;
        }
        else if ((deviceToken != null) && (!deviceToken.isEmpty()))
        {
            addRestriction = Restrictions.eq("deviceToken", deviceToken);
            id = deviceToken;
        }
        else
        {
            return null;
        }
        DeviceDBBean device = null;
        try
        {
            device = (DeviceDBBean) session.createCriteria(DeviceDBBean.class)
                    .add(addRestriction)
                    .uniqueResult();

            if (device == null)
            {
                logger.warn("Device entry is missing for " + id);
            }
        }
        finally {
            closeSession(session);
        }
        return device;
    }

    //Delete the device in customerDevice table and Device table
    public void deleteDeviceForCustomer(DeviceDBBean device)
    {
        Session session = openSession();

        try
        {
            session.delete(device);
        }

        finally {
            closeSession(session);
        }
    }

    //Delete the device in customerDevice table and Device table
    public int deleteDeviceForCustomer(String customerUuid, int deviceId) throws DatabaseException {
        Session session = openSession();
        int rowCount = 0;
        byte x = 0;
        try
        {
            CustomerDBBean customer = customerDao.getCustomerInfo(customerUuid);

            if (customer != null) {

                //Check if the device is already deleted
                CustomerDeviceDBBean customerDevice = (CustomerDeviceDBBean) session.createCriteria(CustomerDeviceDBBean.class)
                        .add(Restrictions.eq("customerId", customer.getId()))
                        .add(Restrictions.eq("deviceId", deviceId))
                        .add(Restrictions.eq("isDeleted", x))
                        .uniqueResult();

                if (customerDevice == null)
                {
                    return rowCount;
                }

                //Delete the device
                x = 1;
                SQLQuery updateQuery = session.createSQLQuery("update CustomerDevice set isDeleted = :isDeleted where device_id = :deviceId AND customer_id =:customerId");
                updateQuery.setParameter("isDeleted", x);
                updateQuery.setParameter("deviceId", deviceId);
                updateQuery.setParameter("customerId", customer.getId());
                rowCount = updateQuery.executeUpdate();
            }
        }
        finally {
            closeSession(session);
        }

        return rowCount;
    }

    //Get devices for customer with UUID with pagination
    public List<DeviceDBBean> getDevicesForCustomer(String customerUuid, int startIndex, int count)
    {
        Session session = openSession();

        try
        {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("customerUuid", customerUuid))
                    .uniqueResult();

            if (customer != null)
            {
                int id = customer.getId();
                byte x = 0;
                List<DeviceDBBean> devices = session.createQuery(" select D from DeviceDBBean D, CustomerDeviceDBBean CD " +
                        "where CD.deviceId = D.id " +
                        "and CD.customerId = :customerId " +
                        "and CD.isDeleted = :isDeleted " +
                        "order by CD.lastAccessTime desc")
                        .setParameter("customerId", id)
                        .setParameter("isDeleted", x)
                        .setFirstResult(startIndex)
                        .setMaxResults(count)
                        .list();

                return devices;
            }

            logger.warn("Customer devices are missing for " + customerUuid);

        }
        finally {
            closeSession(session);
        }
        return null;
    }

    //Get devices for customer with UUID without pagination
    public List<DeviceDBBean> getDevicesForCustomer(String customerUuid)
    {
        Session session = openSession();

        try
        {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("customerUuid", customerUuid))
                    .uniqueResult();

            if (customer != null)
            {
                int id = customer.getId();
                byte x = 0;
                List<DeviceDBBean> devices = session.createQuery(" select D from DeviceDBBean D, CustomerDeviceDBBean CD " +
                        "where CD.deviceId = D.id " +
                        "and CD.customerId = :customerId " +
                        "and CD.isDeleted = :isDeleted " +
                        "order by CD.lastAccessTime desc")
                        .setParameter("customerId", id)
                        .setParameter("isDeleted", x)
                        .list();

                return devices;
            }

            logger.warn("Customer devices are missing for " + customerUuid);
        }
        finally {
            closeSession(session);
        }
        return null;
    }

    //Get devices count for customer with UUID
    public int getDeviceCountForCustomer(String customerUuid)
    {
        Session session = openSession();

        try
        {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("customerUuid", customerUuid))
                    .uniqueResult();

            if (customer != null)
            {
                int id = customer.getId();
                byte x = 0;
                List<DeviceDBBean> devices = session.createQuery(" select D from DeviceDBBean D, CustomerDeviceDBBean CD " +
                        "where CD.deviceId = D.id " +
                        "and CD.customerId = :customerId " +
                        "and CD.isDeleted = :isDeleted " +
                        "order by CD.lastAccessTime desc")
                        .setParameter("customerId", id)
                        .setParameter("isDeleted", x)
                        .list();

                return devices.size();
            }

            logger.warn("No devices for " + customerUuid);
        }
        finally {
            closeSession(session);
        }
        return INVALID_INT_VALUE;
    }

    //Get device for customer with UUID and Device Id
    public Device getDeviceForCustomer(String customerUuid, int deviceId)
    {
        Session session = openSession();

        try
        {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("customerUuid", customerUuid))
                    .uniqueResult();

            if (customer != null)
            {
                int id = customer.getId();
                byte x = 0;
                DeviceDBBean deviceDBBean = (DeviceDBBean) session.createQuery(" select D from DeviceDBBean D, CustomerDeviceDBBean CD " +
                        "where CD.deviceId = D.id " +
                        "and CD.deviceId = :deviceId " +
                        "and CD.customerId = :customerId " +
                        "and CD.isDeleted = :isDeleted ")
                        .setParameter("customerId", id)
                        .setParameter("deviceId", deviceId)
                        .setParameter("isDeleted", x)
                        .uniqueResult();

                if(deviceDBBean != null)
                {
                    //Populate the device details
                    Device device = new Device(deviceDBBean);
                    return device;
                }
            }

            logger.warn("Customer device missing for " + customerUuid);
        }
        finally {
            closeSession(session);
        }
        return null;
    }

    //Get device for customer with UUID and Device Id even device is deleted
    public CustomerDeviceDBBean getDeviceForCustomerIncludingDeleted(String customerUuid, int deviceId)
    {
        Session session = openSession();

        try
        {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("customerUuid", customerUuid))
                    .uniqueResult();

            if (customer != null)
            {
                int id = customer.getId();
                byte x = 0;
                CustomerDeviceDBBean customerDeviceDBBean = (CustomerDeviceDBBean) session.createQuery(" select CD from DeviceDBBean D, CustomerDeviceDBBean CD " +
                        "where CD.deviceId = D.id " +
                        "and CD.deviceId = :deviceId " +
                        "and CD.customerId = :customerId ")
                        .setParameter("customerId", id)
                        .setParameter("deviceId", deviceId)
                        .uniqueResult();

                if(customerDeviceDBBean != null)
                {
                    return customerDeviceDBBean;
                }
            }

            logger.warn("Customer device missing for " + customerUuid);
        }
        finally {
            closeSession(session);
        }
        return null;
    }

    //Add the device to Device table and then update the mapping in customerDevice table
    public int addDeviceForDeletedDevice(CustomerDeviceDBBean customerDevice) throws DatabaseException {
        Session session = openSession();

        //Delete the mappings of the device with other customers
        deletedAllDeviceMappings(customerDevice.getDeviceId());

        try
        {
            if (customerDevice != null)
            {
                //Get current timestamp in UTC
                Timestamp currentTimestamp = CommonUtils.getCurrentTimestampInUTC();
                byte x = 0;
                SQLQuery sqlQuery = session.createSQLQuery("update CustomerDevice set lastAccessTime = :lastAccessTime, isDeleted = :isDeleted where device_id = :deviceId AND customer_id = :customerId");
                sqlQuery.setParameter("lastAccessTime", currentTimestamp);
                sqlQuery.setParameter("isDeleted", x);
                sqlQuery.setParameter("deviceId", customerDevice.getDeviceId());
                sqlQuery.setParameter("customerId", customerDevice.getCustomerId());
                sqlQuery.executeUpdate();

                return customerDevice.getCustomerId();
            }
        }
        catch(Exception e)
        {
            logger.warn("Error while updating customer device for deleted  device: " + e );
        }
        finally {
            closeSession(session);
        }

        return INVALID_INT_VALUE;
    }

    //Delete all the mappings for the device
    public void deletedAllDeviceMappings(int deviceId) throws DatabaseException {

        Session session = openSession();

        try
        {
            //Delete all the mappings with device Id
            byte x = 1;

            SQLQuery sqlQuery = session.createSQLQuery("update CustomerDevice set isDeleted = :isDeleted where device_id = :deviceId");
            sqlQuery.setParameter("isDeleted", x);
            sqlQuery.setParameter("deviceId", deviceId);
            sqlQuery.executeUpdate();

            return;

        } catch (Exception e) {
            logger.warn("Error while updating customer device for deleted  device: " + e);
        } finally {
            closeSession(session);
        }

        return;
    }

}
