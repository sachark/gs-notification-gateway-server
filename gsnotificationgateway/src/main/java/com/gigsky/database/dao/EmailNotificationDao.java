package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.EmailEventDBBean;
import com.gigsky.database.bean.EmailNotificationDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 30/12/16.
 */
public class EmailNotificationDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(EmailNotificationDao.class);

    // Create Email notification with data
    public int createEmailNotification(EmailNotificationDBBean emailNotification)
    {
        Session session = openSession();

        try
        {
            session.save(emailNotification);
            return emailNotification.getId();
        }
        catch(Exception e)
        {
            logger.warn("Email Notification creation Exception: "+e);
        }
        finally {
            closeSession(session);
        }
        return -1;
    }

    // update email notifications for id with data
    public void updateEmailNotification(EmailNotificationDBBean emailNotification)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(emailNotification);
        }
        finally {
            closeSession(session);
        }
    }

    // Get email notification details for id
    public EmailNotificationDBBean getEmailNotification(int id)
    {
        Session session = openSession();

        try
        {
            EmailNotificationDBBean emailNotification = (EmailNotificationDBBean) session.createCriteria(EmailNotificationDBBean.class)
                    .add(Restrictions.eq("id", id))
                    .uniqueResult();

            if (emailNotification != null)
            {
                return emailNotification;
            }

            logger.warn("Email Notification entry is missing for " + id);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    public List<EmailNotificationDBBean> getAllEmailNotifications(int maxCount)
    {
        Session session = openSession();

        try {

            List<EmailNotificationDBBean> list = session.createCriteria(EmailNotificationDBBean.class)
                    .add(Restrictions.or(Restrictions.eq("status", Configurations.StatusType.STATUS_NEW), Restrictions.eq("status", Configurations.StatusType.STATUS_FAILED)))
                    .setMaxResults(maxCount)
                    .addOrder(Order.asc("id"))
                    .list();

            if(CollectionUtils.isNotEmpty(list))
            {
                return list;

            }

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    /*
    * ===== Delete Email Notification ====
    *
    * This method will replace recipient from email notification data with anonymous email id and .
    * content from email notification set to null
    * */
    public void deleteEmailNotification(List<Integer> notificationIds, String anonymousEmailId) {

        Session session = openSession();
        Transaction tx = session.beginTransaction();

        try {

            for(Integer notificationId : notificationIds)
            {
                Query sqlQuery = session.createQuery("update EmailNotificationDBBean E set E.recipient = :recipient, E.content = :content where E.notificationId = :notificationId");
                sqlQuery.setString("recipient", anonymousEmailId);
                sqlQuery.setString("content", null);
                sqlQuery.setInteger("notificationId", notificationId);
                sqlQuery.executeUpdate();
            }
            tx.commit();
        }
        finally {
            closeSession(session);
        }
    }

}
