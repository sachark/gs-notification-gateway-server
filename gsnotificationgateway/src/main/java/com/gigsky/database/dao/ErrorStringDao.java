package com.gigsky.database.dao;

import com.gigsky.database.bean.ErrorStringDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class ErrorStringDao extends AbstractDao {
	public ErrorStringDBBean getErrorStringByErrorId(int errorId) {
		Session session = openSession();
		try {
			return (ErrorStringDBBean) session.createCriteria(ErrorStringDBBean.class)
					.add(Restrictions.eq("errorId", errorId))
					.uniqueResult();
		}
		finally {
			closeSession(session);
		}
	}
}
