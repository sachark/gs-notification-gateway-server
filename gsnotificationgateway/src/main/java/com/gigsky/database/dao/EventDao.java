package com.gigsky.database.dao;

import com.gigsky.database.bean.*;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.rest.exception.ErrorCode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * Created by vinayr on 20/10/15.
 */
public class EventDao extends AbstractDao {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    NotificationDao notificationDao;

    private static final Logger logger = LoggerFactory.getLogger(DeviceDao.class);

    // Create event for first time from EventData
    public int createEvent(EventDBBean event, String customerUuid, String eventData) throws DatabaseException {
        Session session = openSession();
        Transaction txn = null;

        try
        {
            txn = session.beginTransaction();

            session.save(event);

            CustomerDBBean customer = customerDao.getCustomerInfo(customerUuid);

            if (customer == null)
            {
                logger.error("failed to fetch the customer for UUID " + customerUuid);
                txn.rollback();

                return INVALID_INT_VALUE;
            }

            SQLQuery sqlQuery = session.createSQLQuery("insert into CustomerEvent (customer_id, event_id, data, tenant_id) values (:customerId, :eventId, :eventData, :tenantId)");
            sqlQuery.setParameter("customerId", customer.getId());
            sqlQuery.setParameter("eventId", event.getId());
            sqlQuery.setParameter("eventData", eventData);
            sqlQuery.setParameter("tenantId", event.getTenantId());
            sqlQuery.executeUpdate();

            txn.commit();
            txn = null;

            return event.getId();
        }

        catch (Exception ex)
        {
            if (txn != null) {
                txn.rollback();
            }

            logger.error("failed to save the device for " + customerUuid, ex);
            throw new DatabaseException(ErrorCode.EVENT_CREATION_FAILED, "Event creation failed");
        }

        finally {
            closeSession(session);
        }
    }

    public int createEventForEmail(EventDBBean event, String emailIds, String eventData) throws DatabaseException {
        Session session = openSession();
        Transaction txn = null;

        try
        {
            txn = session.beginTransaction();
            session.save(event);

            SQLQuery sqlQuery = session.createSQLQuery("insert into EmailEvent (email_id, event_id, data, tenant_id) values (:emailId, :eventId, :eventData, :tenantId)");
            sqlQuery.setParameter("emailId", emailIds);
            sqlQuery.setParameter("eventId", event.getId());
            sqlQuery.setParameter("eventData", eventData);
            sqlQuery.setParameter("tenantId", event.getTenantId());
            sqlQuery.executeUpdate();

            txn.commit();
            txn = null;

            return event.getId();
        }

        catch (Exception ex)
        {
            if (txn != null) {
                txn.rollback();
            }

            logger.error("failed to save the event for " + ex);
            throw new DatabaseException(ErrorCode.EVENT_CREATION_FAILED, "Event creation failed");
        }

        finally {
            closeSession(session);
        }
    }

    // Get Event details by passing eventId
    public EventDBBean getEventDetails(int eventId, int tenantId)
    {
        Session session = openSession();

        try
        {
            EventDBBean event = (EventDBBean) session.createCriteria(EventDBBean.class)
                    .add(Restrictions.eq("id", eventId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (event != null)
            {
                return event;
            }

            logger.warn("Event entry is missing for " + eventId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Update Event Details for an event using EventData
    public void updateEventDetails(EventDBBean event)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(event);

            CustomerEventDBBean customerEvent = event.getCustomerEvent();

            if (customerEvent != null)
            {
                session.saveOrUpdate(customerEvent);
            }
        }
        finally {
            closeSession(session);
        }
    }

    // Delete Event Details for an event
    public void deleteEventDetails(EventDBBean event)
    {
        Session session = openSession();

        try
        {
            session.delete(event);
        }
        finally {
            closeSession(session);
        }
    }

 //EVENT TYPE Specific functions

    //Get the policies applicable for a particular event type
    public Set<PolicyDBBean> getPoliciesForEventType(String eventType)
    {
        Session session = openSession();

        try
        {
            EventTypeDBBean eventTypeDBBean = (EventTypeDBBean) session.createCriteria(EventTypeDBBean.class)
                    .add(Restrictions.eq("type", eventType))
                    .uniqueResult();

            if (eventTypeDBBean != null)
            {
                return eventTypeDBBean.getPolicies();
            }

            logger.warn("Event type entry is missing for " + eventType);
            return null;

        }
        finally {
            closeSession(session);
        }
    }

    //Init with event type details
    public int createEventType(EventTypeDBBean eventTypeDBBean)
    {
        Session session = openSession();

        try
        {
            session.save(eventTypeDBBean);
            return eventTypeDBBean.getId();
        }
        finally {
            closeSession(session);
        }
    }

    //Get the event type details for an event type ID
    public EventTypeDBBean getEventType(int eventTypeId)
    {
        Session session = openSession();

        try
        {
            EventTypeDBBean eventTypeDBBean = (EventTypeDBBean) session.createCriteria(EventTypeDBBean.class)
                    .add(Restrictions.eq("id", eventTypeId))
                    .uniqueResult();

            if (eventTypeDBBean != null)
            {
                return eventTypeDBBean;
            }

            logger.warn("Event type entry is missing for " + eventTypeId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get the event type details for an event type
    public EventTypeDBBean getEventType(String eventType)
    {
        Session session = openSession();

        try
        {
            EventTypeDBBean eventTypeDBBean = (EventTypeDBBean) session.createCriteria(EventTypeDBBean.class)
                    .add(Restrictions.eq("type", eventType))
                    .uniqueResult();

            if (eventTypeDBBean != null)
            {
                return eventTypeDBBean;
            }

            logger.warn("Event type entry is missing for " + eventType);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get the event type details for an event type
    public EventTypeDBBean getEventTypeBySubEventType(String subEventType)
    {
        Session session = openSession();

        try
        {
            EventTypeDBBean eventTypeDBBean = (EventTypeDBBean) session.createCriteria(EventTypeDBBean.class)
                    .add(Restrictions.eq("subEventType", subEventType))
                    .uniqueResult();

            if (eventTypeDBBean != null)
            {
                return eventTypeDBBean;
            }

            logger.warn("Event type entry is missing for " + subEventType);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Delete the event type details for an event type
    public void deleteEventType(EventTypeDBBean eventTypeDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(eventTypeDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Set policy for an event type
    public void setPolicyForEventType(int eventTypeId, int policyId)
    {
        Session session = openSession();

        try {

            SQLQuery sqlQuery = session.createSQLQuery("insert into EventPolicy (policy_id, eventType_id) values (:policyId, :eventTypeId)");
            sqlQuery.setParameter("policyId",policyId);
            sqlQuery.setParameter("eventTypeId", eventTypeId);
            sqlQuery.executeUpdate();

        }
        finally {
            closeSession(session);
        }
    }


    // Get Events based on status
    //Gets the unprocessed events from the database
    public List<EventDBBean> getEvents(String status, int maxCount)
    {
        Session session = openSession();

        try
        {
            List<EventDBBean> eventsList = session.createCriteria(EventDBBean.class)
                                .add(Restrictions.eq("status", status))
                    .setMaxResults(maxCount)
                    .addOrder(Order.asc("id"))
                    .list();

            return eventsList;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            logger.warn("Event entries are missing for status" + status);
        }
        finally {
            closeSession(session);
        }
        return null;
    }


    //Custom Policy Checks

    //Get the events with status not NEW
//    public List<EventDBBean> getAllEventsExceptNew()
//    {
//        Session session = openSession();
//
//        try
//        {
//            List<EventDBBean> eventsList = session.createCriteria(EventDBBean.class)
//                    .add(Restrictions.ne("status", Configurations.StatusType.STATUS_NEW))
//                    .addOrder(Order.asc("id"))
//                    .list();
//
//            return eventsList;
//        }
//        finally {
//            closeSession(session);
//        }
//    }
//
//    public List<EventDBBean> getEventsForEventType(int eventTypeId)
//    {
//        Session session = openSession();
//
//        try
//        {
//            List<EventDBBean> eventsList = session.createCriteria(EventDBBean.class)
//                    .add(Restrictions.eq("eventTypeId", eventTypeId))
//                    .addOrder(Order.asc("id"))
//                    .list();
//
//            return eventsList;
//        }
//        finally {
//            closeSession(session);
//        }
//    }

    public List<EventDBBean> getEventsForEventTypeAndIccid(int eventTypeId, String iccid, int maxCount)
    {
        Session session = openSession();

        try
        {
            List<EventDBBean> eventsList = session.createQuery(" select E from EventDBBean E, CustomerEventDBBean CE " +
                    "where CE.eventId = E.id " +
                    "and E.eventTypeId = :eventTypeId " +
                    "and CE.data like :iccid " +
                    "order by E.receivedTime desc")
                    .setParameter("eventTypeId", eventTypeId)
                    .setParameter("iccid", "%" + iccid + "%")
                    .setMaxResults(maxCount)
                    .list();


            return eventsList;
        }
        finally {
            closeSession(session);
        }
    }


    public EmailEventDBBean getEmailEvent(int eventId)
    {
        Session session = openSession();
        try
        {
            EmailEventDBBean emailEventDBBean = (EmailEventDBBean) session.createCriteria(EmailEventDBBean.class)
                    .add(Restrictions.eq("eventId", eventId))
                    .uniqueResult();

            if (emailEventDBBean != null)
            {
                return emailEventDBBean;
            }

            logger.warn("EmailEvent type entry is missing for " + eventId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }


    /*
        * ===== Delete Customer Event details ====
        *
        * This method will remove location and replace user id from customer event data with anonymous email id.
        *
        * */
    public List<Integer> deleteCustomerEventDetails(String emailId, int tenantId)
    {
        Session session = openSession();
        Transaction tx = session.beginTransaction();

        try {

            List<Integer> eventIds = new ArrayList<Integer>();

            List<CustomerEventDBBean> customerEventList = session.createCriteria(CustomerEventDBBean.class)
                    .add(Restrictions.like("data", emailId, MatchMode.ANYWHERE))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .list();

            for(CustomerEventDBBean customerEventDBBean : customerEventList) {

                // Add event ids to list
                eventIds.add(customerEventDBBean.getEventId());

                String sqlUpdateQuery = "update CustomerEventDBBean c set c.data = :data where c.eventId = :eventId";

                session.createQuery( sqlUpdateQuery )
                        .setString("data", null)
                        .setInteger("eventId", customerEventDBBean.getEventId())
                        .executeUpdate();
            }

            tx.commit();
            return eventIds;
        }
        finally {
            closeSession(session);
        }
    }

    /*
    * ===== Delete Email Event details ====
    *
    * This method will replace customer email from email event data and email id with anonymous email id.
    *
    * */
    public List<Integer> deleteEmailEventDetails(String emailId, String anonymousEmailId, int tenantId) {
        Session session = openSession();
        Transaction tx = session.beginTransaction();

        try {

            List<Integer> eventIds = new ArrayList<Integer>();

            List<EmailEventDBBean> emailEventList = session.createCriteria(EmailEventDBBean.class)
                    .add(Restrictions.eq("emailId", emailId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .list();


            for(EmailEventDBBean emailEventDBBean : emailEventList)
            {

                // Add event ids to list
                eventIds.add(emailEventDBBean.getEventId());

                Query sqlQuery = session.createQuery("update EmailEventDBBean E set E.data = :data, E.emailId = :emailId where E.id = :id");
                sqlQuery.setString("data", null);
                sqlQuery.setString("emailId", anonymousEmailId);
                sqlQuery.setInteger("id", emailEventDBBean.getId());
                sqlQuery.executeUpdate();
            }

            tx.commit();
            return eventIds;
        }
        finally {
            closeSession(session);
        }
    }

}
