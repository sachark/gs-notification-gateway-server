package com.gigsky.database.dao;

import com.gigsky.database.bean.CustomerGroupDBBean;
import com.gigsky.database.bean.GroupTypeDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vinayr on 07/12/15.
 */
public class GroupDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(GroupDao.class);

//GroupType table related functions

    public List<GroupTypeDBBean> getAllGroups()
    {
        Session session = openSession();

        try {

            List<GroupTypeDBBean> list = session.createCriteria(GroupTypeDBBean.class)
                    .list();

            if(CollectionUtils.isNotEmpty(list))
            {
                return list;

            }

            return null;
        }
        finally {
            closeSession(session);
        }
    }



//CustomerGroup table related functions

    //Get the list of customers of a group
    public List<CustomerGroupDBBean> getCustomersForGroup(int groupId)
    {
        Session session = openSession();

        try
        {
            List<CustomerGroupDBBean> customerGroups = session.createQuery(" select CG  from CustomerGroupDBBean CG " +
                    "where CG.groupId = :groupId " +
                    "order by CG.customerId asc ")
                    .setParameter("groupId", groupId)
                    .list();

            return customerGroups;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            closeSession(session);
        }

        return null;
    }


}
