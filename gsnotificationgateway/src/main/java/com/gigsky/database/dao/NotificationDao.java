package com.gigsky.database.dao;

import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.bean.NotificationTypeDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vinayr on 20/10/15.
 */
public class NotificationDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(NotificationDao.class);

    //Create notification for an event
    public int createNotification(NotificationDBBean notification)
    {
        Session session = openSession();

        try
        {
            session.save(notification);
            return notification.getId();
        }

        catch (Exception ex)
        {
             ex.printStackTrace();
        }

        finally {
            closeSession(session);
        }

        return -1;
    }

    //Update notification details for a notification
    public void updateNotification(NotificationDBBean notification)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(notification);
        }

        finally {
            closeSession(session);
        }
    }


    //Get notification details for a notification
    public NotificationDBBean getNotificationDetails(int notificationId)
    {
        Session session = openSession();

        try
        {

            NotificationDBBean notification = (NotificationDBBean) session.createCriteria(NotificationDBBean.class)
                    .add(Restrictions.eq("id", notificationId))
                    .uniqueResult();

            if (notification != null)
            {
                return notification;
            }

            logger.warn("Notification entry is missing for " + notificationId);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    //Get notification details for a notification
    public NotificationDBBean getNotificationDetails(int notificationId, int tenantId)
    {
        Session session = openSession();

        try
        {
            NotificationDBBean notification = (NotificationDBBean) session.createCriteria(NotificationDBBean.class)
                    .add(Restrictions.eq("id", notificationId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (notification != null)
            {
                return notification;
            }

            logger.warn("Notification entry is missing for " + notificationId);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    //Delete notification details for a notification
    public void deleteNotification(NotificationDBBean notification)
    {
        Session session = openSession();

        try
        {
            session.delete(notification);
        }

        finally {
            closeSession(session);
        }
    }

    public List<NotificationDBBean> getNotifications(int eventId)
    {
        Session session = openSession();

        try
        {
            List<NotificationDBBean> notifications = session.createQuery(" select N from NotificationDBBean N " +
                    " where N.eventId = :eventId " +
                    " order by N.id asc ")
                    .setParameter("eventId", eventId)
                    .list();

            return notifications;

        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            closeSession(session);
        }

        return null;
    }

//NOTIFICATION TYPE related functions

    //Create notification type
    public int createNotificationType(NotificationTypeDBBean notificationTypeDBBean)
    {
        Session session = openSession();

        try
        {
            session.save(notificationTypeDBBean);
            return notificationTypeDBBean.getId();
        }

        finally {
            closeSession(session);
        }
    }

    //Get notification type details for a notification type id
    public NotificationTypeDBBean getNotificationType(int notificationTypeId)
    {
        Session session = openSession();

        try
        {

            NotificationTypeDBBean notificationType = (NotificationTypeDBBean) session.createCriteria(NotificationTypeDBBean.class)
                    .add(Restrictions.eq("id", notificationTypeId))
                    .uniqueResult();

            if (notificationType != null)
            {
                return notificationType;
            }

            logger.warn("Notification Type entry is missing for " + notificationTypeId);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    //Get notification type details for a notification type id
    public NotificationTypeDBBean getNotificationType(String notificationType)
    {
        Session session = openSession();

        try
        {
            NotificationTypeDBBean notificationTypeDBBean = (NotificationTypeDBBean) session.createCriteria(NotificationTypeDBBean.class)
                    .add(Restrictions.eq("type", notificationType))
                    .uniqueResult();

            if (notificationType != null)
            {
                return notificationTypeDBBean;
            }

            logger.warn("Notification Type entry is missing for " + notificationType);

            return null;
        }

        finally {
            closeSession(session);
        }
    }


    //Delete notification type details
    public void deleteNotificationType(NotificationTypeDBBean notificationType)
    {
        Session session = openSession();

        try
        {
            session.delete(notificationType);
        }

        finally {
            closeSession(session);
        }
    }

}
