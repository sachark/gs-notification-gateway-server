package com.gigsky.database.dao;

import com.gigsky.database.bean.PartnerAppDetailsDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Date;


/**
 * Created by vinayr on 15/04/16.
 */
public class PartnerAppDetailsDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(PartnerAppDetailsDao.class);

    public static int MAX_ICCID_LENGTH = 40;

    //Create partner app details
    public int createPartnerAppDetails(PartnerAppDetailsDBBean partnerAppDetailsDBBean)
    {
        Session session = openSession();

        try
        {
            session.save(partnerAppDetailsDBBean);
            return partnerAppDetailsDBBean.getId();
        }

        finally {
            closeSession(session);
        }
    }

    //Update partner app details
    public void updatePartnerAppDetails(PartnerAppDetailsDBBean partnerAppDetailsDBBean)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(partnerAppDetailsDBBean);
        }

        finally {
            closeSession(session);
        }
    }

    //Get partner app details
    public List<PartnerAppDetailsDBBean> getPartnerAppDetails(String deviceType, String ipAddress, String deviceOSVersion, String locale)
    {
        Session session = openSession();

        try
        {
            List<PartnerAppDetailsDBBean> partnerAppDetailsDBBeanList =  session.createCriteria(PartnerAppDetailsDBBean.class)
                    .add(Restrictions.eq("deviceType", deviceType))
                    .add(Restrictions.eq("ipAddress", ipAddress))
                    .add(Restrictions.eq("deviceOsVersion", deviceOSVersion))
                    .add(Restrictions.eq("locale", locale))
                    .list();

            if (partnerAppDetailsDBBeanList.isEmpty())
            {
                logger.warn("Partner App details entry is missing for " + deviceType +"  "+ ipAddress);
            }

            return partnerAppDetailsDBBeanList;
        }
        catch (Exception e)
        {
            // handle DB exception and return appropriate response.
        }
        finally {
            closeSession(session);
        }

        return null;
    }

    //Delete the Partner App Details
    public void deletePartnerAppDetails(PartnerAppDetailsDBBean partnerAppDetailsDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(partnerAppDetailsDBBean);
        }

        finally {
            closeSession(session);
        }
    }


    //Get Expired Partner App details
    public List<PartnerAppDetailsDBBean> getExpiredPartnerAppDetails(long expiryMS, int maxCount)
    {
        Session session = openSession();

        try
        {
            List<PartnerAppDetailsDBBean> partnerAppDetailsDBBeanList = session.createQuery(" select P from PartnerAppDetailsDBBean P " +
                    "where P.registeredDate < :date")
                    .setParameter("date", new Date(System.currentTimeMillis() - expiryMS))
                    .setMaxResults(maxCount)
                    .list();

            return partnerAppDetailsDBBeanList;

        }

        finally {
            closeSession(session);
        }
    }


}
