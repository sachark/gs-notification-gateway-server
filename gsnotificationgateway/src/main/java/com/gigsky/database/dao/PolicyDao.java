package com.gigsky.database.dao;

import com.gigsky.database.bean.PolicyDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vinayr on 26/10/15.
 */
public class PolicyDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(PolicyDao.class);


    //Create policy information
    public int createPolicy(PolicyDBBean policy)
    {
        Session session = openSession();

        try
        {
            session.save(policy);
            return policy.getId();
        }
        catch (Exception ex) {
            logger.error("createPolicy error:"+ex);
        }

        finally {
            closeSession(session);
        }

        return -1;
    }

    //Get policy information for the ID
    public PolicyDBBean getPolicy(int policyId)
    {
        Session session = openSession();

        try
        {
            PolicyDBBean policy = (PolicyDBBean) session.createCriteria(PolicyDBBean.class)
                    .add(Restrictions.eq("id", policyId))
                    .uniqueResult();

            if (policy != null)
            {
                return policy;
            }

            logger.warn("Policy entry is missing for " + policyId);
            return null;
        }

        finally {
            closeSession(session);
        }
    }

    //Delete policy information
    public void deletePolicy(PolicyDBBean policy)
    {
        Session session = openSession();

        try
        {
            session.delete(policy);
        }

        finally {
            closeSession(session);
        }
    }

    public List<PolicyDBBean> getPoliciesByEventType(int eventTypeId, int tenantId)
    {
        Session session = openSession();

        try
        {
            List<PolicyDBBean> policies = session.createQuery(" select P from PolicyDBBean P, EventPolicyDBBean EP " +
                    "where EP.policyId = P.id " +
                    "and EP.eventTypeId = :eventTypeId " +
                    "and EP.tenantId = :tenantId")
                    .setParameter("eventTypeId", eventTypeId)
                    .setParameter("tenantId", tenantId)
                    .list();

            if(policies == null)
            {
                logger.warn("Policies are missing for " + eventTypeId);
            }
            return policies;
        }
        catch (Exception ex) {
            logger.error("getPoliciesByEventType error:"+ex);
        }

        finally {
            closeSession(session);
        }
        return null;
    }

    //Get the policies for the event type and group ID
    public List<PolicyDBBean> getPoliciesByEventType(int eventTypeId, int groupId, int tenantId)
    {
        Session session = openSession();

        try
        {
            List<PolicyDBBean> policies = session.createQuery(" select P from PolicyDBBean P, EventPolicyDBBean EP " +
                    "where EP.policyId = P.id " +
                    "and EP.eventTypeId = :eventTypeId " +
                    "and EP.tenantId = :tenantId " +
                    "and EP.groupId = :groupId")
                    .setParameter("eventTypeId", eventTypeId)
                    .setParameter("groupId", groupId)
                    .setParameter("tenantId", tenantId)
                    .list();

            if(policies == null)
            {
                logger.warn("Policies are missing for " + eventTypeId);
            }
            return policies;
        }
        catch (Exception ex) {
            logger.error("getPoliciesByEventType error:"+ex);
        }

        finally {
            closeSession(session);
        }
        return null;
    }

    //Get the policies for the event type and for all customers
    public List<PolicyDBBean> getPoliciesByEventTypeForAllCustomers(int eventTypeId, int tenantId)
    {
        Session session = openSession();

        try
        {
            List<PolicyDBBean> policies = session.createQuery(" select P from PolicyDBBean P, EventPolicyDBBean EP " +
                    "where EP.policyId = P.id " +
                    "and EP.eventTypeId = :eventTypeId " +
                    "and EP.tenantId = :tenantId " +
                    "and EP.groupId IS NULL")
                    .setParameter("eventTypeId", eventTypeId)
                    .setParameter("tenantId", tenantId)
                    .list();

            if(policies == null)
            {
                logger.warn("Policies are missing for " + eventTypeId);
            }
            return policies;
        }
        catch (Exception ex) {
            logger.error("getPoliciesByEventTypeForAllCustomers error:"+ex);
        }

        finally {
            closeSession(session);
        }
        return null;
    }
}
