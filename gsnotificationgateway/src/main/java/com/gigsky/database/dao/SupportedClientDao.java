package com.gigsky.database.dao;

import com.gigsky.database.bean.SupportedClientDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by prsubbareddy on 30/10/15.
 */
public class SupportedClientDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(SupportedClientDao.class);


    // create supported client information
    public int createSupportClientInfo(SupportedClientDBBean supportedClientDBBean)
    {
        Session session = openSession();

        try
        {
            session.save(supportedClientDBBean);
            return supportedClientDBBean.getId();
        }

        finally {
            closeSession(session);
        }
    }

    //Get supported client information for the ID
    public SupportedClientDBBean getSupportClientInfo(int policyId, String clientType)
    {
        Session session = openSession();

        try
        {
            SupportedClientDBBean supportedClientDBBean = (SupportedClientDBBean) session.createCriteria(SupportedClientDBBean.class)
                    .add(Restrictions.eq("policyId", policyId))
                    .add(Restrictions.eq("clientType", clientType))
                    .uniqueResult();

            if (supportedClientDBBean != null)
            {
                return supportedClientDBBean;
            }

            logger.warn("SupportClientInfo entry is missing for " + policyId);
            return null;
        }

        finally {
            closeSession(session);
        }
    }

    //Delete supported client information
    public void deleteSupportedClientInfo(SupportedClientDBBean supportedClientDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(supportedClientDBBean);
        }

        finally {
            closeSession(session);
        }
    }
}
