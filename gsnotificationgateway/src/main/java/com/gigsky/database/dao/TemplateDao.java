package com.gigsky.database.dao;

import com.gigsky.database.bean.NotificationTemplateDBBean;
import com.gigsky.database.bean.TemplateDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vinayr on 23/10/15.
 */
public class TemplateDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(TemplateDao.class);

    public TemplateDBBean getTemplate(int eventTypeId, int notificationTypeId, int tenantId, String locale)
    {
        Session session = openSession();

        try
        {

            List<NotificationTemplateDBBean> notificationTemplates = session.createCriteria(NotificationTemplateDBBean.class)
                    .add(Restrictions.eq("eventTypeId", eventTypeId))
                    .add(Restrictions.eq("notificationTypeId", notificationTypeId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .list();


            if (CollectionUtils.isNotEmpty(notificationTemplates))
            {
                //Traverse through all notification templates
                for (Object notificationTemplateRow: notificationTemplates)
                {
                    if (notificationTemplateRow instanceof NotificationTemplateDBBean)
                    {
                        NotificationTemplateDBBean notificationTemplate = (NotificationTemplateDBBean) notificationTemplateRow;

                        //Get the template for each row
                        TemplateDBBean template = getTemplateDetails(notificationTemplate.getTemplateId());

                        if (template != null)
                        {
                            //If the locale matches with the template locale then it is returned.
                            if (locale != null && locale.equals(template.getLanguageCode()))
                            {
                                return template;
                            }

                        }
                    }

                }


            }

            logger.warn("Template entry is missing for eventTypeId: " + eventTypeId + " NotificationTypeId: " +notificationTypeId);

            return null;
        }
        catch (Exception ex) {
            logger.error("getTemplate error:"+ex);
        }
        finally {
            closeSession(session);
        }

        return null;
    }

    //Create notification, event and template mapping
    public void createTemplate(int eventTypeId, int notificationTypeId, int templateId)
    {
        Session session = openSession();

        try {

            SQLQuery sqlQuery = session.createSQLQuery("insert into NotificationTemplate (notificationType_id, eventType_id, template_id) values (:notificationTypeId, :eventTypeId, :templateId)");
            sqlQuery.setParameter("notificationTypeId",notificationTypeId);
            sqlQuery.setParameter("eventTypeId", eventTypeId);
            sqlQuery.setParameter("templateId", templateId);
            sqlQuery.executeUpdate();

        }
        finally {
            closeSession(session);
        }

    }

    // Create template
    public int createTemplate(TemplateDBBean template) {
        Session session = openSession();

        try {

            session.save(template);
            return template.getId();
        }
        finally {
            closeSession(session);
        }
    }

    //Get the template for the template ID
    public TemplateDBBean getTemplateDetails(int templateId)
    {
        Session session = openSession();

        try
        {
            TemplateDBBean template = (TemplateDBBean) session.createCriteria(TemplateDBBean.class)
                    .add(Restrictions.eq("id", templateId))
                    .uniqueResult();

            if (template != null)
            {
                return template;
            }

            logger.warn("Template entry is missing for " + templateId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Delete template
    public void deleteTemplate(TemplateDBBean template) {
        Session session = openSession();

        try {

            session.delete(template);
        }
        finally {
            closeSession(session);
        }
    }
}
