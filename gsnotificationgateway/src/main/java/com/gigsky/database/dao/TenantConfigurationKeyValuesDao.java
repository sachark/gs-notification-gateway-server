package com.gigsky.database.dao;

import com.gigsky.database.bean.TenantConfigurationsKeyValueDBBean;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by prsubbareddy on 10/01/19.
 */

public class TenantConfigurationKeyValuesDao extends AbstractDao {

	private static final Logger logger = LoggerFactory.getLogger(TenantConfigurationKeyValuesDao.class);

	public String getConfigurationValue(String configKey, int tenantId) {
		Session session = openSession();
		try {
			String configValue = (String) session.createCriteria(TenantConfigurationsKeyValueDBBean.class)
					.setProjection(Projections.projectionList().add(Projections.property("value")))
					.add(Restrictions.eq("ckey", configKey))
					.add(Restrictions.eq("tenantId", tenantId))
					.uniqueResult();

			if (StringUtils.isNotEmpty(configValue)) {
				return configValue;
			}
			logger.warn("Tenant Configuration entry is missing for " + configKey);
			return StringUtils.EMPTY;
		}
		finally {
			closeSession(session);
		}
	}

	public String getConfigurationValue(String configKey, String defaultValue, int tenantId) {
		String configurationValue = getConfigurationValue(configKey, tenantId);
		if (StringUtils.isEmpty(configurationValue)) {
			configurationValue = defaultValue;
		}
		return configurationValue;
	}

}
