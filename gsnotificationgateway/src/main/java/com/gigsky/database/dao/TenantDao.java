package com.gigsky.database.dao;

import com.gigsky.database.bean.TenantDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by prsubbareddy on 10/01/19.
 */
public class TenantDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(TenantDao.class);

    // Get Tenant details by passing tenantId
    public TenantDBBean getTenantDetails(int tenantId) {
        Session session = openSession();

        try {
            TenantDBBean tenantDBBean = (TenantDBBean) session.createCriteria(TenantDBBean.class)
                    .add(Restrictions.eq("id", tenantId))
                    .uniqueResult();

            if (tenantDBBean != null) {
                return tenantDBBean;
            }

            logger.warn("Tenant entry is missing for " + tenantId);

            return null;
        } finally {
            closeSession(session);
        }
    }

}