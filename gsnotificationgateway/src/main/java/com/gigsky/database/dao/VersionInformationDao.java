package com.gigsky.database.dao;

import com.gigsky.database.bean.VersionInformationDBBean;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.GigskyNotificationGatewayServerException;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;

public class VersionInformationDao extends AbstractDao {
	public VersionInformationDBBean getVersionInformation() {
		Session session = openSession();
		try {
			Criteria criteria = session.createCriteria(VersionInformationDBBean.class);
			List<VersionInformationDBBean> versionInformation = criteria.setMaxResults(1).list();
			if (versionInformation != null) {
				return versionInformation.get(0);
			}
			else {
				throw new GigskyNotificationGatewayServerException(ErrorCode.RESOURCE_NOT_FOUND);
			}
		}
		finally {
			closeSession(session);
		}
	}
}