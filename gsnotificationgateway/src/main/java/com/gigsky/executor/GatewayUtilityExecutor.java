package com.gigsky.executor;

import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.service.PartnerAppService;
import com.gigsky.service.PushNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.*;

/**
 * Created by prsubbareddy on 29/12/15.
 *
 * Task executor will periodically check for Push Notifications updates
 */
public class GatewayUtilityExecutor implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(GatewayUtilityExecutor.class);

    private static ScheduledExecutorService executorService = null;

    private static final long DEFAULT_GATEWAY_UTILITY_EXEC_TIME = 24*60*60*1000;
    private static final int DEFAULT_SHUTDOWN_WAIT_TIME_MS = 10000;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    PushNotificationService pushNotificationService;

    @Autowired
    private PartnerAppService partnerAppService;


    public synchronized void start() throws ExecutionException, InterruptedException {
        logger.debug("GatewayUtilityExecutor: start+++");

        if(executorService == null) {
            executorService = Executors.newSingleThreadScheduledExecutor(Executors.defaultThreadFactory());
        }

        long runnableScheduleTimeMS;

        String gatewayUtilExecTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_UTILITY_EXEC_TIME);

        if (gatewayUtilExecTime.isEmpty())
        {
            runnableScheduleTimeMS = DEFAULT_GATEWAY_UTILITY_EXEC_TIME;
        }

        //TODO check if gatewayUtilExecTime is of valid format
        else
        {
            runnableScheduleTimeMS = new GigskyDuration(gatewayUtilExecTime).convertTimeInMilliSeconds();
        }

        logger.debug("runnableScheduleTimeMS = " + runnableScheduleTimeMS);
        executorService.scheduleAtFixedRate(this, runnableScheduleTimeMS, runnableScheduleTimeMS, TimeUnit.MILLISECONDS);

        logger.debug("GatewayUtilityExecutor: start---");
    }


    public void shutdown() {
        logger.debug("GatewayUtilityExecutor: shutdown+++");

        int shutdownWaitMS;
        String shutdownWaitTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.SHUTDOWN_WAIT_TIME_MS);

        if (shutdownWaitTime.isEmpty())
        {
            shutdownWaitMS = DEFAULT_SHUTDOWN_WAIT_TIME_MS;
        }
        else
        {
            shutdownWaitMS = (int) new GigskyDuration(shutdownWaitTime).convertTimeInMilliSeconds();
        }

        try {
            if (executorService != null) {
                logger.debug("GatewayUtilityExecutor:executorService.shutdown ++");
                executorService.shutdown();
                logger.debug("GatewayUtilityExecutor:executorService.shutdown --");

                while (!executorService.isTerminated()) {
                    logger.debug("GatewayUtilityExecutor:executorService is Terminated flag is false waiting for " +
                            shutdownWaitMS + " millisecs");
                    executorService.awaitTermination(shutdownWaitMS, TimeUnit.MILLISECONDS);
                }
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with execption ", ex);
        }
        logger.debug("GatewayUtilityExecutor:awaitTermination--");
        executorService = null;

        logger.debug("GatewayUtilityExecutor: shutdown---");
    }

    @Override
    public void run() {

        try {
            runInternal();
        } catch (Exception e) {
            logger.error("Exception:" , e);
        }
    }

    private void runInternal() {
        logger.debug("runInternal+++");

        //processing push notification to update entries
        pushNotificationService.processPushNotifications();

        //Process Partner app details and delete the entries which have expired
        partnerAppService.deletePartnerDeviceEntries();

        logger.debug("runInternal---");
    }
}
