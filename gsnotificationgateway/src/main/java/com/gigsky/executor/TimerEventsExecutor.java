package com.gigsky.executor;

import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.*;

/**
 * Created by anant on 27/10/15.
 *
 * Task executor will periodically check for events which are not processed and process them
 */
public class TimerEventsExecutor implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(TimerEventsExecutor.class);

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    EventService eventService;

    private static ScheduledExecutorService executorService = null;

    private static final long DEFAULT_EVENT_EXEC_TIME_MS = 10000;
    private static final int DEFAULT_SHUTDOWN_WAIT_TIME_MS = 10000;


    public synchronized void start() throws ExecutionException, InterruptedException {
        logger.debug("TimerEventsExecutor: start+++");

        long runnableScheduleTimeMS;
        String eventExecTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EVENT_EXEC_TIME_MS);

        if (eventExecTime.isEmpty())
        {
            runnableScheduleTimeMS = DEFAULT_EVENT_EXEC_TIME_MS;
        }

        //TODO check if eventExecTime is of valid format
        else
        {
            runnableScheduleTimeMS = new GigskyDuration(eventExecTime).convertTimeInMilliSeconds();
        }

        if(executorService == null) {
            executorService = Executors.newSingleThreadScheduledExecutor(Executors.defaultThreadFactory());
        }

        logger.debug("runnableScheduleTimeMS = " + runnableScheduleTimeMS);
        executorService.scheduleAtFixedRate(this, runnableScheduleTimeMS, runnableScheduleTimeMS, TimeUnit.MILLISECONDS);

        logger.debug("TimerEventsExecutor: start---");
    }


    public void shutdown() {
        logger.debug("TimerEventsExecutor: shutdown+++");

        int shutdownWaitMS;
        String shutdownWaitTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.SHUTDOWN_WAIT_TIME_MS);

        if (shutdownWaitTime.isEmpty())
        {
            shutdownWaitMS = DEFAULT_SHUTDOWN_WAIT_TIME_MS;
        }
        else
        {
            shutdownWaitMS = (int) new GigskyDuration(shutdownWaitTime).convertTimeInMilliSeconds();
        }

        try {
            if (executorService != null) {
                logger.debug("TimerEventsExecutor:executorService.shutdown ++");
                executorService.shutdown();
                logger.debug("TimerEventsExecutor:executorService.shutdown --");

                while (!executorService.isTerminated()) {
                    logger.debug("TimerEventsExecutor:executorService is Terminated flag is false waiting for " +
                            shutdownWaitMS + " millisecs");
                    executorService.awaitTermination(shutdownWaitMS, TimeUnit.MILLISECONDS);
                }
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with execption ", ex);
        }
        logger.debug("TimerEventsExecutor:awaitTermination--");
        executorService = null;

        logger.debug("TimerEventsExecutor: shutdown---");
    }

    @Override
    public void run() {

        try {
            runInternal();
        } catch (Exception e) {
            logger.error("Exception:" , e);
        }
    }

    private void runInternal() {
        logger.debug("runInternal+++");

        //Check if the events have to be processed or not
        String processEvents = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.PROCESS_EVENTS);

        if ((!processEvents.isEmpty()) && (processEvents.equals("YES")))
        {
            eventService.processEvents();
        }

        logger.debug("runInternal---");
    }
}
