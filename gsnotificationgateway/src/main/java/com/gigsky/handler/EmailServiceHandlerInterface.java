package com.gigsky.handler;

import com.gigsky.event.BaseEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;

/**
 * Created by anant on 28/10/15.
 */
public interface EmailServiceHandlerInterface {

    public GSResult handleService(int pendingEmailId);
}
