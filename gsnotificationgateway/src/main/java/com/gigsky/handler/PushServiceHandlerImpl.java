package com.gigsky.handler;

import com.gigsky.common.Configurations;
import com.gigsky.common.GSResponseKeysFactory;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.CustomerEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.message.PushNotificationMessage;
import com.gigsky.notification.BaseNotification;
import com.gigsky.service.EventService;
import com.urbanairship.api.client.APIClient;
import com.urbanairship.api.client.APIError;
import com.urbanairship.api.client.APIErrorDetails;
import com.urbanairship.api.client.APIRequestException;
import com.urbanairship.api.client.model.APIClientResponse;
import com.urbanairship.api.client.model.APIPushResponse;
import com.urbanairship.api.push.model.*;
import com.urbanairship.api.push.model.audience.Selectors;
import com.urbanairship.api.push.model.notification.Notification;
import com.urbanairship.api.push.model.notification.Notifications;
import com.urbanairship.api.push.model.notification.android.AndroidDevicePayload;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

/**
 * Created by prsubbareddy on 12/11/15.
 */
public class PushServiceHandlerImpl implements PushServiceHandlerInterface {

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    EventDao eventDao;

    @Autowired
    EventService eventService;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    PushNotificationDao pushNotificationDao;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TenantConfigurationKeyValuesDao tenantConfigurationKeyValuesDao;


    private static final Logger logger = LoggerFactory.getLogger(PushServiceHandlerImpl.class);

    private String appKey;
    private String appSecret;
    //private String userId;
    private String location;
    private String simId;
    private String DEFAULT_UA_BASE_URL = "https://go.urbanairship.com/api";
    private int expirySeconds;
    private static int DEFAULT_EXPIRY_S = 2 * 60 * 60 * 1000; //2 hour interval

    @Override
    public GSResult handleService(int pushNotificationId, NotificationMessage message, BaseEvent event, BaseNotification notification, DeviceDBBean device) {

        String pushId = null;

        // Should fetch from tenant specific configuration key values
        appKey = tenantConfigurationKeyValuesDao.getConfigurationValue(Configurations.TenantConfigurationKeyValueType.UA_APP_KEY, event.getTenantId());
        appSecret = tenantConfigurationKeyValuesDao.getConfigurationValue(Configurations.TenantConfigurationKeyValueType.UA_APP_SECRET, event.getTenantId());

        //Dev Mode
//        appKey = Configurations.UrbanAirshipConfigurationKeys.DEV_APP_KEY;
//        appSecret = Configurations.UrbanAirshipConfigurationKeys.DEV_APP_SECRET;

        //Get the expiry for the notification in seconds
        String expiryMS = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.NOTIFICATION_EXPIRY);


        if ((expiryMS != null) && (expiryMS.isEmpty()))
        {
            expirySeconds = DEFAULT_EXPIRY_S;
        }
        else
        {
            expirySeconds = (int) GigskyDuration.convertDurationToSeconds(new GigskyDuration(expiryMS));
        }

        if(Configurations.ANDROID_TYPE.equals(device.getDeviceType()))
        {
            // implementation for android
            pushId = sendPushAndroid(pushNotificationId, message, notification, device, event);
        }
        else if(Configurations.IOS_TYPE.equals(device.getDeviceType()))
        {
            // implementation for ios
            pushId = sendPushIOS(pushNotificationId, message, notification, device, event);
        }
        logger.debug("handle Service for push notificationId-"+pushNotificationId+",and push Id is :"+ pushId);


        GSResult result = new GSResult();
        if(pushId != null)
        {
            result.put(GSResponseKeysFactory.pushNotificationKeys.PUSH_NOTIFICATION_ID, pushNotificationId);
            result.put(GSResponseKeysFactory.pushNotificationKeys.NOTIFICATION_ID, notification.getNotificationId());
            result.put(GSResponseKeysFactory.pushNotificationKeys.DEVICE_ID, device.getId());
            result.put(GSResponseKeysFactory.pushNotificationKeys.PUSH_ID, pushId);
            return  result;
        }
        return null;
    }

    public String sendPushAndroid(int pushNotificationId, NotificationMessage message, BaseNotification baseNotification, DeviceDBBean deviceDBBean, BaseEvent event){

        logger.debug(String.format("Make sure key and secret are set Key:%s Secret:%s",
                appKey, appSecret));

        APIClient apiClient = APIClient.newBuilder()
                .setKey(appKey)
                .setSecret(appSecret)
                .build();

        JSONObject data = ((CustomerEvent)event).getEventData();
        if(data != null) {
            //userId = data.optString("userId");
            location = data.optString("location");
            simId = data.optString("iccId");
        }

        String category = getCategoryForPayload(event);

        AndroidDevicePayload androidPayload = AndroidDevicePayload.newBuilder()
                .setAlert(((PushNotificationMessage) message).getNotificationMessage())
                .addExtraEntry("title", ((PushNotificationMessage) message).getNotificationTitle())
                //.addExtraEntry("userId", userId)
                .addExtraEntry("simId", simId)
                .addExtraEntry("location", location)
                .addExtraEntry("category", category)
                .build();

        //Expiry
        PushExpiry pushExpiry = PushExpiry.newBuilder()
                .setExpirySeconds(expirySeconds)
                .build();

        PushOptions pushOptions = PushOptions.newBuilder()
                .setExpiry(pushExpiry)
                .build();

        Notification notification = Notifications.notification(androidPayload);

        PushPayload payload = PushPayload.newBuilder()
                .setAudience(Selectors.androidChannel(deviceDBBean.getChannelId()))
                .setNotification(notification)
                .setPushOptions(pushOptions)
                .setDeviceTypes(DeviceTypeData.of(DeviceType.ANDROID))
                .build();
        try {
            APIClientResponse<APIPushResponse> response = apiClient.push(payload);
            List<String> pushIDs = response.getApiResponse().getPushIds().get();    // List of Push IDs

            logger.debug("Android PUSH SUCCEEDED");

            EventDBBean eventDBBean = eventDao.getEventDetails(event.getEventId(), event.getTenantId());
            String customerUuid = eventService.getCustomerUuid(eventDBBean);

            logger.info("UA Push succeeded with push_notification_id " + pushNotificationId +
                                " for customer_id " + customerUuid +
                                "  for event_id " + event.getEventId() +
                                "  for event_type_id " + eventDBBean.getEventTypeId() +
                                " to device_id " + deviceDBBean.getId() +
                                " to device_type " + deviceDBBean.getDeviceType() +
                                " to android_channel " + deviceDBBean.getChannelId());

            if (pushIDs.size() > 0)
            {
                logger.debug(String.format("Android PUSH response http pushids:%s", pushIDs.get(0)));
                return pushIDs.get(0);
            }
        }
        catch (APIRequestException ex){

            handleEventForFailedCase(pushNotificationId, baseNotification, deviceDBBean, event.getTenantId());
            logger.error(String.format("APIRequestException " + ex));

            APIError apiError = ex.getError().get();
            logger.error("Error " + apiError.getError());
            if (apiError.getDetails().isPresent())     {
                APIErrorDetails apiErrorDetails = apiError.getDetails().get();
                logger.error("Error details " + apiErrorDetails.getError());
            }
        }
        catch (IOException e){
            logger.error("IOException in API request " + e.getMessage());
        }
        return null;
    }

    public String sendPushIOS(int pushNotificationId, NotificationMessage message, BaseNotification baseNotification, DeviceDBBean deviceDBBean, BaseEvent event){

        logger.debug(String.format("Make sure key and secret are set Key:%s Secret:%s",
                appKey, appSecret));

        try
        {
            StringBuilder url = new StringBuilder(configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.UA_BASE_URL, DEFAULT_UA_BASE_URL));
            url.append("/push");
            logger.debug("url : " + url);

            JSONObject data = ((CustomerEvent)event).getEventData();
            if(data != null) {
                //userId = data.optString("userId");
                location = data.optString("location");
                simId = data.optString("iccId");
            }

            JSONObject extra = new JSONObject();
            //extra.put("userId", userId);
            extra.put("location", location);
            extra.put("simId", simId);

            String category = getCategoryForPayload(event);

            //Audience
            JSONObject audience = new JSONObject();
            audience.put("ios_channel", deviceDBBean.getChannelId());

            //iOS
            JSONObject iOS = new JSONObject();
            iOS.put("title", ((PushNotificationMessage) message).getNotificationTitle());
            iOS.put("extra", extra);
            iOS.put("category", category);

            //Notification
            JSONObject notification = new JSONObject();
            notification.put("alert", ((PushNotificationMessage) message).getNotificationMessage());
            notification.put("ios", iOS);

            //Device types
            JSONArray device_types = new JSONArray();
            device_types.put("ios");

            //Audience
            JSONObject options = new JSONObject();
            options.put("expiry", expirySeconds);

            JSONObject request = new JSONObject();
            request.put("audience", audience);
            request.put("device_types", device_types);
            request.put("notification", notification);
            request.put("options", options);

            String uaToken = tenantConfigurationKeyValuesDao.getConfigurationValue(Configurations.TenantConfigurationKeyValueType.UA_AUTHENTICATION, event.getTenantId());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Accept","application/vnd.urbanairship+json; version=3");
            String authHeader = "Basic " + uaToken;
            headers.set("Authorization", authHeader);

            HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(url.toString(), HttpMethod.POST, entity, String.class);
            String responseBody = response.getBody();
            JSONObject responseJson = new JSONObject(responseBody);

            logger.debug("IOS PUSH SUCCEEDED");

            EventDBBean eventDBBean = eventDao.getEventDetails(event.getEventId(), event.getTenantId());
            String customerUuid = eventService.getCustomerUuid(eventDBBean);

            logger.info("UA Push succeeded with push_notification_id " + pushNotificationId +
                    " for customer_id " + customerUuid +
                    "  for event_id " + event.getEventId() +
                    "  for event_type_id " + eventDBBean.getEventTypeId() +
                    " to device_id " + deviceDBBean.getId() +
                    " to device_type " + deviceDBBean.getDeviceType() +
                    " to ios_channel " + deviceDBBean.getChannelId());

            // List of Push IDs
            JSONArray pushIDs = (JSONArray) responseJson.get("push_ids");

            if (pushIDs.length() > 0)
            {
                logger.debug(String.format("IOS PUSH response http pushids:%s", pushIDs.get(0)));
                return pushIDs.get(0).toString();
            }
        }
        catch (Exception ex)
        {
            handleEventForFailedCase(pushNotificationId, baseNotification, deviceDBBean, event.getTenantId());
            logger.error(String.format("APIRequestException " + ex.getMessage()));
        }

        return null;
    }

    // If Push notification sending fails for any event,
    // removing notification and pushNotification entry from db.
    // event status updating as New and failed count will increase.
    private void handleEventForFailedCase(int pushNotificationId, BaseNotification baseNotification, DeviceDBBean deviceDBBean, int tenantId)
    {
        int maxEventFailedCount = 3;

        String count = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EVENT_FAIL_COUNT);
        if(count != null)
        {
            maxEventFailedCount =  Integer.parseInt(count);
        }

        EventDBBean eventDBBean = eventDao.getEventDetails(baseNotification.getEventId(), tenantId);
        int eventFailedCount = eventDBBean.getFailCount();
        if(eventFailedCount == maxEventFailedCount)
        {
            // update Event DB status with FAILED
            eventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_AFTER_RETRIES);
        } else
        {
            // update Event DB status with IN_PROCESS
            eventDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
            eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_PUSH_PROCESS);
            eventDBBean.setFailCount(eventFailedCount+1);
        }
        eventDao.updateEventDetails(eventDBBean);

        // updating status failed for notification DB and pushNotification DB.
        NotificationDBBean notificationDBBean = notificationDao.getNotificationDetails(baseNotification.getNotificationId());
        notificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
        notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_PUSH_PROCESS);
        notificationDao.updateNotification(notificationDBBean);

        PushNotificationDBBean pushNotificationDBBean = pushNotificationDao.getPushNotificationDetails(pushNotificationId);
        pushNotificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
        pushNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_PUSH_PROCESS);
        pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_FAILED);
        pushNotificationDao.updatePushNotification(pushNotificationDBBean);

        callPushedDevice(pushNotificationId, deviceDBBean.getId());
    }

    // Updating pushNotificationId and deviceId in PushedDevice DB
    private void callPushedDevice(int pushNotificationId, int deviceId)
    {
        PushedDeviceDBBean pushedDeviceDBBean = pushNotificationDao.getPushedDevice(pushNotificationId);
        if(pushedDeviceDBBean == null)
        {
            pushNotificationDao.createPushedDevice(pushNotificationId, deviceId);
            logger.debug("Pushed device table created for pushNotificationId-"+pushNotificationId);
        }
    }

    private String getCategoryForPayload(BaseEvent event) {

        String eventType = event.getEventType();

        if (Configurations.EventType.TYPE_LOW_BALANCE.equals(eventType)) {
            return Configurations.UrbanAirshipCategories.DATA_PLAN_REMAINING;

        } else if (Configurations.EventType.TYPE_DATA_USED.equals(eventType)) {
            return Configurations.UrbanAirshipCategories.DATA_PLAN_USED;

        } else if (Configurations.EventType.TYPE_SUBSCRIPTION_NEARING_EXPIRY.equals(eventType)) {
            return Configurations.UrbanAirshipCategories.DATA_PLAN_EXPIRING_IN_HOURS;

        } else if (Configurations.EventType.TYPE_SUBSCRIPTION_EXPIRED.equals(eventType)) {
            return Configurations.UrbanAirshipCategories.DATA_PLAN_EXPIRED;

        } else if (Configurations.EventType.TYPE_NEW_LOCATION.equals(eventType)) {
            return Configurations.UrbanAirshipCategories.DATA_PLAN_AVAILABLE;
        }

        return null;
    }
}
