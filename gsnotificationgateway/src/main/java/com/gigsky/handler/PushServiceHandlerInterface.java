package com.gigsky.handler;

import com.gigsky.database.bean.DeviceDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;

/**
 * Created by anant on 28/10/15.
 */
public interface PushServiceHandlerInterface {

    public GSResult handleService(int pushNotificationId, NotificationMessage message, BaseEvent event, BaseNotification notification, DeviceDBBean device);
}
