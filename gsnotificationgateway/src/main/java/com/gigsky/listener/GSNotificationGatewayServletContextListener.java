package com.gigsky.listener;

import com.gigsky.executor.GatewayUtilityExecutor;
import com.gigsky.executor.TimerEventsExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.ExecutionException;

public class GSNotificationGatewayServletContextListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(GSNotificationGatewayServletContextListener.class);

	@Autowired
	private TimerEventsExecutor timerEventsExecutor;

	@Autowired
	private GatewayUtilityExecutor gatewayUtilityExecutor;

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		logger.debug("initializing context ...");
		WebApplicationContextUtils
				.getRequiredWebApplicationContext(servletContextEvent.getServletContext())
				.getAutowireCapableBeanFactory()
				.autowireBean(this);

		try {
			startExecutingEvents();
			startExecutingPushNotifications();
		} catch (ExecutionException e) {
			logger.error("contextInitialized error:"+e);
		} catch (InterruptedException e) {
			logger.error("contextInitialized error:"+e);
		}

		logger.debug("context initialized");
	}


	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		logger.debug("context contextDestroyed++");

		stopExecutingEvents();
		stopExecutingPushNotifications();

		logger.debug("context contextDestroyed--");
	}


	//Start the processing of events periodically
	private void startExecutingEvents() throws ExecutionException, InterruptedException {
		try {
			timerEventsExecutor.start();
		} catch (ExecutionException e) {
			logger.error("startExecutingEvents error:"+e);
		} catch (InterruptedException e) {
			logger.error("startExecutingEvents error:"+e);
		}
	}

	//Stopping the processing of events
	private void stopExecutingEvents() {
		timerEventsExecutor.shutdown();
	}


	// start execution of push notifications process to update entries
	private void startExecutingPushNotifications() throws ExecutionException, InterruptedException {
		try
		{
			gatewayUtilityExecutor.start();
		} catch (ExecutionException e) {
			logger.error("startExecutingPushNotifications error:"+e);
		} catch (InterruptedException e) {
			logger.error("startExecutingPushNotifications error:"+e);
		}
	}


	// stop execution of push notifications
	private void stopExecutingPushNotifications() {
		gatewayUtilityExecutor.shutdown();
	}


}
