package com.gigsky.notification;


import java.util.Date;

/**
 * Created by anant on 28/10/15.
 */
public class PushNotification extends BaseNotification {

    public static final String CATEGORY_GROUP = "GROUP";
    public static final String CATEGORY_SINGLE = "SINGLE";

    protected String pushId;
    protected int openCount;
    protected Date pushTime;
    protected String iccId;
    protected String location;
    protected int sendCount;
    protected String locale;
    protected String status;
    protected String lastAttemptStatus;
    protected Date lastAttemptTime;
    protected int attemptCount;
    protected String statusMessage;
    protected int templateId;
    protected String category;

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public int getOpenCount() {
        return openCount;
    }

    public void setOpenCount(int openCount) {
        this.openCount = openCount;
    }

    public Date getPushTime() {
        return pushTime;
    }

    public void setPushTime(Date pushTime) {
        this.pushTime = pushTime;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getSendCount() {
        return sendCount;
    }

    public void setSendCount(int sendCount) {
        this.sendCount = sendCount;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastAttemptStatus() {
        return lastAttemptStatus;
    }

    public void setLastAttemptStatus(String lastAttemptStatus) {
        this.lastAttemptStatus = lastAttemptStatus;
    }

    public Date getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(Date lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public int getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(int attemptCount) {
        this.attemptCount = attemptCount;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
