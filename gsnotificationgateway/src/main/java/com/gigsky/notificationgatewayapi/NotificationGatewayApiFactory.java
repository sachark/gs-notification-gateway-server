package com.gigsky.notificationgatewayapi;

public interface NotificationGatewayApiFactory {
	public NotificationGatewayInterface getNotificationGatewayInterface(String identifier);
}
