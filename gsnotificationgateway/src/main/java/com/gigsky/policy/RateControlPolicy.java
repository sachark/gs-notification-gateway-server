package com.gigsky.policy;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.EventDBBean;
import com.gigsky.database.bean.EventTypeDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.EventDao;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

/**
 * Created by vinayr on 26/12/15.
 */
public class RateControlPolicy extends BasePolicy {

    private static final Logger logger = LoggerFactory.getLogger(RateControlPolicy.class);

    private static final long NEW_LOCATION_RATE_MS = 60 * 60 * 1000; //1 hour interval
    private static final long DATA_USED_RATE_MS = 30 * 60 * 1000; //30 minute interval
    private static final long LOW_BALANCE_RATE_MS = 30 * 60 * 1000; //30 minute interval
    private static final long SUBSCRIPTION_NEARING_EXPIRY_RATE_MS = 30 * 60 * 1000; //30 minute interval
    private static final long SUBSCRIPTION_EXPIRED_RATE_MS = 30 * 60 * 1000; //30 minute interval

    private static final int MAX_EVENT_COUNT = 10;

    @Autowired
    EventDao eventDao;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    public boolean needsRateControl(EventDBBean newEvent)
    {
        boolean needsRateControl = false;

        needsRateControl = isEventToBeRateControlled(newEvent);

        return needsRateControl;
    }


    private boolean isEventToBeRateControlled(EventDBBean newEvent)
    {
        String rateControlMS = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.RATE_CONTROL_TIME_MS);
        long rateControlTimeMS;

        if ((rateControlMS != null) && (rateControlMS.isEmpty()))
        {
            rateControlTimeMS = NEW_LOCATION_RATE_MS;
        }

        else
        {
            rateControlTimeMS = new GigskyDuration(rateControlMS).convertTimeInMilliSeconds();
        }

        try
        {
            String data = newEvent.getCustomerEvent().getData();
            JSONObject jsonData = new JSONObject(data);
            String location = jsonData.getString("location");
            String iccid = jsonData.getString("iccId");

            EventTypeDBBean eventTypeDBBean = eventDao.getEventType(newEvent.getEventTypeId());

            //Get the events with same ICCID and same type
            List<EventDBBean> eventList = eventDao.getEventsForEventTypeAndIccid(eventTypeDBBean.getId(), iccid, MAX_EVENT_COUNT);

            for (Iterator<EventDBBean> it = eventList.iterator(); it.hasNext(); ) {

                EventDBBean eventDBBean = it.next();

                if (Configurations.StatusType.STATUS_NEW.equals(eventDBBean.getStatus())) {
                    //Skip the events which are marked as NEW
                    continue;
                }

                //Get the events from the DB for the same iccid and Location
                String tempEventData = eventDBBean.getCustomerEvent().getData();

                JSONObject tempJsonData = new JSONObject(tempEventData);

                String tempLocation = tempJsonData.getString("location");
                //String tempIccid = tempJsonData.getString("iccId");

                //If it is a Location event then irrespective of location, check for same iccid and rate limit
                if (Configurations.EventType.TYPE_NEW_LOCATION.equals(eventTypeDBBean.getType()))
                {
                    //Get current timestamp in UTC
                    Timestamp currentTimestamp = CommonUtils.getCurrentTimestampInUTC();
                    if((currentTimestamp.getTime() - eventDBBean.getReceivedTime().getTime()) <  rateControlTimeMS)
                    {
                        return true;
                    }
                }

                //Check if it belongs to the same location
                else if (location.equals(tempLocation))
                {
                    //Get current timestamp in UTC
                    Timestamp currentTimestamp = CommonUtils.getCurrentTimestampInUTC();
                    if((currentTimestamp.getTime() - eventDBBean.getReceivedTime().getTime()) <  rateControlTimeMS)
                    {
                        return true;
                    }
                }
            }
        }
        catch (JSONException e)
        {
            logger.warn("json object data creation  Exception:" + e + " for Event Id: " + newEvent.getId());
        }

        return false;
    }
}

