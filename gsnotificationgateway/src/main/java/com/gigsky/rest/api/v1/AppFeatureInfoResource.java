package com.gigsky.rest.api.v1;

import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.AppFeatureInfoDetails;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.AppFeatureInfoService;
import com.gigsky.service.ValidationService;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import util.GSUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by prsubbareddy on 07/10/16.
 */
@Path("/api/v1/appFeatureInfo")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class AppFeatureInfoResource {

    private static final Logger logger = LoggerFactory.getLogger(AppFeatureInfoResource.class);

    @Autowired
    private AppFeatureInfoService appFeatureInfoService;

    @Autowired
    ValidationService validationService;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private ConfigurationKeyValuesDao configurationKeyValuesDao;

    @POST
    public Response addAppFeatureInfo(@QueryParam("clientVersion") String clientVersion,
                                      @QueryParam("clientType") String clientType,
                                      @QueryParam("userId") String userId,
                                      AppFeatureInfoDetails appFeatureInfoDetails, @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException
    {
        // Check tenant id is passed from headers, If available validate and fetch tenant id
        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

        validateAppFeatureInfo(clientVersion, clientType, userId, "ADD", appFeatureInfoDetails);
        AppFeatureInfoDetails featureInfoDetails = appFeatureInfoService.addAppFeatureInfo(clientVersion, clientType, userId, appFeatureInfoDetails, tenantId);
        return Response.status(Response.Status.OK).entity(null).build();
    }

    @PUT
    public Response updateAppFeatureInfo(@QueryParam("clientVersion") String clientVersion,
                                         @QueryParam("clientType") String clientType,
                                         @QueryParam("userId") String userId,
                                         AppFeatureInfoDetails appFeatureInfoDetails, @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException
    {

        // Check tenant id is passed from headers, If available validate and fetch tenant id
        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

        //Validate parameters
        validateAppFeatureInfo(clientVersion, clientType, userId, "UPDATE", appFeatureInfoDetails);

        //Get appfeatureInfo
        AppFeatureInfoDetails featureInfoDetails =
                appFeatureInfoService.updateAppFeatureInfo(clientVersion, clientType, userId, appFeatureInfoDetails, tenantId);
        return Response.status(Response.Status.OK).entity(featureInfoDetails).build();
    }


    @GET
    public Response getAppFeatureInfo(@QueryParam("clientVersion") String clientVersion,
                                      @QueryParam("clientType") String clientType,
                                      @QueryParam("userId") String userId, @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException, JSONException
    {
        // Check tenant id is passed from headers, If available validate and fetch tenant id
        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

        validateAppFeatureInfo(clientVersion, clientType, userId, "GET", null);
        AppFeatureInfoDetails featureInfoDetails = appFeatureInfoService.getAppFeatureInfo(clientVersion, clientType, userId, tenantId);
        return Response.status(Response.Status.OK).entity(featureInfoDetails).build();
    }


    @DELETE
    public Response deleteAppFeatureInfo(@QueryParam("clientVersion") String clientVersion,
                                         @QueryParam("clientType") String clientType,
                                         @QueryParam("userId") String userId, @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException
    {
        // Check tenant id is passed from headers, If available validate and fetch tenant id
        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

        validateAppFeatureInfo(clientVersion, clientType, userId, "DEL", null);
        appFeatureInfoService.deleteAppFeatureInfo(clientVersion, clientType, userId, tenantId);
        return Response.status(Response.Status.OK).entity(null).build();
    }


    public void validateAppFeatureInfo(String clientVersion, String clientType, String userId, String operation, AppFeatureInfoDetails appFeatureInfoDetails) throws ResourceValidationException
    {
        if ((clientVersion == null) || (clientVersion != null && clientVersion.isEmpty()))
        {
            logger.error("App Feature Info Client Version is invalid");
            throw new ResourceValidationException(ErrorCode.APP_FEATURE_INFO_ENTRY_REQUIRES_VALID_CLIENT_VERSION, "App Feature Info Client Version is invalid");
        }

        if ((clientType == null) || (clientType != null && clientType.isEmpty()) || (!isValidClientType(clientType, operation)))
        {
            logger.error("App Feature Info Client Type is invalid");
            throw new ResourceValidationException(ErrorCode.APP_FEATURE_INFO_ENTRY_REQUIRES_VALID_CLIENT_TYPE, "App Feature Info Client Type is invalid");
        }

        if ((userId == null) || (userId != null && userId.isEmpty()))
        {
            logger.error("App Feature Info user Id is invalid");
            throw new ResourceValidationException(ErrorCode.APP_FEATURE_INFO_ENTRY_REQUIRES_VALID_USER_ID, "App Feature Info User Id is invalid");
        }

        if (operation.equals("ADD"))
        {
            if (appFeatureInfoDetails == null)
            {
                logger.error("App Feature Info post body is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "Empty post body");
            }

            if ((appFeatureInfoDetails.getType() == null) ||
               ((appFeatureInfoDetails.getType() != null) && (!appFeatureInfoDetails.getType().equals("AppFeatureInfo"))))
            {
                logger.error("App Feature Info type is invalid");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info type is invalid");
            }

            if ((appFeatureInfoDetails.getFeaturesSupported() == null) ||
               ((appFeatureInfoDetails.getFeaturesSupported() != null) && (appFeatureInfoDetails.getFeaturesSupported().length == 0)))
            {
                logger.error("App Feature Info feature supported is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info feature supported is empty");
            }

            //If UserId is ALL then client version and client type cannot be ALL
            if ((userId.equals("ALL")) && ((clientType.equals("ALL")) || (clientVersion.equals("ALL"))))
            {
                logger.error("App Feature Info value cannot be ALL for client type and client version");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info value cannot be ALL for client type and client version");
            }

            //BaseUrl is empty
            if(StringUtils.isEmpty(appFeatureInfoDetails.getBaseUrl()) ||
                    (appFeatureInfoDetails.getBaseUrl() != null && appFeatureInfoDetails.getBaseUrl().length() == 0))
            {
                logger.error("App Feature Info base url is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info base url is empty");
            }

            //LogoBaseUrl is empty
            if(StringUtils.isEmpty(appFeatureInfoDetails.getLogoBaseUrl()) ||
                    (appFeatureInfoDetails.getLogoBaseUrl() != null &&
                            appFeatureInfoDetails.getLogoBaseUrl().length() == 0))
            {
                logger.error("App Feature Info logo base url is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info logo base url is empty");
            }

            //TroubleshootingBaseUrl is empty
            if(StringUtils.isEmpty(appFeatureInfoDetails.getTroubleshootingBaseUrl()) ||
                    (appFeatureInfoDetails.getTroubleshootingBaseUrl() != null &&
                            appFeatureInfoDetails.getTroubleshootingBaseUrl().length() == 0))
            {
                logger.error("App Feature Info troubleshooting base url is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info troubleshooting base url is empty");
            }

            //UpdateMandatory is empty
            if(StringUtils.isEmpty(appFeatureInfoDetails.getUpdateMandatory()))
            {
                logger.error("App Feature Info update mandatory is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info update mandatory is empty");
            }
            else
            {

                //Invalid value for updateMandatory
                if(!("true".equals(appFeatureInfoDetails.getUpdateMandatory())
                        || "false".equals(appFeatureInfoDetails.getUpdateMandatory())))
                {
                    logger.error("App Feature Info update mandatory is invalid");
                    throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info update mandatory is invalid");
                }
            }
        }

        if (operation.equals("UPDATE"))
        {
            //Validate type field
            if ((appFeatureInfoDetails.getType() == null) ||
                    ((appFeatureInfoDetails.getType() != null) &&
                            (!appFeatureInfoDetails.getType().equals("AppFeatureInfo"))))
            {
                logger.error("App Feature Info type is invalid");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info type is invalid");
            }

            if((appFeatureInfoDetails.getFeaturesSupported() != null) &&
                    (appFeatureInfoDetails.getFeaturesSupported().length == 0))
            {
                logger.error("App Feature Info feature supported is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info feature supported is empty");
            }

            if((appFeatureInfoDetails.getBaseUrl() != null) &&
                    (appFeatureInfoDetails.getBaseUrl().length() == 0))
            {
                logger.error("App Feature Info baseUrl is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info baseUrl is empty");
            }

            if((appFeatureInfoDetails.getLogoBaseUrl() != null) &&
                    (appFeatureInfoDetails.getLogoBaseUrl().length() == 0))
            {
                logger.error("App Feature Info logo baseurl is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info logo baseurl is empty");
            }

            if((appFeatureInfoDetails.getTroubleshootingBaseUrl() != null) &&
                    (appFeatureInfoDetails.getTroubleshootingBaseUrl().length() == 0))
            {
                logger.error("App Feature Info troubleshooting baseurl is empty");
                throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info troubleshooting baseurl is empty");
            }

            //Validate updateMandatory field
            if(appFeatureInfoDetails.getUpdateMandatory() != null)
            {
                //Invalid value for updateMandatory
                if(!("true".equals(appFeatureInfoDetails.getUpdateMandatory())
                        || "false".equals(appFeatureInfoDetails.getUpdateMandatory())))
                {
                    logger.error("App Feature Info update mandatory is invalid");
                    throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "App Feature Info update mandatory is invalid");
                }
            }
        }
    }

    private boolean isValidClientType(String clientType, String operation)
    {
        if (clientType != null)
        {
            //Currently supporting only these clients
            if(clientType.equals(Configurations.IOS_TYPE) ||
               clientType.equals(Configurations.ANDROID_TYPE) ||
               clientType.equals(Configurations.WEB_TYPE) ||
               clientType.equals(Configurations.CAYMAN_TYPE) ||
               clientType.equals("ALL"))
            {
                //GET API does not support ALL as client type
                if (operation.equals("GET") && clientType.equals("ALL"))
                    return false;

                return true;
            }
        }

        return false;
    }

}
