package com.gigsky.rest.api.v1;

import com.gigsky.backend.Plan.PlanManagementInterface;
import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.backend.beans.*;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerContextDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.PlanPurchase;
import com.gigsky.rest.bean.SIMInfo;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.CustomerContextService;
import com.gigsky.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import util.GSCountries;
import util.GSUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by prsubbareddy on 07/11/17.
 */

@Path("/api/v1")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class CustomerContextResource {

    private static final Logger logger = LoggerFactory.getLogger(CustomerContextResource.class);

    @Autowired
    ValidationService validationService;

    @Autowired
    AuthManagementInterface authManagementInterface;

    @Autowired
    CustomerContextService customerContextService;

    @Autowired
    PlanManagementInterface planManagementInterface;

    @Autowired
    Configurations configurations;

    @Autowired
    TenantDao tenantDao;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @PUT
    @Path("/defaultSim")
    public Response setDefaultSIM(SIMInfo simInfo, @Context HttpHeaders httpHeaders) throws ServiceValidationException, BackendRequestException, com.gigsky.auth.BackendRequestException, ResourceValidationException{

        // Check plan purchase mode
        if(configurations.isPlanPurchaseMode()) {
            String token = GSUtil.extractToken(httpHeaders, validationService);

            int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

            //Check if valid token is valid or not
            boolean isValidToken = authManagementInterface.validateCustomerToken(token, tenantId);

            //If invalid token, throw exception
            if (!isValidToken) {

                logger.error("Invalid Token");
                throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
            }

            // validate sim info details
            validateSIMDetails(simInfo);

            // update entry based on gs customer with sim details
            customerContextService.setDefaultSIM(simInfo);
            return Response.status(Response.Status.OK).entity(null).build();
        } else {
            logger.info("API is not supporting currently");
            throw new ResourceValidationException(ErrorCode.API_NOT_SUPPORTED);
        }
    }

    @GET
    @Path("/getBalance")
    public Response getBalance(@QueryParam("countryCode") String countryCode, @Context HttpHeaders httpHeaders) throws ServiceValidationException, BackendRequestException, ResourceValidationException {

        // Check plan purchase mode
        if(configurations.isPlanPurchaseMode()) {
            String token = GSUtil.extractBase64Token(httpHeaders, validationService);

            int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

            // Fetch login response using base 64 token
            LoginResponse loginResponse = authManagementInterface.loginUsingToken(token, tenantId);

            // Update customer context table and return latest data
            CustomerContextDBBean customerContextDBBean = customerContextService.updatedCustomerDetails(loginResponse);

            if (customerContextDBBean.getIccid() == null) {
                logger.error("No SIM ID");
                throw new ResourceValidationException(ErrorCode.NO_SIM_ID);
            }

            // Make a cal for subscription using customer data
            Subscription subscription = planManagementInterface.getSubscriptionDetails(countryCode, customerContextDBBean);
            return Response.status(Response.Status.OK).entity(subscription).build();
        } else {
            logger.info("API is not supporting currently");
            throw new ResourceValidationException(ErrorCode.API_NOT_SUPPORTED);
        }
    }


    @GET
    @Path("/getPlans")
    public Response getPlans(@QueryParam("countryCode") String countryCode, @QueryParam("currentLocation") Boolean currentLocation, @Context HttpHeaders httpHeaders) throws ServiceValidationException, BackendRequestException, ResourceValidationException {

        // Check plan purchase mode
        if(configurations.isPlanPurchaseMode()) {
            String token = GSUtil.extractBase64Token(httpHeaders, validationService);

            int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

            // Fetch login response using base 64 token
            LoginResponse loginResponse = authManagementInterface.loginUsingToken(token, tenantId);

            // Update customer context table and return latest data
            CustomerContextDBBean customerContextDBBean = customerContextService.updatedCustomerDetails(loginResponse);

            if (customerContextDBBean.getIccid() == null) {
                logger.error("No SIM ID");
                throw new ResourceValidationException(ErrorCode.NO_SIM_ID);
            }

            // validation for SIM type
            if(!"GS_SIM_50".equals(customerContextDBBean.getSimType()) && !"GIGSKY_SIM".equals(customerContextDBBean.getSimType())) {
                throw new ResourceValidationException(ErrorCode.SIM_TYPE_NOT_SUPPORTED);
            }

            validateCurrentLocation(currentLocation);

            NetworkGroupPlans networkGroupPlans = customerContextService.getPlans(customerContextDBBean, currentLocation, countryCode);
            return Response.status(Response.Status.OK).entity(networkGroupPlans).build();
        } else {
            logger.info("API is not supporting currently");
            throw new ResourceValidationException(ErrorCode.API_NOT_SUPPORTED);
        }
    }

    @POST
    @Path("/purchasePlan")
    public Response purchasePlan(PlanPurchase planInfo, @Context HttpHeaders httpHeaders) throws ServiceValidationException, BackendRequestException, ResourceValidationException {

        // Check plan purchase mode
        if(configurations.isPlanPurchaseMode())
        {
            String token =  GSUtil.extractBase64Token(httpHeaders, validationService);

            int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

            // Fetch login response using base 64 token
            LoginResponse loginResponse = authManagementInterface.loginUsingToken(token, tenantId);

            // Update customer context table and return latest data
            CustomerContextDBBean customerContextDBBean = customerContextService.updatedCustomerDetails(loginResponse);

            if(customerContextDBBean.getIccid() == null){
                logger.error("No SIM ID");
                throw new ResourceValidationException(ErrorCode.NO_SIM_ID);
            }

            // validate Plan Details
            validatePlanDetails(planInfo);

            // validation for SIM type
            if(!"GS_SIM_50".equals(customerContextDBBean.getSimType()) && !"GIGSKY_SIM".equals(customerContextDBBean.getSimType())) {
                throw new ResourceValidationException(ErrorCode.SIM_TYPE_NOT_SUPPORTED);
            }

            // Make a cal for purchase
            customerContextService.makePurchasePlan(customerContextDBBean, planInfo);
            return Response.status(Response.Status.OK).entity(null).build();
        } else {
            logger.info("API is not supporting currently");
            throw new ResourceValidationException(ErrorCode.API_NOT_SUPPORTED);
        }

    }

    private void validateSIMDetails(SIMInfo simInfo) throws ResourceValidationException{

        if(simInfo.getType() == null || !simInfo.getType().equals("DefaultSIM"))
        {
            logger.error("Invalid type");
            throw new ResourceValidationException(ErrorCode.DEFAULT_SIM_REQUIRES_VALID_TYPE);
        }

        if(simInfo.getGsCustomerId() == null || simInfo.getGsCustomerId() <= 0)
        {
            logger.error("Invalid customer id");
            throw new ResourceValidationException(ErrorCode.DEFAULT_SIM_REQUIRES_VALID_CUSTOMER_ID);
        }

        if(simInfo.getSimNickname() == null  || simInfo.getSimNickname().length() == 0)
        {
            logger.error("Invalid sim nick name");
            throw new ResourceValidationException(ErrorCode.DEFAULT_SIM_REQUIRES_VALID_SIM_NICKNAME);
        }

        if(simInfo.getSimType() == null || simInfo.getSimType().length() == 0)
        {
            logger.error("Invalid sim type ");
            throw new ResourceValidationException(ErrorCode.DEFAULT_SIM_REQUIRES_VALID_SIM_TYPE);
        }

        if(simInfo.getSimId() == null || simInfo.getSimId().length() == 0)
        {
            logger.error("Invalid sim iccid");
            throw new ResourceValidationException(ErrorCode.DEFAULT_SIM_REQUIRES_VALID_SIM_ICCID);
        }
    }

    private void validateCurrentLocation(Boolean currentLocation) throws ResourceValidationException {
        if(currentLocation == null)
        {
            logger.error("Invalid purchase plan current location");
            throw new ResourceValidationException(ErrorCode.GET_PLANS_REQUIRES_VALID_CURRENT_LOCATION);
        }
    }

    private void validatePlanDetails(PlanPurchase planInfo) throws ResourceValidationException {

        if(planInfo.getType() == null || !planInfo.getType().equals("PurchasePlan"))
        {
            logger.error("Invalid type");
            throw new ResourceValidationException(ErrorCode.PURCHASE_PLAN_REQUIRES_VALID_TYPE);
        }

        if(planInfo.getPlanId() == null || planInfo.getPlanId() <= 0)
        {
            logger.error("Invalid purchase plan country");
            throw new ResourceValidationException(ErrorCode.PURCHASE_PLAN_REQUIRES_VALID_PLAN_ID);
        }

        if(planInfo.getCountryCode() == null || planInfo.getCountryCode().length() == 0 || !GSCountries.isCountryCodeMatched(planInfo.getCountryCode()))
        {
            logger.error("Invalid purchase plan country code");
            throw new ResourceValidationException(ErrorCode.PURCHASE_PLAN_REQUIRES_VALID_COUNTRY_CODE);
        }
    }


}
