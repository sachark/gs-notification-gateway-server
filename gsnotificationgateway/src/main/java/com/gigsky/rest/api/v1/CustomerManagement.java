package com.gigsky.rest.api.v1;

import com.gigsky.auth.BackendRequestException;
import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.Customer;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.CustomerManagementService;
import com.gigsky.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import util.GSUtil;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by anant on 29/10/15.
 */

@Path("/api/v1/customers")
@Controller
public class CustomerManagement {

    private final Logger logger = LoggerFactory.getLogger(CustomerManagement.class);

    @Autowired
    private CustomerManagementService customerManagementService;

    @Autowired
    private AuthManagementInterface authManagementInterface;

    @Autowired
    private DeviceResource deviceResource;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private ConfigurationKeyValuesDao configurationKeyValuesDao;


    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response getCustomerInfo (@Context HttpHeaders headers) throws ResourceValidationException, ServiceValidationException, BackendRequestException {
        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = GSUtil.extractTenantId(headers, validationService, tenantDao, configurationKeyValuesDao);
        Customer customerInfo = customerManagementService.getCustomerDetails(token, tenantId);
        return Response.status(Response.Status.OK).entity(customerInfo).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public Response deleteCustomerInfo (@Context HttpHeaders headers, Customer customer) throws ResourceValidationException, ServiceValidationException, com.gigsky.backend.exception.BackendRequestException, com.gigsky.auth.BackendRequestException {

        System.out.println("=============== deleteCustomerInfo ===========");

        String token = GSUtil.extractToken(headers, validationService);

        int tenantId = GSUtil.extractTenantId(headers, validationService, tenantDao, configurationKeyValuesDao);

        //Check if valid token is valid or not
        boolean isValidToken = authManagementInterface.validateCustomerToken(token, tenantId);

        //If invalid token, throw exception
        if (!isValidToken) {

            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.BACKEND_INVALID_TOKEN);
        }


        //validation for customer
        validateCustomer(customer);

        customerManagementService.deleteCustomerDetails(customer, tenantId);

        return Response.status(Response.Status.OK).entity(null).build();
    }


    @Path("/{customerId}/devices")
    public DeviceResource getCommentResource() {
        return deviceResource;
    }

    private void validateCustomer(Customer customer) throws ResourceValidationException, ServiceValidationException {

        if(customer.getType() == null || customer.getType().length() == 0 || !customer.getType().equals("CustomerDetail"))
        {
            logger.error("CustomerResource validateCustomer Invalid type");
            throw new ResourceValidationException(ErrorCode.CUSTOMER_REQUIRES_VALID_TYPE, "InValid type");
        }

        if(StringUtils.isEmpty(customer.getOldEmailId()) || !isEmailValid(customer.getOldEmailId()))
        {
            logger.error("CustomerResource validateCustomer Invalid old email id");
            throw new ResourceValidationException(ErrorCode.CUSTOMER_REQUIRES_VALID_OLD_EMAIL_ID, "InValid old email id");
        }

        if(StringUtils.isEmpty(customer.getAnonymousEmailId()) || !isEmailValid(customer.getAnonymousEmailId()))
        {
            logger.error("CustomerResource validateCustomer Invalid anonymous email id");
            throw new ResourceValidationException(ErrorCode.CUSTOMER_REQUIRES_VALID_ANONYMOUS_EMAIL_ID, "InValid anonymous email id");
        }

    }


    private static boolean isEmailValid(String emailId)
    {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailId);
        return matcher.matches();
    }
}
