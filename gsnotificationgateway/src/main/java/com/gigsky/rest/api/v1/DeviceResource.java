package com.gigsky.rest.api.v1;

import com.gigsky.auth.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.exception.PartnerAppException;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.*;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.GigskyNotificationGatewayServerException;
import com.gigsky.service.CustomerManagementService;
import com.gigsky.service.DeviceManagementService;
import com.gigsky.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import util.GSUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by anant on 18/11/15.
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class DeviceResource {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private CustomerManagementService customerManagementService;

    @Autowired
    private DeviceManagementService deviceManagementService;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private ConfigurationKeyValuesDao configurationKeyValuesDao;

    private final Logger logger = LoggerFactory.getLogger(DeviceResource.class);

    @GET
    @Path("/")
    public Response getDevices(@PathParam("customerId") String customerId,
                               @DefaultValue("0") @QueryParam("startIndex") int startIndex,
                               @DefaultValue("10") @QueryParam("count") int count,
                               @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, BackendRequestException
    {
        validationService.validateStartIndexAndCount(startIndex, count);
        validateCustomerId(customerId, httpHeaders);

        PageInfo inputPageInfo = new PageInfo(startIndex, count);
        PageInfo outputPageInfo = new PageInfo();

        validationService.validatePage(inputPageInfo);

        List<Device> deviceList = deviceManagementService.getDevices(customerId, inputPageInfo, outputPageInfo);
        DeviceList outputDeviceList = new DeviceList();
        outputDeviceList.setType("DeviceDetails");
        outputDeviceList.setCustomerId(customerId);
        outputDeviceList.setStartIndex(outputPageInfo.getStart());
        outputDeviceList.setCount(deviceList.size());
        outputDeviceList.setTotalCount(outputPageInfo.getCount());
        outputDeviceList.setList(deviceList);

        return Response.status(Response.Status.OK).entity(outputDeviceList).build();
    }

    @POST
    @Path("/")
    public Response addDevice(@PathParam("customerId") String customerId,
                              Device device,
                              @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, BackendRequestException, DatabaseException, PartnerAppException {
        validateCustomerId(customerId, httpHeaders);
        validateDevice(device, false);
        Device output = deviceManagementService.addDevice(customerId, device);

        logger.info("Added device_type "+ output.getDeviceType() + "with device_id "+ output.getDeviceId() + " for customer customer_id " + customerId);


        return Response.status(Response.Status.OK).entity(output).build();
    }



    @PUT
    @Path("/{deviceId}")
    public Response updateDevice(@PathParam("customerId") String customerId,
                                 @PathParam("deviceId") int deviceId,
                                 Device device,
                                 @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException, PartnerAppException, BackendRequestException
    {

        validateCustomerId(customerId, httpHeaders);
        validateDevice(device, true);
        Device output = deviceManagementService.updateDevice(customerId, deviceId, device);

        logger.info("Updated device_type "+ output.getDeviceType() + " with device_id "+ deviceId + " for customer customer_id " + customerId);

        return Response.status(Response.Status.OK).entity(output).build();
    }



    @DELETE
    @Path("/{deviceId}")
    public Response deleteDevice(@PathParam("customerId") String customerId,
                                 @PathParam("deviceId") int deviceId,
                                 @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException, BackendRequestException {

        validateCustomerId(customerId, httpHeaders);
        deviceManagementService.deleteDevice(customerId, deviceId);
        return Response.status(Response.Status.OK).entity(null).build();
    }


    @GET
    @Path("/{deviceId}")
    public Response getMessage(@PathParam("customerId") String customerId,
                               @PathParam("deviceId") int deviceId,
                               @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, BackendRequestException
    {
        validateCustomerId(customerId, httpHeaders);
        Device output = deviceManagementService.getDevice(customerId, deviceId);
        return Response.status(Response.Status.OK).entity(output).build();
    }


    private void validateCustomerId(String customerId, HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, BackendRequestException
    {
        validationService.validateRegistrationId(customerId);
        String token = GSUtil.extractToken(httpHeaders, validationService);
        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);
        Customer customerInfo = customerManagementService.getCustomerDetails(token, tenantId);

        if(!customerId.equals(customerInfo.getCustomerId())) {
            logger.error("DeviceResource validateCustomerId INVALID_CUSTOMER_UUID");
            throw new GigskyNotificationGatewayServerException(ErrorCode.INVALID_CUSTOMER_UUID);
        }
    }

    private void validateDevice(Device device, boolean forUpdate) throws ResourceValidationException, ServiceValidationException, PartnerAppException {
        if(device.getType() == null || !(device.getType().equals("DeviceDetail")))
        {
            logger.error("DeviceResource validateDevice Invalid type");
            throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "Invalid type");
        }

        if (device.getDeviceType() == null ||
                (!(Configurations.ANDROID_TYPE.equals(device.getDeviceType()) ||
                        Configurations.IOS_TYPE.equals(device.getDeviceType())))) {
            logger.error("DeviceResource validateDevice Invalid device type");
            throw new ResourceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_DEVICE_TYPE, "Invalid device type");
        }

        validationService.checkStringParam(device.getDeviceOSVersion(),1, Configurations.MAX_OS_VERSION_LENGTH, Configurations.CommonType.OS_VERSION);
        validationService.checkStringParam(device.getAppVersion(), 1, Configurations.MAX_APP_VERSION_LENGTH, Configurations.CommonType.APP_VERSION);
        validationService.checkStringParam(device.getLocale(),1, Configurations.MAX_LOCALE_LENGTH, Configurations.CommonType.LOCALE);
        validationService.checkStringParam(device.getChannelId(), 1, Configurations.MAX_CHANNEL_ID_LENGTH, Configurations.CommonType.CHANNEL_ID);

        if (Configurations.ANDROID_TYPE.equals(device.getDeviceType())) {
            validationService.checkStringParam(device.getInstallationId(), 1, Configurations.MAX_INSTALLATION_ID_LENGTH, Configurations.CommonType.INSTALLATION_ID);

            //Device Token should be empty for Android Device
            if ((device.getDeviceToken() != null) && (!device.getDeviceToken().isEmpty()))
            {
                logger.error("DeviceResource validateDevice Device registration failed");
                throw new ResourceValidationException(ErrorCode.DEVICE_REGISTRATION_FAILED, "Device registration failed");
            }
        }

        if (Configurations.IOS_TYPE.equals(device.getDeviceType())) {
            validationService.checkStringParam(device.getDeviceToken(), 1, Configurations.MAX_DEVICE_TOKEN_LENGTH, Configurations.CommonType.DEVICE_TOKEN);

            //Installation Id should be empty for iOS Device
            if ((device.getInstallationId() != null) && (!device.getInstallationId().isEmpty()))
            {
                logger.error("DeviceResource validateDevice Device registration failed");
                throw new ResourceValidationException(ErrorCode.DEVICE_REGISTRATION_FAILED, "Device registration failed");
            }
        }

        validatePreferences(device.getPreferences());

        if(!forUpdate) {

            validationService.checkStringParam(device.getModel(), 1, Configurations.MAX_MODEL_LENGTH, Configurations.CommonType.MODEL_LENGTH);
        }
    }

    private void validatePreferences(Preference preferences) throws ResourceValidationException
    {
        if (preferences == null)
        {
            logger.error("DeviceResource validatePreferences Device registration requires valid preferences");
            throw new ResourceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_PREFERENCES, "Device registration requires valid preferences");
        }

        //Check for location
        String location = preferences.getLocation();

        if (location == null || location.isEmpty() ||
                (!Configurations.CommonType.ALLOW.equals(location) && !Configurations.CommonType.DENY.equals(location)))
        {
            logger.error("DeviceResource validatePreferences Device registration requires valid location");
            throw new ResourceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_LOCATION, "Device registration requires valid location");
        }

    }
}
