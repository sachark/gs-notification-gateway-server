package com.gigsky.rest.bean;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by prsubbareddy on 07/10/16.
 *
 * {
  All fields are mandatory
//    "type": "AppFeatureInfo",
//            "featuresSupported": [
//            "networkTest", //Names of features supported are returned
//            "etc..."
//            ],
//            "baseUrl": "https://gigsky.com/api/v5/", //Gigsky backend base url
//            "logoBaseUrl": "https://gigsky.com/logoServer", //Resource base url
//            "troubleshootingBaseUrl": "https://www.gigsky.com/troubleshooting/api/v1/",
//            "updateMandatory": "true/false" //Force upgrade
//            }
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppFeatureInfoDetails {

    private String type;
    private String[] featuresSupported;
    private String baseUrl;
    private String logoBaseUrl;
    private String troubleshootingBaseUrl;
    private String updateMandatory;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getFeaturesSupported() {
        return featuresSupported;
    }

    public void setFeaturesSupported(String[] featuresSupported) {

        this.featuresSupported = featuresSupported;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getLogoBaseUrl() {
        return logoBaseUrl;
    }

    public void setLogoBaseUrl(String logoBaseUrl) {
        this.logoBaseUrl = logoBaseUrl;
    }

    public String getTroubleshootingBaseUrl() {
        return troubleshootingBaseUrl;
    }

    public void setTroubleshootingBaseUrl(String troubleshootingBaseUrl) {
        this.troubleshootingBaseUrl = troubleshootingBaseUrl;
    }

    public String getUpdateMandatory() {
        return updateMandatory;
    }

    public void setUpdateMandatory(String updateMandatory) {
        this.updateMandatory = updateMandatory;
    }
}
