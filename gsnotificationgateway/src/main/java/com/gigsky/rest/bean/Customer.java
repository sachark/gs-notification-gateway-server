package com.gigsky.rest.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by anant on 29/10/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Customer {

    private String type;

    @XmlElement(name = "customerId", defaultValue = "")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String customerId;

    @XmlElement(name = "oldEmailId", defaultValue = "")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String oldEmailId;

    @XmlElement(name = "anonymousEmailId", defaultValue = "")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String anonymousEmailId;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOldEmailId() {
        return oldEmailId;
    }

    public void setOldEmailId(String oldEmailId) {
        this.oldEmailId = oldEmailId;
    }

    public String getAnonymousEmailId() {
        return anonymousEmailId;
    }

    public void setAnonymousEmailId(String anonymousEmailId) {
        this.anonymousEmailId = anonymousEmailId;
    }

}
