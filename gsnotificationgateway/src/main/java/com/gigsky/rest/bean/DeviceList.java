package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by anant on 23/11/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceList {

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Device> getList() {
        return list;
    }

    public void setList(List<Device> list) {
        this.list = list;
    }


    private String type;
    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String customerId;
    private int startIndex;
    private int count;
    private int totalCount;

    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private List<Device> list;
}
