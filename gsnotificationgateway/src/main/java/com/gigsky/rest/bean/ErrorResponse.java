package com.gigsky.rest.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ErrorResponse {
	private final String type = "error";
	private int errorInt;
	private String errorStr;

	public ErrorResponse()
	{

	}

	public ErrorResponse(int code, String message)
	{
		errorInt = code;
		errorStr = message;
	}

	public String getType() {
		return type;
	}

	public int getErrorInt() {
		return errorInt;
	}

	public void setErrorInt(int errorInt) {
		this.errorInt = errorInt;
	}

	public String getErrorStr() {
		return errorStr;
	}

	public void setErrorStr(String errorStr) {
		this.errorStr = errorStr;
	}
}
