package com.gigsky.rest.bean;

import javax.ws.rs.QueryParam;

/**
 * Created by anant on 23/11/15.
 */
public class PageInfo {

    public PageInfo()
    {
        start = 0;
        count = 0;
    }

    public PageInfo(int s, int c)
    {
        start = s;
        count = c;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @QueryParam("startIndex") int start;
    @QueryParam("count") int count;

}
