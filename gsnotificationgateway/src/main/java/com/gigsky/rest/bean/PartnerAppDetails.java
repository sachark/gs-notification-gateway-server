package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by vinayr on 15/04/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerAppDetails {

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDeviceOSVersion() {
        return deviceOSVersion;
    }

    public void setDeviceOSVersion(String deviceOSVersion) {
        this.deviceOSVersion = deviceOSVersion;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    public PartnerAppDetails(String deviceType, String ipAddress, String deviceOSVersion, String locale,
                             String iccId, String location, String partnerCode, String sdkVersion)
    {
        this.type = "PartnerAppDetail";
        this.deviceType = deviceType;
        this.ipAddress = ipAddress;
        this.deviceOSVersion = deviceOSVersion;
        this.locale = locale;
        this.iccId = iccId;
        this.location = location;
        this.partnerCode = partnerCode;
        this.sdkVersion = sdkVersion;
    }

    private String type;
    private String deviceType;
    private String deviceOSVersion;
    private String locale;
    private String ipAddress;
    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String iccId;
    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String location;
    private String partnerCode;
    private String sdkVersion;
}
