package com.gigsky.rest.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by vinayr on 29/01/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Preference {

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private String location;
}
