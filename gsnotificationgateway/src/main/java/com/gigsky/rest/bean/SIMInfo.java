package com.gigsky.rest.bean;

/**
 * Created by prsubbareddy on 07/11/17.
 */
public class SIMInfo {

    private String type = "DefaultSIM";
    private Long gsCustomerId;
    private String simNickname;
    private String simId;
    private String simType;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getGsCustomerId() {
        return gsCustomerId;
    }

    public void setGsCustomerId(Long gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    public String getSimNickname() {
        return simNickname;
    }

    public void setSimNickname(String simNickname) {
        this.simNickname = simNickname;
    }

    public String getSimId() {
        return simId;
    }

    public void setSimId(String simId) {
        this.simId = simId;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

}
