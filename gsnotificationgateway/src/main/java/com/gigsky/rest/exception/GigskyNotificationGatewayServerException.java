package com.gigsky.rest.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class GigskyNotificationGatewayServerException extends WebApplicationException {
	private ErrorCode errorCode;
	private String message;

	public GigskyNotificationGatewayServerException(ErrorCode errorCode) {
		super(new Throwable(String.valueOf(errorCode.statusCode)));
		this.errorCode = errorCode;
	}

	public GigskyNotificationGatewayServerException(ErrorCode errorCode, String message) {
		super(new Throwable(String.valueOf(errorCode.statusCode)));
		this.message = message;
		this.errorCode = errorCode;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public Response getResponse() {
		return super.getResponse();
	}
}

