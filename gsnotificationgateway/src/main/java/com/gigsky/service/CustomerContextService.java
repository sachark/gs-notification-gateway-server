package com.gigsky.service;

import com.gigsky.backend.Plan.PlanManagementInterface;
import com.gigsky.backend.beans.*;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.database.bean.CustomerContextDBBean;
import com.gigsky.database.dao.CustomerContextDao;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.PlanPurchase;
import com.gigsky.rest.bean.SIMInfo;
import com.gigsky.rest.exception.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import util.GSCountries;

import java.util.List;

/**
 * Created by prsubbareddy on 07/11/17.
 */
public class CustomerContextService {

    private final Logger logger = LoggerFactory.getLogger(CustomerContextService.class);

    @Autowired
    CustomerContextDao customerContextDao;

    @Autowired
    PlanManagementInterface planManagementInterface;


    public void setDefaultSIM(SIMInfo simInfo) {

        long gsCustomerId = simInfo.getGsCustomerId();
        CustomerContextDBBean customerContextDBBean = customerContextDao.getCustomerContext(gsCustomerId);

        if(customerContextDBBean != null){
            //TODO: update all fields with check enabled
            customerContextDBBean.setSimNickname(simInfo.getSimNickname());
            customerContextDBBean.setSimType(simInfo.getSimType());
            customerContextDBBean.setIccid(simInfo.getSimId());
            //customerContextDBBean.setDeviceId("1");
            //customerContextDBBean.setUserId("user");
            customerContextDao.updateCustomerContext(customerContextDBBean);

        } else {

            customerContextDBBean = new CustomerContextDBBean();
            customerContextDBBean.setGsCustomerId(gsCustomerId);
            customerContextDBBean.setIccid(simInfo.getSimId());
            customerContextDBBean.setSimNickname(simInfo.getSimNickname());
            customerContextDBBean.setSimType(simInfo.getSimType());
            //customerContextDBBean.setDeviceId("1");
            //customerContextDBBean.setUserId("user");

            customerContextDao.addCustomerContext(customerContextDBBean);
        }
    }

    public CustomerContextDBBean updatedCustomerDetails(LoginResponse loginResponse) {

        Long gsCustomerId = Long.parseLong(loginResponse.getCustomerId());
        CustomerContextDBBean customerContextDBBean = customerContextDao.getCustomerContext(gsCustomerId);

        // We assume that entry will be there already for this scenario.
        if(customerContextDBBean != null){
            //TODO: update all fields with check enabled
            customerContextDBBean.setToken(loginResponse.getToken());
            //customerContextDBBean.setDeviceId("1");
            //customerContextDBBean.setUserId("user");
            customerContextDao.updateCustomerContext(customerContextDBBean);
        } else {
            customerContextDBBean = new CustomerContextDBBean();
            customerContextDBBean.setGsCustomerId(gsCustomerId);
            customerContextDBBean.setToken(loginResponse.getToken());

            customerContextDao.addCustomerContext(customerContextDBBean);
        }
        return customerContextDBBean;
    }


    public NetworkGroupPlans getPlans(CustomerContextDBBean customerContextDBBean, boolean currentLocation, String countryCode) throws BackendRequestException, ServiceValidationException {

        NetworkGroupResponse networkGroupResponse;
        if(!currentLocation)
        {
            // Validate country code
            validateCountryCode(countryCode);

            // Check for forbidden country
            checkForbiddenCountry(countryCode, customerContextDBBean);

            // Get network group response using country code
            networkGroupResponse = planManagementInterface.getNetworkGroupForCountry(customerContextDBBean, countryCode);
        } else {

            SIMStatusResponse simStatusResponse = planManagementInterface.getSIMStatus(customerContextDBBean);

            if(simStatusResponse.getAvailableNetworkGroupList().size() > 0)
            {
                networkGroupResponse = simStatusResponse.getAvailableNetworkGroupList().get(0);
            } else {
                throw new BackendRequestException(ErrorCode.CURRENTLY_SIM_IS_NOT_CONNECTED);
            }

            countryCode = getCountryCode(simStatusResponse.getConnectionInfo());
            if(countryCode == null)
            {
                throw new BackendRequestException(ErrorCode.CURRENTLY_SIM_IS_NOT_CONNECTED);
            }

            // Check for forbidden country
            checkForbiddenCountry(countryCode, customerContextDBBean);
        }

        // Get plan response using network group id
       return planManagementInterface.getPlansForNetwork(customerContextDBBean, networkGroupResponse, countryCode);
    }

    public void makePurchasePlan(CustomerContextDBBean customerContextDBBean, PlanPurchase planInfo) throws BackendRequestException, ResourceValidationException {

        // Get Payment status to check available gigsky credit and billing id if requires
        PaymentStatusResponse paymentStatusResponse = planManagementInterface.getPaymentStatus(customerContextDBBean);

        // Make Plan Purchase
        planManagementInterface.makePlanPurchase(customerContextDBBean, planInfo, paymentStatusResponse);
    }


    private void checkForbiddenCountry(String countryCode, CustomerContextDBBean customerContextDBBean) throws BackendRequestException {

        // Check if sim type id SIM 5.0 , country is forbidden or not
        if("GS_SIM_50".equals(customerContextDBBean.getSimType()))
        {
            ForbiddenCountryResponse forbiddenCountryResponse = planManagementInterface.getForbiddenCountries();

            if(forbiddenCountryResponse.getList().size() > 0)
            {
                List<Country> forbiddenList = forbiddenCountryResponse.getList();

                for(Country country: forbiddenList){
                    String cCode = country.getCode();
                    if(countryCode.toLowerCase().equals(cCode.toLowerCase()))
                    {
                        throw new BackendRequestException(ErrorCode.FORBIDDEN_COUNTRY_NOT_SUPPORTED);
                    }
                }
            }
        }
    }

    private String getCountryCode(ConnectionInfo connectionInfo)
    {
        if(connectionInfo != null){
            List<String> countryCodes = connectionInfo.getCountries();
            if(countryCodes != null)
            {
                return countryCodes.get(0);
            }
        }

        return null;
    }

    private void validateCountryCode(String countryCode) throws ServiceValidationException {
        if(countryCode == null || countryCode.length() ==  0 || !GSCountries.isCountryCodeMatched(countryCode))
        {
            logger.error("Invalid purchase plan country code");
            throw new ServiceValidationException(ErrorCode.GET_PLANS_REQUIRES_VALID_COUNTRY_CODE);
        }
    }

}
