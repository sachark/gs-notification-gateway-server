package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.CustomerEvent;
import com.gigsky.event.EmailEvent;
import com.gigsky.event.GroupEvent;
import com.gigsky.notification.BaseNotification;
import com.gigsky.notification.NotificationFactory;
import com.gigsky.policy.OutOfOrderCheckPolicy;
import com.gigsky.policy.RateControlPolicy;
import com.gigsky.task.*;
import org.apache.commons.collections.CollectionUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by prsubbareddy on 05/11/15.
 */
public class EventService {

    private static final Logger logger = LoggerFactory.getLogger(EventService.class);

    private static final int DEFAULT_THREAD_COUNT = 4;
    private static final int DEFAULT_EVENTS_COUNT = 500;
    private static final int MAX_THREAD_COUNT = 8;
    private static final int DEFAULT_THREAD_IDLE_TIME = 10;
    private static final int DEFAULT_QUEUE_SIZE = 500;

    private static ThreadPoolExecutor threadPoolExecutor = null;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    DeviceDao deviceDao;

    @Autowired
    EventDao eventDao;

    @Autowired
    MockPushNotificationDao mockPushNotificationDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    NotificationFactory notificationFactory;

    @Autowired
    PolicyDao policyDao;

    @Autowired
    NotificationService notificationService;

    @Autowired
    PolicyService policyService;

    @Autowired
    PushNotificationDao pushNotificationDao;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    OutOfOrderCheckPolicy outOfOrderCheckPolicy;

    @Autowired
    RateControlPolicy rateControlPolicy;

    @Autowired
    PushNotificationsFactory pushNotificationsFactory;

    @Autowired
    EmailNotificationsFactory emailNotificationsFactory;

    @Autowired
    EmailNotificationDao emailNotificationDao;

    private BaseNotification baseNotification;
    private BaseEvent baseEvent;



    public void initialize() {
        logger.debug("EventService:initialize++");

        if (threadPoolExecutor == null) {
            synchronized (EventService.class) {
                    int numOfThreads = DEFAULT_THREAD_COUNT;
                    String processingThreads = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.NOTIFICATION_PROCESSING_THREADS);

                    if(!processingThreads.isEmpty())
                    {
                        numOfThreads = Integer.parseInt(processingThreads);
                    }

                    //executorService = Executors.newFixedThreadPool(numOfThreads, Executors.defaultThreadFactory());
                    threadPoolExecutor = new ThreadPoolExecutor(numOfThreads, MAX_THREAD_COUNT, DEFAULT_THREAD_IDLE_TIME, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(DEFAULT_QUEUE_SIZE));
            }
        }

        logger.debug("EventService:initialize--");
    }

    public void processEvents()
    {
        int eventsCount = DEFAULT_EVENTS_COUNT;
        int pendingTasks;
        String eventsCountValue = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EVENTS_PROCESSING_COUNT);

        if(!eventsCountValue.isEmpty())
        {
            eventsCount = Integer.parseInt(eventsCountValue);
        }

        //TODO: Check how many pending items are there in executor service queue and subtract from events count
        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        eventsCount -= pendingTasks;

        if (eventsCount > 0)
        {
            // Processing Events for NEW status
            handleEventsProcessForStatusNew(eventsCount);
        }

        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        eventsCount -= pendingTasks;

        if (eventsCount > 0)
        {
            // Processing Events for FAILED_PUSH_PROCESS status
            handleEventsProcessForStatusFailed(eventsCount);
        }

        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        eventsCount -= pendingTasks;
        if(eventsCount > 0)
        {
            // Processing of all Emails
            handleEmailSendingProcess(eventsCount);
        }

    }


    private void handleEventsProcessForStatusNew(int eventsCount)
    {
        boolean executeEvent = false;
        List<EventDBBean> eventsList = eventDao.getEvents(Configurations.StatusType.STATUS_NEW, eventsCount);

        for(Iterator<EventDBBean> it = eventsList.iterator(); it.hasNext(); )
        {
            EventDBBean eventDBBean = it.next();

            EventTypeDBBean eventTypeDBBean =  eventDao.getEventType(eventDBBean.getEventTypeId());

            if(!Configurations.EventType.TYPE_EMAIL.equals(eventTypeDBBean.getType()))
            {
                //Check if this needs to be picked for processing or not
                executeEvent = processEventCheck(eventDBBean);
                if (executeEvent == false)
                {
                    continue;
                }
            }

            // Changing event status to IN_PROCESS and update to DB.
            eventDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
            eventDao.updateEventDetails(eventDBBean);

            // getting BaseEvent from event DB Bean
            baseEvent = getBaseEvent(eventDBBean, eventTypeDBBean);

            List<PolicyDBBean> policiesList =  policyService.getPoliciesForEventType(baseEvent, eventDBBean);
            logger.info("list of polices for event_type_id  " +eventDBBean.getEventTypeId()+" and for event_id "+eventDBBean.getId());

            boolean eventStatusComplete = false;

            //Iterate through policies and create notifications
            for(Iterator<PolicyDBBean> policy = policiesList.iterator(); policy.hasNext();)
            {
                PolicyDBBean policyDBBean = policy.next();
                logger.info("policy db bean " + policyDBBean + " for event_id " + eventDBBean.getId());

                baseNotification = notificationService.processEventForNotification(baseEvent, policyDBBean);
                logger.info("base notification after policy process:" +baseNotification);

                if(baseNotification != null && Configurations.EventType.TYPE_EMAIL.equals(baseEvent.getEventType()))
                {
                    // make a call to email notification task to compose message
                    callEmailNotificationCreationTask(baseEvent, baseNotification, policyDBBean.getId());
                    eventStatusComplete = true;
                }
                else if(baseNotification != null)
                {
                    // customer Uuid from Event
                    String customerUuid = getCustomerUuid(eventDBBean);
                    logger.info("customer_id " + customerUuid + " for event_id " + eventDBBean.getId());
                    // make a call to push notification task to compose message
                    callPushNotificationTask(baseEvent, baseNotification, policyDBBean.getId(), customerUuid);
                    eventStatusComplete = true;
                }
            }

            if(eventStatusComplete)
            {
                // Changing event status to COMPLETE and update to DB.
                eventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_WITH_NOTIFICATION);
                eventDao.updateEventDetails(eventDBBean);
            }
            else
            {
                eventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_WITHOUT_NOTIFICATION);
                eventDao.updateEventDetails(eventDBBean);
            }
        }
    }

    private void handleEventsProcessForStatusFailed(int eventsCount)
    {
        // List of IN_PROCESS events with failed PUSH PROCESS status message
        List<EventDBBean> inProcessEventsList = eventDao.getEvents(Configurations.StatusType.STATUS_IN_PROCESS, eventsCount);

        for(Iterator<EventDBBean> eventBean = inProcessEventsList.iterator(); eventBean.hasNext(); )
        {
            EventDBBean eventDBBean = eventBean.next();

            if(Configurations.StatusType.STATUS_FAILED_PUSH_PROCESS.equals(eventDBBean.getStatusMessage()))
            {
                // getting BaseEvent from event DB Bean
                EventTypeDBBean eventTypeDBBean =  eventDao.getEventType(eventDBBean.getEventTypeId());
                baseEvent = getBaseEvent(eventDBBean, eventTypeDBBean);

                // List of Failed Notifications
                List<NotificationDBBean> listNotifications = notificationDao.getNotifications(eventDBBean.getId());
                if(CollectionUtils.isNotEmpty(listNotifications))
                {
                    for(Iterator<NotificationDBBean> bean = listNotifications.iterator(); bean.hasNext(); )
                    {
                        NotificationDBBean notificationDBBean = bean.next();
                        if(Configurations.StatusType.STATUS_FAILED.equals(notificationDBBean.getStatus()))
                        {
                            NotificationTypeDBBean notificationTypeDBBean = notificationDao.getNotificationType(notificationDBBean.getNotificationTypeId());

                            //Create appropriate base notification using notification factory.
                            baseNotification = notificationFactory.createNotification(baseEvent, notificationTypeDBBean.getType(), notificationDBBean);

                            // Changing event status to COMPLETE and update to DB.
                            eventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                            eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_WITH_NOTIFICATION);
                            eventDao.updateEventDetails(eventDBBean);

                            String gateWayMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE, "TEST");
                            if(gateWayMode != null && gateWayMode.equals("TEST"))
                            {
                                // List of Failed mock push notifications
                                List<MockPushNotificationDBBean> listMockPushNotifications =  mockPushNotificationDao.getMockPushNotifications(notificationDBBean.getId());
                                for(Iterator<MockPushNotificationDBBean> mockPushBean = listMockPushNotifications.iterator(); mockPushBean.hasNext();)
                                {
                                    MockPushNotificationDBBean mockPushNotificationDBBean = mockPushBean.next();
                                    if(Configurations.StatusType.STATUS_FAILED.equals(mockPushNotificationDBBean.getStatus()))
                                    {
                                        DeviceDBBean deviceDBBean = deviceDao.getDevice(mockPushNotificationDBBean.getDeviceId());

                                        RetryPushNotificationTask retryPushNotificationTask = (RetryPushNotificationTask) pushNotificationsFactory.createNotificationTask("RETRY_PUSH_TASK");
                                        retryPushNotificationTask.setPushNotificationId(mockPushNotificationDBBean.getId());
                                        retryPushNotificationTask.setBaseEvent(baseEvent);
                                        retryPushNotificationTask.setBaseNotification(baseNotification);
                                        retryPushNotificationTask.setDeviceDBBean(deviceDBBean);

                                        threadPoolExecutor.submit(retryPushNotificationTask);
                                    }
                                }

                            }
                            else if(gateWayMode!= null && gateWayMode.equals("LIVE")) {

                                // List of Failed push notifications
                                List<PushNotificationDBBean> listPushNotifications = pushNotificationDao.getPushNotifications(notificationDBBean.getId());
                                for(Iterator<PushNotificationDBBean> pushBean = listPushNotifications.iterator(); pushBean.hasNext();)
                                {
                                    PushNotificationDBBean pushNotificationDBBean = pushBean.next();
                                    if(Configurations.StatusType.STATUS_FAILED.equals(pushNotificationDBBean.getStatus()))
                                    {
                                        // Device DB Bean from PushedDeviceDBBean using pushNotificationId
                                        PushedDeviceDBBean pushedDeviceDBBean = pushNotificationDao.getPushedDevice(pushNotificationDBBean.getId());
                                        DeviceDBBean deviceDBBean = deviceDao.getDevice(pushedDeviceDBBean.getDeviceId());

                                        RetryPushNotificationTask retryPushNotificationTask = (RetryPushNotificationTask) pushNotificationsFactory.createNotificationTask("RETRY_PUSH_TASK");
                                        retryPushNotificationTask.setPushNotificationId(pushNotificationDBBean.getId());
                                        retryPushNotificationTask.setBaseEvent(baseEvent);
                                        retryPushNotificationTask.setBaseNotification(baseNotification);
                                        retryPushNotificationTask.setDeviceDBBean(deviceDBBean);

                                        threadPoolExecutor.submit(retryPushNotificationTask);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //TODO check the status message
                    eventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                    eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_WITHOUT_NOTIFICATION);
                    eventDao.updateEventDetails(eventDBBean);
                }
            }
        }
    }


    // Push notification task run
    private void callPushNotificationTask(BaseEvent baseEvent, BaseNotification baseNotification, int policyId, String customerUuid)
    {
        // Changing Notification status to IN_PROCESS and update to DB
        NotificationDBBean notificationDBBean =  notificationDao.getNotificationDetails(baseNotification.getNotificationId());
        notificationDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
        notificationDao.updateNotification(notificationDBBean);

        PushNotificationTask pushNotificationTask = (PushNotificationTask) pushNotificationsFactory.createNotificationTask("PUSH_TASK");

        pushNotificationTask.setPolicyId(policyId);
        pushNotificationTask.setCustomerUUid(customerUuid);
        pushNotificationTask.setBaseEvent(baseEvent);
        pushNotificationTask.setBaseNotification(baseNotification);

        threadPoolExecutor.submit(pushNotificationTask);
    }

    // Email Notification task will handle creation of All Emails
    private void callEmailNotificationCreationTask(BaseEvent baseEvent, BaseNotification baseNotification, int policyId)
    {
        // Changing Notification status to IN_PROCESS and update to DB
        NotificationDBBean notificationDBBean =  notificationDao.getNotificationDetails(baseNotification.getNotificationId());
        notificationDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
        notificationDao.updateNotification(notificationDBBean);

        EmailNotificationCreationTask emailNotificationTask = (EmailNotificationCreationTask) emailNotificationsFactory.createNotificationTask("EMAIL_CREATE_TASK");
        emailNotificationTask.setPolicyId(policyId);
        emailNotificationTask.setBaseEvent(baseEvent);
        emailNotificationTask.setBaseNotification(baseNotification);

        threadPoolExecutor.submit(emailNotificationTask);
    }

    // This will take care to send Emails with status NEW and FAILED
    private void handleEmailSendingProcess(int emailsCount)
    {
        List<EmailNotificationDBBean> emailNotificationsList = emailNotificationDao.getAllEmailNotifications(emailsCount);

        if(emailNotificationsList != null)
        {
            for(Iterator<EmailNotificationDBBean> it = emailNotificationsList.iterator(); it.hasNext(); )
            {
                EmailNotificationDBBean emailNotificationDBBean = it.next();

                // updating status from NEW to IN_PROCESS for EMail Notiifcation entry
                emailNotificationDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
                emailNotificationDao.updateEmailNotification(emailNotificationDBBean);

                EmailNotificationSendTask emailNotificationSendTask = (EmailNotificationSendTask) emailNotificationsFactory.createNotificationTask("EMAIL_SEND_TASK");
                emailNotificationSendTask.setEmailNotificationId(emailNotificationDBBean.getId());

                threadPoolExecutor.submit(emailNotificationSendTask);
            }
        }
    }

    // Getting customerUuid using customer id.
    public String getCustomerUuid(EventDBBean eventDBBean)
    {
        String customerUuid = null;
        CustomerEventDBBean customerEventDBBean  = eventDBBean.getCustomerEvent();
        int customerId =  customerEventDBBean.getCustomerId();

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByCustomerId(customerId);

        if(customerDBBean != null)
        {
            customerUuid = customerDBBean.getCustomerUuid();
            return customerUuid;
        }
        return customerUuid;
    }

    private BaseEvent getBaseEvent(EventDBBean eventDBBean, EventTypeDBBean eventTypeDBBean)
    {
        if(Configurations.EventType.TYPE_EMAIL.equals(eventTypeDBBean.getType()))
        {
            EmailEvent emailEvent = new EmailEvent();
            emailEvent.setEventId(eventDBBean.getId());
            emailEvent.setEventTypeId(eventDBBean.getEventTypeId());
            emailEvent.setEventType(eventTypeDBBean.getType());
            emailEvent.setEventCategory(eventDBBean.getEventCategory());
            emailEvent.setEventStatus("IN_PROCESS");
            emailEvent.setSubEventType(eventTypeDBBean.getSubEventType());
            emailEvent.setTenantId(eventDBBean.getTenantId());

            EmailEventDBBean emailEventDBBean = eventDao.getEmailEvent(eventDBBean.getId());

            String emailIds = emailEventDBBean.getEmailId();
            String[] emails = emailIds.split(",");
            List<String> listEmails = new ArrayList<String>();
            if(emails.length > 0)
            {
                for(int i = 0; i < emails.length; i++ )
                {
                    listEmails.add(emails[i]);
                }
            }

            emailEvent.setEmailIds(listEmails);

            String data = eventDao.getEmailEvent(eventDBBean.getId()).getData();
            try {
                JSONObject jsonData = new JSONObject(data);
                emailEvent.setEventData(jsonData);
                return emailEvent;
            }catch (JSONException e) {
                logger.warn("json object data creation  Exception:" +e +" for event_id  "+ eventDBBean.getId());
            }
        } else
        {
            if(Configurations.CategoryType.CATEGORY_CUSTOMER.equals(eventDBBean.getEventCategory()))
            {
                CustomerEvent customerEvent = new CustomerEvent();
                customerEvent.setEventId(eventDBBean.getId());
                customerEvent.setEventTypeId(eventDBBean.getEventTypeId());
                customerEvent.setEventType(eventTypeDBBean.getType());
                customerEvent.setEventCategory(eventDBBean.getEventCategory());
                customerEvent.setEventStatus("IN_PROCESS");
                customerEvent.setTenantId(eventDBBean.getTenantId());

                String data = eventDBBean.getCustomerEvent().getData();
                try {
                    JSONObject jsonData = new JSONObject(data);
                    customerEvent.setEventData(jsonData);
                    return customerEvent;
                }catch (JSONException e) {
                    logger.warn("json object data creation  Exception:" +e +" for event_id  "+ eventDBBean.getId());
                }
            }
            else if(Configurations.CategoryType.CATEGORY_GROUP.equals(eventDBBean.getEventCategory()))
            {
                GroupEvent groupEvent = new GroupEvent();
                groupEvent.setEventId(eventDBBean.getId());
                groupEvent.setEventTypeId(eventDBBean.getEventTypeId());
                groupEvent.setEventType(eventTypeDBBean.getType());
                groupEvent.setEventCategory(eventDBBean.getEventCategory());
                groupEvent.setEventStatus(eventDBBean.getStatus());
                groupEvent.setTenantId(eventDBBean.getTenantId());
                return groupEvent;
            }
        }
        return null;
    }

    private  boolean processEventCheck(EventDBBean eventDBBean)
    {
        //Check for out of order of the event
        Boolean isOutOfOrder = outOfOrderCheckPolicy.isOutOfOrder(eventDBBean);
        if (isOutOfOrder == true)
        {
            String simId = null;

            //Event is out of order. Processing is not required
            eventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_OUT_OF_ORDER);
            eventDao.updateEventDetails(eventDBBean);

            //Extract SIM ID
            String data = eventDBBean.getCustomerEvent().getData();
            if(!data.isEmpty())
            {
                try
                {
                    JSONObject jsonData = new JSONObject(data);
                    simId = jsonData.optString("iccId");
                }
                catch (JSONException e) {
                    logger.warn("json object out of order Exception:" +e +" for event_id  "+ eventDBBean.getId());
                }
            }

            logger.info("Out of order event with event_id " + eventDBBean.getId() + " for event_type_id " + eventDBBean.getEventTypeId() +
                        "  for customer with customer_id " + getCustomerUuid(eventDBBean) +
                        "  for sim_id " + simId);

            return false;
        }

        Boolean needsRateControl = rateControlPolicy.needsRateControl(eventDBBean);
        if (needsRateControl == true)
        {
            String simId = null;

            //Event should be rate controlled. Processing is not required
            eventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_RATE_CONTROLLED);
            eventDao.updateEventDetails(eventDBBean);

            //Extract SIM ID
            String data = eventDBBean.getCustomerEvent().getData();
            if(!data.isEmpty())
            {
                try
                {
                    JSONObject jsonData = new JSONObject(data);
                    simId = jsonData.optString("iccId");
                }
                catch (JSONException e) {
                    logger.warn("json object rate control Exception:" +e +" for event_id  "+ eventDBBean.getId());
                }
            }

            logger.info("Rate controlled event with event_id " + eventDBBean.getId() + " for event_type_id " + eventDBBean.getEventTypeId() +
                        "  for customer with customer_id " + getCustomerUuid(eventDBBean) +
                        "  for  sim_id " + simId);

            return false;
        }

        return true;
    }

    public void destroy(){
        logger.debug("EventService:destroy++");
        try {
            if (threadPoolExecutor != null) {
                logger.debug("EventService:executorService.shutdown ++");
                threadPoolExecutor.shutdown();
                logger.debug("EventService:executorService.shutdown --");

                while (!threadPoolExecutor.isTerminated()) {
                    logger.debug("EventService:executorService is Terminated flag is false waiting for 1 secs");
                    threadPoolExecutor.awaitTermination(1, TimeUnit.SECONDS);
                }
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with exeception ", ex);
        }
        logger.debug("EventService:awaitTermination--");
        logger.debug("EventService:destroy--");
    }

}
