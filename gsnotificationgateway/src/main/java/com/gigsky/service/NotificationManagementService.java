package com.gigsky.service;

import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.bean.NotificationTypeDBBean;
import com.gigsky.database.dao.NotificationDao;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.Notification;
import com.gigsky.rest.exception.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by anant on 23/11/15.
 */
public class NotificationManagementService {

    @Autowired
    private NotificationDao notificationDao;

    private final Logger logger = LoggerFactory.getLogger(NotificationManagementService.class);

    public Notification getNotification(long notificationId, int tenantId) throws ServiceValidationException {

        NotificationDBBean notificationDBBean = notificationDao.getNotificationDetails((int) notificationId, tenantId);

        if (notificationDBBean != null)
        {
            //Create notification response
            Notification notification = new Notification();
            notification.setType("NotificationDetail");
            notification.setStatus(notificationDBBean.getStatus());
            notification.setStatusMessage(notificationDBBean.getStatusMessage());
            notification.setNotificationId(notificationId);
            notification.setEventId(notificationDBBean.getEventId());

            NotificationTypeDBBean notificationTypeDBBean = notificationDao.getNotificationType(notificationDBBean.getNotificationTypeId());
            if (notificationTypeDBBean != null)
            {
                notification.setNotificationType(notificationTypeDBBean.getType());
            }

            return notification;
        }

        else
        {
            logger.error("NotificationManagementService getNotification Invalid type");
            throw new ServiceValidationException(ErrorCode.INVALID_ARGUMENTS, "Invalid type");
        }
    }
}
