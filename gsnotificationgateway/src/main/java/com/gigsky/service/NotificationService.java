package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.EventTypeDBBean;
import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.bean.NotificationTypeDBBean;
import com.gigsky.database.bean.PolicyDBBean;
import com.gigsky.database.dao.EventDao;
import com.gigsky.database.dao.NotificationDao;
import com.gigsky.database.dao.PushNotificationDao;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.CustomerEvent;
import com.gigsky.notification.BaseNotification;
import com.gigsky.notification.NotificationFactory;
import com.gigsky.template.NotificationTemplateFactory;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by prsubbareddy on 06/11/15.
 */
public class NotificationService {

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    PushNotificationDao pushNotificationDao;

    @Autowired
    NotificationTemplateFactory notificationTemplateFactory;

    @Autowired
    NotificationFactory notificationFactory;

    @Autowired
    EventDao eventDao;

    private static final Logger logger = LoggerFactory.getLogger(EventService.class);

    public BaseNotification processEventForNotification(BaseEvent event, PolicyDBBean policy)
    {
        // check type of notification to be sent
        NotificationTypeDBBean notificationTypeDBBean = notificationDao.getNotificationType(policy.getNotificationTypeId());
        BaseNotification baseNotification = null;
        if(BaseNotification.TYPE_PUSH.equals(notificationTypeDBBean.getType()))
        {
            // check if push notification is required or not
            if(checkIfPushNotificationRequired(event))
            {
                //Create an entry in the notification DB table for this event
                NotificationDBBean notificationDBBean = createNotification(event, notificationTypeDBBean.getId(), policy.getId());

                if(notificationDBBean != null)
                {
                    //Create appropriate base notification using notification factory.
                    baseNotification = notificationFactory.createNotification(event, notificationTypeDBBean.getType(), notificationDBBean);
                }
            }
        }
        else if(BaseNotification.TYPE_EMAIL.equals(notificationTypeDBBean.getType()))
        {
            // check if email notification is required or not
            if(checkIfEmailNotificationRequired(event))
            {
                //Create an entry in the notification DB table for this event
                NotificationDBBean notificationDBBean = createNotification(event, notificationTypeDBBean.getId(), policy.getId());

                if(notificationDBBean != null)
                {
                    //Create appropriate base notification using notification factory.
                    baseNotification = notificationFactory.createNotification(event, notificationTypeDBBean.getType(), notificationDBBean);
                }
            }
        }
        else if(BaseNotification.TYPE_SMS.equals(notificationTypeDBBean.getType()))
        {
            checkIfSMSNotificationRequired(event);
        }

        return baseNotification;
    }

    private boolean checkIfPushNotificationRequired(BaseEvent event)
    {
        boolean enableNotification = false;

        EventTypeDBBean eventTypeDBBean = eventDao.getEventType(event.getEventTypeId());

        if (eventTypeDBBean != null)
        {
            enableNotification = eventTypeDBBean.getEnableNotification() != 0;
        }

        //For these types of events, check if the notification is enabled and send the same
        if((Configurations.EventType.TYPE_LOW_BALANCE.equals(event.getEventType())) ||
                (Configurations.EventType.TYPE_DATA_USED.equals(event.getEventType())) ||
                (Configurations.EventType.TYPE_SUBSCRIPTION_NEARING_EXPIRY.equals(event.getEventType())) ||
                (Configurations.EventType.TYPE_SUBSCRIPTION_EXPIRED.equals(event.getEventType())) ||
                (Configurations.EventType.TYPE_NEW_LOCATION.equals(event.getEventType())))
        {
            return enableNotification;
        }

        return false;
    }

    private boolean checkIfEmailNotificationRequired(BaseEvent event)
    {
        boolean enableNotification = false;

        EventTypeDBBean eventTypeDBBean = eventDao.getEventType(event.getEventTypeId());

        if (eventTypeDBBean != null)
        {
            enableNotification = eventTypeDBBean.getEnableNotification() != 0;
        }

        //For these types of events, check if the notification is enabled and send the same
        if((Configurations.EventType.TYPE_EMAIL.equals(event.getEventType())))
        {
            return enableNotification;
        }

        return false;
    }

    private boolean checkIfSMSNotificationRequired(BaseEvent event)
    {
        return false;
    }

    public NotificationDBBean createNotification(BaseEvent event, int notificationTypeId, int policyId)
    {
        NotificationDBBean notificationDBBean = new NotificationDBBean();
        notificationDBBean.setEventId(event.getEventId());
        notificationDBBean.setNotificationTypeId(notificationTypeId);
        notificationDBBean.setStatus(Configurations.StatusType.STATUS_NEW);
        notificationDBBean.setPolicyId(policyId);
        notificationDBBean.setTenantId(event.getTenantId());

        if(event != null && !Configurations.EventType.TYPE_EMAIL.equals(event.getEventType())&&
                Configurations.CategoryType.CATEGORY_CUSTOMER.equals(event.getEventCategory()))
        {
            JSONObject object = ((CustomerEvent) event).getEventData();

            if(object != null)
            {
                String time = object.optString("expireTime");
                if ((time != null) && (!time.isEmpty()))
                {
                    try{
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date parsedDate = dateFormat.parse(time);
                        Timestamp timestamp = new Timestamp(parsedDate.getTime());
                        notificationDBBean.setExpiry(timestamp);
                    }catch(Exception e){
                        logger.error("Creating Notification TimeStamp Parsing Exception:" +e);
                    }
                }
            }
        }

        int id = notificationDao.createNotification(notificationDBBean);
        if(id != -1)
        {
            return notificationDBBean;
        }
        return null;
    }

}
