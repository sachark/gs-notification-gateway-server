package com.gigsky.service;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.PartnerAppDetailsDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.PartnerAppDetailsDao;
import com.gigsky.exception.PartnerAppException;
import com.gigsky.rest.bean.PartnerAppDetails;
import com.gigsky.rest.exception.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by vinayr on 18/04/16.
 */
public class PartnerAppService {

    @Autowired
    private PartnerAppDetailsDao partnerAppDetailsDao;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    private static final long DEFAULT_EXPIRY_TIME = 24*60*60*1000;
    private static final int DEFAULT_MAX_ENTRIES = 500;

    private static final Logger logger = LoggerFactory.getLogger(PartnerAppService.class);

    //Register Partner app device details
    public void registerDevice(PartnerAppDetails partnerAppDetails) throws PartnerAppException
    {
        List<PartnerAppDetailsDBBean> partnerAppDetailsDBBeanList = partnerAppDetailsDao.getPartnerAppDetails(
                partnerAppDetails.getDeviceType(),
                partnerAppDetails.getIpAddress(),
                partnerAppDetails.getDeviceOSVersion(),
                partnerAppDetails.getLocale());


        //Check if the partner app entry is within the 24 hours
        PartnerAppDetailsDBBean partnerAppDetailsDBBean = checkIfWithinTimeRange(partnerAppDetailsDBBeanList);

        //Parnter App details are not present
        if (partnerAppDetailsDBBean == null)
        {
            partnerAppDetailsDBBean = new PartnerAppDetailsDBBean(partnerAppDetails);
            partnerAppDetailsDao.createPartnerAppDetails(partnerAppDetailsDBBean);
        }

        else
        {
            logger.debug("Partner App Device Details exists");

            partnerAppDetailsDBBean.setPartnerCode(partnerAppDetails.getPartnerCode());
            partnerAppDetailsDBBean.setIccId(partnerAppDetails.getIccId());
            partnerAppDetailsDBBean.setLocation(partnerAppDetails.getLocation());
            partnerAppDetailsDBBean.setSdkVersion(partnerAppDetails.getSdkVersion());
            partnerAppDetailsDao.updatePartnerAppDetails(partnerAppDetailsDBBean);
        }
    }

    //Fetch Partner app device details
    public PartnerAppDetails getDevice(PartnerAppDetails partnerAppDetails) throws PartnerAppException
    {
        List<PartnerAppDetailsDBBean> partnerAppDetailsDBBeanList = partnerAppDetailsDao.getPartnerAppDetails(
                partnerAppDetails.getDeviceType(),
                partnerAppDetails.getIpAddress(),
                partnerAppDetails.getDeviceOSVersion(),
                partnerAppDetails.getLocale());

        //Check if the partner app entry is within the 24 hours
        PartnerAppDetailsDBBean partnerAppDetailsDBBean = checkIfWithinTimeRange(partnerAppDetailsDBBeanList);

        //Parnter App details are not present
        if (partnerAppDetailsDBBean == null)
        {
            logger.error("PartnerAppService getDevice Partner App Device Not Found");
            throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_DETAILS_NOT_FOUND, "Partner App Device Not Found");
        }

        //Update the details fetched from database
        partnerAppDetails.setIccId(partnerAppDetailsDBBean.getIccId());
        partnerAppDetails.setLocation(partnerAppDetailsDBBean.getLocation());
        partnerAppDetails.setPartnerCode(partnerAppDetailsDBBean.getPartnerCode());
        partnerAppDetails.setSdkVersion(partnerAppDetailsDBBean.getSdkVersion());

        return partnerAppDetails;
    }

    PartnerAppDetailsDBBean checkIfWithinTimeRange(List<PartnerAppDetailsDBBean> partnerAppDetailsDBBeanList)
    {
        PartnerAppDetailsDBBean partnerAppDetailsDBBean;
        Timestamp currentUtcTime = CommonUtils.getCurrentTimestampInUTC();

        //Check if it is within 24 hours
        for(int index = 0; index < partnerAppDetailsDBBeanList.size(); index++ )
        {
            partnerAppDetailsDBBean = partnerAppDetailsDBBeanList.get(index);

            Timestamp registeredDate =  partnerAppDetailsDBBean.getRegisteredDate();


            logger.debug("Time difference " + (currentUtcTime.getTime() - registeredDate.getTime()));

            if ((currentUtcTime.getTime() - registeredDate.getTime()) < CommonUtils.ONE_DAY_MS)
            {
                return partnerAppDetailsDBBean;
            }
        }

        return null;
    }

    //Delete the entries after 24 hour duration
    public void deletePartnerDeviceEntries()
    {
        long expiryTimeMS;
        int maxCount = DEFAULT_MAX_ENTRIES;

        String partnerDetailsExpiryTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.PARTNER_APP_DETAILS_EXPIRY);

        if (partnerDetailsExpiryTime.isEmpty())
        {
            expiryTimeMS = DEFAULT_EXPIRY_TIME;
        }

        //TODO check if partnerDetailsExpiryTime is of valid format
        else
        {
            expiryTimeMS = new GigskyDuration(partnerDetailsExpiryTime).convertTimeInMilliSeconds();

        }

        List<PartnerAppDetailsDBBean> partnerAppDetailsDBBeanList = partnerAppDetailsDao.getExpiredPartnerAppDetails(expiryTimeMS, maxCount);

        PartnerAppDetailsDBBean partnerAppDetailsDBBean;

        //Delete the entries fetched from the database
        for(int index = 0; index < partnerAppDetailsDBBeanList.size(); index++ )
        {
            partnerAppDetailsDBBean = partnerAppDetailsDBBeanList.get(index);
            partnerAppDetailsDao.deletePartnerAppDetails(partnerAppDetailsDBBean);
        }
    }
}
