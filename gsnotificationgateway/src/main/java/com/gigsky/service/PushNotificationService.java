package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.PushNotificationDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.PushNotificationDao;
import com.gigsky.task.PeriodicPushNotificationReportTask;
import com.gigsky.task.PushNotificationsFactory;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by prsubbareddy on 29/12/15.
 */
public class PushNotificationService {

    private static final Logger logger = LoggerFactory.getLogger(PushNotificationService.class);

    private static final int DEFAULT_THREAD_COUNT = 2;
    private static final int MAX_THREAD_COUNT = 4;
    private static final int DEFAULT_THREAD_IDLE_TIME = 10;
    private static final int DEFAULT_QUEUE_SIZE = 500;
    private static final int DEFAULT_PUSH_NOTIFICATIONS_COUNT = 200;
    private static final int MAX_SLEEP_COUNT = 5;
    private static final int THREAD_SLEEP_TIME = 2* 60 * 1000;

    private static ThreadPoolExecutor threadPoolExecutor = null;


    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    PushNotificationDao pushNotificationDao;

    @Autowired
    PushNotificationsFactory pushNotificationsFactory;

   public void initialize()
   {
       logger.debug("PushNotificationService:initialize++");

       if (threadPoolExecutor == null) {
           synchronized (EventService.class) {

                   int numOfThreads = DEFAULT_THREAD_COUNT;
                   String processingThreads = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.NOTIFICATION_PROCESSING_THREADS);

                   if(!processingThreads.isEmpty())
                   {
                       numOfThreads = Integer.parseInt(processingThreads);
                   }

                   //executorService = Executors.newFixedThreadPool(numOfThreads, Executors.defaultThreadFactory());
                   threadPoolExecutor = new ThreadPoolExecutor(numOfThreads, MAX_THREAD_COUNT, DEFAULT_THREAD_IDLE_TIME, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(DEFAULT_QUEUE_SIZE));
           }
       }

       logger.debug("PushNotificationService:initialize--");
   }


    public void processPushNotifications()
    {

        Long c  = pushNotificationDao.getInProcessPushNotificationsCount(Configurations.StatusType.STATUS_IN_PROCESS);

        int count = (int) (long) c;

        int forCount = count/DEFAULT_PUSH_NOTIFICATIONS_COUNT;

        int startCount = 0;

        boolean complete;

        for(int i = 0; i <= forCount; i++)
        {
            List<PushNotificationDBBean> pushNotificationsList = pushNotificationDao.getAllPushNotifications(startCount, Configurations.StatusType.STATUS_IN_PROCESS, DEFAULT_PUSH_NOTIFICATIONS_COUNT);
            if(CollectionUtils.isNotEmpty(pushNotificationsList))
            {
                complete = false;
                int sleepCount = 0;

                do
                {
                    // check queue size available or not
                    // if available, start processing push notification reports
                    // else sleep for 2 minutes and check next time

                    int pendingItems = threadPoolExecutor.getQueue().size();

                    if( (DEFAULT_QUEUE_SIZE - pendingItems ) > DEFAULT_PUSH_NOTIFICATIONS_COUNT)
                    {
                        processPeriodicPushNotificationReports(pushNotificationsList);
                        complete = true;
                    }
                    else
                    {
                        try
                        {
                            Thread.sleep(THREAD_SLEEP_TIME);
                            sleepCount ++;

                            // if sleep count exceeds max sleep count
                            // get out of that loop
                            if(sleepCount >= MAX_SLEEP_COUNT)
                            {
                                complete = true;
                            }

                        } catch (Exception e)
                        {
                            logger.error("Catch Exception in Thread pool executor in push notification reports");
                        }
                    }
                }while (!complete);

            }

            startCount += DEFAULT_PUSH_NOTIFICATIONS_COUNT;
        }
    }


    public void processPeriodicPushNotificationReports(List<PushNotificationDBBean> pushNotificationsList)
    {
        for(Iterator<PushNotificationDBBean> it = pushNotificationsList.iterator(); it.hasNext();)
        {
            PushNotificationDBBean pushNotificationDBBean =  it.next();

            PeriodicPushNotificationReportTask periodicPushNotificationReportTask = (PeriodicPushNotificationReportTask) pushNotificationsFactory.createNotificationTask("PERIODIC_PUSH_TASK");
            periodicPushNotificationReportTask.setPushNotificationDBBean(pushNotificationDBBean);
            threadPoolExecutor.submit(periodicPushNotificationReportTask);
        }
    }


    public void destroy(){
        logger.debug("PushNotificationService:destroy++");
        try {
            if (threadPoolExecutor != null) {
                logger.debug("PushNotificationService:executorService.shutdown ++");
                threadPoolExecutor.shutdown();
                logger.debug("PushNotificationService:executorService.shutdown --");

                while (!threadPoolExecutor.isTerminated()) {
                    logger.debug("PushNotificationService:executorService is Terminated flag is false waiting for 1 secs");
                    threadPoolExecutor.awaitTermination(1, TimeUnit.SECONDS);
                }
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with exeception ", ex);
        }
        logger.debug("PushNotificationService:awaitTermination--");
        logger.debug("PushNotificationService:destroy--");
    }

}
