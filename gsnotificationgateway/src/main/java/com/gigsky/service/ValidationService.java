package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.exception.PartnerAppException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.exception.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class ValidationService {

	private static final Logger logger = LoggerFactory.getLogger(ValidationService.class);

	public void validateStartIndexAndCount(int startIndex, int count) {
		if(startIndex < 0) {
			logger.error("ValidationService validateStartIndexAndCount start index [" + startIndex + "] is invalid ");
			throw new IllegalArgumentException("start index [" + startIndex + "] is invalid");
		}
		if(count <= 0) {
			logger.error("ValidationService validateStartIndexAndCount count [" + count + "] is invalid ");
			throw new IllegalArgumentException("count [" + count + "] is invalid");
		}
	}

	public void validateId(int id) {
		if(id <= 0) {
			logger.error("ValidationService validateId Id [" + id + "] is invalid");
			throw new IllegalArgumentException("Id [" + id + "] is invalid");
		}
	}

	public void validateTenantId(int tenantId) throws ServiceValidationException {
		if(tenantId <= 0) {
			logger.error("ValidationService validateTenantId is invalid");
			throw new ServiceValidationException(ErrorCode.INVALID_TENANT_ID, "Tenant id is invalid");
		}
	}

	public void validateToken(String authCode) throws ServiceValidationException
	{
		if(authCode == null || authCode.length() < 38 || !authCode.startsWith("Basic "))
		{
			logger.error("ValidationService validateToken Token is invalid");
			throw new ServiceValidationException(ErrorCode.INVALID_TOKEN, "Token is invalid");
		}

		String token = authCode.substring(6);

		if(token.length() != 32)
		{
			logger.error("ValidationService validateToken Token is invalid");
			throw new ServiceValidationException(ErrorCode.INVALID_TOKEN, "Token is invalid");
		}
	}

	public void validateRegistrationId(String customerId) throws ServiceValidationException {
		if(!isUUID(customerId))
		{
			logger.error("ValidationService validateRegistrationId Invalid customer Uuid ");
			throw new ServiceValidationException(ErrorCode.INVALID_CUSTOMER_UUID, "Invalid customer Uuid");
		}
	}

	public void validatePage(PageInfo info)
	{
		if(info.getStart() < 0 || info.getCount() < 0)
		{
			logger.error("ValidationService validatePage Invalid page parameters");
			throw new IllegalArgumentException("Invalid page parameters");
		}
	}

	private boolean isUUID(String string) {
		try {
			UUID.fromString(string);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public void checkStringParam(String value, int minLength, int maxLength, String type) throws ServiceValidationException, PartnerAppException {

		if(value == null || value.length() == 0)
		{
			throwExceptionForType(type);
		}

		if(minLength != -1)
		{
			if(value.length() < minLength)
			{
				throwExceptionForType(type);
			}
		}

		if(maxLength != -1)
		{
			if(value.length() > maxLength)
			{
				throwExceptionForType(type);
			}
		}
	}

	public void throwExceptionForType(String type) throws ServiceValidationException, PartnerAppException
	{
		if (Configurations.CommonType.DEVICE_TOKEN.equals(type))
		{
			logger.error("ValidationService Device registration requires a valid device token");
			throw new ServiceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_DEVICE_TOKEN, "Device registration requires a valid device token");
		}
		else if (Configurations.CommonType.INSTALLATION_ID.equals(type))
		{
			logger.error("ValidationService Device registration requires a valid installation id");
			throw new ServiceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_INSTALLATION_ID, "Device registration requires a valid installation id");
		}
		else if (Configurations.CommonType.LOCALE.equals(type))
		{
			logger.error("ValidationService Device registration requires a valid locale");
			throw new ServiceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_LOCALE, "Device registration requires a valid locale");
		}
		else if (Configurations.CommonType.CHANNEL_ID.equals(type))
		{
			logger.error("ValidationService Device registration requires a valid channel id");
			throw new ServiceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_CHANNEL_ID, "Device registration requires a valid channel id");
		}
		else if (Configurations.CommonType.APP_VERSION.equals(type))
		{
			logger.error("ValidationService Device registration requires a valid app version");
			throw new ServiceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_APP_VERSION, "Device registration requires a valid app version");
		}
		else if (Configurations.CommonType.OS_VERSION.equals(type))
		{
			logger.error("ValidationService Device registration requires a valid os version");
			throw new ServiceValidationException(ErrorCode.DEVICE_REGISTRATION_REQUIRES_VALID_OS_VERSION, "Device registration requires a valid os version");
		}
		else if (Configurations.CommonType.ICCID_LENGTH.equals(type))
		{
			logger.error("ValidationService Event registration requires a valid iccid");
			throw new ServiceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_ICCID, "Event registration requires a valid iccid");
		}
		else if (Configurations.CommonType.CREATION_LENGTH.equals(type))
		{
			logger.error("ValidationService Event registration requires a valid creation time");
			throw new ServiceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_CREATION_TIME, "Event registration requires a valid creation time");
		}
		else if (Configurations.CommonType.EXPIRY_LENGTH.equals(type))
		{
			logger.error("ValidationService Event registration requires a valid expiry");
			throw new ServiceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_EXPIRY, "Event registration requires a valid expiry");
		}
		else if (Configurations.CommonType.IP_ADDRESSS_LENGTH.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid ip address");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_IP_ADDRESS, "Partner app device registration requires a valid ip address");
		}
		else if (Configurations.CommonType.PARTNER_APP_OS_VERSION.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid OS version");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_OS_VERSION, "Partner app device registration requires a valid OS version");
		}
		else if (Configurations.CommonType.PARTNER_APP_LOCALE.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid locale");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_LOCALE, "Partner app device registration requires a valid locale");
		}
		else if (Configurations.CommonType.PARTNER_APP_DATE.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid date");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_DATE, "Partner app device registration requires a valid date");
		}
		else if (Configurations.CommonType.PARTNER_APP_ICCID_LENGTH.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid iccid");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_ICCID, "Partner app device registration requires a valid iccid");
		}
		else if (Configurations.CommonType.PARTNER_APP_LOCATION.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid location");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_LOCATION, "Partner app device registration requires a valid location");
		}
		else if (Configurations.CommonType.PARTNER_APP_CODE.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid partner code");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_PARTNER_CODE, "Partner app device registration requires a valid partner code");
		}
		else if (Configurations.CommonType.PARTNER_APP_SDK.equals(type))
		{
			logger.error("ValidationService Partner app device registration requires a valid SDK version");
			throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_SDK, "Partner app device registration requires a valid SDK version");
		}
		else
		{
			logger.error("ValidationService Invalid input");
			throw new ServiceValidationException(ErrorCode.INVALID_ARGUMENTS, "Invalid input");
		}

	}
}
