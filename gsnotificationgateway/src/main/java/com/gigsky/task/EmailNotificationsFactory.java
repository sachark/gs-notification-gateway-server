package com.gigsky.task;

/**
 * Created by prsubbareddy on 29/12/16.
 */
public interface EmailNotificationsFactory {
    TaskInterface createNotificationTask(String name);
}
