package com.gigsky.task;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;

import com.gigsky.database.bean.PushNotificationDBBean;
import com.gigsky.database.dao.NotificationDao;
import com.gigsky.database.dao.PushNotificationDao;
import com.gigsky.database.dao.TenantConfigurationKeyValuesDao;
import com.urbanairship.api.client.APIClient;
import com.urbanairship.api.client.model.APIClientResponse;
import com.urbanairship.api.reports.model.SinglePushInfoResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * Created by prsubbareddy on 24/12/15.
 */
public class PeriodicPushNotificationReportTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(PeriodicPushNotificationReportTask.class);


    @Autowired
    PushNotificationDao pushNotificationDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    TenantConfigurationKeyValuesDao tenantConfigurationKeyValuesDao;

    protected PushNotificationDBBean pushNotificationDBBean;

    public PushNotificationDBBean getPushNotificationDBBean() {
        return pushNotificationDBBean;
    }

    public void setPushNotificationDBBean(PushNotificationDBBean pushNotificationDBBean) {
        this.pushNotificationDBBean = pushNotificationDBBean;
    }


    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        // Fetch tenant id using notification id from push notification bean.
        int tenantId = notificationDao.getNotificationDetails(pushNotificationDBBean.getNotificationId()).getTenantId();

        String appKey = tenantConfigurationKeyValuesDao.getConfigurationValue(Configurations.TenantConfigurationKeyValueType.UA_APP_KEY, tenantId);
        String appSecret = tenantConfigurationKeyValuesDao.getConfigurationValue(Configurations.TenantConfigurationKeyValueType.UA_APP_SECRET, tenantId);

        //String appKey = Configurations.UrbanAirshipConfigurationKeys.APP_KEY;
        //String appSecret = Configurations.UrbanAirshipConfigurationKeys.APP_SECRET;

        logger.debug(String.format("Make sure key and secret are set Key:%s Secret:%s",
                appKey, appSecret));

        APIClient apiClient = APIClient.newBuilder()
                .setKey(appKey)
                .setSecret(appSecret)
                .build();

        String pushId = pushNotificationDBBean.getPushId();

        try
        {
            APIClientResponse<SinglePushInfoResponse> response = apiClient.listIndividualPushResponseStatistics(pushId);

            SinglePushInfoResponse obj = response.getApiResponse();

            // Push Time
            Date pushTime = obj.getPushTime().toDate();
            String pushTimeUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(pushTime);

            // Converting push time to timestamp
            Timestamp pushTimeStamp = CommonUtils.getCurrentTimestampInUTC(pushTimeUTC);

            // Direct Responses
            int directResponses = obj.getDirectResponses();

            // Sends
            int sends = obj.getSends();

            pushNotificationDBBean.setPushTime(pushTimeStamp);
            pushNotificationDBBean.setSendCount(sends);
            pushNotificationDBBean.setOpenCount(directResponses);

            int lastAttemptCount = 0;
            int maxAttemptCount = 3;

            if(pushNotificationDBBean.getAttemptCount() != null)
            {
                lastAttemptCount = pushNotificationDBBean.getAttemptCount().intValue();
            }

            if(lastAttemptCount == maxAttemptCount)
            {
                pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_COMPLETE);
            }
            else
            {
                if(sends == 0 || directResponses == 0)
                {
                    pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_IN_PROCESS);
                    pushNotificationDBBean.setAttemptCount(lastAttemptCount+1);
                }
                else
                {
                    pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_COMPLETE);
                }
            }
            // updating push notification object
            pushNotificationDao.updatePushNotification(pushNotificationDBBean);

        }
        catch (Exception e)
        {
            logger.debug("getting push reports for individual push id failed for: "+pushId);

            int lastAttemptCount = 0;
            int maxAttemptCount = 3;

            if(pushNotificationDBBean.getAttemptCount() != null)
            {
                lastAttemptCount = pushNotificationDBBean.getAttemptCount().intValue();
            }

            if(lastAttemptCount < maxAttemptCount)
            {
                pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_IN_PROCESS);
                pushNotificationDBBean.setAttemptCount(lastAttemptCount+1);
            }
            else
            {
                pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_FAILED);
            }

            // updating push notification object
            pushNotificationDao.updatePushNotification(pushNotificationDBBean);
        }
    }
}
