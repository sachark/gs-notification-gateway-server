package com.gigsky.task;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.composer.MessageComposer;
import com.gigsky.database.bean.DeviceDBBean;
import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.bean.SupportedClientDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.event.BaseEvent;
import com.gigsky.handler.GSResult;
import com.gigsky.handler.NotificationServiceHandler;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by anant on 27/10/15.
 */
public class PushNotificationTask implements TaskInterface {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PushNotificationTask.class);

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    DeviceDao deviceDao;

    @Autowired
    MessageComposer messageComposer;

    @Autowired
    NotificationServiceHandler notificationServiceHandler;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    PolicyDao policyDao;

    @Autowired
    SupportedClientDao supportedClientDao;

    @Autowired
    GroupDao groupDao;

    protected String customerUuid;
    protected BaseEvent baseEvent;
    protected BaseNotification baseNotification;
    protected int policyId;

    public String getCustomerUUid() {
        return customerUuid;
    }

    public void setCustomerUUid(String customerUUid) {
        this.customerUuid = customerUUid;
    }

    public BaseEvent getBaseEvent() {
        return baseEvent;
    }

    public void setBaseEvent(BaseEvent baseEvent) {
        this.baseEvent = baseEvent;
    }

    public BaseNotification getBaseNotification() {
        return baseNotification;
    }

    public void setBaseNotification(BaseNotification baseNotification) {
        this.baseNotification = baseNotification;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }


    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }


    @Override
    public void run() {

        // Check against expiry time...if expired, need to update DB and need not send push notification.
        // Exclude event of type Subscription expired as the expiry time will always be older timestamp
        Timestamp expTime =  baseNotification.getExpiryTime();
        Timestamp currentUtcTime = CommonUtils.getCurrentTimestampInUTC();
        if(!(Configurations.EventType.TYPE_SUBSCRIPTION_EXPIRED.equals(baseEvent.getEventType())) &&
                (expTime != null) && expTime.getTime() <= currentUtcTime.getTime())
        {
            // Changing Notification status to EXPIRED and update to DB
            NotificationDBBean notificationDBBean =  notificationDao.getNotificationDetails(baseNotification.getNotificationId());
            notificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_EXPIRED);
            notificationDao.updateNotification(notificationDBBean);
        }
        else
        {
            // handle push notification task for customer
            handleTaskForCustomer(customerUuid);
        }
    }


    // handling push notification task for customer
    private void handleTaskForCustomer(String customerUUid)
    {
        // sorted customer devices based on last access time.
        List<DeviceDBBean> customerDevices = deviceDao.getDevicesForCustomer(customerUUid);
        // Preparing final list of devices based on
        // supporting version and max count of devices.
        List<DeviceDBBean> finalListDevices = getFinalListDevices(customerDevices);
        // list of languages from final list of devices
        // to prepare push notification message
        List<String> langList = getLanguageList(finalListDevices);

        if(langList != null)
        {
            for (Iterator<String > it = langList.iterator(); it.hasNext(); )
            {
                String lang = it.next();

                try {
                    NotificationMessage msg =  messageComposer.composeMessage(baseEvent, baseNotification, lang);
                    logger.debug("Push notification task message for customer_id " + customerUuid + " and for lang: " + lang + " is : " + msg);

                    // call notification service handler
                    handleServiceCall(msg, lang, finalListDevices);

                } catch (Exception e){
                    logger.error("composeMessage got Exception: " +e + " for event_id "+baseEvent.getEventId());
                }
            }
        } else
        {
            // Changing Notification status to COMPLETE and update to DB
            NotificationDBBean notificationDBBean =  notificationDao.getNotificationDetails(baseNotification.getNotificationId());
            notificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_NOT_DELIVERED);
            notificationDao.updateNotification(notificationDBBean);
        }
    }

    // Preparing final list of devices up to maximum count
    private List<DeviceDBBean> getFinalListDevices(List<DeviceDBBean> customerDevices) {
        int maxCount = 3;
        int count = 0;

        //config key value to specify device count to consider..
        String countValue = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.DEVICE_COUNT);
        if(countValue != null && countValue.length() > 0)
        {
            maxCount = Integer.parseInt(countValue);
        }

        List<DeviceDBBean> finalListDevices = new ArrayList<DeviceDBBean>();

        if(customerDevices != null && customerDevices.size() > 0)
        {
            for (Iterator<DeviceDBBean> it = customerDevices.iterator(); it.hasNext(); )
            {
                DeviceDBBean deviceDBBean = it.next();

                //Check if location permission is enabled for that device in case of New Location notification
                boolean isLocationEnabled = checkLocationPermission(deviceDBBean);
                if (isLocationEnabled == false)
                {
                    //Device is not enabled for location permission in case of New Location notification
                    continue;
                }

                String version = deviceDBBean.getAppVersion();
                SupportedClientDBBean supportedClientDBBean = supportedClientDao.getSupportClientInfo(policyId, deviceDBBean.getDeviceType());

                if (supportedClientDBBean != null) {

                    String supportVersion = supportedClientDBBean.getMinClientVersion();

                    DefaultArtifactVersion supportedArtifactVersion = new DefaultArtifactVersion(supportVersion);

                    DefaultArtifactVersion artifactVersion = new DefaultArtifactVersion(version);


                    if (supportVersion == null || supportVersion.length() == 0 || artifactVersion.compareTo(supportedArtifactVersion) >= 0) {
                        if (count == maxCount) {
                            return finalListDevices;
                        } else {
                            count++;
                            finalListDevices.add(deviceDBBean);
                        }
                    }
                }
            }
            return finalListDevices;
        }
        return null;
    }

    // Preparing list of languages
    private List<String> getLanguageList(List<DeviceDBBean> finalCustomerDevices)
    {
        List<String> langList = new ArrayList<String>();

        if(finalCustomerDevices != null && finalCustomerDevices.size() > 0)
        {
            for (Iterator<DeviceDBBean> it = finalCustomerDevices.iterator(); it.hasNext(); )
            {
                DeviceDBBean deviceDBBean = it.next();

                String lang = deviceDBBean.getLocale();
                if(! langList.contains(lang))
                {
                    langList.add(lang);
                }
            }
            return langList;
        }

        return null;
    }

    private void handleServiceCall(NotificationMessage msg, String lang, List<DeviceDBBean> finalListDevices)
    {
        if(finalListDevices != null && finalListDevices.size() > 0) {
            for (Iterator<DeviceDBBean> it = finalListDevices.iterator(); it.hasNext(); ) {

                DeviceDBBean deviceDBBean = it.next();

                if(lang != null && lang.equals(deviceDBBean.getLocale()))
                {
                    logger.trace("calling mock push service handler");
                    GSResult result = notificationServiceHandler.handleService(msg, baseEvent, baseNotification, deviceDBBean, null);
                    logger.debug("Push notification task handler result: " +result);
                }
            }
        }
    }

    private boolean checkLocationPermission(DeviceDBBean deviceDBBean)
    {
        if(Configurations.EventType.TYPE_NEW_LOCATION.equals(baseEvent.getEventType()))
        {
            //It is a new location event. See the device preferences
            String preferences = deviceDBBean.getPreferences();

            if ((preferences != null) && (!preferences.isEmpty()))
            {
                try
                {
                    JSONObject preferenceData = new JSONObject(preferences);
                    if (preferenceData.has("location"))
                    {
                        String locationPermission = preferenceData.get("location").toString();
                        if (!locationPermission.isEmpty())
                        {
                            //Check if it is DENY then return false
                            if (locationPermission.equals(Configurations.CommonType.DENY))
                            {
                                return false;
                            }
                        }
                    }
                }
                catch (JSONException e)
                {
                    logger.error("checkLocationPermission json parsing preferences error:"+ e);
                }
            }
        }

        return true;
    }

}