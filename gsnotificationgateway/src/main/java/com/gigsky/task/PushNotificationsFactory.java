package com.gigsky.task;

/**
 * Created by vinayr on 14/04/16.
 */
public interface PushNotificationsFactory {
    TaskInterface createNotificationTask(String name);
}
