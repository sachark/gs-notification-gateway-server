package com.gigsky.template;

import com.gigsky.database.bean.TemplateDBBean;
import com.gigsky.database.dao.TemplateDao;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by anant on 28/10/15.
 */
public class NotificationTemplateFactory {

    @Autowired
    private TemplateDao templateDao;

    public TemplateDBBean getTemplate(int eventTypeId, int notificationTypeId, int tenantId, String locale){
        return templateDao.getTemplate(eventTypeId,notificationTypeId,tenantId,locale);
    }
}
