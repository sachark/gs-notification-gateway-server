package util;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by prsubbareddy on 13/11/17.
 */
public class GSCountries {

    private static final Logger logger = LoggerFactory.getLogger(GSCountries.class);

    private static String countriesInfo = "{\"type\":\"CountryList\",\"startIndex\":0,\"count\":175,\"totalCount\":175,\"list\":[{\"type\":\"country\",\"name\":\"Afghanistan\",\"code\":\"AF\"},{\"type\":\"country\",\"name\":\"Albania\",\"code\":\"AL\"}," +
            "{\"type\":\"country\",\"name\":\"Algeria\",\"code\":\"DZ\"},{\"type\":\"country\",\"name\":\"American Samoa\",\"code\":\"AS\"},{\"type\":\"country\",\"name\":\"Anguilla\",\"code\":\"AI\"}," +
            "{\"type\":\"country\",\"name\":\"Antigua and Barbuda\",\"code\":\"AG\"},{\"type\":\"country\",\"name\":\"Argentina\",\"code\":\"AR\"},{\"type\":\"country\",\"name\":\"Armenia\",\"code\":\"AM\"}," +
            "{\"type\":\"country\",\"name\":\"Aruba\",\"code\":\"AW\"},{\"type\":\"country\",\"name\":\"Australia\",\"code\":\"AU\"},{\"type\":\"country\",\"name\":\"Austria\",\"code\":\"AT\"}," +
            "{\"type\":\"country\",\"name\":\"Azerbaijan\",\"code\":\"AZ\"},{\"type\":\"country\",\"name\":\"Bahamas\",\"code\":\"BS\"},{\"type\":\"country\",\"name\":\"Bahrain\",\"code\":\"BH\"}," +
            "{\"type\":\"country\",\"name\":\"Bangladesh\",\"code\":\"BD\"},{\"type\":\"country\",\"name\":\"Barbados\",\"code\":\"BB\"},{\"type\":\"country\",\"name\":\"Belarus\",\"code\":\"BY\"},{" +
            "\"type\":\"country\",\"name\":\"Belgium\",\"code\":\"BE\"},{\"type\":\"country\",\"name\":\"Belize\",\"code\":\"BZ\"},{\"type\":\"country\",\"name\":\"Bermuda\",\"code\":\"BM\"}," +
            "{\"type\":\"country\",\"name\":\"Bhutan\",\"code\":\"BT\"},{\"type\":\"country\",\"name\":\"Bolivia\",\"code\":\"BO\"},{\"type\":\"country\",\"name\":\"Bonaire, Sint Eustatius, and Saba\",\"code\":\"BQ\"}," +
            "{\"type\":\"country\",\"name\":\"Bosnia and Herzegovina\",\"code\":\"BA\"},{\"type\":\"country\",\"name\":\"Botswana\",\"code\":\"BW\"},{\"type\":\"country\",\"name\":\"Brazil\",\"code\":\"BR\"}," +
            "{\"type\":\"country\",\"name\":\"British Virgin Islands\",\"code\":\"VG\"},{\"type\":\"country\",\"name\":\"Bulgaria\",\"code\":\"BG\"},{\"type\":\"country\",\"name\":\"Burkina Faso\",\"code\":\"BF\"}," +
            "{\"type\":\"country\",\"name\":\"Burundi\",\"code\":\"BI\"},{\"type\":\"country\",\"name\":\"Cambodia\",\"code\":\"KH\"},{\"type\":\"country\",\"name\":\"Cameroon\",\"code\":\"CM\"}," +
            "{\"type\":\"country\",\"name\":\"Canada\",\"code\":\"CA\"},{\"type\":\"country\",\"name\":\"Cape Verde\",\"code\":\"CV\"},{\"type\":\"country\",\"name\":\"Cayman Islands\",\"code\":\"KY\"}," +
            "{\"type\":\"country\",\"name\":\"Chad\",\"code\":\"TD\"},{\"type\":\"country\",\"name\":\"Chile\",\"code\":\"CL\"},{\"type\":\"country\",\"name\":\"China\",\"code\":\"CN\"}," +
            "{\"type\":\"country\",\"name\":\"Christmas Island\",\"code\":\"CX\"},{\"type\":\"country\",\"name\":\"Colombia\",\"code\":\"CO\"},{\"type\":\"country\",\"name\":\"Costa Rica\",\"code\":\"CR\"}," +
            "{\"type\":\"country\",\"name\":\"Côte d'Ivoire\",\"code\":\"CI\"},{\"type\":\"country\",\"name\":\"Croatia\",\"code\":\"HR\"},{\"type\":\"country\",\"name\":\"Curaçao\",\"code\":\"CW\"}," +
            "{\"type\":\"country\",\"name\":\"Cyprus\",\"code\":\"CY\"},{\"type\":\"country\",\"name\":\"Czech Republic\",\"code\":\"CZ\"},{\"type\":\"country\",\"name\":\"Denmark\",\"code\":\"DK\"}," +
            "{\"type\":\"country\",\"name\":\"Dominica\",\"code\":\"DM\"},{\"type\":\"country\",\"name\":\"Dominican Republic\",\"code\":\"DO\"},{\"type\":\"country\",\"name\":\"Ecuador\",\"code\":\"EC\"}," +
            "{\"type\":\"country\",\"name\":\"Egypt\",\"code\":\"EG\"},{\"type\":\"country\",\"name\":\"El Salvador\",\"code\":\"SV\"},{\"type\":\"country\",\"name\":\"Estonia\",\"code\":\"EE\"}," +
            "{\"type\":\"country\",\"name\":\"Faroe Islands\",\"code\":\"FO\"},{\"type\":\"country\",\"name\":\"Fiji\",\"code\":\"FJ\"},{\"type\":\"country\",\"name\":\"Finland\",\"code\":\"FI\"}," +
            "{\"type\":\"country\",\"name\":\"France\",\"code\":\"FR\"},{\"type\":\"country\",\"name\":\"French Guiana\",\"code\":\"GF\"},{\"type\":\"country\",\"name\":\"French Polynesia\",\"code\":\"PF\"}," +
            "{\"type\":\"country\",\"name\":\"Georgia\",\"code\":\"GE\"},{\"type\":\"country\",\"name\":\"Germany\",\"code\":\"DE\"},{\"type\":\"country\",\"name\":\"Ghana\",\"code\":\"GH\"}," +
            "{\"type\":\"country\",\"name\":\"Gibraltar\",\"code\":\"GI\"},{\"type\":\"country\",\"name\":\"Greece\",\"code\":\"GR\"},{\"type\":\"country\",\"name\":\"Grenada\",\"code\":\"GD\"}," +
            "{\"type\":\"country\",\"name\":\"Guadeloupe\",\"code\":\"GP\"},{\"type\":\"country\",\"name\":\"Guam\",\"code\":\"GU\"},{\"type\":\"country\",\"name\":\"Guatemala\",\"code\":\"GT\"}," +
            "{\"type\":\"country\",\"name\":\"Guernsey\",\"code\":\"GG\"},{\"type\":\"country\",\"name\":\"Guyana\",\"code\":\"GY\"},{\"type\":\"country\",\"name\":\"Haiti\",\"code\":\"HT\"}," +
            "{\"type\":\"country\",\"name\":\"Honduras\",\"code\":\"HN\"},{\"type\":\"country\",\"name\":\"Hong Kong\",\"code\":\"HK\"},{\"type\":\"country\",\"name\":\"Hungary\",\"code\":\"HU\"}," +
            "{\"type\":\"country\",\"name\":\"Iceland\",\"code\":\"IS\"},{\"type\":\"country\",\"name\":\"India\",\"code\":\"IN\"},{\"type\":\"country\",\"name\":\"Indonesia\",\"code\":\"ID\"}," +
            "{\"type\":\"country\",\"name\":\"Iraq\",\"code\":\"IQ\"},{\"type\":\"country\",\"name\":\"Ireland\",\"code\":\"IE\"},{\"type\":\"country\",\"name\":\"Isle of Man\",\"code\":\"IM\"}," +
            "{\"type\":\"country\",\"name\":\"Israel\",\"code\":\"IL\"},{\"type\":\"country\",\"name\":\"Italy\",\"code\":\"IT\"},{\"type\":\"country\",\"name\":\"Jamaica\",\"code\":\"JM\"}," +
            "{\"type\":\"country\",\"name\":\"Japan\",\"code\":\"JP\"},{\"type\":\"country\",\"name\":\"Jersey\",\"code\":\"JE\"},{\"type\":\"country\",\"name\":\"Kazakhstan\",\"code\":\"KZ\"}," +
            "{\"type\":\"country\",\"name\":\"Kenya\",\"code\":\"KE\"},{\"type\":\"country\",\"name\":\"Kuwait\",\"code\":\"KW\"},{\"type\":\"country\",\"name\":\"Kyrgyzstan\",\"code\":\"KG\"},{" +
            "\"type\":\"country\",\"name\":\"Laos\",\"code\":\"LA\"},{\"type\":\"country\",\"name\":\"Latvia\",\"code\":\"LV\"},{\"type\":\"country\",\"name\":\"Liberia\",\"code\":\"LR\"}," +
            "{\"type\":\"country\",\"name\":\"Liechtenstein\",\"code\":\"LI\"},{\"type\":\"country\",\"name\":\"Lithuania\",\"code\":\"LT\"},{\"type\":\"country\",\"name\":\"Luxembourg\",\"code\":\"LU\"}," +
            "{\"type\":\"country\",\"name\":\"Macau\",\"code\":\"MO\"},{\"type\":\"country\",\"name\":\"Madagascar\",\"code\":\"MG\"},{\"type\":\"country\",\"name\":\"Malaysia\",\"code\":\"MY\"}," +
            "{\"type\":\"country\",\"name\":\"Malta\",\"code\":\"MT\"},{\"type\":\"country\",\"name\":\"Martinique\",\"code\":\"MQ\"},{\"type\":\"country\",\"name\":\"Mexico\",\"code\":\"MX\"}," +
            "{\"type\":\"country\",\"name\":\"Moldova\",\"code\":\"MD\"},{\"type\":\"country\",\"name\":\"Mongolia\",\"code\":\"MN\"},{\"type\":\"country\",\"name\":\"Montenegro\",\"code\":\"ME\"}," +
            "{\"type\":\"country\",\"name\":\"Montserrat\",\"code\":\"MS\"},{\"type\":\"country\",\"name\":\"Morocco\",\"code\":\"MA\"},{\"type\":\"country\",\"name\":\"Myanmar\",\"code\":\"MM\"}," +
            "{\"type\":\"country\",\"name\":\"Namibia\",\"code\":\"NA\"},{\"type\":\"country\",\"name\":\"Nauru\",\"code\":\"NR\"},{\"type\":\"country\",\"name\":\"Nepal\",\"code\":\"NP\"}," +
            "{\"type\":\"country\",\"name\":\"Netherlands\",\"code\":\"NL\"},{\"type\":\"country\",\"name\":\"New Zealand\",\"code\":\"NZ\"},{\"type\":\"country\",\"name\":\"Nicaragua\",\"code\":\"NI\"}," +
            "{\"type\":\"country\",\"name\":\"Nigeria\",\"code\":\"NG\"},{\"type\":\"country\",\"name\":\"Norway\",\"code\":\"NO\"},{\"type\":\"country\",\"name\":\"Oman\",\"code\":\"OM\"}," +
            "{\"type\":\"country\",\"name\":\"Pakistan\",\"code\":\"PK\"},{\"type\":\"country\",\"name\":\"Palestine\",\"code\":\"PS\"},{\"type\":\"country\",\"name\":\"Panama\",\"code\":\"PA\"}," +
            "{\"type\":\"country\",\"name\":\"Papua New Guinea\",\"code\":\"PG\"},{\"type\":\"country\",\"name\":\"Paraguay\",\"code\":\"PY\"},{\"type\":\"country\",\"name\":\"Peru\",\"code\":\"PE\"}," +
            "{\"type\":\"country\",\"name\":\"Philippines\",\"code\":\"PH\"},{\"type\":\"country\",\"name\":\"Poland\",\"code\":\"PL\"},{\"type\":\"country\",\"name\":\"Portugal\",\"code\":\"PT\"}," +
            "{\"type\":\"country\",\"name\":\"Puerto Rico\",\"code\":\"PR\"},{\"type\":\"country\",\"name\":\"Qatar\",\"code\":\"QA\"},{\"type\":\"country\",\"name\":\"Réunion\",\"code\":\"RE\"}," +
            "{\"type\":\"country\",\"name\":\"Romania\",\"code\":\"RO\"},{\"type\":\"country\",\"name\":\"Russia\",\"code\":\"RU\"},{\"type\":\"country\",\"name\":\"Rwanda\",\"code\":\"RW\"}," +
            "{\"type\":\"country\",\"name\":\"Saint Barthélemy\",\"code\":\"BL\"},{\"type\":\"country\",\"name\":\"Saint Kitts and Nevis\",\"code\":\"KN\"},{\"type\":\"country\",\"name\":\"Saint Lucia\",\"code\":\"LC\"}," +
            "{\"type\":\"country\",\"name\":\"Saint Martin\",\"code\":\"MF\"},{\"type\":\"country\",\"name\":\"Saint Vincent and the Grenadines\",\"code\":\"VC\"},{\"type\":\"country\",\"name\":\"Samoa\",\"code\":\"WS\"}," +
            "{\"type\":\"country\",\"name\":\"San Marino\",\"code\":\"SM\"},{\"type\":\"country\",\"name\":\"Saudi Arabia\",\"code\":\"SA\"},{\"type\":\"country\",\"name\":\"Senegal\",\"code\":\"SN\"}," +
            "{\"type\":\"country\",\"name\":\"Serbia\",\"code\":\"RS\"},{\"type\":\"country\",\"name\":\"Sierra Leone\",\"code\":\"SL\"},{\"type\":\"country\",\"name\":\"Singapore\",\"code\":\"SG\"}," +
            "{\"type\":\"country\",\"name\":\"Sint Maarten\",\"code\":\"SX\"},{\"type\":\"country\",\"name\":\"Slovakia\",\"code\":\"SK\"},{\"type\":\"country\",\"name\":\"Slovenia\",\"code\":\"SI\"}," +
            "{\"type\":\"country\",\"name\":\"South Africa\",\"code\":\"ZA\"},{\"type\":\"country\",\"name\":\"South Korea\",\"code\":\"KR\"},{\"type\":\"country\",\"name\":\"Spain\",\"code\":\"ES\"}," +
            "{\"type\":\"country\",\"name\":\"Sri Lanka\",\"code\":\"LK\"},{\"type\":\"country\",\"name\":\"Sudan\",\"code\":\"SD\"},{\"type\":\"country\",\"name\":\"Suriname\",\"code\":\"SR\"}," +
            "{\"type\":\"country\",\"name\":\"Svalbard and Jan Mayen\",\"code\":\"SJ\"},{\"type\":\"country\",\"name\":\"Swaziland\",\"code\":\"SZ\"},{\"type\":\"country\",\"name\":\"Sweden\",\"code\":\"SE\"}," +
            "{\"type\":\"country\",\"name\":\"Switzerland\",\"code\":\"CH\"},{\"type\":\"country\",\"name\":\"Taiwan\",\"code\":\"TW\"},{\"type\":\"country\",\"name\":\"Tajikistan\",\"code\":\"TJ\"}," +
            "{\"type\":\"country\",\"name\":\"Thailand\",\"code\":\"TH\"},{\"type\":\"country\",\"name\":\"Tonga\",\"code\":\"TO\"},{\"type\":\"country\",\"name\":\"Trinidad and Tobago\",\"code\":\"TT\"}," +
            "{\"type\":\"country\",\"name\":\"Turkey\",\"code\":\"TR\"},{\"type\":\"country\",\"name\":\"Turks and Caicos Islands\",\"code\":\"TC\"},{\"type\":\"country\",\"name\":\"U.S. Virgin Islands\",\"code\":\"VI\"}," +
            "{\"type\":\"country\",\"name\":\"Uganda\",\"code\":\"UG\"},{\"type\":\"country\",\"name\":\"Ukraine\",\"code\":\"UA\"},{\"type\":\"country\",\"name\":\"United Kingdom\",\"code\":\"GB\"}," +
            "{\"type\":\"country\",\"name\":\"United States\",\"code\":\"US\"},{\"type\":\"country\",\"name\":\"Uruguay\",\"code\":\"UY\"},{\"type\":\"country\",\"name\":\"Uzbekistan\",\"code\":\"UZ\"}," +
            "{\"type\":\"country\",\"name\":\"Vatican City\",\"code\":\"VA\"},{\"type\":\"country\",\"name\":\"Venezuela\",\"code\":\"VE\"},{\"type\":\"country\",\"name\":\"Vietnam\",\"code\":\"VN\"}," +
            "{\"type\":\"country\",\"name\":\"Yemen\",\"code\":\"YE\"},{\"type\":\"country\",\"name\":\"Zimbabwe\",\"code\":\"ZW\"}]}";

    //TODO: fetch countries list info using API cal at least once a day

    public static boolean isCountryCodeMatched(String countryCode) {

        try {
            JSONObject countriesObject = new JSONObject(countriesInfo);
            JSONArray countriesList = countriesObject.getJSONArray("list");

            for(int i = 0; i < countriesList.length(); i++) {

                JSONObject countriesInfo =  countriesList.getJSONObject(i);
                String cCode = countriesInfo.optString("code");
                if(countryCode.toLowerCase().equals(cCode.toLowerCase()))
                {
                    return true;
                }
            }
        }
        catch (Exception e) {
            logger.error("Countries parsing exception:" +e.getMessage());
        }
        return false;
    }
}
