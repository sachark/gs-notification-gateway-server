package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.DeviceDBBean;
import com.gigsky.rest.bean.Device;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by vinayr on 20/10/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class CustomerDaoTest {
    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private DeviceDao deviceDao;

    protected String testCustomerUuid;
    protected int testDeviceId;

    @Before
    public void init() throws Exception {

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "bc81d4fae-7dec-11d0-a765-00a0c91e6ab1";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());

        //Create the device for running the suite
        DeviceDBBean testDevice = new DeviceDBBean();
        testDevice.setDeviceType(Configurations.ANDROID_TYPE);
        testDevice.setDeviceOsVersion("4.1");
        testDevice.setModel("MotoG");
        testDevice.setChannelId("1df6e8c7-5a73-47c0-9716-99fd3d419bc9");
        testDevice.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFL12G");
        testDevice.setLocale("en");
        testDevice.setAppVersion("2.6");

        testDeviceId = deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice);
        Assert.assertNotNull(testDevice.getId());
    }

    @After
    public void tearDown() throws Exception {

        //Delete the device if it still exists
        DeviceDBBean testDevice = deviceDao.getDevice(testDeviceId);

        if (testDevice != null)
        {
            deviceDao.deleteDeviceForCustomer(testDevice);
        }

        //Delete the customer if it still exists
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }
    }

    @Test
    public void testCreateCustomerInfo() throws Exception {

        CustomerDBBean testCustomer = createCustomerData();

        customerDao.createCustomerInfo(testCustomer);
        Assert.assertNotNull(customerDao);
        Assert.assertNotNull(testCustomer.getId());
    }

    @Test
    public void testGetCustomerInfo() throws Exception {

        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);
        Assert.assertNotNull(testCustomer);
    }

    @Test
    public void testUpdateCustomerInfo() throws Exception {

        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);
        Assert.assertNotNull(testCustomer);

        testCustomer.setCustomerType("ENTERPRISE");

        customerDao.updateCustomerInfo(testCustomer);

        CustomerDBBean testCustomer2 = customerDao.getCustomerInfo(testCustomerUuid);

        Assert.assertEquals("ENTERPRISE", testCustomer2.getCustomerType());
    }

    @Test
    public void testGetDevicesForCustomer() throws Exception {

        int startIndex = 0;
        int count = 3;
        List<DeviceDBBean> customerDevices = deviceDao.getDevicesForCustomer(testCustomerUuid, startIndex, count);
        Assert.assertNotNull(customerDevices);
    }

    @Test
    public void testDeleteCustomerInfo() throws Exception {

        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);
        Assert.assertNotNull(testCustomer);

        customerDao.deleteCustomerInfo(testCustomer);

        CustomerDBBean testCustomer2 = customerDao.getCustomerInfo(testCustomerUuid);
        Assert.assertNull(testCustomer2);
    }

    @Test
    public void testGetDeviceForCustomer() throws Exception {

        Device customerDevice = deviceDao.getDeviceForCustomer(testCustomerUuid, testDeviceId);
        Assert.assertNotNull(customerDevice);
    }


    protected CustomerDBBean createCustomerData()
    {
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerDBBean.setCustomerUuid("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");
        return testCustomerDBBean;
    }
}