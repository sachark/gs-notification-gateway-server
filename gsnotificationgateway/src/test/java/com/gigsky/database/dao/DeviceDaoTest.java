package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.DeviceDBBean;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by vinayr on 21/10/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class DeviceDaoTest {

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private CustomerDao customerDao;

    protected String testCustomerUuid;
    protected int testDeviceId;
    protected String testChannelId;
    protected String testInstallationId;

    @Before
    public void init() throws Exception{

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "pr51d4fae-7dec-11d0-a765-00a0c91e5718";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());

        //Create the device for running the suite
        DeviceDBBean testDevice = new DeviceDBBean();
        testDevice.setDeviceType(Configurations.ANDROID_TYPE);
        testDevice.setDeviceOsVersion("4.1");
        testDevice.setModel("iPhone");
        testChannelId = "1df6e8c7-5a73-47c0-9716-99fd3d419bc9";
        testDevice.setChannelId(testChannelId);
        testInstallationId = "11111111-1111-1111";
        testDevice.setInstallationId(testInstallationId);
        testDevice.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFL12G");
        testDevice.setLocale("en");
        testDevice.setAppVersion("2.6");

        testDeviceId = deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice);
        Assert.assertNotNull(testDevice.getId());
    }

    @After
    public void tearDown() throws Exception {

        //Delete the device if it still exists
        DeviceDBBean testDevice = deviceDao.getDevice(testDeviceId);

        if (testDevice != null)
        {
            deviceDao.deleteDeviceForCustomer(testDevice);
        }

        //Delete the customer if it still exists
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }
    }

    @Test
    public void testAddDevice() throws Exception {

        DeviceDBBean testDevice = getTestDevice();

        deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice);
        Assert.assertNotNull(testDevice.getId());
    }

    @Test
    public void testAddDeviceForCustomer() throws Exception {

        DeviceDBBean testDevice = getTestDevice();

        deviceDao.addDevice(testDevice);
        Assert.assertNotNull(testDevice.getId());

        deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice.getId());
        Assert.assertNotNull(testDevice.getId());
    }



    @Test (expected = Exception.class)
    public void testAddDeviceWithWrongCustomerId() throws Exception {

        DeviceDBBean testDevice = getTestDevice();

        String customerUuid = "f81d4fae-7dec-11d0-a765-00a0c91e6bf0";

        deviceDao.addDeviceForCustomer(customerUuid, testDevice);
    }

    @Test
    public void testUpdateDevice() throws Exception {

        DeviceDBBean testDevice = deviceDao.getDevice(testDeviceId);
        testDevice.setLocale("ja");

        deviceDao.updateDeviceForCustomer(testCustomerUuid, testDevice);
    }

    @Test
    public void testGetDevice() throws Exception {

        DeviceDBBean testDevice = deviceDao.getDevice(testDeviceId);
        Assert.assertNotNull(testDevice);
    }

    @Test
    public void testGetDeviceForCustomer() throws Exception {

        DeviceDBBean testDevice = deviceDao.getDevice(testInstallationId, testChannelId, testChannelId);
        Assert.assertNotNull(testDevice);
    }

    @Test
    public void testDeleteDevice() throws Exception {

        DeviceDBBean testDevice = deviceDao.getDevice(testDeviceId);
        Assert.assertNotNull(testDevice);

        deviceDao.deleteDeviceForCustomer(testDevice);

        DeviceDBBean testDevice2 = deviceDao.getDevice(testDeviceId);
        Assert.assertNull(testDevice2);
    }

    private DeviceDBBean getTestDevice() {

        DeviceDBBean testDevice = new DeviceDBBean();
        testDevice.setDeviceType(Configurations.IOS_TYPE);
        testDevice.setDeviceOsVersion("9.0.1");
        testDevice.setModel("iPhone");
        testDevice.setChannelId("9c36e8c7-5a73-47c0-9716-99fd3d4197d5");
        testDevice.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFE008");
        testDevice.setLocale("en");
        testDevice.setAppVersion("2.6");
        return testDevice;
    }
}
