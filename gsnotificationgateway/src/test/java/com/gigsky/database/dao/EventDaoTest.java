package com.gigsky.database.dao;

import com.gigsky.database.bean.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by vinayr on 21/10/15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class EventDaoTest {

    @Autowired
    private EventDao eventDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private PolicyDao policyDao;

    protected int testEventTypeId;
    protected int testEventId;
    protected String testCustomerUuid;
    protected int testNotificationTypeId;
    protected int testPolicyId;

    @Before
    public void init() throws Exception{

        //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = new EventTypeDBBean();
        testEventType.setDescription("Subcription Low balance");
        testEventType.setType("LOW_BALANCE");
        testEventType.setEnableNotification((byte) 1);
        testEventTypeId = eventDao.createEventType(testEventType);
        Assert.assertNotNull(testEventTypeId);

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "Bc81d4fae-7dec-11d0-a765-00a0c91e6kl1";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());


        //Create event required for running the test suite
        EventDBBean testEvent = new EventDBBean();
        testEvent.setStatus("NEW");
        testEvent.setEventTypeId(testEventTypeId);
        testEvent.setEventCategory("SINGLE_EVENT");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testEvent.setReceivedTime(newTimeStamp);

        String eventData = getEventData();

        testEventId = eventDao.createEvent(testEvent, testCustomerUuid, eventData);
        Assert.assertNotNull(testEventId);

        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = new NotificationTypeDBBean();
        testNotificationType.setDescription("Push notification");
        testNotificationType.setType("PUSH");
        testNotificationTypeId = notificationDao.createNotificationType(testNotificationType);
        Assert.assertNotNull(testNotificationTypeId);

        //Create a policy
        PolicyDBBean policy = new PolicyDBBean();
        policy.setDescription("Push notificationPolicy");
        policy.setNotificationTypeId(testNotificationTypeId);
        testPolicyId = policyDao.createPolicy(policy);
        Assert.assertNotNull(testPolicyId);

        //Set the entry for policy in the Event policy table
        eventDao.setPolicyForEventType(testEventTypeId, testPolicyId);
    }

    @After
    public void tearDown() throws Exception {

        //Delete the event created
        EventDBBean testEvent = eventDao.getEventDetails(testEventId, 1);

        if (testEvent != null)
        {
            eventDao.deleteEventDetails(testEvent);
        }

        //Delete the event type created
        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        if (testEventType != null)
        {
            eventDao.deleteEventType(testEventType);
        }


        //Delete the customer
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }

        //Delete notification type
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType(testNotificationTypeId);

        if (testNotificationType != null)
        {
            notificationDao.deleteNotificationType(testNotificationType);
        }

        //Delete Policy
        PolicyDBBean testPolicy = policyDao.getPolicy(testPolicyId);

        if (testPolicy != null)
        {
            policyDao.deletePolicy(testPolicy);
        }
    }


    @Test
    public void testCreateEvent() throws Exception {

        EventDBBean testEvent = getEvent();
        String eventData = getEventData();

        eventDao.createEvent(testEvent, testCustomerUuid, eventData);
        Assert.assertNotNull(testEvent.getId());
    }

    @Test
    public void testGetEventDetails() throws Exception {

        EventDBBean testEvent = eventDao.getEventDetails(testEventId, 1);
        Assert.assertNotNull(testEvent);
        Assert.assertNotNull(testEvent.getCustomerEvent());
    }

    @Test
    public void testUpdateEventDetails() throws Exception {

        EventDBBean testEvent = eventDao.getEventDetails(testEventId, 1);
        Assert.assertNotNull(testEvent);

        testEvent.setStatus("IN_PROCESS");

        testEvent.getCustomerEvent().setData("{\"userId\":\"abc@gigsky.com\",\"location\":\"IN\"}");

        eventDao.updateEventDetails(testEvent);

        EventDBBean testEvent2 = eventDao.getEventDetails(testEventId, 1);
        Assert.assertEquals("IN_PROCESS", testEvent2.getStatus());
    }

    @Test
    public void testDeleteEventDetails() throws Exception {

        EventDBBean testEvent = eventDao.getEventDetails(testEventId, 1);
        Assert.assertNotNull(testEvent);

        eventDao.deleteEventDetails(testEvent);

        EventDBBean testEvent2 = eventDao.getEventDetails(testEventId, 1);
        Assert.assertNull(testEvent2);
    }

    @Test
    public void testGetPoliciesForEventType() throws Exception {

        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        Set<PolicyDBBean> policies = eventDao.getPoliciesForEventType(testEventType.getType());
        Assert.assertNotNull(policies);
    }

    private EventDBBean getEvent() {

        EventDBBean testEvent = new EventDBBean();
        testEvent.setStatus("NEW");
        testEvent.setEventTypeId(testEventTypeId);
        testEvent.setEventCategory("SINGLE_EVENT");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testEvent.setReceivedTime(newTimeStamp);

        return testEvent;
    }


    private String getEventData() {
        String eventData = "{\"userId\":\"xyz@gigsky.com\",\"location\":\"US\"}";
        return eventData;
    }
}
