package com.gigsky.database.dao;

import com.gigsky.database.bean.MockUserDBBean;
import com.gigsky.database.bean.MockUserTokenDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by vinayr on 23/11/15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class MockUserDaoTest {

    @Autowired
    private MockUserDao mockUserDao;

    protected int testMockUserId;
    protected int testMockUserTokenId;
    protected String testBasicToken = "122-122";

    @Before
    public void init() {

        MockUserDBBean mockUserDBBean = new MockUserDBBean();
        mockUserDao.createMockUser(mockUserDBBean);
        Assert.assertNotNull(mockUserDBBean.getUserId());
        testMockUserId = mockUserDBBean.getUserId();

        MockUserTokenDBBean mockUserToken = new MockUserTokenDBBean();
        mockUserToken.setUserId(testMockUserId);
        mockUserToken.setBasicToken(testBasicToken);
        mockUserDao.createMockUserToken(mockUserToken);
        Assert.assertNotNull(mockUserToken.getId());
        testMockUserTokenId = mockUserToken.getId();

    }

    @After
    public void tearDown() throws Exception {

        MockUserDBBean mockUser = mockUserDao.getMockUser(testMockUserId, 1);

        if (mockUser != null)
        {
            mockUserDao.deleteMockUser(mockUser);
        }

        MockUserTokenDBBean mockUserToken = mockUserDao.getMockUserToken(testMockUserTokenId, 1);

        if (mockUserToken != null)
        {
            mockUserDao.deleteMockUserToken(mockUserToken);
        }
    }


    @Test
    public void testCreateMockUser() throws Exception {

        MockUserDBBean mockUserDBBean = new MockUserDBBean();
        mockUserDao.createMockUser(mockUserDBBean);
        Assert.assertNotNull(mockUserDBBean.getUserId());
    }

    @Test
    public void testGetMockUser() throws Exception {

        MockUserDBBean mockUser = mockUserDao.getMockUser(testMockUserId, 1);
        Assert.assertNotNull(mockUser);
    }

    @Test
    public void testUpdateMockUser() throws Exception {
        MockUserDBBean mockUser = mockUserDao.getMockUser(testMockUserId, 1);
        Assert.assertNotNull(mockUser);

        mockUser.setUserUuid("123-121212-1112112");
        mockUserDao.updateMockUser(mockUser);

        MockUserDBBean mockUser2 = mockUserDao.getMockUser(testMockUserId, 1);
        Assert.assertNotNull(mockUser2);

        Assert.assertEquals("123-121212-1112112", mockUser2.getUserUuid());
    }

    //MockUserToken Tests
    @Test
    public void testCreateMockUserToken() throws Exception {

        MockUserTokenDBBean mockUserToken = new MockUserTokenDBBean();
        mockUserToken.setUserId(testMockUserId);
        mockUserToken.setBasicToken("121-121");

        mockUserDao.createMockUserToken(mockUserToken);
        Assert.assertNotNull(mockUserToken.getUserId());
    }

    @Test
    public void testGetMockUserToken() throws Exception {

        int mockUserId = mockUserDao.getMockUserId(testBasicToken, 1);
        Assert.assertEquals(testMockUserId, mockUserId);
    }

}
