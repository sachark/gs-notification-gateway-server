package com.gigsky.executor;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vinayr on 17/11/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class TimerEventsExecutorTest {

    @Autowired
    private TimerEventsExecutor timerEventsExecutor;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private DeviceDao deviceDao;



    protected String testCustomerUuid;
    protected int testDeviceId1;

    protected int testEventTypeId;
    protected int testEventId;
    protected int testNotificationTypeId;

    @Before
    public void init() throws Exception {

       //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = eventDao.getEventType("LOW_BALANCE");
        testEventTypeId = testEventType.getId();
        Assert.assertNotNull(testEventTypeId);

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "bc81d4fae-7dec-11d0-a765-00a0c91e6ab1";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());

        //Create event required for running the test suite
        EventDBBean testEvent = new EventDBBean();
        testEvent.setStatus("NEW");
        testEvent.setEventTypeId(testEventTypeId);
        testEvent.setFailCount(1);
        testEvent.setEventCategory("SINGLE_EVENT");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testEvent.setReceivedTime(newTimeStamp);

        String eventData = getEventData();

        testEventId = eventDao.createEvent(testEvent, testCustomerUuid, eventData);
        Assert.assertNotNull(testEventId);

        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType("PUSH");
        testNotificationTypeId = testNotificationType.getId();
        Assert.assertNotNull(testNotificationTypeId);

        //Create the device for running the suite
        DeviceDBBean testDevice1 = new DeviceDBBean();
        testDevice1.setDeviceType(Configurations.ANDROID_TYPE);
        testDevice1.setDeviceOsVersion("6.0");
        testDevice1.setModel("Nexus5");
        testDevice1.setChannelId("3d3d5279-5a30-4b86-b1fa-dac12b00f153");// for ios 97284c1d-64ae-451b-a773-a3d92a8776b6 and for android 3d3d5279-5a30-4b86-b1fa-dac12b00f153
        testDevice1.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFL12G");
        testDevice1.setLocale("en");
        testDevice1.setAppVersion("2.6");

        testDeviceId1 = deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice1);
        Assert.assertNotNull(testDevice1.getId());

    }

    @Test
    public void testTaskExecutor() throws Exception {

        timerEventsExecutor.start();
    }


    @After
    public void tearDown() throws Exception {

        //Delete the device if it still exists
        DeviceDBBean testDevice1 = deviceDao.getDevice(testDeviceId1);

        if (testDevice1 != null)
        {
            deviceDao.deleteDeviceForCustomer(testDevice1);
        }

        //Delete the customer if it still exists
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }
    }

    private String getEventData() {

        Timestamp currentUtcTimeStamp = CommonUtils.getCurrentTimestampInUTC();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(currentUtcTimeStamp.getTime()));
        cal.add(Calendar.HOUR_OF_DAY, 1);
        Timestamp timestamp = new Timestamp(cal.getTime().getTime());

        String eventData = "{\"balanceRemaining\":\"1024\",\"location\":\"US\",\"iccId\":\"8910300000000000000\"," +
                "\"expireTime\":\""+timestamp.toString()+"\"}";
        return eventData;
    }
}
