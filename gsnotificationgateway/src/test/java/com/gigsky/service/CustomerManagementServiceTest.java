package com.gigsky.service;

import com.gigsky.auth.UserManagementInterface;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.rest.bean.Customer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anant on 30/10/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class CustomerManagementServiceTest {

    @Autowired
    private CustomerManagementService customerManagementService;

    @Autowired
    private UserManagementInterface userManagementInterface;

    @Autowired
    private CustomerDao customerDao;

    private Map<String,Integer> userList = new HashMap<String,Integer>();

    private String getValidToken()
    {
        return "ABCDEFGH00001";
    }

    private String getInvalidToken()
    {
        return "XYZSDSDSDF0001";
    }

    @Before
    public void init() {

        userList.clear();
        userList.put("ABCDEFGH00001",1001);
        userManagementInterface.initUserInfo(userList);

    }

    @After
    public void tearDown() throws Exception
    {
        userList.clear();
        userManagementInterface.clearUserInfo();
    }

    @Test
    public void testGetCustomerDetails() throws Exception {

        String customerId = null;
        Customer customerDetails = customerManagementService.getCustomerDetails(getValidToken(), 1);

        Assert.notNull(customerDetails);
        Assert.notNull(customerDetails.getCustomerId());
        customerId = customerDetails.getCustomerId();

        customerDetails = customerManagementService.getCustomerDetails(getValidToken(), 1);
        Assert.isTrue(customerDetails.getCustomerId().equals(customerId));

    }


}
