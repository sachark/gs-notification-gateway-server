package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class EventServiceTest {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private TemplateDao templateDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private PolicyDao policyDao;

    @Autowired
    private SupportedClientDao supportedClientDao;

    @Autowired
    EventService eventService;

    protected String testCustomerUuid;
    protected int testDeviceId1;
    protected int testDeviceId2;

    protected int testEventTypeId;
    protected int testEventId;
    protected int testNotificationTypeId;
    protected int testTemplateId;
    protected int testPolicyId;
    protected int testSupportClientId;


    @Before
    public void init() throws Exception {

        //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = new EventTypeDBBean();
        testEventType.setDescription("Subcription Low balance");
        testEventType.setType("LOW_BALANCE");
        testEventType.setEnableNotification((byte) 1);
        testEventTypeId = eventDao.createEventType(testEventType);
        Assert.assertNotNull(testEventTypeId);

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "bc81d4fae-7dec-11d0-a765-00a0c91e6ab1";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());

        //Create event required for running the test suite
        EventDBBean testEvent = new EventDBBean();
        testEvent.setStatus("NEW");
        testEvent.setEventTypeId(testEventTypeId);
        testEvent.setEventCategory("SINGLE_EVENT");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testEvent.setReceivedTime(newTimeStamp);

        String eventData = getEventData();

        testEventId = eventDao.createEvent(testEvent, testCustomerUuid, eventData);
        Assert.assertNotNull(testEventId);

        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = new NotificationTypeDBBean();
        testNotificationType.setDescription("Push notification");
        testNotificationType.setType("PUSH");
        testNotificationTypeId = notificationDao.createNotificationType(testNotificationType);
        Assert.assertNotNull(testNotificationTypeId);

        //Create a policy
        PolicyDBBean policy = new PolicyDBBean();
        policy.setDescription("Push notificationPolicy");
        policy.setNotificationTypeId(testNotificationTypeId);
        testPolicyId = policyDao.createPolicy(policy);
        Assert.assertNotNull(testPolicyId);

        SupportedClientDBBean supportedClientDBBean = new SupportedClientDBBean();
        supportedClientDBBean.setPolicyId(testPolicyId);
        supportedClientDBBean.setClientType(Configurations.ANDROID_TYPE);
        supportedClientDBBean.setMinClientVersion("2.6");

        //Set the entry for policy in the Event policy table
        eventDao.setPolicyForEventType(testEventTypeId, testPolicyId);

        testSupportClientId = supportedClientDao.createSupportClientInfo(supportedClientDBBean);
        Assert.assertNotNull(testSupportClientId);

        //Create the device for running the suite
        DeviceDBBean testDevice1 = new DeviceDBBean();
        testDevice1.setDeviceType(Configurations.ANDROID_TYPE);
        testDevice1.setDeviceOsVersion("6.0");
        testDevice1.setModel("Nexus5");
        testDevice1.setChannelId("3d3d5279-5a30-4b86-b1fa-dac12b00f153");// for ios 97284c1d-64ae-451b-a773-a3d92a8776b6 and for android 3d3d5279-5a30-4b86-b1fa-dac12b00f153
        testDevice1.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFL12G");
        testDevice1.setLocale("en");
        testDevice1.setAppVersion("2.6");

        testDeviceId1 = deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice1);
        Assert.assertNotNull(testDevice1.getId());

        DeviceDBBean testDevice2 = new DeviceDBBean();
        testDevice2.setDeviceType(Configurations.ANDROID_TYPE);
        testDevice2.setDeviceOsVersion("5.1");
        testDevice2.setModel("MotoG");
        testDevice2.setChannelId("3d3d5279-5a30-4b86-b1fa-dac12b00f153");// for ios 97284c1d-64ae-451b-a773-a3d92a8776b6 and for android 3d3d5279-5a30-4b86-b1fa-dac12b00f153
        testDevice2.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFL12G");
        testDevice2.setLocale("ja");
        testDevice2.setAppVersion("2.6");

        testDeviceId2 = deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice2);
        Assert.assertNotNull(testDevice2.getId());

        //Create Template
        TemplateDBBean templateDBBean = new TemplateDBBean();
        templateDBBean.setDescription("Push notification");
        templateDBBean.setContentType("PUSH");
        templateDBBean.setContent("{\"title\":\"Low Balance\",\"message\":\"Data balance is %s for location %s\"}");
        //templateDBBean.setContent("{\"title\":\"Nearing Expiry\",\"message\":\"Data balance is %s for location %s\"}");
        //templateDBBean.setContent("{\"title\":\"New Location\",\"message\":\"Welcome to location %s\"}");

        templateDBBean.setLanguageCode("en");

        testTemplateId = templateDao.createTemplate(templateDBBean);
        Assert.assertNotNull(testTemplateId);

        //Create notification template entry
        templateDao.createTemplate(testEventTypeId, testNotificationTypeId, testTemplateId);

        templateDBBean.setContent("{\"title\":\"Low Balance Ja\",\"message\":\"Data balance in Japanese is %s for location %s\"}");
        templateDBBean.setLanguageCode("ja");

        testTemplateId = templateDao.createTemplate(templateDBBean);
        Assert.assertNotNull(testTemplateId);

        //Create notification template entry
        templateDao.createTemplate(testEventTypeId, testNotificationTypeId, testTemplateId);

    }

    @After
    public void tearDown() throws Exception
    {
        //Delete the device if it still exists
        DeviceDBBean testDevice1 = deviceDao.getDevice(testDeviceId1);

        if (testDevice1 != null)
        {
            deviceDao.deleteDeviceForCustomer(testDevice1);
        }

        DeviceDBBean testDevice2 = deviceDao.getDevice(testDeviceId2);

        if (testDevice2 != null)
        {
            deviceDao.deleteDeviceForCustomer(testDevice2);
        }

        //Delete the customer if it still exists
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }

        //Delete Support client info
        SupportedClientDBBean testSupportInfo = supportedClientDao.getSupportClientInfo(testPolicyId, "Iphone");
        if(testSupportInfo != null)
        {
            supportedClientDao.deleteSupportedClientInfo(testSupportInfo);
        }

        //Delete the event type created
        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        if (testEventType != null)
        {
            eventDao.deleteEventType(testEventType);
        }

        //Delete notification type
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType(testNotificationTypeId);

        if (testNotificationType != null)
        {
            notificationDao.deleteNotificationType(testNotificationType);
        }

        //Delete Policy
        PolicyDBBean testPolicy = policyDao.getPolicy(testPolicyId);

        if (testPolicy != null)
        {
            policyDao.deletePolicy(testPolicy);
        }

        //Delete template
        TemplateDBBean testTemplate = templateDao.getTemplateDetails(testTemplateId);

        if (testTemplate != null)
        {
            templateDao.deleteTemplate(testTemplate);
        }
    }

    @Test
    public void testProcessEvents() throws Exception {
        eventService.processEvents();
    }

    private String getEventData() {
        String eventData = "{\"balanceRemaining\":\"1024\",\"location\":\"US\",\"openCount\":\"1\",\"iccId\":\"8910300000000000000\"," +
                "\"sendCount\":\"1\",\"languageCode\":\"en\",\"attemptCount\":\"1\",\"notificationCategory\":\"SINGLE_NOTIFICATION\"}";
        return eventData;
    }
}