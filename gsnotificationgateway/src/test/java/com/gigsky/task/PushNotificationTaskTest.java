package com.gigsky.task;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.event.CustomerEvent;
import com.gigsky.notification.BaseNotification;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class PushNotificationTaskTest {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private DeviceDao deviceDao;

    @Autowired
    private TemplateDao templateDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private PolicyDao policyDao;

    @Autowired
    private PushNotificationTask pushNotificationTask;

    @Autowired
    private SupportedClientDao supportedClientDao;


    protected String testCustomerUuid;
    protected int testDeviceId1;
    protected int testDeviceId2;

    protected int testEventTypeId;
    protected int testEventId;
    protected int testNotificationTypeId;
    protected int testTemplateId1;
    protected int testTemplateId2;
    protected int testPolicyId;
    protected int testSupportClientId;

    private BaseNotification baseNotification;
    private CustomerEvent customerEvent;

    @Before
    public void init() throws Exception
    {
        //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = new EventTypeDBBean();
        testEventType.setDescription("Subcription Low balance");
        testEventType.setType("LOW_BALANCE");
        testEventType.setEnableNotification((byte) 1);
        testEventTypeId = eventDao.createEventType(testEventType);
        Assert.assertNotNull(testEventTypeId);

        customerEvent = new CustomerEvent();
        customerEvent.setEventType(testEventType.getType());
        customerEvent.setEventTypeId(testEventTypeId);

        JSONObject jsonData = new JSONObject();
        jsonData.put("balanceRemaining","1024");
        jsonData.put("location","US");
        customerEvent.setEventData(jsonData);
        customerEvent.setEventCategory(Configurations.CategoryType.CATEGORY_CUSTOMER);

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "bc81d4fae-7dec-11d0-a765-00a0c91e6ab1";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());

        //Create event required for running the test suite
        EventDBBean testEvent = new EventDBBean();
        testEvent.setStatus("NEW");
        testEvent.setEventTypeId(testEventTypeId);
        testEvent.setEventCategory("SINGLE_EVENT");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testEvent.setReceivedTime(newTimeStamp);

        String eventData = getEventData();

        testEventId = eventDao.createEvent(testEvent, testCustomerUuid, eventData);
        Assert.assertNotNull(testEventId);

        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = new NotificationTypeDBBean();
        testNotificationType.setDescription("Push notification");
        testNotificationType.setType("PUSH");
        testNotificationTypeId = notificationDao.createNotificationType(testNotificationType);
        Assert.assertNotNull(testNotificationTypeId);

        baseNotification = new BaseNotification();
        baseNotification.setNotificationType(testNotificationType.getType());
        baseNotification.setNotificationTypeId(testNotificationTypeId);
        baseNotification.setExpiryTime(CommonUtils.getCurrentTimestampInUTC());

        //Create a policy
        PolicyDBBean policy = new PolicyDBBean();
        policy.setDescription("Push notificationPolicy");
        policy.setNotificationTypeId(testNotificationTypeId);
        testPolicyId = policyDao.createPolicy(policy);
        Assert.assertNotNull(testPolicyId);

        SupportedClientDBBean supportedClientDBBean = new SupportedClientDBBean();
        supportedClientDBBean.setPolicyId(testPolicyId);
        supportedClientDBBean.setClientType(Configurations.IOS_TYPE);
        supportedClientDBBean.setMinClientVersion("2.6");

        //Set the entry for policy in the Event policy table
        eventDao.setPolicyForEventType(testEventTypeId, testPolicyId);

        testSupportClientId = supportedClientDao.createSupportClientInfo(supportedClientDBBean);
        Assert.assertNotNull(testSupportClientId);

        //Create the device for running the suite
        DeviceDBBean testDevice1 = new DeviceDBBean();
        testDevice1.setDeviceType(Configurations.IOS_TYPE);
        testDevice1.setDeviceOsVersion("9.0.2");
        testDevice1.setModel("iPhone6");
        testDevice1.setChannelId("1df6e8c7-5a73-47c0-9716-99fd3d419bc9");
        testDevice1.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFL12G");
        testDevice1.setLocale("en");
        testDevice1.setAppVersion("2.6");

        testDeviceId1 = deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice1);
        Assert.assertNotNull(testDevice1.getId());

        DeviceDBBean testDevice2 = new DeviceDBBean();
        testDevice2.setDeviceType(Configurations.IOS_TYPE);
        testDevice2.setDeviceOsVersion("9.0.1");
        testDevice2.setModel("iPhone5");
        testDevice2.setChannelId("1df6e8c7-5a73-47c0-9716-99fd3d419bc9");
        testDevice2.setDeviceToken("645A5C6C06AFB2AE095B079135168A04A5F974FBF27163F3EC6FE1F2D5AFL12G");
        testDevice2.setLocale("ja");
        testDevice2.setAppVersion("2.6");

        testDeviceId2 = deviceDao.addDeviceForCustomer(testCustomerUuid, testDevice2);
        Assert.assertNotNull(testDevice2.getId());

        //Create Template
        TemplateDBBean templateDBBean = new TemplateDBBean();
        templateDBBean.setDescription("Push notification");
        templateDBBean.setContentType("PUSH");
        templateDBBean.setContent("{\"title\":\"Low Balance\",\"message\":\"Data balance is %s for location %s\"}");
        //templateDBBean.setContent("{\"title\":\"Nearing Expiry\",\"message\":\"Data balance is %s for location %s\"}");
        //templateDBBean.setContent("{\"title\":\"New Location\",\"message\":\"Welcome to location %s\"}");

        templateDBBean.setLanguageCode("en");

        testTemplateId1 = templateDao.createTemplate(templateDBBean);
        Assert.assertNotNull(testTemplateId1);

        templateDBBean.setContent("{\"title\":\"Low Balance Ja\",\"message\":\"Data balance in Japanese is %s for location %s\"}");
        templateDBBean.setLanguageCode("ja");


        testTemplateId2 = templateDao.createTemplate(templateDBBean);
        Assert.assertNotNull(testTemplateId2);

        //Create notification template entry
        templateDao.createTemplate(testEventTypeId, testNotificationTypeId, testTemplateId1);

        //Create notification template entry
        templateDao.createTemplate(testEventTypeId, testNotificationTypeId, testTemplateId2);
    }

    @After
    public void tearDown()
    {
        //Delete the device if it still exists
        DeviceDBBean testDevice1 = deviceDao.getDevice(testDeviceId1);

        if (testDevice1 != null)
        {
            deviceDao.deleteDeviceForCustomer(testDevice1);
        }

        DeviceDBBean testDevice2 = deviceDao.getDevice(testDeviceId2);

        if (testDevice2 != null)
        {
            deviceDao.deleteDeviceForCustomer(testDevice2);
        }

        //Delete the customer if it still exists
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }

        //Delete Support client info
        SupportedClientDBBean testSupportInfo = supportedClientDao.getSupportClientInfo(testPolicyId, "Iphone");
        if(testSupportInfo != null)
        {
            supportedClientDao.deleteSupportedClientInfo(testSupportInfo);
        }

        //Delete the event type created
        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        if (testEventType != null)
        {
            eventDao.deleteEventType(testEventType);
        }

        //Delete notification type
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType(testNotificationTypeId);

        if (testNotificationType != null)
        {
            notificationDao.deleteNotificationType(testNotificationType);
        }

        //Delete Policy
        PolicyDBBean testPolicy = policyDao.getPolicy(testPolicyId);

        if (testPolicy != null)
        {
            policyDao.deletePolicy(testPolicy);
        }

        //Delete template
        TemplateDBBean testTemplate1 = templateDao.getTemplateDetails(testTemplateId1);

        if (testTemplate1 != null)
        {
            templateDao.deleteTemplate(testTemplate1);
        }

        //Delete template
        TemplateDBBean testTemplate2 = templateDao.getTemplateDetails(testTemplateId2);

        if (testTemplate2 != null)
        {
            templateDao.deleteTemplate(testTemplate2);
        }
    }

    @Test
    public void testRun() throws Exception {

        pushNotificationTask.setCustomerUUid(testCustomerUuid);
        pushNotificationTask.setBaseEvent(customerEvent);
        pushNotificationTask.setBaseNotification(baseNotification);
        pushNotificationTask.setPolicyId(testPolicyId);
        pushNotificationTask.run();
    }

    private String getEventData() {
        String eventData = "{\"userId\":\"xyz@gigsky.com\",\"location\":\"US\"}";
        return eventData;
    }
}