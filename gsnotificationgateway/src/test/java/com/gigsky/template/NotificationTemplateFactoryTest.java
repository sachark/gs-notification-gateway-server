package com.gigsky.template;

import com.gigsky.database.bean.TemplateDBBean;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class NotificationTemplateFactoryTest {

    @Autowired
    NotificationTemplateFactory notificationTemplateFactory;

    @Test
    public void testGetTemplate() throws Exception {
        String locale = getLocale();

        TemplateDBBean testTemplate =  notificationTemplateFactory.getTemplate(1, 1, 1, locale);
        Assert.assertNotNull(testTemplate);
    }

    public String getLocale() {
        return "en";
    }
}