package com.gigsky.abi;

public class AuthTokenInfo {
    private String token;
    private String customerId;
    private String simId;
    private String pin;

    public String getSimId() {
        return simId;
    }

    public void setSimId(String simId) {
        this.simId = simId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "AuthTokenInfo{" + "token=" + token + ", customerId=" + customerId + ", simId=" + simId + ", pin=" + pin + '}';
    }
}
