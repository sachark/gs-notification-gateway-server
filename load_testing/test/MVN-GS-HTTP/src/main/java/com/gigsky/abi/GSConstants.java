package com.gigsky.abi;

public interface GSConstants {
    // Endpoint constants
    public static final String HANDOFFTOKEN_DETAILS = "account/handOffToken";
    public static final String LOGIN = "account/login";
    public static final String LOGOUT = "account/logout";
    public static final String CREATE_ACCOUNT = "account";
    public static final String CONFIRM_ACCOUNT = "account/confirm/account";
    public static final String GET_SIMS = "account/{0}/SIM";
    public static final String GET_SIM_STATUS = "account/{0}/SIM/{1}/simStatus";
    public static final String GET_SIM = "account/{0}/SIM/{1}";
    public static final String GET_PAYMENT_STATUS = "account/{0}/paymentStatus";
    public static final String GET_PAYMENT_METHODS = "account/billing/paymentMethods";
    public static final String GET_SUBSCRIPTION_BY_NWID = "account/{0}/SIM/{1}/networkgroup/{2}/subscription";
    public static final String GET_SUBSCRIPTIONS = "account/{0}/SIM/{1}/subscriptions?type={2}";
    public static final String GET_SUBSCRIPTION_BY_ID = "account/{0}/SIM/{1}/subscriptions/{2}";
    public static final String CREDIT_CARD = "account/{0}/billing";
    public static final String GET_TRANSACTIONS = "account/{0}/credit/gigskyCredit/transactions";
    public static final String GET_TRANSACTION_BY_ID = "account/{0}/SIM/{1}/transactions/{2}";
    
    public static final String PLAN_SEARCH = "planSearch";

    public static final String LOCATIONS = "locations";
    public static final String CALL_DATA_RECORDS = "calldatarecords";

    public static final String FORBIDDEN_COUNTRIES = "countries/forbidden/buySameCountry";
    public static final String COUNTRIES_ALL = "countries/all";
    public static final String COUNTRIES = "countries";
    public static final String COUNTRY_NETWORK_GROUPS = "countries/{0}";
    public static final String NETWORK_GROUP_DETAILS = "networkGroups/{0}/details";
    public static final String NETWORK_GROUP_PLANS = "networkGroups/{0}/plans";
    public static final String NETWORK_GROUP_PLANS_EXT = "networkGroups/{0}/plansExt";
    public static final String BUY_PLAN = "account/{0}/SIM/{1}/transactions";
    public static final String GET_AVAILABLE_NETWORK_GROUPS =
        "account/{0}/SIM/{1}/availableNetworkGroups";
    
    public static final String GIGSKY_CREDIT =
        "account/{0}/credit/gigskyCredit";
    public static final String ADD_SIM = "account/{0}/SIM";

    public static final String TELNA_EVENT = "telnaevents";
    
    public static final String GET_ACCOUNT = "account/{0}";
    
    // Parameter constants
    public static final String START_INDEX = "startIndex";
    public static final String COUNT = "count";
    public static final String TOKEN = "token";
    public static final String AUTHORIZATION = "Authorization";
    
    //imsi 
    public static final String IMSI_LOGIN = "login";
    
    //Subscriber
    public static final String URL_SUBSCRIBER = "subscriber";
    public static final String URL_ADD_IMSI_ISDN = "ImsiMsisdn";
    public static final String URL_SUBSCRIBER_MAX_COUNTERS = "/balanceCounters";
    public static final String URL_SUBSCRIBER_COUNTERS = "/counters";
    public static final String URL_SUBSCRIBER_BALANCE = "/balance";
    public static final String URL_SUBSCRIBER_CAPTIVE_COUNTER = "/captiveCounter";
    public static final String URL_SUBSCRIBER_CALL_DATA_RECORDS = "/calldatarecords";

    //locations
    public static final String URL_LOCATIONS = "/locations";

    //notification gateway
    public static final String URL_NGW_EVENT = "events";
}
