package com.gigsky.abi;

public class GSException extends Exception {
    public GSException() {
        super();
    }
    
    public GSException(String message) {
        super(message);
    }
    
    
    public GSException(Throwable t) {
        super(t);
    }
}
