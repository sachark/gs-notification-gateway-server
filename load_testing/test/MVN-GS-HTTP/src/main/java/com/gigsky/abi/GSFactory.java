package com.gigsky.abi;

import com.gigsky.abi.handler.GSHandler;
import java.lang.reflect.Constructor;
import org.apache.log4j.Logger;

public class GSFactory {
    private static final Logger log = Logger.getLogger(GSFactory.class);
    
    public static <T> T get(Class<? extends GSHandler> handlerClass,
                            int userNumber) {
        T retVal = null;
        
        String packageName = handlerClass.getPackage().getName();
        
        String implClassName =
            packageName + ".impl." + handlerClass.getSimpleName() + "Impl";
        try {
            Class implClass = Class.forName(implClassName);
            
            Constructor<T> ctor = implClass.getConstructor(int.class);
            
            retVal = ctor.newInstance(userNumber);
        } catch (Exception ex) {
            // Should never happen
            ex.printStackTrace();
        }
        
        return retVal;
    }
}
