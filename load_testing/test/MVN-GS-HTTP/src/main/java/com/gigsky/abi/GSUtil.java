package com.gigsky.abi;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

public class GSUtil implements GSConstants {
    public static String smartCapitalize(String str) {
        String retVal = null;
        
        if (str != null && str.length() > 0) {
            boolean shouldCapitalize =
                Character.isLowerCase(str.charAt(1));

            char firstChar = shouldCapitalize ?
                Character.toUpperCase(str.charAt(0)) :
                str.charAt(0);
            
            retVal = "" + firstChar;
            
            if (str.length() > 1) {
                retVal = retVal + str.substring(1);
            }
        }
        
        return retVal;
    }

    public static String urlEncode(String original) {
        String retVal = null;

        if (valid(original)) {
            try {
                retVal = URLEncoder.encode(original, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                // Should not happen
                ex.printStackTrace();
            }
        }
        
        return retVal;
    }

    public static String urlDecode(String original) {
        String retVal = null;

        if (valid(original)) {
            try {
                retVal = URLDecoder.decode(original, "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                // Should not happen
                ex.printStackTrace();
            }
        }
        
        return retVal;
    }

    public static Integer[] splitDimenion(String value) {
        List<Integer> retVal = new ArrayList<>();
        
        if (valid(value)) {
            String[] values = value.split("x");

            if (values != null) {
                for (String str: values) {
                    retVal.add(Integer.parseInt(str));
                }
            }
        }
        
        return retVal.toArray(new Integer[0]);
    }
    
    public static int yearFromDateTime(long dateTime) {
        Calendar utcCalendar = utcCalendar();
        utcCalendar.setTimeInMillis(dateTime*1000);
        return utcCalendar.get(Calendar.YEAR)-1900;
    }
    
    public static Set<String> fromCSV(String value) {
        Set<String> retVal = new LinkedHashSet<>();
        
        addToCollection(value, retVal);
        
        return retVal;
    }

    public static Map<String, String> mapFromCSV(String value) {
        Map<String, String> retVal = new LinkedHashMap<>();
        
        addToCollection(value, retVal);
        
        return retVal;
    }

    public static String toCSV(Set<String> set) {
        StringBuilder retVal = new StringBuilder();
        
        boolean first = true;
        
        for (String item: set) {
            if (!first) {
                retVal.append(",");
            }
            
            retVal.append(item);
            
            first = false;
        }
        
        return retVal.toString();
    }
    
    public static String toCSV(Map<String, String> map) {
        StringBuilder retVal = new StringBuilder();
        
        boolean first = true;
        
        for (String key: map.keySet()) {
            if (!first) {
                retVal.append(",");
            }
            
            retVal.append(key).append("=").append(map.get(key));
            
            first = false;
        }
        
        return retVal.toString();
    }

    public static void delay(int seconds) {
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            // Ignore it
        }
    }

    public static int randomDelay(int ceiling) {
        int retVal = new Random(utcTime()).nextInt(ceiling);
        
        if (retVal == 0) {
            retVal = 1;
        }

        delay(ceiling);
        
        return retVal;
    }

    public static <T> Set<T> fromCSV(String value, Class<T> type) {
        Set<T> retVal = new LinkedHashSet<>();
        
        addToCollection(value, retVal, type);
        
        return retVal;
    }
    
    public static <T> Set<T> valueAsSet(T value) {
        Set<T> retVal = new HashSet<>();
        
        retVal.add(value);
        
        return retVal;
    }

    public static String generateId() {
        return uniqueString();
    }
    
    public static String uniqueString() {
        return UUID.randomUUID().toString();
    }
    
    public static Calendar utcCalendar() {
        return new GregorianCalendar(TimeZone.getTimeZone("GMT"));
    }
    
    public static Calendar laCalendar() {
        return
            new GregorianCalendar(TimeZone.getTimeZone("America/Los_Angeles"));
    }
    
    public static String formatDate(int secondsAfter) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatGmt.format(new Date(new Date().getTime()+secondsAfter));
    }
    
    public static long utcTime() {
        Calendar calendar = utcCalendar();
        return calendar.getTimeInMillis()/1000;
    }
    
    public static boolean valid(String str) {
        return str != null && str.trim().length() > 0;
    }
    
    public static boolean valid(Boolean b) {
        return b != null;
    }
    
    public static boolean valid(Number n) {
        return n != null;
    }
    
    public static boolean valid(Collection<?> data) {
        return data != null && data.size() > 0;
    }
    
    public static boolean valid(Map<?, ?> data) {
        return data != null && data.size() > 0;
    }
    
    public static boolean positive(int value) {
        return value > 0;
    }
    
    public static boolean positive(float f) {
        return f > 0;
    }
    
    public static String toString(Exception e) {
        String retVal = null;
        
        StringWriter sw = new StringWriter();

        try (PrintWriter pw = new PrintWriter(sw)) {
            e.printStackTrace(pw);
            sw.flush();
            retVal = sw.toString();
            try {
                sw.close();
            } catch (IOException ex) {
                // Should not happen
            }
        }
        return retVal;
    }

    private static <T> void addToCollection(String value,
                                            Collection<T> collection,
                                            Class<T> type) {
        if (valid(value)) {
            String[] strings = value.split(",");
            
            for (String str: strings) {
                String trimmedValue = null;
                
                if (str != null) {
                    trimmedValue = str.trim();
                }
                
                T val = null;
                
                if (type.equals(String.class)) {
                    val = (T) trimmedValue;
                } else if (type.equals(Integer.class)) {
                    val = (T) Integer.valueOf(trimmedValue);
                }
                
                collection.add(val);
            }
        }
    }

    private static void addToCollection(String value,
                                        Collection<String> collection) {
        if (valid(value)) {
            String[] strings = value.split(",");
            
            for (String str: strings) {
                String trimmedValue = null;
                
                if (str != null) {
                    trimmedValue = str.trim();
                }
                if (valid(trimmedValue)) {
                    collection.add(trimmedValue);
                }
            }
        }
    }

    private static void addToCollection(String value,
                                        Map<String, String> map) {
        if (valid(value)) {
            String[] strings = value.split(",");
            
            for (String str: strings) {
                String trimmedValue = null;
                
                if (str != null) {
                    trimmedValue = str.trim();
                }
                if (valid(trimmedValue)) {
                    String[] keyValue = trimmedValue.split("=");
                    
                    map.put(keyValue[0], keyValue[1]);
                }
            }
        }
    }
}
