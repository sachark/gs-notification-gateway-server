package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.accounts.v4.beans.*;
import com.gigsky.data.AccountCreationInput;
import com.gigsky.data.LoginInput;
import org.apache.jmeter.samplers.SampleResult;

public interface AccountHandler extends GSHandler {
    public AccountCreationResV4 createAccount(SampleResult sampleResult,
                                              AccountCreationInput input)
        throws GSException;
    public LoginResposeV4 confirmAccount(SampleResult sampleResult,
                                         String token)
        throws GSException;
    public LoginResposeV4 login(SampleResult sampleResult, LoginInput input)
        throws GSException;
    public HandOffTokenResponse getHandoffTokenDetails(SampleResult samplResult, HandOffTokenRequest request) throws GSException;
    public AccountsV4 getAccount(SampleResult sampleResult) throws GSException;
    public void logout(SampleResult sampleResult) throws GSException;
}
