package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.restapi.bean.CallDataRecords;
import com.gigsky.restapi.bean.Locations;
import org.apache.jmeter.samplers.SampleResult;

public interface EventHandler extends GSHandler {
    public Locations getLocations(SampleResult sampleResult) throws GSException;
    public CallDataRecords getCallDataRecords(SampleResult sampleResult)
        throws GSException;
}
