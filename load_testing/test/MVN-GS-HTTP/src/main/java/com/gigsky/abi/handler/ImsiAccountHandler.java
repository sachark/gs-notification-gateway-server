package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.data.LoginInput;
import com.gigsky.restapi.bean.LoginResponse;
import org.apache.jmeter.samplers.SampleResult;

public interface ImsiAccountHandler extends GSHandler {
    public LoginResponse loginIMSI(SampleResult sampleResult,
                                   LoginInput input) throws GSException;
}
