package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.accounts.v4.beans.*;
import com.gigsky.data.AddSimInput;
import org.apache.jmeter.samplers.SampleResult;

public interface SimHandler extends GSHandler {
    public SimDetailsV4 getSims(SampleResult sampleResult,
                                String customerId,
                                int startIndex,
                                int count) throws GSException;
    
    public SimV4 getSim(SampleResult sampleResult,
                        String customerId,
                        String simId) throws GSException;
    
    public SimStatusV4 getSimStatus(SampleResult sampleResult,
                                    String customerId,
                                    String simId) throws GSException;
    
    public AvailableNetworkGroups getAvailableNetworkGroups(
        SampleResult sampleResult, String customerId, String simId)
        throws GSException;
    
    public void addSim(SampleResult sampleResult,
                       String customerId,
                       AddSimInput addSimInput) throws GSException;

    public SubscriptionsV4 getSubscriptionOfSimById(SampleResult sampleResult,
                                                    String customerId,
                                                    String simId,
                                                    long subscriptionId) throws GSException;

    public SubscriptionsList getSubscriptionsOfSim(SampleResult sampleResult,
                                               String customerId,
                                               String simId,
                                               String type) throws GSException;

    public SubscriptionsV4 getSubscriptionsOfSimForNetworkGroup(SampleResult sampleResult,
                                                                String customerId,
                                                                String simId,
                                                                String networkGroupId,
                                                                String type) throws GSException;
}
