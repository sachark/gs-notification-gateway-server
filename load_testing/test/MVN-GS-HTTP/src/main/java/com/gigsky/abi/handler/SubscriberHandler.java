package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.data.BodyInput;
import com.gigsky.data.LocationEventSchema;
import com.gigsky.data.SubscriberInput;
import com.gigsky.restapi.bean.BalanceCounters;
import com.gigsky.restapi.bean.Subscriber;
import org.apache.jmeter.samplers.SampleResult;

import java.util.List;

public interface SubscriberHandler extends GSHandler {
    public void addSubscriber(SampleResult sampleResult,
                              SubscriberInput input) throws GSException;

    public Subscriber getSubscriber(SampleResult sampleResult,
                                    String subscriberId) throws GSException;

    public BalanceCounters getSubscriberMaximumCounters(
        SampleResult subscriberMaximumCounters,
        String subId) throws GSException;

    public void deleteSubscriber(SampleResult deleteSubscriber, String subId)
        throws GSException;

    public void setCaptivePortalBalance(SampleResult setCaptivePortalBalance,
                                        String subId,
                                        BodyInput bodyInput) throws GSException;
    
    public void setSubscriberOrTopUpBalance(
        SampleResult setSubscriberOrTopUpBalance,
        String subId,
        String counterIndex,
        BodyInput bodyInput) throws GSException;

    public void setSubscriberCaptivePortalBalance (
        SampleResult setSubscriberCaptivePortalBalance,
        String customerId,
        String token,
        BodyInput bodyInput,
        SubscriberInput subscriberInput) throws GSException;

    public void resetBalanceOfAllCounters(
        SampleResult resetSubscriberBalanceOfAllCounters,
        String subId,
        BodyInput bodyInput) throws GSException;

    public void resetBalanceOfACounter(
        SampleResult resetSubscriberBalanceOfACounters,
        String subId,
        String resetCounterIndex,
        BodyInput balanceOfACountersBodyInput) throws GSException;

    public void addLocation(SampleResult location,
                            List<LocationEventSchema> schemas)
        throws GSException;
    
}
