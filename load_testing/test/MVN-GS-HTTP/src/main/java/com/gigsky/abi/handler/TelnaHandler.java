package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import org.apache.jmeter.samplers.SampleResult;

public interface TelnaHandler extends GSHandler {
    public void cdrUpdate(SampleResult sampleResult, String xml)
        throws GSException;
    
    public void locationUpdate(SampleResult sampleResult, String xml)
        throws GSException;
}
