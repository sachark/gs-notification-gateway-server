package com.gigsky.abi.handler.impl;

import com.gigsky.abi.AuthTokenInfo;
import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.AccountHandler;
import com.gigsky.abi.http.NVPair;
import com.gigsky.abi.http.ServerType;
import com.gigsky.accounts.v4.beans.*;
import com.gigsky.data.AccountCreationInput;
import com.gigsky.data.LoginInput;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

import java.text.MessageFormat;

public class AccountHandlerImpl extends HandlerBase implements AccountHandler {
    private static final Logger log =
        Logger.getLogger(AccountHandlerImpl.class);

    public AccountHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
    @Override
    public LoginResposeV4 confirmAccount(SampleResult sampleResult,
                                         String token) throws GSException {
        sampleResult.setSampleLabel(CONFIRM_ACCOUNT);
        LoginResposeV4 retVal =
            sendPut(sampleResult,
                    true,
                    CONFIRM_ACCOUNT,
                    null,
                    LoginResposeV4.class,
                    NVPair.from(TOKEN, token));
        
        AuthTokenInfo authTokenInfo = new AuthTokenInfo();
        authTokenInfo.setToken(token);
        authTokenInfo.setCustomerId(retVal.getCustomerId());
        
        AuthTokenFactory.get().accept(userNumber, authTokenInfo);
        
        return retVal;
    }
    
    @Override
    public AccountCreationResV4 createAccount(SampleResult sampleResult,
                                              AccountCreationInput input)
        throws GSException {
        log.debug("Input is: " + input);
        sampleResult.setSampleLabel(CREATE_ACCOUNT);

        return sendPost(sampleResult,
                        true,
                        CREATE_ACCOUNT,
                        input,
                        AccountCreationResV4.class);
    }
    
    
    @Override
    public LoginResposeV4 login(SampleResult sampleResult, LoginInput input)
        throws GSException {
//        log.info("Input is: " + input);

        if(input.getHandOffToken() != null) {
            sampleResult.setSampleLabel(LOGIN+"(HandOffToken)");
        } else {
            sampleResult.setSampleLabel(LOGIN);
        }
        LoginResposeV4 retVal =
            sendPost(sampleResult, true, LOGIN, input, LoginResposeV4.class);

//        log.info("returned:"+retVal.toString());

        AuthTokenInfo authTokenInfo = new AuthTokenInfo();
        authTokenInfo.setToken(retVal.getToken());
        authTokenInfo.setCustomerId(retVal.getCustomerId());

//        log.info(Thread.currentThread().getId()+" "+"Storing authToken:"+retVal.getToken()+" for userNumber:"+userNumber);
        AuthTokenFactory.get().accept(userNumber, authTokenInfo);

        return retVal;
    }

    @Override
    public HandOffTokenResponse getHandoffTokenDetails(SampleResult sampleResult, HandOffTokenRequest request) throws GSException {
        sampleResult.setSampleLabel(HANDOFFTOKEN_DETAILS);
        HandOffTokenResponse retVal = sendPost(sampleResult, true, HANDOFFTOKEN_DETAILS, request, HandOffTokenResponse.class);

        AuthTokenInfo authTokenInfo = new AuthTokenInfo();
        authTokenInfo.setToken(retVal.getToken());
        authTokenInfo.setCustomerId(retVal.getCustomerId());

//        log.info(Thread.currentThread().getId()+" "+"Storing authToken:"+retVal.getToken()+" for userNumber:"+userNumber);
        AuthTokenFactory.get().accept(userNumber, authTokenInfo);

        return retVal;
    }

    @Override
    public void logout(SampleResult sampleResult) throws GSException {
        sampleResult.setSampleLabel(LOGOUT);
        sendPost(sampleResult, false, LOGOUT, null, null);
        AuthTokenFactory.get().clear(userNumber);
    }
    
    @Override
    public AccountsV4 getAccount(SampleResult sampleResult) throws GSException {
        String customerId =
            AuthTokenFactory.get().get(userNumber).getCustomerId();
        sampleResult.setSampleLabel(GET_ACCOUNT);

        String endpoint = MessageFormat.format(GET_ACCOUNT, customerId);
        sampleResult.setSampleLabel(GET_ACCOUNT);
        return sendGet(sampleResult, false, endpoint, AccountsV4.class);
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.GIGSKY;
    }
}
