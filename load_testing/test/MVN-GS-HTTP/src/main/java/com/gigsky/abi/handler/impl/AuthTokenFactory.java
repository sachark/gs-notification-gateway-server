package com.gigsky.abi.handler.impl;

import com.gigsky.abi.AuthTokenHandler;
import com.gigsky.abi.AuthTokenInfo;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AuthTokenFactory implements AuthTokenHandler {
    private static final AuthTokenFactory instance;
    private final Map<Integer, AuthTokenInfo> tokenMap;

    private AuthTokenFactory() {
        tokenMap =
            Collections.synchronizedMap(new HashMap<Integer, AuthTokenInfo>());
    }
    
    public static AuthTokenHandler get() {
        return instance;
    }
    
    @Override
    public AuthTokenInfo get(int userNumber) {
        return tokenMap.get(userNumber);
    }
    
    @Override
    public void clear(int userNumber) {
        tokenMap.remove(userNumber);
    }

    @Override
    public void accept(int userNumber, AuthTokenInfo authTokenInfo) {
        tokenMap.put(userNumber, authTokenInfo);
    }
    
    static {
        instance = new AuthTokenFactory();
    }
}
