package com.gigsky.abi.handler.impl;

import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.BillingHandler;
import com.gigsky.abi.http.ServerType;
import com.gigsky.accounts.v4.beans.AccountPaymentStatus;
import com.gigsky.accounts.v4.beans.CreditBalanceDetailsV4;
import com.gigsky.accounts.v4.beans.CreditTransactionsV4;
import com.gigsky.accounts.v4.beans.TransactionV4;
import com.gigsky.common.payment.beans.BillingInfoListBean;
import com.gigsky.common.payment.beans.PaymentMethodListBean;
import com.gigsky.data.AddGigskyCreditInput;
import com.gigsky.data.CreditCardInput;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

import java.text.MessageFormat;

public class BillingHandlerImpl extends HandlerBase implements BillingHandler {
    private static final Logger log =
        Logger.getLogger(BillingHandlerImpl.class);

    public BillingHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
    @Override
    public PaymentMethodListBean getPaymentMethods(SampleResult sampleResult)
        throws GSException {
        sampleResult.setSampleLabel(GET_PAYMENT_METHODS);
        return sendGet(sampleResult,
                       false,
                       GET_PAYMENT_METHODS,
                       PaymentMethodListBean.class);
    }
    
    @Override
    public void addCredit(SampleResult sampleResult,
                          String customerId,
                          AddGigskyCreditInput input)
        throws GSException {
        String endpoint = MessageFormat.format(GIGSKY_CREDIT, customerId);

        sampleResult.setSampleLabel(GIGSKY_CREDIT);
        sendPost(sampleResult, false, endpoint, input, null);
    }
    
    @Override
    public CreditBalanceDetailsV4 getCredit(SampleResult sampleResult,
                                            String customerId)
        throws GSException {
        String endpoint = MessageFormat.format(GIGSKY_CREDIT, customerId);

        sampleResult.setSampleLabel(GIGSKY_CREDIT);
        return sendGet(sampleResult,
                       false,
                       endpoint,
                       CreditBalanceDetailsV4.class);
    }
    
    @Override
    public CreditTransactionsV4 getTransactions(SampleResult sampleResult,
                                                String customerId)
        throws GSException {
        String endpoint = MessageFormat.format(GET_TRANSACTIONS, customerId);

        sampleResult.setSampleLabel(GET_TRANSACTIONS);
        return sendGet(sampleResult,
                       false,
                       endpoint,
                       CreditTransactionsV4.class);
    }


    @Override
    public TransactionV4 getTransactionById(SampleResult sampleResult, String customerId, String simId, long transactionId)
            throws GSException {
        String endpoint = MessageFormat.format(GET_TRANSACTION_BY_ID, customerId, simId, String.valueOf(transactionId));
        sampleResult.setSampleLabel(GET_TRANSACTION_BY_ID);
        return sendGet(sampleResult,
                false,
                endpoint,
                TransactionV4.class);
    }

    @Override
    public void addCreditCard(SampleResult sampleResult,
                              String customerId,
                              CreditCardInput input)
        throws GSException {
        String endpoint = MessageFormat.format(CREDIT_CARD, customerId);
        sampleResult.setSampleLabel(CREDIT_CARD);

        sendPost(sampleResult, true, endpoint, input, null);
    }

    @Override
    public BillingInfoListBean getBillingInfo(SampleResult sampleResult,
                                              String customerId)
        throws GSException {
        String endpoint = MessageFormat.format(CREDIT_CARD, customerId);
        sampleResult.setSampleLabel(CREDIT_CARD);
        return sendGet(sampleResult,
                       false,
                       endpoint,
                       BillingInfoListBean.class);
    }

    @Override
    public AccountPaymentStatus getPaymentStatus(SampleResult sampleResult, String customerId) throws GSException {
        String endpoint = MessageFormat.format(GET_PAYMENT_STATUS, customerId);
        sampleResult.setSampleLabel(GET_PAYMENT_STATUS);
        return sendGet(sampleResult, true, endpoint, AccountPaymentStatus.class);
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.GIGSKY;
    }
}
