package com.gigsky.abi.handler.impl;

import com.gigsky.abi.AuthTokenInfo;
import com.gigsky.abi.GSConstants;
import com.gigsky.abi.GSException;
import com.gigsky.abi.http.NVPair;
import com.gigsky.abi.handler.GSHandler;
import com.gigsky.abi.http.ParamType;
import com.gigsky.abi.http.RestClient;
import com.gigsky.abi.http.ServerType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.jmeter.samplers.SampleResult;

public abstract class HandlerBase implements GSHandler, GSConstants {
    protected final int userNumber;
    
    public HandlerBase(int userNumber) {
        this.userNumber = userNumber;
    }
    
    protected <T> T sendGet(SampleResult sampleResult,
                            boolean failOnError,
                            String uri,
                            Class<T> responseType,
                            NVPair... nvPairs) throws GSException {
        sampleResult.setSampleLabel(uri);
        sampleResult.sampleStart();
        
        T retVal = null;

        RestClient restClient = null;
        
        try {
            restClient = new RestClient();
            
            List<NVPair> parameters = makeNVPairsFrom(nvPairs);
        
            retVal =
                restClient.sendGet(sampleResult,
                                   getServerType(),
                                   uri,
                                   responseType,
                                   parameters);
            sampleResult.setSuccessful(true);
        } catch (Exception e) {
            sampleResult.setSuccessful(false);
            
            if (failOnError) {
                throw new GSException(e);
            }
        } finally {
            sampleResult.sampleEnd();
            if (restClient != null) {
                restClient.close();
                restClient = null;
            }
        }
        
        return retVal;
    }

    protected <T> T sendDelete(SampleResult sampleResult,
                               boolean failOnError,
                               String uri,
                               Class<T> responseType,
                               NVPair... nvPairs) throws GSException {
        sampleResult.setSampleLabel(uri);
        sampleResult.sampleStart();

        T retVal = null;

        RestClient restClient = null;
        
        try {
            restClient = new RestClient();
            
            List<NVPair> parameters = makeNVPairsFrom(nvPairs);
        
            retVal =
                restClient.sendDelete(sampleResult,
                                      getServerType(),
                                      uri,
                                      responseType,
                                      parameters);
            sampleResult.setSuccessful(true);
        } catch (Exception e) {
            sampleResult.setSuccessful(false);
            
            if (failOnError) {
                throw new GSException(e);
            }
        } finally {
            sampleResult.sampleEnd();
            if (restClient != null) {
                restClient.close();
                restClient = null;
            }
        }
        
        return retVal;
    }

    protected <T> T sendPost(SampleResult sampleResult,
                             boolean failOnError,
                             String uri,
                             Object body,
                             Class<T> responseType,
                             NVPair... nvPairs) throws GSException {
        sampleResult.setSampleLabel(uri);
        sampleResult.sampleStart();

        T retVal = null;

        RestClient restClient = null;
        
        try {
            restClient = new RestClient();
            
            List<NVPair> parameters = makeNVPairsFrom(nvPairs);
        
            retVal =
                restClient.sendPost(sampleResult,
                                    getServerType(),
                                    uri,
                                    body,
                                    responseType,
                                    parameters);
            
            sampleResult.setSuccessful(true);
        } catch (Exception e) {
            sampleResult.setSuccessful(false);
            
            if (failOnError) {
                throw new GSException(e);
            }
        } finally {
            sampleResult.sampleEnd();
            if (restClient != null) {
                restClient.close();
                restClient = null;
            }
        }
        
        return retVal;
    }

    protected <T> T sendPut(SampleResult sampleResult,
                            boolean failOnError,
                            String uri,
                            Object body,
                            Class<T> responseType,
                            NVPair... nvPairs) throws GSException {
        sampleResult.setSampleLabel(uri);
        sampleResult.sampleStart();

        T retVal = null;

        RestClient restClient = null;
        
        try {
            restClient = new RestClient();
            
            List<NVPair> parameters = makeNVPairsFrom(nvPairs);
        
            retVal =
                restClient.sendPut(sampleResult,
                                   getServerType(),
                                   uri,
                                   body,
                                   responseType,
                                   parameters);
            sampleResult.setSuccessful(true);
        } catch (Exception e) {
            sampleResult.setSuccessful(false);
            
            if (failOnError) {
                throw new GSException(e);
            }
        } finally {
            sampleResult.sampleEnd();
            if (restClient != null) {
                restClient.close();
                restClient = null;
            }
        }
        
        return retVal;
    }

    private List<NVPair> makeNVPairsFrom(NVPair... nvPairs) {
        List<NVPair> retVal = new ArrayList<>();
        
        if (nvPairs != null) {
            retVal.addAll(Arrays.asList(nvPairs));
        }

        AuthTokenInfo authTokenInfo = AuthTokenFactory.get().get(userNumber);
        
        if (authTokenInfo != null) {
            retVal.add(NVPair.from(AUTHORIZATION,
                                   authTokenInfo.getToken(),
                                   ParamType.AUTH));
        }

        return retVal;
    }
    
    protected abstract ServerType getServerType();
}
