package com.gigsky.abi.handler.impl;

import com.gigsky.abi.AuthTokenInfo;
import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.ImsiAccountHandler;
import com.gigsky.abi.http.ServerType;
import com.gigsky.data.LoginInput;
import com.gigsky.restapi.bean.LoginResponse;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

public class ImsiAccountHandlerImpl extends HandlerBase
    implements ImsiAccountHandler {
    private static final Logger log =
        Logger.getLogger(ImsiAccountHandlerImpl.class);

    public ImsiAccountHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
    @Override
    public LoginResponse loginIMSI(SampleResult sampleResult,
                                   LoginInput input) throws GSException
    {
        log.debug ( "Input is: " + input );
        sampleResult.setSampleLabel(IMSI_LOGIN);
        LoginResponse retVal =
            sendPost(sampleResult,
                     true,
                     IMSI_LOGIN,
                     input,
                     LoginResponse.class);
        AuthTokenInfo authTokenInfo = new AuthTokenInfo ();
        authTokenInfo.setToken ( retVal.getToken () );
        authTokenInfo.setCustomerId ( retVal.getToken () );
        AuthTokenFactory.get().accept(userNumber, authTokenInfo);
        return retVal;
    }
    
    @Override
    protected ServerType getServerType() {
        return ServerType.IMSI;
    }
}
