package com.gigsky.abi.handler.impl;

import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.LocationHandler;
import com.gigsky.abi.http.ServerType;
import com.gigsky.accounts.v4.beans.SimLocationHistoryV4;
import com.gigsky.data.LocationInput;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

public class LocationHandlerImpl extends HandlerBase implements LocationHandler
{

    private static final Logger log = Logger.getLogger ( LocationHandlerImpl.class );

    public LocationHandlerImpl ( int userNumber )
    {
        super ( userNumber );
    }

    @Override
    public SimLocationHistoryV4 getLocationsOfAllSubscribers ( SampleResult sampleResult, String customerId, String token, LocationInput input ) throws GSException
    {
        //URL: baseURL + /subscriber + / locations ? startDate='2014-02-01 00:00:00' & endDate='2014-01-01 00:00:00' & startIndex = 0 & count = 10
        String endpoint = URL_SUBSCRIBER + URL_LOCATIONS + "?startDate=" + input.getStartDateStr () + "&endDate=" + input.getEndDateStr () + "&startIndex" + input.getStartIndex () + "&count=" + input.getCount ();
        sampleResult.setSampleLabel(URL_SUBSCRIBER + URL_LOCATIONS);
        return sendGet ( sampleResult, false, endpoint, SimLocationHistoryV4.class );
    }

    @Override
    public SimLocationHistoryV4 getLocationsOfASubscriber ( SampleResult sampleResult, String customerId, String token, LocationInput input ) throws GSException
    {
        //URL: baseURL + /subscriber + / <Subscriber ID> + /locations ? startDate='2014-02-01 00:00:00' & endDate='2014-01-01 00:00:00' & startIndex = 0 & count = 10
        String endpoint = URL_SUBSCRIBER + "/" + input.getSubscriptionId () + URL_LOCATIONS + "?startDate=" + input.getStartDateStr () + "&endDate=" + input.getEndDateStr () + "&startIndex" + input.getStartIndex () + "&count=" + input.getCount ();
        sampleResult.setSampleLabel(URL_SUBSCRIBER + "/SubscriptionId" + URL_LOCATIONS);
        return sendGet ( sampleResult, false, endpoint, SimLocationHistoryV4.class );
    }

    @Override
    public SimLocationHistoryV4 getCallDataRecordsOfAllSubscribers ( SampleResult sampleResult, String customerId, String token, LocationInput input ) throws GSException
    {
        //URL: baseURL + /subscriber +/calldatarecords ? startDate='2014-02-01 00:00:00' & endDate='2014-01-01 00:00:00' & startIndex = 0 & count = 10
        String endpoint = URL_SUBSCRIBER + URL_SUBSCRIBER_CALL_DATA_RECORDS + "?startDate=" + input.getStartDateStr () + "&endDate=" + input.getEndDateStr () + "&startIndex" + input.getStartIndex () + "&count=" + input.getCount ();
        sampleResult.setSampleLabel(URL_SUBSCRIBER + URL_SUBSCRIBER_CALL_DATA_RECORDS);
        return sendGet ( sampleResult, false, endpoint, SimLocationHistoryV4.class );
    }

    @Override
    public SimLocationHistoryV4 getCallDataRecordsOfASubscriber ( SampleResult sampleResult, String customerId, String token, LocationInput input ) throws GSException
    {
        //URL: baseURL + /subscriber + / <Subscriber ID> +/calldatarecords ? startDate='2014-02-01 00:00:00' &endDate='2014-01-01 00:00:00' & startIndex = 0 & count = 10
        String endpoint = URL_SUBSCRIBER + "/" + input.getSubscriptionId () + URL_SUBSCRIBER_CALL_DATA_RECORDS + "?startDate=" + input.getStartDateStr () + "&endDate=" + input.getEndDateStr () + "&startIndex" + input.getStartIndex () + "&count=" + input.getCount ();
        sampleResult.setSampleLabel(URL_SUBSCRIBER + "/SubscriptionId" + URL_SUBSCRIBER_CALL_DATA_RECORDS);
        return sendGet ( sampleResult, false, endpoint, SimLocationHistoryV4.class );
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.EVENT;
    }
}
