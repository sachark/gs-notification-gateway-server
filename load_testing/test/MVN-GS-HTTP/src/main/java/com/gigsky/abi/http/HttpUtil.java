package com.gigsky.abi.http;

import com.gigsky.abi.GSConfig;

public class HttpUtil {
    public static String makeRestUrl(String resource, ServerType serverType) {
        String retVal = null;
        
        String urlPrefix = null;
        
        switch (serverType) {
            case GIGSKY:
                urlPrefix = GSConfig.gigskyUrlPrefix();
                break;
            case EVENT:
                urlPrefix = GSConfig.eventUrlPrefix();
                break;
            case IMSI:
                urlPrefix = GSConfig.imsiUrlPrefix();
                break;
        }
        
        if (!resource.startsWith("http")) {
            retVal = urlPrefix + resource;
        } else {
            retVal = resource;
        }
        
        return retVal;
    }
}
