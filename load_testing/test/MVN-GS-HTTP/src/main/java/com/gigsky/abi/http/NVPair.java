package com.gigsky.abi.http;

import com.gigsky.abi.GSUtil;

public class NVPair {
    private String name;
    private Object value;
    private ParamType paramType;

    public static NVPair from(String name, Object value) {
        return from(name, value, ParamType.REQUEST);
    }
    
    public static NVPair from(String name, Object value, ParamType paramType) {
        NVPair retVal = new NVPair();
        
        retVal.name = name;
        retVal.paramType = paramType;
        
        if (value instanceof String) {
            String str = (String) value;
            value = GSUtil.urlEncode(str);
        } else if (value != null) {
            String str = value.toString();
            value = GSUtil.urlEncode(str);
        }
        
        retVal.value = value;
        
        return retVal;
    }

    public ParamType getParamType() {
        return paramType;
    }

    public void setParamType(ParamType paramType) {
        this.paramType = paramType;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValueAsString() {
        String retVal = null;
        
        if (value instanceof String) {
            retVal = (String) value;
        } else if (value != null) {
            retVal = value.toString();
        }
        
        return retVal;
    }
    
    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return
            "NVPair{" + "name=" + name + ", value=" + value + ", paramType=" +
            paramType + '}';
    }
}
