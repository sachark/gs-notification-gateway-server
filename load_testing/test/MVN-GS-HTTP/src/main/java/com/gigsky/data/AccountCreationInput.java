package com.gigsky.data;

public class AccountCreationInput {
    private String actCreationType;
    private String country;
    private String emailId;
    private String password;
    private String type;
    private String transactionPIN;

    public String getTransactionPIN() {
        return transactionPIN;
    }

    public void setTransactionPIN(String transactionPIN) {
        this.transactionPIN = transactionPIN;
    }
    
    public String getActCreationType() {
        return actCreationType;
    }

    public void setActCreationType(String actCreationType) {
        this.actCreationType = actCreationType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AccountCreationInput{" + "actCreationType=" + actCreationType + ", country=" + country + ", emailId=" + emailId + ", password=" + password + ", type=" + type + ", transactionPIN=" + transactionPIN + '}';
    }
}
