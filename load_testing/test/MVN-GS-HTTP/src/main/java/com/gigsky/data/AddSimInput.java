package com.gigsky.data;

public class AddSimInput {

    public final static String SIMTYPE_TELNA_SIM="TELNA_SIM";
    public final static String SIMTYPE_GIGSKY_SIM="GIGSKY_SIM";
    public final static String SIMTYPE_ACME_SIM="ACME_SIM";

    private String type;
    private String simType = "TELNA_SIM";

    public String getHandOffToken() {
        return handOffToken;
    }

    public void setHandOffToken(String handOffToken) {
        this.handOffToken = handOffToken;
    }

    private String handOffToken;
    private String activationCode;
    private String description;

    public AddSimInput() {
        type = "AddSIM";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return
            "AddSimInput{" + "type=" + type + ", activationCode=" +
            activationCode + ", description=" + description + '}';
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }
}
