package com.gigsky.data;

public class Params {
    private String vlr;
    private String time2;
    private String type = "gprs";
    private String imsi;
    private String gsn;
    private String time1;

    public String getVlr() {
        return vlr;
    }

    public void setVlr(String vlr) {
        this.vlr = vlr;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getGsn() {
        return gsn;
    }

    public void setGsn(String gsn) {
        this.gsn = gsn;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    @Override
    public String toString() {
        return "Params{" + "vlr=" + vlr + ", time2=" + time2 + ", type=" + type + ", imsi=" + imsi + ", gsn=" + gsn + ", time1=" + time1 + '}';
    }
}
