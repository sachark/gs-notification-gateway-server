package com.gigsky.data;

/**
 * Created on 03/02/15.
 */
public class SubscriptionOperationDetails {
    private long subscriptionId;
    private long txnTopUpData;
    private long initialSubscriptionBalance;
    private long cdrDeductData;
    private int networkId;

    public long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public long getTxnTopUpData() {
        return txnTopUpData;
    }

    public void setTxnTopUpData(long txnTopUpData) {
        this.txnTopUpData = txnTopUpData;
    }

    public long getCdrDeductData() {
        return cdrDeductData;
    }

    public void setCdrDeductData(long cdrDeductData) {
        this.cdrDeductData = cdrDeductData;
    }

    public int getNetworkId() {
        return networkId;
    }

    public void setNetworkId(int networkId) {
        this.networkId = networkId;
    }

    public long getInitialSubscriptionBalance() {
        return initialSubscriptionBalance;
    }

    public void setInitialSubscriptionBalance(long initialSubscriptionBalance) {
        this.initialSubscriptionBalance = initialSubscriptionBalance;
    }

    @Override
    public String toString() {
        return "SubscriptionOperationDetails{" +
                "subscriptionId=" + subscriptionId +
                ", txnTopUpData=" + txnTopUpData +
                ", initialSubscriptionBalance=" + initialSubscriptionBalance +
                ", cdrDeductData=" + cdrDeductData +
                ", networkId=" + networkId +
                '}';
    }
}
