package com.gigsky.ngw.data;

import java.util.Map;

public class EventInput {

	private String type = "EventDetail";
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getBalanceRemaining() {
		return balanceRemaining;
	}
	public void setBalanceRemaining(Integer balanceRemaining) {
		this.balanceRemaining = balanceRemaining;
	}
	public Integer getPercentageConsumed() {
		return percentageConsumed;
	}
	public void setPercentageConsumed(Integer percentageConsumed) {
		this.percentageConsumed = percentageConsumed;
	}
	
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	public String getIccId() {
		return iccId;
	}
	public void setIccId(String iccId) {
		this.iccId = iccId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public Map<String, Object> getOptions() {
		return options;
	}
	public void setOptions(Map<String, Object> options) {
		this.options = options;
	}
	private String eventType;
	private String customerId;
	private String userId;
	private Integer balanceRemaining;
	private Integer percentageConsumed;
	
	private String expiry;
	private String iccId;
	private String location;
	private String creationTime;
	private Map<String, Object> options;
	
}
