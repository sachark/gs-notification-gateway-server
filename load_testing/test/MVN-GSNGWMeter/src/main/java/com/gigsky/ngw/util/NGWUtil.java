package com.gigsky.ngw.util;

import org.apache.jmeter.samplers.SampleResult;

import com.gigsky.abi.GSConfig;

public class NGWUtil {
	
	public static String getUserRegistrationId(int userNumber)
	{
		String base = GSConfig.getRegistrationIdBase();
		return String.format("%s%011d",base,userNumber);
	}
	
	public static String getUserToken(int userNumber)
	{
		String base = GSConfig.getTokenBase();
		return String.format("%s%011d",base,userNumber);
	}
	
	public static String getIccid(int userNumber)
	{
		String base = GSConfig.getIccidBase();
		return String.format("%s%011d",base,userNumber);
	}
	
	 public static int successfulCount(SampleResult[] results) {
	        int retVal = 0;
	        
	        for (SampleResult result: results) {
	            if (result.isSuccessful()) {
	                retVal++;
	            }
	        }
	        
	        return retVal;
	    }

}
