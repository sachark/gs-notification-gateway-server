package com.gigsky.tests.dbaccess;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBAccessUtils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

public class NotificationGatewayDB {
    private static final NotificationGatewayDB instance = new NotificationGatewayDB();

    private NotificationGatewayDB()
    {}

    public static NotificationGatewayDB getInstance()
    {
        return instance;
    }

    public String getMockCustomerToken(DBConfigInfo dbInfo,  String id) throws Exception {
        String query = "select basicToken from gigskyNotificationGateway.MockUserToken where id="+id+";";
        List<String> tokens = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return tokens.get(0);
    }

    public String getMockUserUUID(DBConfigInfo dbInfo,int id) throws Exception{
        String query="select userUuid from gigskyNotificationGateway.MockUser where userId="+id+";";
        List<String> userUUid = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return userUUid.get(0);

    }


    public Boolean validateCustomerEventTable(DBConfigInfo dbInfo,String eventId) throws Exception{
        String query = "select data from gigskyNotificationGateway.CustomerEvent where event_id="+eventId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.size()==0;
    }
    public String validateDeviceIdMockPushNotificationTable(DBConfigInfo dbInfo, String notificationId) throws Exception{
        String query = "select deviceId from gigskyNotificationGateway.MockPushNotification where notificationId="+notificationId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return (String) data.get(0);
    }


    public void updateDeviceCount(DBConfigInfo dbInfo) throws Exception {
        String query = "UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='2' WHERE `ckey`='DEVICE_COUNT';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query));

    }


    public void disable_DataUsedEventType(DBConfigInfo dbInfo) throws Exception {
        String query = "UPDATE `gigskyNotificationGateway`.`EventType` SET `enableNotification`='0' WHERE `id`='2'";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query));

    }
    public void enable_DataUsedEventType(DBConfigInfo dbInfo) throws Exception {
        String query = "UPDATE `gigskyNotificationGateway`.`EventType` SET `enableNotification`='1' WHERE `id`='2'";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query));

    }

    public void updateUA_FAILURE_MODE_YES(DBConfigInfo dbInfo) throws Exception {
        String query = "UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='YES' WHERE `ckey`='UA_FAILURE_MODE';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query));

    }
    public void update_RATECONTROL_KEY(DBConfigInfo dbInfo) throws Exception {
        String query = "UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='000000000060000' WHERE `ckey`='RATE_CONTROL_TIME_MS';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query));

    }


    public void update_Revert_RATECONTROL_KEY(DBConfigInfo dbInfo) throws Exception {
        String query = "UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='000000000000001' WHERE `ckey`='RATE_CONTROL_TIME_MS';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query));

    }
    public void update_PartnerApp_Expiry_Time(DBConfigInfo dbInfo) throws Exception {
        String query1 = "UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='000000000060000' WHERE `ckey`='PARTNER_APP_DETAILS_EXPIRY';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query1));
        String query2="UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='000000000005000' WHERE `ckey`='GATEWAY_UTILITY_EXEC_TIME';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query2));

    }

    public void revert_PartnerApp_Expiry_Time(DBConfigInfo dbInfo) throws Exception {
        String query1 = "UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='000001000000000' WHERE `ckey`='PARTNER_APP_DETAILS_EXPIRY';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query1));
        String query2="UPDATE `gigskyNotificationGateway`.`ConfigurationsKeyValue` SET `value`='000001000000000' WHERE `ckey`='GATEWAY_UTILITY_EXEC_TIME';";
        DBAccessUtils.executeMysqlUpdateStatements(dbInfo, Collections.singletonList(query2));

    }

    public String getFailCount(DBConfigInfo dbInfo,String eventId) throws Exception{
        String query="select failCount from gigskyNotificationGateway.Event where id ="+eventId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo,query);
        return  data.get(0);

    }
    public String partnerAppDevice(DBConfigInfo dbInfo,String ipAddress) throws Exception{
        String query="select iccId FROM gigskyNotificationGateway.PartnerAppDetails where ipAddress='"+ipAddress+"';";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo,query);
        return  data.get(0);

    }
    public Boolean validateMockPushNotificationTable(DBConfigInfo dbInfo,String eventId) throws Exception{
        String query = "select location from gigskyNotificationGateway.MockPushNotification where eventId="+eventId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.size()==0;
    }

    public Boolean validateEmailEventTable(DBConfigInfo dbInfo,String eventId) throws Exception{
        String query = "select data from gigskyNotificationGateway.EmailEvent where event_id="+eventId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.size()==0;
    }

    public String getEmailIdFromEmailEventTable(DBConfigInfo dbInfo,String eventId) throws Exception{
        String query = "select email_id from gigskyNotificationGateway.EmailEvent where event_id="+eventId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.get(0);
    }

    public Boolean validateMockEmailNotificationSentTable(DBConfigInfo dbInfo,String notificationId) throws Exception{
        String query = "select content from gigskyNotificationGateway.MockEmailNotificationSent where Notification_id="+notificationId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.size()==0;
    }

    public String getEmailIdFromMockEmailSentTable(DBConfigInfo dbInfo,String notificationId) throws Exception{
        String query = "select recipient from gigskyNotificationGateway.MockEmailNotificationSent where Notification_id="+notificationId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.get(0);
    }

    public Boolean validateEmailNotificationTable(DBConfigInfo dbInfo,String notificationId) throws Exception{
        String query = "select content from gigskyNotificationGateway.EmailNotification where notification_id="+notificationId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.size()==0;
    }

    public String getEmailIdFromEmailNotificationTable(DBConfigInfo dbInfo,String notificationId) throws Exception{
        String query = "select recipient from gigskyNotificationGateway.EmailNotification where notification_id="+notificationId+";";
        List<String> data = DBAccessUtils.executeMysqlQueryStatement(dbInfo, query);
        return data.get(0);
    }



}
