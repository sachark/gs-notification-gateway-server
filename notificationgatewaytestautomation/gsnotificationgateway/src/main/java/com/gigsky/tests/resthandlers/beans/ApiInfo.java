package com.gigsky.tests.resthandlers.beans;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "type",
            "jenkins_build",
            "date_time",
            "svn_version",
            "db_version",
            "db_type"
    })
    public class ApiInfo extends BaseBean{

        @JsonProperty("type")
        private String type;
        @JsonProperty("jenkins_build")
        private Integer jenkinsBuild;
        @JsonProperty("date_time")
        private String dateTime;
        @JsonProperty("svn_version")
        private Integer svnVersion;
        @JsonProperty("db_version")
        private Integer dbVersion;
        @JsonProperty("db_type")
        private String dbType;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("type")
        public String getType() {
            return type;
        }

        @JsonProperty("type")
        public void setType(String type) {
            this.type = type;
        }

        @JsonProperty("jenkins_build")
        public Integer getJenkinsBuild() {
            return jenkinsBuild;
        }

        @JsonProperty("jenkins_build")
        public void setJenkinsBuild(Integer jenkinsBuild) {
            this.jenkinsBuild = jenkinsBuild;
        }

        @JsonProperty("date_time")
        public String getDateTime() {
            return dateTime;
        }

        @JsonProperty("date_time")
        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        @JsonProperty("svn_version")
        public Integer getSvnVersion() {
            return svnVersion;
        }

        @JsonProperty("svn_version")
        public void setSvnVersion(Integer svnVersion) {
            this.svnVersion = svnVersion;
        }

        @JsonProperty("db_version")
        public Integer getDbVersion() {
            return dbVersion;
        }

        @JsonProperty("db_version")
        public void setDbVersion(Integer dbVersion) {
            this.dbVersion = dbVersion;
        }

        @JsonProperty("db_type")
        public String getDbType() {
            return dbType;
        }

        @JsonProperty("db_type")
        public void setDbType(String dbType) {
            this.dbType = dbType;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
