package com.gigsky.tests.resthandlers.beans;


import com.fasterxml.jackson.annotation.*;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AppFeature extends BaseBean {


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "type",
            "featuresSupported",
            "baseUrl",
            "logoBaseUrl",
            "troubleshootingBaseUrl",
            "updateMandatory"
    })


    @JsonProperty("type")
    private String type;
    @JsonProperty("featuresSupported")
    private java.util.List<String> featuresSupported = null;
    @JsonProperty("baseUrl")
    private String baseUrl;
    @JsonProperty("logoBaseUrl")
    private String logoBaseUrl;
    @JsonProperty("troubleshootingBaseUrl")
    private String troubleshootingBaseUrl;
    @JsonProperty("updateMandatory")
    private String updateMandatory;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("featuresSupported")
    public java.util.List<String> getFeaturesSupported() {
        return featuresSupported;
    }

    @JsonProperty("featuresSupported")
    public void setFeaturesSupported(List<String> featuresSupported) {
        this.featuresSupported = featuresSupported;
    }

    @JsonProperty("baseUrl")
    public String getBaseUrl() {
        return baseUrl;
    }

    @JsonProperty("baseUrl")
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @JsonProperty("logoBaseUrl")
    public String getLogoBaseUrl() {
        return logoBaseUrl;
    }

    @JsonProperty("logoBaseUrl")
    public void setLogoBaseUrl(String logoBaseUrl) {
        this.logoBaseUrl = logoBaseUrl;
    }

    @JsonProperty("troubleshootingBaseUrl")
    public String getTroubleshootingBaseUrl() {
        return troubleshootingBaseUrl;
    }

    @JsonProperty("troubleshootingBaseUrl")
    public void setTroubleshootingBaseUrl(String troubleshootingBaseUrl) {
        this.troubleshootingBaseUrl = troubleshootingBaseUrl;
    }

    @JsonProperty("updateMandatory")
    public String getUpdateMandatory() {
        return updateMandatory;
    }

    @JsonProperty("updateMandatory")
    public void setUpdateMandatory(String updateMandatory) {
        this.updateMandatory = updateMandatory;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

