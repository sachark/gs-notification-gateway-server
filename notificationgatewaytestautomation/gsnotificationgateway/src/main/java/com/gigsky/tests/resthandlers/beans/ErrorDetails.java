package com.gigsky.tests.resthandlers.beans;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "errorInt",
        "errorStr"
})
public class ErrorDetails extends BaseBean{

    @JsonProperty("type")
    private String type;
    @JsonProperty("errorInt")
    private Integer errorInt;
    @JsonProperty("errorStr")
    private String errorStr;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("errorInt")
    public Integer getErrorInt() {
        return errorInt;
    }

    @JsonProperty("errorInt")
    public void setErrorInt(Integer errorInt) {
        this.errorInt = errorInt;
    }

    @JsonProperty("errorStr")
    public String getErrorStr() {
        return errorStr;
    }

    @JsonProperty("errorStr")
    public void setErrorStr(String errorStr) {
        this.errorStr = errorStr;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

