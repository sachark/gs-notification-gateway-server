package com.gigsky.tests.resthandlers.beans;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "type",
        "eventType",
        "subEventType",
        "emailIds",
        "language",
        "eventId",
        "customerId",
        "userId",
        "balanceRemaining",
        "expiry",
        "iccId",
        "location",
        "percentageConsumed",
        "creationTime",
        "options",
        "statusMessage"
})
public class Event extends BaseBean{

    @JsonProperty("type")
    private String type;
    @JsonProperty("eventType")
    private String eventType;
    @JsonProperty("eventId")
    private String eventId;
    @JsonProperty("customerId")
    private String customerId;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("balanceRemaining")
    private Integer balanceRemaining;
    @JsonProperty("expiry")
    private String expiry;
    @JsonProperty("iccId")
    private String iccId;
    @JsonProperty("location")
    private String location;
    @JsonProperty("percentageConsumed")
    private Integer percentageConsumed;
    @JsonProperty("creationTime")
    private String creationTime;
    @JsonProperty("options")
    private Options options;

    @JsonProperty("subEventType")
    private String subEventType;
    @JsonProperty("emailIds")
    private List<String> emailIds = null;
    @JsonProperty("language")
    private String language;

    @JsonProperty("notificationIds")
    private List<String> notificationIds = null;

    @JsonProperty("statusMessage")
    private String statusMessage;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("eventType")
    public String getEventType() {
        return eventType;
    }

    @JsonProperty("eventType")
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @JsonProperty("eventId")
    public String getEventId() {
        return eventId;
    }

    @JsonProperty("eventId")
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @JsonProperty("customerId")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customerId")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("balanceRemaining")
    public Integer getBalanceRemaining() {
        return balanceRemaining;
    }

    @JsonProperty("balanceRemaining")
    public void setBalanceRemaining(Integer balanceRemaining) {
        this.balanceRemaining = balanceRemaining;
    }

    @JsonProperty("expiry")
    public String getExpiry() {
        return expiry;
    }

    @JsonProperty("expiry")
    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    @JsonProperty("iccId")
    public String getIccId() {
        return iccId;
    }

    @JsonProperty("iccId")
    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("percentageConsumed")
    public Integer getPercentageConsumed() {
        return percentageConsumed;
    }

    @JsonProperty("percentageConsumed")
    public void setPercentageConsumed(Integer percentageConsumed) {
        this.percentageConsumed = percentageConsumed;
    }

    @JsonProperty("creationTime")
    public String getCreationTime() {
        return creationTime;
    }

    @JsonProperty("creationTime")
    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    @JsonProperty("subEventType")
    public String getSubEventType() {
        return subEventType;
    }

    @JsonProperty("subEventType")
    public void setSubEventType(String subEventType) {
        this.subEventType = subEventType;
    }

    @JsonProperty("emailIds")
    public List<String> getEmailIds() {
        return emailIds;
    }

    @JsonProperty("emailIds")
    public void setEmailIds(List<String> emailIds) {
        this.emailIds = emailIds;
    }

    @JsonProperty("notificationIds")
    public String getNotificationIds(Integer index) {
        return notificationIds.get(index);
    }

    @JsonProperty("notificationIds")
    public void setNotificationIds(List<String> notificationIds) {
        this.notificationIds = notificationIds;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("options")
    public Options getOptions() {
        return options;
    }

    @JsonProperty("options")
    public void setOptions(Options options) {
        this.options = options;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}