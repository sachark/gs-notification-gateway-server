package com.gigsky.tests.resthandlers.beans;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "simName",
        "customerName",
        "customerEmail",
        "gsCustomerId",
        "promoAmount",
        "currency",
        "csn",
        "referralCode",
        "advocateAmount",
        "referralAmount",
        "newCustomerEmail",
        "creditLimit",
        "creditWarningLimit"
})
public class Options {

    @JsonProperty("simName")
    private String simName;
    @JsonProperty("customerName")
    private String customerName;
    @JsonProperty("customerEmail")
    private String customerEmail;
    @JsonProperty("gsCustomerId")
    private Integer gsCustomerId;
    @JsonProperty("promoAmount")
    private Integer promoAmount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("csn")
    private String csn;
    @JsonProperty("referralCode")
    private String referralCode;
    @JsonProperty("advocateAmount")
    private String advocateAmount;
    @JsonProperty("referralAmount")
    private String referralAmount;

    @JsonProperty("newCustomerEmail")
    private String newCustomerEmail;

    @JsonProperty("creditLimit")
    private String creditLimit;

    @JsonProperty("creditWarningLimit")
    private String creditWarningLimit;

    @JsonProperty("creditLimit")
    public String getCreditLimit() {
        return creditLimit;
    }

    @JsonProperty("simName")
    public String getSimName() {
        return simName;
    }
    @JsonProperty("simName")
    public void setSimName(String simName) {
        this.simName = simName;
    }


    @JsonProperty("creditLimit")
    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("customerName")
    public String getCustomerName() {
        return customerName;
    }

    @JsonProperty("customerName")
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @JsonProperty("customerEmail")
    public String getCustomerEmail() {
        return customerEmail;
    }

    @JsonProperty("customerEmail")
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    @JsonProperty("gsCustomerId")
    public Integer getGsCustomerId() {
        return gsCustomerId;
    }

    @JsonProperty("gsCustomerId")
    public void setGsCustomerId(Integer gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    @JsonProperty("promoAmount")
    public Integer getPromoAmount() {
        return promoAmount;
    }

    @JsonProperty("promoAmount")
    public void setPromoAmount(Integer promoAmount) {
        this.promoAmount = promoAmount;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("csn")
    public String getCsn() {
        return csn;
    }

    @JsonProperty("csn")
    public void setCsn(String csn) {
        this.csn = csn;
    }

    @JsonProperty("referralCode")
    public String getReferralCode() {
        return referralCode;
    }

    @JsonProperty("referralCode")
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    @JsonProperty("advocateAmount")
    public String getAdvocateAmount() {
        return advocateAmount;
    }

    @JsonProperty("advocateAmount")
    public void setAdvocateAmount(String advocateAmount) {
        this.advocateAmount = advocateAmount;
    }

    @JsonProperty("referralAmount")
    public String getReferralAmount() {
        return referralAmount;
    }

    @JsonProperty("referralAmount")
    public void setReferralAmount(String referralAmount) {
        this.referralAmount = referralAmount;
    }

    @JsonProperty("newCustomerEmail")
    public String getNewCustomerEmail() {
        return newCustomerEmail;
    }
    @JsonProperty("newCustomerEmail")
    public void setNewCustomerEmail(String newCustomerEmail) {
        this.newCustomerEmail = newCustomerEmail;
    }

    @JsonProperty("creditWarningLimit")
    public String getCreditWarningLimit() {
        return creditWarningLimit;
    }
    @JsonProperty("creditWarningLimit")
    public void setCreditWarningLimit(String creditWarningLimit) {
        this.creditWarningLimit = creditWarningLimit;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
