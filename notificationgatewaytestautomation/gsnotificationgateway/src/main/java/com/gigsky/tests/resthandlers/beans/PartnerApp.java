package com.gigsky.tests.resthandlers.beans;

        import java.util.HashMap;
        import java.util.Map;
        import com.fasterxml.jackson.annotation.JsonAnyGetter;
        import com.fasterxml.jackson.annotation.JsonAnySetter;
        import com.fasterxml.jackson.annotation.JsonIgnore;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.fasterxml.jackson.annotation.JsonProperty;
        import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "deviceType",
        "deviceOSVersion",
        "locale",
        "ipAddress",
        "iccId",
        "location",
        "partnerCode",
        "sdkVersion"
})
public class PartnerApp extends  BaseBean {

    @JsonProperty("type")
    private String type;
    @JsonProperty("deviceType")
    private String deviceType;
    @JsonProperty("deviceOSVersion")
    private String deviceOSVersion;
    @JsonProperty("locale")
    private String locale;
    @JsonProperty("ipAddress")
    private String ipAddress;
    @JsonProperty("iccId")
    private String iccId;
    @JsonProperty("location")
    private String location;
    @JsonProperty("partnerCode")
    private String partnerCode;
    @JsonProperty("sdkVersion")
    private String sdkVersion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("deviceType")
    public String getDeviceType() {
        return deviceType;
    }

    @JsonProperty("deviceType")
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @JsonProperty("deviceOSVersion")
    public String getDeviceOSVersion() {
        return deviceOSVersion;
    }

    @JsonProperty("deviceOSVersion")
    public void setDeviceOSVersion(String deviceOSVersion) {
        this.deviceOSVersion = deviceOSVersion;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    @JsonProperty("ipAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ipAddress")
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @JsonProperty("iccId")
    public String getIccId() {
        return iccId;
    }

    @JsonProperty("iccId")
    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("partnerCode")
    public String getPartnerCode() {
        return partnerCode;
    }

    @JsonProperty("partnerCode")
    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    @JsonProperty("sdkVersion")
    public String getSdkVersion() {
        return sdkVersion;
    }

    @JsonProperty("sdkVersion")
    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
