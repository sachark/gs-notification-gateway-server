package com.gigsky.tests.resthandlers.beans;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.*;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "deviceType",
        "deviceOSVersion",
        "locale",
        "appVersion",
        "channelId",
        "installationId",
        "deviceToken",
        "preferences"
})
public class UpdateDevice extends BaseBean {

    @JsonProperty("type")
    private String type;
    @JsonProperty("deviceType")
    private String deviceType;
    @JsonProperty("deviceOSVersion")
    private String deviceOSVersion;
    @JsonProperty("locale")
    private String locale;
    @JsonProperty("appVersion")
    private String appVersion;
    @JsonProperty("channelId")
    private String channelId;
    @JsonProperty("installationId")
    private String installationId;
    @JsonProperty("deviceToken")
    private String deviceToken;
    @JsonProperty("preferences")
    private Preferences preferences;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("deviceType")
    public String getDeviceType() {
        return deviceType;
    }

    @JsonProperty("deviceType")
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @JsonProperty("deviceOSVersion")
    public String getDeviceOSVersion() {
        return deviceOSVersion;
    }

    @JsonProperty("deviceOSVersion")
    public void setDeviceOSVersion(String deviceOSVersion) {
        this.deviceOSVersion = deviceOSVersion;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    @JsonProperty("appVersion")
    public String getAppVersion() {
        return appVersion;
    }

    @JsonProperty("appVersion")
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    @JsonProperty("channelId")
    public String getChannelId() {
        return channelId;
    }

    @JsonProperty("channelId")
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    @JsonProperty("installationId")
    public String getInstallationId() {
        return installationId;
    }

    @JsonProperty("installationId")
    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    @JsonProperty("deviceToken")
    public String getDeviceToken() {
        return deviceToken;
    }

    @JsonProperty("deviceToken")
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @JsonProperty("preferences")
    public Preferences getPreferences() {
        return preferences;
    }

    @JsonProperty("preferences")
    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}