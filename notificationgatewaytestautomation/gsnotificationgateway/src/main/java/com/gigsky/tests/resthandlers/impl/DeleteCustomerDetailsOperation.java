package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.DeleteCustomerDetails;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;

import static org.testng.Assert.assertTrue;

public class DeleteCustomerDetailsOperation extends APIOperations {
    private static final DeleteCustomerDetailsOperation instance = new DeleteCustomerDetailsOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
    public static DeleteCustomerDetailsOperation getInstance() {
        return instance;
    }

    public BaseBean deleteCustomerDetails(DeleteCustomerDetails inputData, String token, String method, Integer responseCode, ErrorDetails errorDetails) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.addHeader(AUTHORIZATION_HEADER, "Basic " + token);
        apiHandlerConfig.setHttpMethod(method);
        apiHandlerConfig.setRequestBody(inputData);
        apiHandlerConfig.setApiName(APIUrls.APIName.DELETE_CUSTOMER_DETAILS.name());
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        if(errorDetails!=null) {
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
            apiHandlerConfig.setExpectedResponseBody(errorDetails);
            apiHandlerConfig.setResponseBodyValidator(new DeleteCustomerDetailsResponseValidator());
        }

        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response = null;
        if(method.equals("DELETE")){
            response = apiHandler.delete(apiHandlerConfig);
        }
        else if(method.equals("PUT"))
            response = apiHandler.put(apiHandlerConfig);
        else
            response = apiHandler.post(apiHandlerConfig);

        return (BaseBean) response.getResponseBody();
    }

    public void validateTablesForDeletedCustomer(String eventType, String eventId, String anonymousEmail, String notificationId) throws Exception {
        DBConfigInfo notificationGatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
        NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
        assertTrue(gatewayDB.validateCustomerEventTable(notificationGatewayDBConfigInfo, eventId), "Data Column is not set to null in Customer Event Table");
        if(!(eventType.equals("EMAIL"))) {
            assertTrue(gatewayDB.validateMockPushNotificationTable(notificationGatewayDBConfigInfo, eventId), "Data Column is not set to null in Customer Event Table");
        }
        else{
            assertTrue(gatewayDB.validateEmailEventTable(notificationGatewayDBConfigInfo, eventId), "Data Column is not set to null in Email Event Table");
            assertTrue(gatewayDB.getEmailIdFromEmailEventTable(notificationGatewayDBConfigInfo, eventId).equals(anonymousEmail), "Email_id column is not set to anonymous email in Email Event table");

            //Validate Mock Email Notification Sent table
            assertTrue(gatewayDB.validateMockEmailNotificationSentTable(notificationGatewayDBConfigInfo,notificationId), "Content column is not set to null in MockEmailNotification Sent table");
            assertTrue(gatewayDB.getEmailIdFromMockEmailSentTable(notificationGatewayDBConfigInfo, notificationId).equals(anonymousEmail), "Recipient email is not set to anonymous email in MockEmailNotificationSent Table");

            //Validate Email Notification table
            assertTrue(gatewayDB.validateEmailNotificationTable(notificationGatewayDBConfigInfo,notificationId), "Content column is not set to null in EmailNotification table");
            assertTrue(gatewayDB.getEmailIdFromEmailNotificationTable(notificationGatewayDBConfigInfo, notificationId).equals(anonymousEmail), "Recipient email is not set to anonymous email in EmailNotification Table");

        }
    }

    private class DeleteCustomerDetailsResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}
