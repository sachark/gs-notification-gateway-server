package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;

import javax.ws.rs.HttpMethod;

public class DeleteDeviceOperation extends APIOperations {
    private static final DeleteDeviceOperation instance = new DeleteDeviceOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
    public static DeleteDeviceOperation getInstance() {
        return instance;
    }


    public BaseBean deleteDevice(String customerUId,String deviceID,String token,Integer responseCode,ErrorDetails errorDetails) throws Exception {
    APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.CUSTOMER_UUID.name(),customerUId);
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.DEVICE_ID.name(),deviceID);
        apiHandlerConfig.setApiName(APIUrls.APIName.DELETE_DEVICE.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        if(!(token.equals("empty")))
            apiHandlerConfig.addHeader(AUTHORIZATION_HEADER, "Basic " + token);
        apiHandlerConfig.setHttpMethod(HttpMethod.DELETE);
        apiHandlerConfig.setRequestBody(null);
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        if(errorDetails instanceof ErrorDetails)
    {
        apiHandlerConfig.setExpectedResponseBody(errorDetails);
        apiHandlerConfig.setResponseValueType(ErrorDetails.class);
    }

    apiHandlerConfig.setResponseBodyValidator(new DeleteDeviceOperation.DeleteDeviceResponseValidator());
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
    APIResponse response  = apiHandler.delete(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();
}

    private class DeleteDeviceResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}
