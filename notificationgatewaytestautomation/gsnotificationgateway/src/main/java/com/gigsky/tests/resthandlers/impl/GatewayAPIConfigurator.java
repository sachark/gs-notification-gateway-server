package com.gigsky.tests.resthandlers.impl;

import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.impl.APIOperations;

public class GatewayAPIConfigurator extends APIOperations {

    private static String tenantID;

    public static APIHandlerConfig getAPIConfig(){
        APIHandlerConfig apiHandlerConfig = new APIHandlerConfig();
        if(tenantID!=null)
            apiHandlerConfig.addHeader(TENANTSID, tenantID);
        apiHandlerConfig.getApiServer();
        return apiHandlerConfig;
    }

    public static void setTenantID(String tenantsID){
        tenantID = tenantsID;
    }

    public static Integer getTenantID(){return Integer.valueOf(tenantID);};
}
