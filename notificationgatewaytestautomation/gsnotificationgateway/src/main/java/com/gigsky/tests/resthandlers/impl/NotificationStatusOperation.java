package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.NotificationStatus;
import org.apache.http.HttpStatus;

import javax.ws.rs.HttpMethod;

public class NotificationStatusOperation extends APIOperations {
    private static final NotificationStatusOperation instance = new NotificationStatusOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();

    public static NotificationStatusOperation getInstance() {
        return instance;
    }

    public NotificationStatus getnotificationStatus(NotificationStatus expectedResponse, String notificationID) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.NOTIFICATION_ID.name(), notificationID);
        apiHandlerConfig.setApiName(APIUrls.APIName.NOTIFICATION_STATUS.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.setHttpMethod(HttpMethod.GET);
        apiHandlerConfig.setResponseValueType(NotificationStatus.class);
        apiHandlerConfig.setExpectedResponseBody(expectedResponse);
        apiHandlerConfig.setResponseBodyValidator(new NotificationStatusOperation.NotificationStatusValidator());
        apiHandlerConfig.setExpectedResponseCode(HttpStatus.SC_OK);
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response = apiHandler.get(apiHandlerConfig);
        return (NotificationStatus) response.getResponseBody();


    }

    private class NotificationStatusValidator extends ResponseBodyValidator<NotificationStatus> {
        @Override
        public boolean validateResponseBody(NotificationStatus expectedResponseBody,
                                            NotificationStatus actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}


