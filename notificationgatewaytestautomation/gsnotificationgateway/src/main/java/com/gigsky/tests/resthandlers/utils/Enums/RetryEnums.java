package com.gigsky.tests.resthandlers.utils.Enums;

public enum RetryEnums {
    RETRY_COUNT(5),
    RETRY_TIME(30000);

    Integer retryCount;
    RetryEnums(Integer value) {
        retryCount = value;
    }

    public Integer getRetryCount(){return retryCount;}
}
