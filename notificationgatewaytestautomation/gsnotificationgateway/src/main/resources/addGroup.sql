SET autocommit=0;

-- Add new policies to support Alpha group i.e. Group 1
LOCK TABLES `Policy` WRITE;
/*!40000 ALTER TABLE `Policy` DISABLE KEYS */;

-- Policies for Beta group
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (17,'Low Balance Policy',0,0,1);
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (18,'Data Usage Policy',0,0,1);
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (19,'Subscription Near Expiry Policy',0,0,1);
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (20,'Subscription Expired Policy',0,0,1);

-- Policies for Alphpa group
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (21,'Low Balance Policy',0,0,1);
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (22,'Data Usage Policy',0,0,1);
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (23,'Subscription Near Expiry Policy',0,0,1);
INSERT INTO `Policy` (`id`, `description`, `interval`, `repeatCount`, `notificationType_id`) VALUES (24,'Subscription Expired Policy',0,0,1);

/*!40000 ALTER TABLE `Policy` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `EventPolicy` WRITE;
/*!40000 ALTER TABLE `EventPolicy` DISABLE KEYS */;

INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (17,1,1,2);
INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (18,2,1,2);
INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (19,3,1,2);
INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (20,4,1,2);

INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (21,1,1,1);
INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (22,2,1,1);
INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (23,3,1,1);
INSERT INTO `EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `groupType_id`) VALUES (24,4,1,1);

/*!40000 ALTER TABLE `EventPolicy` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `SupportedClient` WRITE;
/*!40000 ALTER TABLE `SupportedClient` DISABLE KEYS */;
-- Policy id for Alpha group
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (11,'ANDROID','2.5',17);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (12,'ANDROID','2.5',18);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (13,'ANDROID','2.5',19);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (14,'ANDROID','2.5',20);

INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (15,'IOS','2.6',21);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (16,'IOS','2.6',22);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (17,'IOS','2.6',23);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (18,'IOS','2.6',24);

/*!40000 ALTER TABLE `SupportedClient` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `MockUser` WRITE;
/*!40000 ALTER TABLE `MockUser` DISABLE KEYS */;
UPDATE `gigskyNotificationGateway`.`MockUser` SET `userUuid`='046b6c7f-0b8a-43b9-b35d-6489e6daee92' WHERE `userId`='2';
/*!40000 ALTER TABLE `MockUser` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `Customer` WRITE;
/*!40000 ALTER TABLE `Customer` DISABLE KEYS */;
INSERT INTO `gigskyNotificationGateway`.`Customer` (`id`, `customerUuid`, `tenant_id`, `customerType`) VALUES ('2', '046b6c7f-0b8a-43b9-b35d-6489e6daee92', 1, 'CONSUMER');
/*!40000 ALTER TABLE `Customer` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `CustomerGroup` WRITE;
/*!40000 ALTER TABLE `CustomerGroup` DISABLE KEYS */;

INSERT INTO `CustomerGroup` (`customer_id`, `groupType_id`) VALUES (1,2);
INSERT INTO `CustomerGroup` (`customer_id`, `groupType_id`) VALUES (2,1);

/*!40000 ALTER TABLE `CustomerGroup` ENABLE KEYS */;
UNLOCK TABLES;

COMMIT;