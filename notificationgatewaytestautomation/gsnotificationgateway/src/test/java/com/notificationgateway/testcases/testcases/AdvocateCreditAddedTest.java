package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.*;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;
import java.util.Arrays;
import java.util.List;

public class AdvocateCreditAddedTest extends GSNotificationGatewayBaseTest{
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();
    GetCustomerDetailsOperation customerInfo = GetCustomerDetailsOperation.getInstance();
    EventOperation eventOperation = EventOperation.getInstance();
    NotificationStatusOperation notificationStatus = NotificationStatusOperation.getInstance();


    private GetCustomerDetails getExpectedResponse() {
        GetCustomerDetails info = new GetCustomerDetails();
        info.setType("CustomerDetail");
        return info;

    }
    private String getDeviceID(GetDevice deviceDetails, Integer index) {
        return deviceDetails.getList().get(index).getDeviceId();
    }

   // Verify creating 'Advocate Credit' Event with multiple email ids
   @Test
    public void  advocateCreditTest1() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "10");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(),token, HttpStatus.SC_OK);
        AddDevice device=apiUtils.getAddDeviceExpectedResponse("ANDROID","5.1.1","en","2.8","ALLOW");
        AddDevice deviceDetails=(AddDevice)deviceOperation.addDevice(device,token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.POST);
       GetDevice deviceAdded=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceDetails,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event eventDetails=apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED","smaragal@gigsky.com" );

        //Setting Multiple Email-Ids List
        List<String> emailList=Arrays.asList("jchinta@gigsky.com","smaragal@gigsky.com");
        eventDetails.setEmailIds(emailList);
       Options options = eventDetails.getOptions();
       options.setCustomerEmail("jchinta@gigsky.com");
       options.setNewCustomerEmail("jdande@gigsky.com");
       eventDetails.setOptions(options);

       Event eventCreated = (Event) eventOperation.createEvent(eventDetails, null, HttpStatus.SC_OK, HttpMethod.POST);

        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "EMAIL", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
       deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceAdded,0),token,HttpStatus.SC_OK,null);



   }
    //Verify creating 'Advocate Credit' Event with language jp
    @Test(enabled = true)
    public void  advocateCreditTest2() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "11");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(),token, HttpStatus.SC_OK);
        AddDevice device=apiUtils.getAddDeviceExpectedResponse("IOS","5.1.1","en","2.8","ALLOW");
        AddDevice deviceDetails=(AddDevice)deviceOperation.addDevice(device,token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.POST);
        GetDevice deviceAdded=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceDetails,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event eventDetails=apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED","smaragal@gigsky.com" );
        eventDetails.setLanguage("jp");
        Event eventCreated = (Event) eventOperation.createEvent(eventDetails, null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "EMAIL", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceAdded,0),token,HttpStatus.SC_OK,null);
        }

        //Verify creating 'Advocate Credit Added' Event with Invalid Options
        @Test
        public void  advocateCreditTest3() throws Exception {
            String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "10");
            BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
            AddDevice device = apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "2.8", "ALLOW");
            AddDevice deviceDetails = (AddDevice) deviceOperation.addDevice(device, token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
            GetDevice deviceAdded = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceDetails, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
           //Create event without customer Email Id
            Event eventDetailsWithoutCustomerEmail = apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com");
            Options options1 = eventDetailsWithoutCustomerEmail.getOptions();
            options1.setCustomerEmail("");
            eventDetailsWithoutCustomerEmail.setOptions(options1);
            eventOperation.createEvent(eventDetailsWithoutCustomerEmail, apiUtils.getExpectedErrorResponse(11216,"Event creation requires valid customer email"), HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);

            //Create event without new customer Email Id
            Event eventDetailsWithoutNewCustomerEmail = apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com");
            Options options2 = eventDetailsWithoutNewCustomerEmail.getOptions();
            options2.setNewCustomerEmail("");
            eventDetailsWithoutNewCustomerEmail.setOptions(options2);
            eventOperation.createEvent(eventDetailsWithoutNewCustomerEmail, apiUtils.getExpectedErrorResponse(11222,"Event creation requires valid new customer email"), HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);

            //Create event without advocate Amount
            Event eventDetailsWithoutAdvocateAmount = apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com");
            Options options3 = eventDetailsWithoutAdvocateAmount.getOptions();
            options3.setAdvocateAmount("0");
            eventDetailsWithoutAdvocateAmount.setOptions(options3);
            eventOperation.createEvent(eventDetailsWithoutAdvocateAmount, apiUtils.getExpectedErrorResponse(11219,"Event creation requires valid advocate amount"), HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);

            //Create event without Currency
            Event eventDetailsWithoutCurrency = apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com");
            Options options4 = eventDetailsWithoutCurrency.getOptions();
            options4.setCurrency("");
            eventDetailsWithoutAdvocateAmount.setOptions(options4);
            eventOperation.createEvent(eventDetailsWithoutCurrency, apiUtils.getExpectedErrorResponse(11221,"Event creation requires valid currency"), HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);

            //Create event without emailIds option
            Event eventDetailsWithoutemailIds = apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com");
            eventDetailsWithoutemailIds.setEmailIds(null);
            eventOperation.createEvent(eventDetailsWithoutemailIds, apiUtils.getExpectedErrorResponse(11205,"Event creation requires valid user id"), HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);

            //Create event with empty emailIds option
            Event eventDetailEmptyEmailIds= apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com");
            List<String> emailList=Arrays.asList();
            eventDetailEmptyEmailIds.setEmailIds(emailList);
            eventOperation.createEvent(eventDetailEmptyEmailIds, apiUtils.getExpectedErrorResponse(11205,"Event creation requires valid user id"), HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);

            //Create event with GET,PUT and DELETE
            eventOperation.createEvent(apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com"), apiUtils.getExpectedErrorResponse(11007,"Resource not found"), HttpStatus.SC_NOT_FOUND, HttpMethod.PUT);
            eventOperation.createEvent(apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com"), apiUtils.getExpectedErrorResponse(11007,"Resource not found"), HttpStatus.SC_NOT_FOUND, HttpMethod.GET);
            eventOperation.createEvent(apiUtils.emailEventDetails("ADVOCATE_CREDIT_ADDED", "smaragal@gigsky.com"), apiUtils.getExpectedErrorResponse(11007,"Resource not found"), HttpStatus.SC_NOT_FOUND, HttpMethod.DELETE);
            deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceAdded,0),token,HttpStatus.SC_OK,null);

        }



        }
