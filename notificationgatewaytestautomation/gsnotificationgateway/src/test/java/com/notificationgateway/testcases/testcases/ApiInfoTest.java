package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.resthandlers.beans.ApiInfo;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;
import com.gigsky.tests.resthandlers.impl.ApiInfoOperation;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;

public class ApiInfoTest extends GSNotificationGatewayBaseTest{
    APIUtils apiUtils = APIUtils.getInstance();

    private ApiInfo getExpectedResponse(){
        ApiInfo info = new ApiInfo();
        info.setType("BuildInfo");
        info.setDbType("dev");
        info.setDbVersion(100);
        return info;
    }

    @Test(dataProvider = "apiInfo_Expected")
    public void getApiInfo(BaseBean bean, Class responseType, String method, int responseCode) throws Exception {
        ApiInfoOperation apiInfo = ApiInfoOperation.getInstance();
        apiInfo.getApi_Info(bean,responseType,method,responseCode);
    }

    @DataProvider(name = "apiInfo_Expected")
    public Object[][] createData(){
        return new Object[][] {
                {getExpectedResponse(),ApiInfo.class,HttpMethod.GET,HttpStatus.SC_OK},
                {apiUtils.getExpectedErrorResponse(11007,"Resource not found"),ErrorDetails.class,HttpMethod.POST,HttpStatus.SC_NOT_FOUND},
                {apiUtils.getExpectedErrorResponse(11007,"Resource not found"),ErrorDetails.class,HttpMethod.PUT,HttpStatus.SC_NOT_FOUND},
                {apiUtils.getExpectedErrorResponse(11007,"Resource not found"),ErrorDetails.class,HttpMethod.DELETE,HttpStatus.SC_NOT_FOUND}
        };
    }
}
