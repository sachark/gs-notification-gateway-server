package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.AppFeature;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.impl.AppFeatureOperation;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import java.util.Arrays;
import java.util.List;


public class AppFeatureTest  extends GSNotificationGatewayBaseTest{
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    AppFeatureOperation appFeature = AppFeatureOperation.getInstance();




    private AppFeature appFeatureDetails(String featureInfo)
    {
        List<String> featureList=Arrays.asList("Feature1");
        AppFeature appInfo=new AppFeature();

      if(featureInfo=="noAppFeatureInfo")//When featureInfo is set to null
        appInfo.setType(null);
      else
        appInfo.setType("AppFeatureInfo");

      if(featureInfo=="noLogoBaseUrl")
       appInfo.setLogoBaseUrl(null);//When LogoBaseUrl is set to null
      else
       appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");

        if(featureInfo=="noBaseURL")
            appInfo.setBaseUrl(null);//When BaseURL is set to null
        else
            appInfo.setBaseUrl("https://gigsky.com/api/v5/");

        if(featureInfo=="noTroubleShootingBaseUrl")
            appInfo.setTroubleshootingBaseUrl(null);//When TroubleShootingBaseUrl is set to null
        else
            appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");

        if(featureInfo=="mandatoryField")
            appInfo.setUpdateMandatory("false");//When UpdateMandatory field is set to false
        else if(featureInfo=="noMandatoryField")
            appInfo.setUpdateMandatory(null);//When UpdateMandatory field is set to null
        else
            appInfo.setUpdateMandatory("true");


        if(featureInfo=="noFeatureList")
            appInfo.setFeaturesSupported(null);//When FeaturesSupported is set to null
        else
            appInfo.setFeaturesSupported(featureList);



        return appInfo;


    }

    //Verify same set of features can be added to same clientType, clientVersion but different users

    @Test
  public void appFeature1() throws Exception
   {
       AppFeature appInfo=new AppFeature();
        List<String> featureList=Arrays.asList("Feature1","Feature2");



        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList);
        appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user1@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user2@gigsky.com",HttpStatus.SC_OK,null);

        appFeature.deleteAppFeatures("ANDROID","3.2","user1@gigsky.com",HttpStatus.SC_OK);
        appFeature.deleteAppFeatures("ANDROID","3.2","user2@gigsky.com",HttpStatus.SC_OK);


    }


//Verify different set of features can be added to same clientType, clientVersion and different users
@Test
     public void appFeature2() throws Exception
    {
        AppFeature appInfo=new AppFeature();
        List<String> featureList1=Arrays.asList("Feature1","Feature2");
        List<String> featureList2=Arrays.asList("Feature3","Feature4");


        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList1);
        appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user3@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"ANDROID","3.2","user3@gigsky.com",HttpStatus.SC_OK,null);


        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList2);
        appFeature.addAppFeatures(appInfo,"IOS","3.3","user4@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"IOS","3.3","user4@gigsky.com",HttpStatus.SC_OK,null);

       appFeature.deleteAppFeatures("ANDROID","3.2","user3@gigsky.com",HttpStatus.SC_OK);
       appFeature.deleteAppFeatures("IOS","3.3","user4@gigsky.com",HttpStatus.SC_OK);



    }

  //When ALL is passed as Query Parameter in getAppFeatures
    @Test
    public void appFeature3() throws Exception
    {
        appFeature.addAppFeatures(appFeatureDetails(null),"IOS","3.2","user5@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appFeatureDetails(null),"ALL","3.2","user5@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type"));
        appFeature.getAppFeatures(appFeatureDetails(null),"IOS","ALL","user5@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found"));
        appFeature.getAppFeatures(appFeatureDetails(null),"IOS","3.2","ALL",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found"));
        appFeature.deleteAppFeatures("IOS","3.2","user5@gigsky.com",HttpStatus.SC_OK);


    }

    //Verify featuresSupported list cant be added without providing featuresSupported key
    @Test
    public void appFeature4() throws Exception
    {
        AppFeature appInfo=new AppFeature();
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user6@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"));
        appFeature.getAppFeatures(appInfo,"ANDROID","3.2","user6@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found"));
        appFeature.deleteAppFeatures("ANDROID","3.2","user6@gigsky.com",HttpStatus.SC_BAD_REQUEST);


    }

    //Verify 11 features can be added as App feature info
    @Test
    public void appFeature5() throws Exception
    {
        AppFeature appInfo=new AppFeature();
        List<String> featureList=Arrays.asList("Feature1","Feature2","Feature3","Feature4","Feature5","Feature6","Feature7","Feature8","Feature9","Feature10","Feature11");
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList);
        appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user7@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"ANDROID","3.2","user7@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("ANDROID","3.2","user7@gigsky.com",HttpStatus.SC_OK);
    }

    //Verify once a app feature is added it cant be edited
   @Test
    public void appFeature6() throws Exception
    {
        AppFeature appInfo=new AppFeature();
        List<String> featureList1=Arrays.asList("networkTest");
        List<String> featureList2=Arrays.asList("PaymentTest");
        List<String> featureList3=Arrays.asList("networkTest");



        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList1);
        appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user8@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"ANDROID","3.2","user8@gigsky.com",HttpStatus.SC_OK,null);

       appInfo.setFeaturesSupported(featureList2);
       appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user8@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11601,"App feature info already exist"));

       appInfo.setFeaturesSupported(featureList3);
       appFeature.addAppFeatures(appInfo,"ANDROID","3.2","user8@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11601,"App feature info already exist"));


       appFeature.deleteAppFeatures("ANDROID","3.2","user8@gigsky.com",HttpStatus.SC_OK);


    }



  @Test(dataProvider = "App_Feature_Expected")
    public void appFeature(BaseBean expectedResponse, String clientType, String clientVersion, String userId,Integer responseCode,BaseBean errorDetails,BaseBean getDeviceErrorDetails) throws Exception {
        appFeature.addAppFeatures(expectedResponse,clientType,clientVersion,userId,responseCode,errorDetails);
        appFeature.getAppFeatures(expectedResponse,clientType,clientVersion,userId,responseCode,getDeviceErrorDetails);
        appFeature.deleteAppFeatures(clientType,clientVersion,userId,responseCode);


    }

    @DataProvider(name = "App_Feature_Expected")
    public Object[][] createData() throws Exception {
        return new Object[][]{
                //Verify single app feature info can be added for clientType=IOS
                {appFeatureDetails(null),"IOS","3.3","tester1@gigsky.com",HttpStatus.SC_OK,null,null},

                //Verify single app feature info can be added for clientType=Android
                {appFeatureDetails(null),"ANDROID","3.3","tester2@gigsky.com",HttpStatus.SC_OK,null,null},

                //Verify single app feature info cant be added for clientType=ALL and clientVersion=ALL and userId=ALL
                {appFeatureDetails(null),"WEB","ALL","ALL",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")},
                {appFeatureDetails(null),"CAYMAN","ALL","ALL",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")},
                {appFeatureDetails(null),"ALL","ALL","ALL",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},

                //Verify single app feature info can be added for clientType=IOS and clientVersion=3.3.1
                {appFeatureDetails(null),"IOS","3.3.1","tester3@gigsky.com",HttpStatus.SC_OK,null,null},

                //Verify featuresSupported list cant be blank while adding App feature Info
                {appFeatureDetails("noFeatureList"),"IOS","3.3.1","tester4@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")},

                //Verify featuresSupported list cant be added without providing type = AppFeatureInfo
                {appFeatureDetails("noAppFeatureInfo"),"IOS","3.3.2","tester5@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")},

                //Verify Add App feature info can be added for clientVersion=2 and clientType=IOS
                {appFeatureDetails(null),"IOS","2","tester6@gigsky.com",HttpStatus.SC_OK,null,null},
                {appFeatureDetails(null),"IOS","0","tester7igsky.com",HttpStatus.SC_OK,null,null},
                {appFeatureDetails(null),"IOS","0.1","tester8@gigsky.com",HttpStatus.SC_OK,null,null},

                //Verify Add App feature info cant be added with clientType = linux/Ios/android/iOS
                //Verify get App feature info cant work with clientType = linux/Ios/android/iOS
                //Verify Delete App feature info cant work with clientType = linux/Ios/android/iOS
                {appFeatureDetails(null),"Linux","0.1","tester9@ggissky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type"),apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},
                {appFeatureDetails(null),"android","0.1","tester10@ggissky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type"),apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},
                {appFeatureDetails(null),"iOS","0.1","tester11@ggissky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type"),apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},
                {appFeatureDetails(null),"Ios","0.1","tester12@ggissky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type"),apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},

                //Verify Add app feature info cant be added without clientType or ClientVersion or userID
                {appFeatureDetails(null),null,"0.1","tester13@ggissky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type"),apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},
                {appFeatureDetails(null),"IOS",null,"tester14@ggissky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11603,"App feature info requires valid client Version"),apiUtils.getExpectedErrorResponse(11603,"App feature info requires valid client version")},
                {appFeatureDetails(null),"IOS","0.1",null,HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11604,"App feature info requires valid user id"),apiUtils.getExpectedErrorResponse(11604,"App feature info requires valid user id")},

                //Verify Add app feature info cant be added when userID=ALL and clientVersion=ALL
                {appFeatureDetails(null),"Linux","ALL","ALL",HttpStatus.SC_BAD_REQUEST,null,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},

                {appFeatureDetails(null),"ALL","0.1","ALL",HttpStatus.SC_BAD_REQUEST,null,apiUtils.getExpectedErrorResponse(11602,"App feature info requires valid client type")},
                {appFeatureDetails("mandatoryField"), "IOS", "3.3.1", "tester15@gigsky.com", HttpStatus.SC_OK,null,null},

                //Verify app feature info cant be added without baselogourl,baseurl,troubleshooturl,updateMandatory
                {appFeatureDetails("noBaseURL"), "IOS", "3.3.1", "tester16@gigsky.com", HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")},
                {appFeatureDetails("noLogoBaseUrl"), "IOS", "3.3.1", "tester17@gigsky.com", HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")},
                {appFeatureDetails("noTroubleShootingBaseUrl"),"IOS","3.3.2","tester18@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")},
                {appFeatureDetails("noMandatoryField"),"IOS","3.3.2","tester19@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(1005,"Invalid arguements"),apiUtils.getExpectedErrorResponse(11600,"App feature info entry not found")}
                };
    }

}
