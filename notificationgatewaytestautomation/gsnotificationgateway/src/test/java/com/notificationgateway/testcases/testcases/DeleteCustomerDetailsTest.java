package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.*;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;

public class DeleteCustomerDetailsTest  extends GSNotificationGatewayBaseTest{

    APIUtils apiUtils = new APIUtils();
    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    EventOperation eventOperation = EventOperation.getInstance();
    DeleteCustomerDetailsOperation deleteCustomerDetails = DeleteCustomerDetailsOperation.getInstance();
    NotificationStatusOperation notificationStatus=NotificationStatusOperation.getInstance();
    NotificationGatewayDB notificationdb=NotificationGatewayDB.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();

    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");


    private DeleteCustomerDetails getExpectedDeleteCustomerDetailsRequestBody(String oldEmailId, String anonymousEmail){
        DeleteCustomerDetails details = new DeleteCustomerDetails();
        details.setOldEmailId(oldEmailId);
        details.setAnonymousEmailId(anonymousEmail);
        details.setType("CustomerDetail");
        return details;
    }
    private String getDeviceID(GetDevice deviceDetails, Integer index) {
        return deviceDetails.getList().get(index).getDeviceId();
    }

    @Test(dataProvider = "deleteCustomerDetails_Expected")
    public void deleteCustomerDetails(String oldEmailId, String anonymousEmailId, String eventType, String method, int responseCode, ErrorDetails errorDetails) throws Exception {
        String actualEmailId =null;
        if(errorDetails!=null)
            actualEmailId = "errorDetails@gigsky.com";
        else
            actualEmailId = oldEmailId;

        //Call Get Customer Details API and Get UUID
        GetCustomerDetails customerDetails = (GetCustomerDetails) apiUtils.getCustomerDetails();
        String customerId = customerDetails.getCustomerId();
        String token = customerDetails.getAdditionalProperties().get("token").toString();

        //Add Device to Customer
        AddDevice addDeviceResponse = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceDetails(),token,customerId, 200, null, HttpMethod.POST);

        //Call Get Device API and Validate the Added Device
        GetDevice getDevice = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(addDeviceResponse,customerId),customerId,token, HttpMethod.GET, HttpStatus.SC_OK);

        //Send Push Notification Event or Email Event for Device
        Event event = (Event) eventOperation.createEvent(apiUtils.getEventData(eventType,customerId,actualEmailId), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);

        //Call Get Event Details API and validate the response
        event.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(event,event.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(15000);

        //Call Get Notification API
       NotificationStatus status =notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0),"COMPLETE",getEvent.getEventId(),getEvent.getEventType(),"COMPLETE_SENT"),getEvent.getNotificationIds(0));

        //Verify Mock Push Notification table
        if(status.getNotificationType()=="PUSH")
        Assert.assertTrue (notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo,status.getNotificationId()).equals(getDeviceID(getDevice, 0)),"Device id is not Present in table");
        else if(status.getNotificationType()=="EMAIL")
        {

        }

        //Call Delete Customer Details API and validate Details
        DeleteCustomerDetails requestData = getExpectedDeleteCustomerDetailsRequestBody(oldEmailId, anonymousEmailId);
        deleteCustomerDetails.deleteCustomerDetails(requestData,token, method, responseCode, errorDetails);
        Thread.sleep(5000);

        //Validate tables whether data is set to null or not
        if(errorDetails==null)
            deleteCustomerDetails.validateTablesForDeletedCustomer(event.getEventType(),event.getEventId(), anonymousEmailId, getEvent.getNotificationIds(0));

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(getDevice, 0), token, HttpStatus.SC_OK, null);

    }

    @DataProvider(name = "deleteCustomerDetails_Expected")
    public Object[][] createData() {
        return new Object[][] {
                //{oldEmailId, anonymousEmailId, EventType, HttpMethod, HttpStatus, ErrorDetails}
                {"test@gigsky.com", "testNew@gigsky.com", "LOW_BALANCE", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test1@gigsky.com", "testNew1@gigsky.com", "SUBSCRIPTION_NEARING_EXPIRY", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test2@gigsky.com", "testNew2@gigsky.com", "NEW_LOCATION", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test3@gigsky.com", "testNew3@gigsky.com", "SUBSCRIPTION_EXPIRED", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test4@gigsky.com", "testNew4@gigsky.com", "DATA_USED", HttpMethod.DELETE, HttpStatus.SC_OK, null},

                {"test5@gigsky.com", "testNew5@gigsky.com", "REFERRAL_INITIATION", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test6@gigsky.com", "testNew6@gigsky.com", "ADVOCATE_CREDIT_ADDED", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test7@gigsky.com", "testNew7@gigsky.com", "INTERNAL_CREDIT_ALERT", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test8@gigsky.com", "testNew8@gigsky.com", "INTERNAL_CREDIT_DEDUCT", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test9@gigsky.com", "testNew9@gigsky.com", "INTERNAL_CREDIT_WARNING", HttpMethod.DELETE, HttpStatus.SC_OK, null},
                {"test10@gigsky.com", "testNew10@gigsky.com", "ICCID_PROMOTION_CREDIT", HttpMethod.DELETE, HttpStatus.SC_OK, null},

                {"acbd@c", "testNew11@gigsky.com", "LOW_BALANCE", HttpMethod.DELETE, HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11801,"Customer requires valid old email id")},
                {"", "testNew12@gigsky.com", "LOW_BALANCE", HttpMethod.DELETE, HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11801,"Customer requires valid old email id")},
                {"errorDetails@gigsky.com", "gg@i.c", "LOW_BALANCE", HttpMethod.DELETE, HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11802,"Customer requires valid anonymous email id")},
                {"errorDetails@gigsky.com", "ddd", "LOW_BALANCE", HttpMethod.DELETE, HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11802,"Customer requires valid anonymous email id")},
                {"errorDetails@gigsky.com", "errorDetailNews@gigsky.com", "LOW_BALANCE", HttpMethod.PUT, HttpStatus.SC_NOT_FOUND, apiUtils.getExpectedErrorResponse(11007,"Resource not found")},
                {"errorDetails@gigsky.com", "errorDetailNews@gigsky.com", "LOW_BALANCE", HttpMethod.POST, HttpStatus.SC_NOT_FOUND, apiUtils.getExpectedErrorResponse(11007,"Resource not found")}
        };
    }
}
