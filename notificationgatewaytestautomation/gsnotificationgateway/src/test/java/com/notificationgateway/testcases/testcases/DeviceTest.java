package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBAccessUtils;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.*;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.junit.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;


public class DeviceTest extends GSNotificationGatewayBaseTest{

    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    AppFeatureOperation appFeature = AppFeatureOperation.getInstance();

    GetCustomerDetailsOperation customerInfo = GetCustomerDetailsOperation.getInstance();


    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();

    private GetCustomerDetails getExpectedResponse() {
        GetCustomerDetails info = new GetCustomerDetails();
        info.setType("CustomerDetail");
        return info;

    }

    private String getDeviceID(GetDevice deviceDetails, Integer index) {
        return deviceDetails.getList().get(index).getDeviceId();
    }

    public AddDevice getAddDeviceExpectedResponseInfo(String deviceModel, String channelID, String deviceType, String deviceVersion, String locale, String appVersion, String location, String installationId, String devicetoken) {
        Random random = new Random();
        AddDevice addDevice = new AddDevice();
        addDevice.setType("DeviceDetail");
        if(deviceType!="empty")
            addDevice.setDeviceType(deviceType);
        if(deviceVersion!="empty")
            addDevice.setDeviceOSVersion(deviceVersion);
        if(deviceModel!="empty")
            addDevice.setModel(deviceModel);
        if(channelID!="empty")
            addDevice.setChannelId(channelID+ String.valueOf(random.nextInt(26000)));
        if(deviceType.equals("ANDROID")) {
            if (installationId != "empty")
                addDevice.setInstallationId(installationId + String.valueOf(random.nextInt(25000)));
        }
        else
            if(devicetoken!="empty")
            addDevice.setDeviceToken(devicetoken+ String.valueOf(random.nextInt(25000)));
        if(locale!="empty")
            addDevice.setLocale(locale);
        if(appVersion!="empty")
            addDevice.setAppVersion(appVersion);
        Preferences preferences = new Preferences();
        if(location!="empty")
            preferences.setLocation(location);
        if(location!="emptyPreference")
            addDevice.setPreferences(preferences);
        return addDevice;
    }



    //Verify for an android device, OS, locale and app version can be updated
    @Test
    public void deviceTest1() throws Exception {
    String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "2");
    BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
    AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
    GetDevice deviceDetailsIOS =(GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
    AddDevice updatedDevice=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "6.0", "es", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), HttpStatus.SC_OK, null, HttpMethod.PUT);
    getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice,customerDetails.getCustomerId()),customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
    deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_OK, null);
    }



    //Verify for a device only locale can be updated
    @Test
    public void deviceTest2() throws Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "3");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        //Add android device to customer
        AddDevice deviceANDROID = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "6.0", "en", "2.8", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        //Add a iOS device to customer
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.1", "en", "2.8", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        //Get device after adding a iOS and android device to customer
        GetDevice deviceDetails= (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceANDROID,deviceIOS, null, customerDetails.getCustomerId(),2 ) ,customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        //update locale of android device
        AddDevice updatedDevice=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "6.0", "ja", "2.8", "ALLOW"), token, customerDetails.getCustomerId(), getDeviceID(deviceDetails, 0), HttpStatus.SC_OK, null, HttpMethod.PUT);
        //Get device after updating the device should return android first and iOS second
        GetDevice deviceDetailsAfterUpdate=(GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice,deviceIOS, null,customerDetails.getCustomerId() ,2 ) ,customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        //Delete android device which was updated
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsAfterUpdate, 0), token, HttpStatus.SC_OK, null);
        //Get device after deleting android and only iOS device should be returned
        GetDevice only_IOS=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS,customerDetails.getCustomerId()),customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        //Delete iOS device
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(only_IOS, 0), token, HttpStatus.SC_OK, null);
        //Get device should be empty
        GetDevice empty_List=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS,deviceANDROID, null,customerDetails.getCustomerId() ,0 ),customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

    }


    //Verify deviceID which doesnt belong to given CustomerID can not be updated or deleted
    @Test
    public void deviceTest3() throws Exception {
        String token1 = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "4");
        BaseBean customerDetails1 = customerInfo.getCustomerDetails(getExpectedResponse(), token1, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), token1, customerDetails1.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails1.getCustomerId()), customerDetails1.getCustomerId(), token1, HttpMethod.GET, HttpStatus.SC_OK);
        //Get token from DB for second user using sql query
        String token2 = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "5");
        BaseBean customerDetails2 = customerInfo.getCustomerDetails(getExpectedResponse(), token2, HttpStatus.SC_OK);
        //update app version for the device
        BaseBean updatedDevice=(BaseBean) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "3.1", "en", "2.6.1", "ALLOW"), token2, customerDetails2.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11108,"Device not assigned to user"), HttpMethod.PUT);
        //using user 2 try deleting device which belongs to user 1
        deleteDeviceOperation.deleteDevice(customerDetails2.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token2, HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11108,"Device not assigned to user"));
        //delete the device
        deleteDeviceOperation.deleteDevice(customerDetails1.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token1, HttpStatus.SC_OK, null);

    }



    @Test(dataProvider = "Update_Parameters")
    public void deviceTest4(String customerId,String deviceType, String deviceVersion, String locale, String appVersion, String location) throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerId);
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        AddDevice updatedDevice=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse(deviceType, deviceVersion, locale, appVersion, location), token, customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), HttpStatus.SC_OK, null, HttpMethod.PUT);
        getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice,customerDetails.getCustomerId()),customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_OK, null);

    }

    @DataProvider(name = "Update_Parameters")
    public Object[][] createUpdateParameters()  {
        return new Object[][]{
                //Verify device OS, locale and app version can be updated
                {"6","IOS","9.0.1","ja","2.7","ALLOW"},
                //update OS version of the device
                {"7","IOS","9.0.1","en","2.6.1","ALLOW"},
                //Verify for a device only app version can be updated
                {"8","IOS","9.0","en","2.7","ALLOW"},

        };
    }



    @Test
    public void deviceTest5() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "9");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), token,customerDetails.getCustomerId(), HttpStatus.SC_OK,null, HttpMethod.POST);
        GetDevice deviceDetailsIOS =(GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        //Get device without token
        getDeviceOperation.getDeviceDetails(apiUtils.getExpectedErrorResponse(11305,"Token missing"), customerDetails.getCustomerId(), "empty", HttpMethod.GET, HttpStatus.SC_UNAUTHORIZED);
         //Get device with invalid token
        getDeviceOperation.getDeviceDetails(apiUtils.getExpectedErrorResponse(11301,"Invalid token"), customerDetails.getCustomerId(), "4e567898765", HttpMethod.GET, HttpStatus.SC_UNAUTHORIZED);
        //Verify update device can not be performed without Authorization token
        deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), "empty", customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0), HttpStatus.SC_UNAUTHORIZED, apiUtils.getExpectedErrorResponse(11305,"Token missing"), HttpMethod.PUT);
        //Verify iOS device can not be updated without deviceToken
        deviceOperation.updateDevice(getAddDeviceExpectedResponseInfo("Basic","012344-8ae1-cd3129","IOS","9.0","en","2.6.1","ALLOW",null,"empty"),token,customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0),HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11105,"Device registration requires valid device token"),HttpMethod.PUT);
        //Verify update device can not be performed by providing wrong or invalid Token
        deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), "4e567898765", customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0), HttpStatus.SC_UNAUTHORIZED, apiUtils.getExpectedErrorResponse(11301,"Invalid token"), HttpMethod.PUT);
        //update android device with wrong customerID and deviceID
        deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), token, "1234","1111", HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11303,"Invalid Customer ID (UUID)"), HttpMethod.PUT);
        //Verify device cant be updated without valid Preferences
        deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "emptyPreference"), token, customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11113,"Device registration requires valid preferences"), HttpMethod.PUT);
        deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "empty"), token, customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11114,"Device registration requires valid location"), HttpMethod.PUT);
        deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "G"), token, customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11114,"Device registration requires valid location"), HttpMethod.PUT);

        //delete  device using wrong token
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0),"4e567898765", HttpStatus.SC_UNAUTHORIZED, apiUtils.getExpectedErrorResponse(11301,"Invalid token"));
        //Delete  device using empty token
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0),"empty", HttpStatus.SC_UNAUTHORIZED, apiUtils.getExpectedErrorResponse(11305,"Token missing"));
        //Delete the device with valid details
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0),  token, HttpStatus.SC_OK, null);
        //Verify updating a device which is deleted
        deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), token, customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11108,"Device not assigned to user"), HttpMethod.PUT);
        //Delete device which is already deleted
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11108,"Device not assigned to user"));
    }



    @Test(dataProvider = "Add_Device_details_Invalid_Parameter")
    public void deviceTest6(String customerId,AddDevice deviceDetail, ErrorDetails errorDetails) throws Exception {
        //Get token from DB using sql query
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerId);
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        BaseBean device = deviceOperation.addDevice(deviceDetail, token, customerDetails.getCustomerId(), HttpStatus.SC_BAD_REQUEST, errorDetails, HttpMethod.POST);
    }

    @DataProvider(name = "Add_Device_details_Invalid_Parameter")
    public Object[][] createInvalidDeviceData() throws Exception {
        return new Object[][]{
                {"10",getAddDeviceExpectedResponseInfo("Samsung Galaxy S6", "012344-8ae0-ab2122", "ssssssssssssssssssssssssssssssssssssssssssssss", "5.1.1", "en", "two.8", "ALLOW", "123231-123121-111", "64123A4512314111"), apiUtils.getExpectedErrorResponse(11102, "Device registration requires valid device type")},
                {"11",getAddDeviceExpectedResponseInfo("ssssssssssssssssssssssssssssssssssssssssssssss", "012344-8ae0-ab2122", "IOS", "9.0", "en", "2.6.1", "ALLOW", "123231-123121-111", "64123A4512314111"), apiUtils.getExpectedErrorResponse(11005, "Invalid arguments")},
                {"12",getAddDeviceExpectedResponseInfo("iPhone", "012344-8ae0-ab2122", "IOS", "ssssssssssssssssssssssssssssssssssssssssssssss", "en", "2.6.1", "ALLOW", "123231-123121-111", "64123A4512314111"), apiUtils.getExpectedErrorResponse(11112, "Device registration requires valid os version")},
                {"13",getAddDeviceExpectedResponseInfo("iPhone", "ssssssssssssssssssssssssssssssssssssssssssssss", "IOS", "9.0", "en", "2.6.1", "ALLOW", "123231-123121-111", "64123A4512314111"), apiUtils.getExpectedErrorResponse(11103, "Device registration requires valid channel Id")},
                {"14",getAddDeviceExpectedResponseInfo("iPhone", "012344-8ae0-ab2122", "IOS", "9.0", "ssssssssssssssssssssssssssssssssssssssssssssss", "2.6.1", "ALLOW", "123231-123121-111", "64123A4512314111"), apiUtils.getExpectedErrorResponse(11104, "Device registration requires valid locale")},
                {"15",getAddDeviceExpectedResponseInfo("ANDROID", "012344-8ae0-ab2122", "ANDROID", "9.0", "en", "2.6.1", "ALLOW", "ssssssssssssssssssssssssssssssssssssssssssssss", "64123A4512314111"), apiUtils.getExpectedErrorResponse(11106, "Device registration requires valid installation Id")},


        };
    }
    @Test(dataProvider = "Add_Device_details_ValidParameters")
    public void deviceTest7(String customerId,AddDevice deviceDetail) throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerId);
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device = (AddDevice) deviceOperation.addDevice(deviceDetail, token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetails = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetails, 0), token, HttpStatus.SC_OK, null);
    }

    @DataProvider(name = "Add_Device_details_ValidParameters")
    public Object[][] createValidDeviceData() throws Exception {
        return new Object[][]{
                {"15",getAddDeviceExpectedResponseInfo("iPhone", "012344-8ae0-ab2122", "IOS", "9.0", "en", "2.6.1", "ALLOW", "123231-123121-111", "64123A4512314111")},
                {"16",getAddDeviceExpectedResponseInfo("Samsung Galaxy S6", "012344-8ae0-ab2122", "IOS", "5.1.1", "en", "2.8", "ALLOW", "123231-123121-111", "64123A4512314111")},
                {"17",getAddDeviceExpectedResponseInfo("Samsung Galaxy S6", "012344-8ae0-ab2122", "IOS", "5*one.1", "en", "2.8", "ALLOW", "123231-123121-111", "64123A4512314111")},
                {"18",getAddDeviceExpectedResponseInfo("Samsung Galaxy #6", "012344-8ae0-ab2122", "IOS", "5.1.1", "en", "2.8", "ALLOW", "123231-123121-111", "64123A4512314111")},
                {"19",getAddDeviceExpectedResponseInfo("Samsung Galaxy S6", "01234four-8ae0-ab2122", "IOS", "5.1.1", "A!@11|", "2.8", "ALLOW", "123231-123121-111", "64123A4512314111")},
                {"20",getAddDeviceExpectedResponseInfo("Samsung Galaxy S6", "012344-8ae0-ab2122", "IOS", "5.1.1", "en", "two.8", "ALLOW", "123231-123121-111", "64123A4512314111")},
                {"21",getAddDeviceExpectedResponseInfo("Samsung Galaxy S6", "012344-8ae0-ab2122", "IOS", "5.1.1", "en", "two.8", "ALLOW", "123231-123121-!B*1", "64123A4512314111")},
                {"22",getAddDeviceExpectedResponseInfo("Samsung Galaxy S6", "012344-8ae0-ab2122", "IOS", "5.1.1", "en", "two.8", "ALLOW", "123231-123121-111", "64123A4512314AB*")},
                {"23",getAddDeviceExpectedResponseInfo("iPhone", "012344-8ae0-ab2122", "IOS", "9.0", "en", "2.6.1", "ALLOW", "123231-123121-111", "ssssssssssssssssssssssssssssssssssssssssssssss")},


        };
    }

    //Verify a device can be updated repeatedly
    @Test
    public void deviceTest8() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "24");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        int count = 3;
        GetDevice deviceUpdatedDetailsIOS=null;

        while (count != 0)
        {
            AddDevice updatedIOSDevice = (AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), HttpStatus.SC_OK, null, HttpMethod.PUT);
            deviceUpdatedDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedIOSDevice, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
            count--;
        }

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceUpdatedDetailsIOS, 0), token, HttpStatus.SC_OK, null);


    }

    @Test
    public void deviceTest9() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "25");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        //Get device with wrong customer ID
          getDeviceOperation.getDeviceDetails(apiUtils.getExpectedErrorResponse(11303,"Invalid Customer ID (UUID)"), "1234", token, HttpMethod.GET, HttpStatus.SC_BAD_REQUEST);
        //Verify Get device doesnt support PUT and Delete methods
          getDeviceOperation.getDeviceDetails(apiUtils.getExpectedErrorResponse(11007,"Resource not found"), customerDetails.getCustomerId(), token, HttpMethod.PUT, HttpStatus.SC_NOT_FOUND);
          getDeviceOperation.getDeviceDetails(apiUtils.getExpectedErrorResponse(11007,"Resource not found"), customerDetails.getCustomerId(), token, HttpMethod.DELETE, HttpStatus.SC_NOT_FOUND);

    }

    @Test
    public void deviceTest10() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "26");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        //Verify iOS device's device Token and ChannelID can be update
        AddDevice updatedIOSDetails=(AddDevice) deviceOperation.updateDevice(getAddDeviceExpectedResponseInfo("Basic","012344-8ae1-cd3129","IOS","9.0","en","2.6.1","ALLOW",null,"64123B4512314107"),token,customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetailsIOSAfterUpdate = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedIOSDetails, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOSAfterUpdate, 0), token, HttpStatus.SC_OK, null);


        AddDevice deviceANDROID= (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "9.0", "en", "2.6.1", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsANDROID= (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceANDROID, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        //Verify Android device's InstalltionID and ChannelID can be updated
        AddDevice updatedANDROIDDetails=(AddDevice) deviceOperation.updateDevice(getAddDeviceExpectedResponseInfo("Basic","012344-8ae1-ab2202","ANDROID","9.0","en","2.6.1","ALLOW","123231-123121-2102","64123B4512314107"),token,customerDetails.getCustomerId(),getDeviceID(deviceDetailsANDROID,0),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetailsANDROIDAfterUpdate = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedANDROIDDetails, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsANDROIDAfterUpdate, 0), token, HttpStatus.SC_OK, null);

    }
    //Verify Device cant be added without Location Field
    @Test
    public void deviceTest11() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "27");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        //Verify Device cant be added without Location Field
        deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "empty"), token, customerDetails.getCustomerId(), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11114,"Device registration requires valid location"), HttpMethod.POST);
        //Verify Device cant be added without valid Preference
        deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "emptyPreference"), token, customerDetails.getCustomerId(), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11113,"Device registration requires valid preferences"), HttpMethod.POST);
        //Verify Device cant be added with wrong location preference data
        deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "2.6.1", "G"), token, customerDetails.getCustomerId(), HttpStatus.SC_BAD_REQUEST, apiUtils.getExpectedErrorResponse(11114,"Device registration requires valid location"), HttpMethod.POST);


    }
    @Test(dataProvider = "update_Without_Mandatory_Fields")
    public void deviceTest12(String customerId,AddDevice deviceDetail, ErrorDetails errorDetails) throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerId);
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "9.0", "en", "2.6.1", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        deviceOperation.updateDevice(deviceDetail,token,customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,0),HttpStatus.SC_BAD_REQUEST,errorDetails,HttpMethod.PUT);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_OK, null);

    }

    @DataProvider(name = "update_Without_Mandatory_Fields")
    public Object[][] createIValidMandatoryFields() throws Exception {
        return new Object[][]{
                //update android device without OS version
                {"29",getAddDeviceExpectedResponseInfo("Basic","012344-8ae1-cd3129","ANDROID","empty","en","2.6.1","ALLOW","123231-123121-1117","64123B4512314107"),apiUtils.getExpectedErrorResponse(11112,"Device registration requires valid os version")},
                {"30",getAddDeviceExpectedResponseInfo("Basic","012344-8ae1-cd3129","ANDROID","9.0","empty","2.6.1","ALLOW","123231-123121-1117","64123B4512314107"),apiUtils.getExpectedErrorResponse(11104,"Device registration requires valid locale")},
                {"01",getAddDeviceExpectedResponseInfo("Basic","012344-8ae1-cd3129","ANDROID","9.0","en","empty","ALLOW","123231-123121-1117","64123B4512314107"),apiUtils.getExpectedErrorResponse(11111,"Device registration requires valid app version")},
                {"02",getAddDeviceExpectedResponseInfo("Basic","empty","ANDROID","9.0","en","2.6.1","ALLOW","123231-123121-1117","64123B4512314107"),apiUtils.getExpectedErrorResponse(11103,"Device registration requires valid channel Id")},
                {"03",getAddDeviceExpectedResponseInfo("Basic","012344-8ae1-cd3129","ANDROID","9.0","en","2.6.1","ALLOW","empty","64123B4512314107"),apiUtils.getExpectedErrorResponse(11106,"Device registration requires valid installation Id")}
        };

    }


}


