package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.*;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.*;

import javax.ws.rs.HttpMethod;

import static org.testng.Assert.assertTrue;

public class EventGroupTest extends GSNotificationGatewayBaseTest {
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    AppFeatureOperation appFeature = AppFeatureOperation.getInstance();
    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();
    GetCustomerDetailsOperation customerInfo = GetCustomerDetailsOperation.getInstance();
    EventOperation eventOperation = EventOperation.getInstance();
    NotificationStatusOperation notificationStatus = NotificationStatusOperation.getInstance();
    NotificationGatewayDB notificationdb = NotificationGatewayDB.getInstance();

    private GetCustomerDetails getExpectedResponse() {
        GetCustomerDetails info = new GetCustomerDetails();
        info.setType("CustomerDetail");
        return info;

    }

    private String getDeviceID(GetDevice deviceDetails, Integer index) {
        return deviceDetails.getList().get(index).getDeviceId();
    }

    //Verify notification New_Location event will be sent to Beta user
    @Test
    public void eventTest1() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "1");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceANDROID = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "9.0", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsANDROID = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceANDROID, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        Event eventCreated=(Event)eventOperation.createEvent(apiUtils.getEventData("LOW_BALANCE",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsANDROID, 0)), "Device id is not Present in table");
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),deviceDetailsANDROID.getList().get(0).getDeviceId(),token,HttpStatus.SC_OK,null);

    }
    //Verify New_Location event will not be created to user who is part of Alpha group
    @Test
    public void eventTest2() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "2");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        Event eventCreated = (Event) eventOperation.createEvent(apiUtils.getEventData("LOW_BALANCE", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsIOS, 0)), "Device id is not Present in table");
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),deviceDetailsIOS.getList().get(0).getDeviceId(),token,HttpStatus.SC_OK,null);

    }

    //Verify events can be created for a user who belongs to general group
    @Test
    public void eventTest3() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "3");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.0", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        Event eventCreated = (Event) eventOperation.createEvent(apiUtils.getEventData("SUBSCRIPTION_EXPIRED", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsIOS, 0)), "Device id is not Present in table");
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),deviceDetailsIOS.getList().get(0).getDeviceId(),token,HttpStatus.SC_OK,null);

    }





}
