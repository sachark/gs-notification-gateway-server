package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.*;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;

import static org.testng.Assert.assertTrue;


public class EventTest  extends GSNotificationGatewayBaseTest{
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();
    GetCustomerDetailsOperation customerInfo = GetCustomerDetailsOperation.getInstance();
    EventOperation eventOperation = EventOperation.getInstance();
    NotificationStatusOperation notificationStatus = NotificationStatusOperation.getInstance();
    NotificationGatewayDB notificationdb = NotificationGatewayDB.getInstance();


    private GetCustomerDetails getExpectedResponse() {
        GetCustomerDetails info = new GetCustomerDetails();
        info.setType("CustomerDetail");
        return info;

    }

    private String getDeviceID(GetDevice deviceDetails, Integer index) {
        return deviceDetails.getList().get(index).getDeviceId();
    }

    public String createDevice() throws Exception {
        //Call Get Customer Details API and Get UUID
        GetCustomerDetails customerDetails = (GetCustomerDetails) apiUtils.getCustomerDetails();
        String customerId = customerDetails.getCustomerId();
        String token = customerDetails.getAdditionalProperties().get("token").toString();

        //Add Device to Customer
        AddDevice addDeviceResponse = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceDetails(),token,customerId, 200, null, HttpMethod.POST);

        //Call Get Device API and Validate the Added Device
        GetDevice getDevice = (GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(addDeviceResponse,customerId),customerId,token, HttpMethod.GET, HttpStatus.SC_OK);
        return getDevice.getCustomerId();
    }

    public BaseBean getEventData(String customerID,String type, String eventType, String emailID, String creationTime, String expiryTime, String iccid, String location, Integer balanceRemaining, Integer percentageConsumed) throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerID);
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        if (eventType.contains("CREDIT") || eventType.equals("REFERRAL_INITIATION"))
            return apiUtils.emailEventDetails(eventType, emailID);
        else {
            Event eventData = new Event();
            if (type != "empty")
                eventData.setType("EventDetail");
            if (eventType != "empty")
                eventData.setEventType(eventType);
            eventData.setCustomerId(customerDetails.getCustomerId());
            if (emailID != "empty")
                eventData.setUserId(emailID);

            if (eventType.equals("LOW_BALANCE") || eventType.equals("SUBSCRIPTION_NEARING_EXPIRY")) {
                if (balanceRemaining != 0)
                    eventData.setBalanceRemaining(balanceRemaining);
            } else if (eventType.equals("DATA_USED")) {
                if (percentageConsumed != 0)
                    eventData.setPercentageConsumed(percentageConsumed);
            }
            if (!(eventType.equals("NEW_LOCATION"))) {
                if (expiryTime != "empty")
                    eventData.setExpiry(expiryTime);
            }

            if (creationTime != "empty")
                eventData.setCreationTime(creationTime);
            if (iccid != "empty")
                eventData.setIccId(iccid);
            if (location != "empty")
                eventData.setLocation(location);

            Options options = new Options();
            options.setSimName("TEST");
            eventData.setOptions(options);
//            deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetails), token, HttpStatus.SC_OK, null);
            return eventData;
        }
    }

   @Test(enabled = true,dataProvider = "Mandatory_Event_Fields")
    public void eventTest1(BaseBean eventDetails,ErrorDetails errorDetails) throws Exception {
        BaseBean eventCreated= eventOperation.createEvent(eventDetails, errorDetails, HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);
        Thread.sleep(5000);

    }
    @DataProvider(name = "Mandatory_Event_Fields")
        public Object[][] createMandatoryFieldData() throws Exception {
            return new Object[][]{
                    //Create event without Type
                    {getEventData("1","empty","DATA_USED","smaragal@gigsky.com","2018-12-14 22:51:04","2025-12-03 12:37:55","8910300000000006173","US",5,50),apiUtils.getExpectedErrorResponse(11202,"Event creation requires valid type")},
                    //Create event without EventType
                     {getEventData("2","EventDetail","empty","smaragal@gigsky.com","2018-12-14 22:51:04","2025-12-03 12:37:55","8910300000000006173","US",5,50),apiUtils.getExpectedErrorResponse(11203,"Event creation requires valid event type")},
                   //Create event without userId
                   {getEventData("3","EventDetail","DATA_USED","smaragal@gigsky.com","2018-12-14 22:51:04","2025-12-03 12:37:55","8910300000000006173","empty",5,50),apiUtils.getExpectedErrorResponse(11204 ,"Event creation requires valid location")},
                    //Create event without ICCID
                    {getEventData("4","EventDetail","DATA_USED","smaragal@gigsky.com","2018-12-14 22:51:04","2025-12-03 12:37:55","empty","US",5,50),apiUtils.getExpectedErrorResponse(11211,"Event creation requires valid iccid")},
                   //Create event without creationTime
                  {getEventData("5","EventDetail","DATA_USED","smaragal@gigsky.com","empty","2025-12-03 12:37:55","8910300000000006173","US",5,50),apiUtils.getExpectedErrorResponse(11210,"Event creation requires valid creation time")},
                    //Verify location is mandatory field to create NEW_LOCATION event
                   {getEventData("6","EventDetail","NEW_LOCATION","smaragal@gigsky.com","018-12-14 22:51:04","2025-12-03 12:37:55","8910300000000006173","empty",5,50),apiUtils.getExpectedErrorResponse(11204,"Event creation requires valid location")},
                   //Verify percentageConsumed is mandatory field to create DATA_USED event
                  {getEventData("7","EventDetail","DATA_USED","smaragal@gigsky.com","018-12-14 22:51:04","2025-12-03 12:37:55","8910300000000006173","US",5,0),apiUtils.getExpectedErrorResponse(11209,"Event creation requires valid percentage consumed")},
                   //Verify  balance remaining is mandatory field to create LOW_BALANCE event
                   {getEventData("8","EventDetail","LOW_BALANCE","smaragal@gigsky.com","018-12-14 22:51:04","2025-12-03 12:37:55","8910300000000006173","US",0,50),apiUtils.getExpectedErrorResponse(11207,"Event creation requires valid balance remaining")},
                  //Verify expiry is mandatory field to create LOW_BALANCE event
                   {getEventData("9","EventDetail","LOW_BALANCE","smaragal@gigsky.com","018-12-14 22:51:04","empty","8910300000000006173","US",5,50),apiUtils.getExpectedErrorResponse(11206,"Event creation requires valid expiry")}

            };
        }

    //Verify creating an event to user who has no devices registered
    @Test(enabled = true)
    public void eventTest2() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "10");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        Event eventCreated = (Event) eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "NOT_DELIVERED"), getEvent.getNotificationIds(0));

    }
    //Verify an event can be created for an user having 3 devices
    @Test
    public void eventTest3() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "11");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        AddDevice deviceANDROID = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsAndroid = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, deviceANDROID, null,customerDetails.getCustomerId(),2 ), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        AddDevice deviceIOS2 = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "3.0", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS2 = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, deviceANDROID, deviceIOS2,customerDetails.getCustomerId(),3 ), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        Event eventCreated = (Event) eventOperation.createEvent(apiUtils.getEventData("LOW_BALANCE", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));

        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsIOS2, 0)), "Device id is not Present in table");


        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_OK, null);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsAndroid, 1), token, HttpStatus.SC_OK, null);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS2, 2), token, HttpStatus.SC_OK, null);

    }




//Verify after deleting a device event will not be sent to that device
    @Test(enabled = true)
    public void eventTest4() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "12");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        AddDevice deviceANDROID = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetails = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, deviceANDROID, null, customerDetails.getCustomerId(),2 ), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        Event eventCreated = (Event) eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_OK, null);
        GetDevice deviceDetailsAfterDeletion = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceANDROID, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        Event eventCreatedNEW_LOCATION = (Event) eventOperation.createEvent(apiUtils.getEventData("SUBSCRIPTION_EXPIRED", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreatedNEW_LOCATION.setNotificationIds(null);
        Event getEventLOCATION = (Event) eventOperation.getEvent(eventCreatedNEW_LOCATION, eventCreatedNEW_LOCATION.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEventLOCATION.getNotificationIds(0), "COMPLETE", getEventLOCATION.getEventId(), "PUSH", "COMPLETE_SENT"), getEventLOCATION.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEventLOCATION.getNotificationIds(0)).equals(getDeviceID(deviceDetailsAfterDeletion, 0)), "Device id is not Present in table");

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsAfterDeletion, 0), token, HttpStatus.SC_OK, null);



    }
    //Verify notification will be sent to device which updated recently when deviceCount=2

    @Test
    public void eventTest5() throws Exception {
        //Update device count as 2 in config key values table
        notificationdb.updateDeviceCount(gatewayDBConfigInfo);

        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "13");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        AddDevice deviceANDROID = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsAndroid = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, deviceANDROID, null,customerDetails.getCustomerId(),2 ), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        AddDevice deviceIOS2 = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "6.0", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS2 = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, deviceANDROID, deviceIOS2,customerDetails.getCustomerId(),3 ), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        Event eventCreated = (Event) eventOperation.createEvent(apiUtils.getEventData("SUBSCRIPTION_NEARING_EXPIRY", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsIOS2, 0)), "Device id is not Present in table");

        AddDevice updatedDevice=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","6.0","en","3.0","ALLOW"),token,customerDetails.getCustomerId(),getDeviceID(deviceDetailsAndroid,1),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice afterUpdating = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice,deviceIOS2,deviceIOS,customerDetails.getCustomerId(),3), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);


        Event eventCreatedAfterUpdate = (Event) eventOperation.createEvent(apiUtils.getEventData("SUBSCRIPTION_EXPIRED", customerDetails.getCustomerId(), "smaragal@gigsky.com"), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event eventAfterUpdate = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", eventAfterUpdate.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsIOS2, 0)), "Device id is not Present in table");

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(afterUpdating, 0), token, HttpStatus.SC_OK, null);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(afterUpdating, 1), token, HttpStatus.SC_OK, null);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(afterUpdating, 2), token, HttpStatus.SC_OK, null);

    }

    @Test(enabled = true,dataProvider = "Past_Expiry_Date")
    public void eventTest6(String eventType, Integer balanceRemaining, Integer percentageConsumed,String statusMessage, String iccid) throws Exception {

        Event eventCreated = (Event) eventOperation.createEvent(getEventData("14","EventDetail", eventType, "smaragal@gigsky.com", "2015-12-14 22:51:04", "2015-12-03 12:37:55", iccid, "US", balanceRemaining, percentageConsumed), null, HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEventLOCATION = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEventLOCATION.getNotificationIds(0), "COMPLETE", getEventLOCATION.getEventId(), "PUSH", statusMessage), getEventLOCATION.getNotificationIds(0));
    }

    @DataProvider(name = "Past_Expiry_Date")
    public Object[][] createPastExpiryDate() throws Exception {

        return new Object[][]{
                //Verify for a LOW_BALANCE event with past expiry date Notification statusMessage will be EXPIRED
                {"LOW_BALANCE", 5, 0,"EXPIRED","8910300000000006182"},
                {"DATA_USED", 0, 50,"EXPIRED","8910300000000006183"},
                {"SUBSCRIPTION_NEARING_EXPIRY", 15, 0,"EXPIRED","8910300000000006184"},
                {"SUBSCRIPTION_EXPIRED", 0, 0,"NOT_DELIVERED","8910300000000006185"}
        };
    }

    @Test(enabled = true)
    public void eventTest7() throws Exception {
        //Verify event can not be created by providing invalid CustomerID
        Event event=(Event)getEventData("15","EventDetail", "DATA_USED", "smaragal@gigsky.com", "2015-12-14 22:51:04", "2025-12-31 12:37:55", "8910300000000006186", "US", 0, 0);
        event.setCustomerId("1234");
        BaseBean eventCreated =  eventOperation.createEvent(event,apiUtils.getExpectedErrorResponse(11303,"Invalid Customer ID (UUID)"), HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);
        //"Verify details of an event by providing invalid event ID
        eventOperation.getEvent(apiUtils.getExpectedErrorResponse(11005,"Invalid arguments"), "1111111", HttpStatus.SC_BAD_REQUEST);
         }

        // Verify for a user with 2 devices LocationEvent will be delivered to only one device with Location preference=ALLOW
    @Test(enabled = true)
    public void eventTest8() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "16");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "5.1.1", "en", "3.0", "DENY"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        AddDevice deviceIOS2 = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "6.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetails = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS,deviceIOS2, null, customerDetails.getCustomerId(),2 ), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        Event eventCreated =  (Event )eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEventLOCATION = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEventLOCATION.getNotificationIds(0), "COMPLETE", getEventLOCATION.getEventId(), "PUSH", "COMPLETE_SENT"), getEventLOCATION.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEventLOCATION.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 1)), "Device id is not Present in table");

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_OK, null);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetails, 1), token, HttpStatus.SC_OK, null);


    }
    //Verify notifications status will be failed after retrying
    @Test(enabled = true)
    public void eventTest9() throws Exception {
        notificationdb.updateUA_FAILURE_MODE_YES(gatewayDBConfigInfo);

        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "17");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "5.1.1", "en", "3.0", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
         Thread.sleep(1000);
        Event eventCreated=(Event)eventOperation.createEvent(apiUtils.getEventData("LOW_BALANCE",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null, HttpStatus.SC_OK, HttpMethod.POST);
        //Verify failCount = 2 after trying for first time
        Thread.sleep(15000);
        assertTrue(notificationdb.getFailCount(gatewayDBConfigInfo,eventCreated.getEventId()).equals("2"),"Fail count is incorrect");


        eventCreated.setNotificationIds(null);
        eventCreated.setStatusMessage("FAILED_AFTER_RETRIES");
        Thread.sleep(5000);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "FAILED", getEvent.getEventId(), "PUSH", "FAILED_PUSH_PROCESS"), getEvent.getNotificationIds(0));
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(), getDeviceID(deviceDetailsIOS, 0), token, HttpStatus.SC_OK, null);


    }
}