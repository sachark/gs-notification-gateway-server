package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.common.utils.TestUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.*;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;

import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class EventTest1 extends GSNotificationGatewayBaseTest {
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    AppFeatureOperation appFeature = AppFeatureOperation.getInstance();
    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();
    GetCustomerDetailsOperation customerInfo = GetCustomerDetailsOperation.getInstance();
    EventOperation eventOperation = EventOperation.getInstance();
    NotificationStatusOperation notificationStatus = NotificationStatusOperation.getInstance();
    NotificationGatewayDB notificationdb = NotificationGatewayDB.getInstance();


    private GetCustomerDetails getExpectedResponse() {
        GetCustomerDetails info = new GetCustomerDetails();
        info.setType("CustomerDetail");
        return info;

    }

    private String getDeviceID(GetDevice deviceDetails, Integer index) {
        return deviceDetails.getList().get(index).getDeviceId();
    }

    @After
    public void revertRateControll() throws Exception {
        notificationdb.update_Revert_RATECONTROL_KEY(gatewayDBConfigInfo);

    }

    //Verify notification will not be sent to iOS device having app lower than min client version
    @Test(enabled = true)
    public void eventTest1() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "1");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.1", "en", "2.5", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        Event eventCreated=(Event)eventOperation.createEvent(apiUtils.getEventData("LOW_BALANCE",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "NOT_DELIVERED"), getEvent.getNotificationIds(0));
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),deviceDetailsIOS.getList().get(0).getDeviceId(),token,HttpStatus.SC_OK,null);
    }
//Verify notification will not be sent to Android device having app lower than min client version
    @Test(enabled = true)
    public void eventTest2() throws Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "2");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice device = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "9.1", "en", "2.5", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetails = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
        Event eventCreated=(Event)eventOperation.createEvent(apiUtils.getEventData("LOW_BALANCE",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "NOT_DELIVERED"), getEvent.getNotificationIds(0));
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),deviceDetails.getList().get(0).getDeviceId(),token,HttpStatus.SC_OK,null);
    }
    //Verify notification will be sent to only iOS device which is supported by min client version
    @Test(enabled = true)
    public  void eventTest3() throws Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "3");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "2.5", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetails = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.1.1", "en", "2.6.1", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS,device,null, customerDetails.getCustomerId(),2), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        Event eventCreated=(Event)eventOperation.createEvent(apiUtils.getEventData("SUBSCRIPTION_NEARING_EXPIRY",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsIOS, 1)), "Device id is not Present in table");

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,1),token,HttpStatus.SC_OK,null);

    }
    //Verify notification will be sent to only android device which is supported by min client version
    @Test(enabled = true)
    public  void eventTest4() throws Exception{


        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "4");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "2.6", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetails = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device, customerDetails.getCustomerId()), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        AddDevice deviceIOS = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "9.1.1", "en", "2.5", "ALLOW"), token, customerDetails.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS,device,null, customerDetails.getCustomerId(),2), customerDetails.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);

        Event eventCreated=(Event)eventOperation.createEvent(apiUtils.getEventData("SUBSCRIPTION_NEARING_EXPIRY",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event getEvent = (Event) eventOperation.getEvent(eventCreated, eventCreated.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEvent.getNotificationIds(0), "COMPLETE", getEvent.getEventId(), "PUSH", "COMPLETE_SENT"), getEvent.getNotificationIds(0));
        //Verify notification is sent to only iOS device mockPushNotificationtable
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEvent.getNotificationIds(0)).equals(getDeviceID(deviceDetailsIOS, 0)), "Device id is not Present in table");

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetailsIOS,1),token,HttpStatus.SC_OK,null);

    }
    //Verify same event can not be created repeatedly and its controlled by rateControl
    @Test(enabled = true)
    public  void eventTest5() throws Exception{
        notificationdb.update_RATECONTROL_KEY(gatewayDBConfigInfo);
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "5");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);
        Event eventCreated=(Event)eventOperation.createEvent(apiUtils.getEventData("DATA_USED",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", "COMPLETE_SENT"), event.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, event.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");


        Event eventCreated2=(Event)eventOperation.createEvent(apiUtils.getEventData("DATA_USED",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated2.setNotificationIds(null);
        eventCreated2.setStatusMessage("RATE_CONTROLLED");
        eventOperation.getEvent(eventCreated2,eventCreated2.getEventId(),HttpStatus.SC_OK);

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);
        notificationdb.update_Revert_RATECONTROL_KEY(gatewayDBConfigInfo);

    }
    //Out_Of_order policy when data_used event is created after creating Low_balance event
    @Test(enabled = true)
    public  void eventTest6() throws  Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "6");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData = (Event) apiUtils.getEventData("LOW_BALANCE",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setCreationTime("2015-12-14 22:51:04");

        Event eventCreated=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", "COMPLETE_SENT"), event.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, event.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");

        createEventData = (Event) apiUtils.getEventData("DATA_USED",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setCreationTime("2015-12-14 22:47:04");
        Event eventCreatedOutOfOrder=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);

        Thread.sleep(5000);
        eventCreatedOutOfOrder.setStatusMessage("OUT_OF_ORDER");
       eventOperation.getEvent(eventCreatedOutOfOrder,eventCreatedOutOfOrder.getEventId(),HttpStatus.SC_OK);
       deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);

    }
    //Test Out_Of_order policy when sub_nearing_expiry event is created after creating Subscription_expired event
    @Test(enabled = true)
    public void  eventTest7() throws Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "7");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);
        Event createEventData = (Event) apiUtils.getEventData("SUBSCRIPTION_EXPIRED",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setCreationTime("2015-12-14 22:51:04");
        Event eventCreated=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", "COMPLETE_SENT"), event.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, event.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");

        createEventData = (Event) apiUtils.getEventData("SUBSCRIPTION_NEARING_EXPIRY",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setCreationTime("2015-12-14 22:47:04");
        Event eventCreatedOutOfOrder=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);

        Thread.sleep(5000);
        eventCreatedOutOfOrder.setStatusMessage("OUT_OF_ORDER");
        eventOperation.getEvent(eventCreatedOutOfOrder,eventCreatedOutOfOrder.getEventId(),HttpStatus.SC_OK);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);

    }
    //Verify rate control is not applicable for Different ICCID
    @Test(enabled = true)
    public void eventTest8() throws  Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "8");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setIccId("8910300000000006174");
        Event eventCreated=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", "COMPLETE_SENT"), event.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, event.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");

        Event createEventDataNewICCID= (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventDataNewICCID.setIccId("8910300000000006178");
        Event eventCreatedNewICCID=(Event)eventOperation.createEvent(createEventDataNewICCID,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreatedNewICCID.setNotificationIds(null);
        Event eventNewICCID=(Event)eventOperation.getEvent(eventCreatedNewICCID,eventCreatedNewICCID.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(eventNewICCID.getNotificationIds(0), "COMPLETE", eventNewICCID.getEventId(), "PUSH", "COMPLETE_SENT"), eventNewICCID.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, eventNewICCID.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);

        }
    @Test(enabled = true,dataProvider = "Location_Preference_DENY")
    public  void  eventTest9(String customerId,String eventType,String Iccid,String statusMessage) throws Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerId);
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","DENY"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);


        Event createEventData = (Event) apiUtils.getEventData(eventType,customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setIccId(Iccid);
        Event eventCreated=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", statusMessage), event.getNotificationIds(0));
        if(eventType!="NEW_LOCATION")
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, event.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");
      deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);

    }
    @DataProvider(name = "Location_Preference_DENY")
    public Object[][] locationPreferenceDeny() throws Exception {

        return new Object[][]{
                {"9","NEW_LOCATION","8910300000000006173","NOT_DELIVERED"},
                {"10","DATA_USED","8910300000000006110","COMPLETE_SENT"},
                {"11","LOW_BALANCE","8910300000000006187","COMPLETE_SENT"},
                {"12","SUBSCRIPTION_NEARING_EXPIRY","8910300000000006173","COMPLETE_SENT"},
                {"13","SUBSCRIPTION_EXPIRED","8910300000000006174","COMPLETE_SENT"}
        };
    }
    //Update Location preference from ALLOW to DENY
    @Test(enabled = true)
    public void eventTest10() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "14");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setIccId("8910300000000006180");
        Event eventCreated=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", "COMPLETE_SENT"), event.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, event.getNotificationIds(0)).equals(getDeviceID(deviceDetails, 0)), "Device id is not Present in table");

        AddDevice updatedDevice=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","DENY"),token,customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceUpdatedDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventDataUpdated = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventDataUpdated.setIccId("8910300000000006177");
        Event updatedEventCreated=(Event)eventOperation.createEvent(createEventDataUpdated,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        updatedEventCreated.setNotificationIds(null);
        updatedEventCreated.setStatusMessage("COMPLETE_WITH_NOTIFICATION");
        Event updatedEvent=(Event)eventOperation.getEvent(updatedEventCreated,updatedEventCreated.getEventId(),HttpStatus.SC_OK);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(updatedEvent.getNotificationIds(0), "COMPLETE", updatedEvent.getEventId(), "PUSH", "NOT_DELIVERED"), updatedEvent.getNotificationIds(0));


        AddDevice updatedDevice2=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),getDeviceID(deviceUpdatedDetails,0),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceUpdatedDetails2=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice2,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData1 = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData1.setIccId("8910300000000006181");
        Event eventCreated1=(Event)eventOperation.createEvent(createEventData1,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated1.setNotificationIds(null);
        Event event1=(Event)eventOperation.getEvent(eventCreated1,eventCreated1.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event1.getNotificationIds(0), "COMPLETE", event1.getEventId(), "PUSH", "COMPLETE_SENT"), event1.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, event1.getNotificationIds(0)).equals(getDeviceID(deviceUpdatedDetails2, 0)), "Device id is not Present in table");

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceUpdatedDetails2,0),token,HttpStatus.SC_OK,null);


    }
    //Update Location preference from DENY to ALLOW
    @Test(enabled = true)
    public void eventTest11() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "15");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","DENY"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData.setIccId("8910300000000006180");
        Event eventCreated=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", "NOT_DELIVERED"), event.getNotificationIds(0));

        AddDevice updatedDevice=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceUpdatedDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventDataUpdated = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventDataUpdated.setIccId("8910300000000006177");
        Event updatedEventCreated=(Event)eventOperation.createEvent(createEventDataUpdated,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        updatedEventCreated.setNotificationIds(null);
        Event updatedEvent=(Event)eventOperation.getEvent(updatedEventCreated,updatedEventCreated.getEventId(),HttpStatus.SC_OK);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(updatedEvent.getNotificationIds(0), "COMPLETE", updatedEvent.getEventId(), "PUSH", "COMPLETE_SENT"), updatedEvent.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, updatedEvent.getNotificationIds(0)).equals(getDeviceID(deviceUpdatedDetails, 0)), "Device id is not Present in table");


        AddDevice updatedDevice2=(AddDevice) deviceOperation.updateDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","DENY"),token,customerDetails.getCustomerId(),getDeviceID(deviceUpdatedDetails,0),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceUpdatedDetails2=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(updatedDevice2,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData1 = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com");
        createEventData1.setIccId("8910300000000006181");
        Event eventCreated1=(Event)eventOperation.createEvent(createEventData1,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated1.setNotificationIds(null);
        Event event1=(Event)eventOperation.getEvent(eventCreated1,eventCreated1.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event1.getNotificationIds(0), "COMPLETE", event1.getEventId(), "PUSH", "NOT_DELIVERED"), event1.getNotificationIds(0));

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceUpdatedDetails2,0),token,HttpStatus.SC_OK,null);


    }
   // Create an event with length of SIM name = 255 chars
    @Test(enabled = true)
    public void eventTest12() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "16");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.POST);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData = (Event) apiUtils.getEventData("NEW_LOCATION",customerDetails.getCustomerId(),"smaragal@gigsky.com","8910300000000006180","ABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXY12345");

        Event eventCreated=(Event)eventOperation.createEvent(createEventData,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        eventCreated.setNotificationIds(null);
        Event event=(Event)eventOperation.getEvent(eventCreated,eventCreated.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(5000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(event.getNotificationIds(0), "COMPLETE", event.getEventId(), "PUSH", "COMPLETE_SENT"), event.getNotificationIds(0));
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);


    }
    //Create an event with length of SIM name = 256 chars
@Test(enabled = true)
    public void eventTest13() throws Exception {
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "17");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.POST);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event createEventData = (Event) apiUtils.getEventData("SUBSCRIPTION_EXPIRED",customerDetails.getCustomerId(),"smaragal@gigsky.com","8910300000000006180","ABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXYABCDEFGHIJKLMNOPQRSTUVWXY123456");

        eventOperation.createEvent(createEventData,apiUtils.getExpectedErrorResponse(11213,"Event creation requires valid SIM name"),HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);
        //Create a sub_expired  event without options
        Event createEventDataWithoutOptions = (Event) apiUtils.getEventData("SUBSCRIPTION_EXPIRED",customerDetails.getCustomerId(),"smaragal@gigsky.com","8910300000000006180",null);
        createEventDataWithoutOptions.setOptions(null);
        eventOperation.createEvent(createEventDataWithoutOptions,apiUtils.getExpectedErrorResponse(11212,"Event creation requires valid options"),HttpStatus.SC_BAD_REQUEST, HttpMethod.POST);
       deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);

    }
    //Verify when Data_Used event is disabled, Notification will not be created
@Test(enabled = true)
    public void eventTest14() throws Exception{

        notificationdb.disable_DataUsedEventType(gatewayDBConfigInfo);
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "18");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event event=(Event) eventOperation.createEvent(apiUtils.getEventData("DATA_USED",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        event.setStatusMessage("COMPLETE_WITHOUT_NOTIFICATION");
        Event eventCreated=(Event)eventOperation.getEvent(event,event.getEventId(),HttpStatus.SC_OK);
        notificationdb.enable_DataUsedEventType(gatewayDBConfigInfo);
        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);
    }
    //Restart of tomcat after creating an event
    @Test(enabled = true)
    public void  eventTest15() throws Exception{
        String token = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "19");
        BaseBean customerDetails = customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW"),token,customerDetails.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails.getCustomerId()),customerDetails.getCustomerId(),token,HttpMethod.GET,HttpStatus.SC_OK);

        Event event=(Event) eventOperation.createEvent(apiUtils.getEventData("SUBSCRIPTION_NEARING_EXPIRY",customerDetails.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        TestUtils.executeShellScript("stopTomcat.sh");
        Thread.sleep(15000);
        TestUtils.executeShellScript("startTomcat.sh");
        Thread.sleep(20000);
        event.setNotificationIds(null);
        Event eventDetails=(Event) eventOperation.getEvent(event,event.getEventId(),HttpStatus.SC_OK);
        Thread.sleep(20000);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(eventDetails.getNotificationIds(0), "COMPLETE", eventDetails.getEventId(), "PUSH", "COMPLETE_SENT"), eventDetails.getNotificationIds(0));

        deleteDeviceOperation.deleteDevice(customerDetails.getCustomerId(),getDeviceID(deviceDetails,0),token,HttpStatus.SC_OK,null);

    }

    //Verify event is delivered to only one user
    @Test(enabled = true)
    public  void eventTest16()throws  Exception{
        //Get token for User 1
        String token1 = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "20");
        BaseBean customerDetails1 = customerInfo.getCustomerDetails(getExpectedResponse(), token1, HttpStatus.SC_OK);
        //Add IOS device
        AddDevice device=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW"),token1,customerDetails1.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        String channelId=device.getChannelId();
        String installationId=device.getInstallationId();
        String deviceToken=device.getDeviceToken();
        //Get the Device details
        GetDevice deviceDetails=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails1.getCustomerId()),customerDetails1.getCustomerId(),token1,HttpMethod.GET,HttpStatus.SC_OK);

        //Get Token for user 2
        String token2 = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "21");
        BaseBean customerDetails2 = customerInfo.getCustomerDetails(getExpectedResponse(), token2, HttpStatus.SC_OK);
        //Add the same IOS device for user2
        AddDevice deviceDetails2=apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW");
        deviceDetails2.setChannelId(channelId);
        deviceDetails2.setInstallationId(installationId);
        deviceDetails2.setDeviceToken(deviceToken);
        AddDevice device2=(AddDevice) deviceOperation.addDevice(deviceDetails2,token2,customerDetails2.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        //Get the device of user2
        GetDevice deviceDetailsUser2=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device2,customerDetails2.getCustomerId()),customerDetails2.getCustomerId(),token2,HttpMethod.GET,HttpStatus.SC_OK);

        //after adding same device to user2 Get device for user1 should be zero
        GetDevice deviceDetailsUser1= apiUtils.getDeviceDetailsExpectedResponse(device,customerDetails1.getCustomerId());
        List<DeviceList> deviceLists = new ArrayList<>();
        deviceDetailsUser1.setCount(0);
        deviceDetailsUser1.setTotalCount(0);
        deviceDetailsUser1.setList(deviceLists);
        GetDevice deviceDetailsUSer1=(GetDevice)getDeviceOperation.getDeviceDetails(deviceDetailsUser1,customerDetails1.getCustomerId(),token1,HttpMethod.GET,HttpStatus.SC_OK);

        //Create a new location event for user 1
        Event event=(Event) eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION",customerDetails1.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        event.setNotificationIds(null);
        Event eventDetails=(Event) eventOperation.getEvent(event,event.getEventId(),HttpStatus.SC_OK);
        //Verify notification status is complete not delivered
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(eventDetails.getNotificationIds(0), "COMPLETE", eventDetails.getEventId(), "PUSH", "NOT_DELIVERED"), eventDetails.getNotificationIds(0));

        //Create a new location event for user 2
        Event event2=(Event) eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION",customerDetails2.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        event2.setNotificationIds(null);
        Event eventDetails2=(Event) eventOperation.getEvent(event2,event2.getEventId(),HttpStatus.SC_OK);
        //Verify notification status is complete and  delivered
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(eventDetails2.getNotificationIds(0), "COMPLETE", eventDetails2.getEventId(), "PUSH", "COMPLETE_SENT"), eventDetails2.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, eventDetails2.getNotificationIds(0)).equals(getDeviceID(deviceDetailsUser2, 0)), "Device id is not Present in table");

        // Add same iOS device to user 1
        AddDevice deviceDetails01=apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW");
        deviceDetails01.setChannelId(channelId);
        deviceDetails01.setInstallationId(installationId);
        deviceDetails01.setDeviceToken(deviceToken);
        AddDevice device01=(AddDevice) deviceOperation.addDevice(deviceDetails01,token1,customerDetails1.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        //Get the device of user1
        GetDevice deviceDetailsUser01=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device01,customerDetails1.getCustomerId()),customerDetails1.getCustomerId(),token1,HttpMethod.GET,HttpStatus.SC_OK);

        //after adding same device to user1 Get device for user2 should be zero
        GetDevice deviceDetailsUser02= apiUtils.getDeviceDetailsExpectedResponse(device2,customerDetails2.getCustomerId());
        List<DeviceList> lists2 = new ArrayList<>();
        deviceDetailsUser02.setCount(0);
        deviceDetailsUser02.setTotalCount(0);
        deviceDetailsUser02.setList(lists2);
        GetDevice deviceDetailsUSer02=(GetDevice)getDeviceOperation.getDeviceDetails(deviceDetailsUser02,customerDetails2.getCustomerId(),token2,HttpMethod.GET,HttpStatus.SC_OK);

        //Create a new location event for user 2
        Event event02=(Event) eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION",customerDetails2.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        event02.setNotificationIds(null);
        Event eventDetails02=(Event) eventOperation.getEvent(event02,event02.getEventId(),HttpStatus.SC_OK);
        //Verify notification status is complete not delivered
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(eventDetails02.getNotificationIds(0), "COMPLETE", eventDetails02.getEventId(), "PUSH", "NOT_DELIVERED"), eventDetails02.getNotificationIds(0));


        //Create a new location event for user 1
        Event event01=(Event) eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION",customerDetails1.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        event01.setNotificationIds(null);
        Event eventDetails01=(Event) eventOperation.getEvent(event01,event01.getEventId(),HttpStatus.SC_OK);
        //Verify notification status is complete and  delivered
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(eventDetails01.getNotificationIds(0), "COMPLETE", eventDetails01.getEventId(), "PUSH", "COMPLETE_SENT"), eventDetails01.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, eventDetails01.getNotificationIds(0)).equals(getDeviceID(deviceDetailsUser01, 0)), "Device id is not Present in table");


        deleteDeviceOperation.deleteDevice(customerDetails1.getCustomerId(),getDeviceID(deviceDetailsUser01,0),token1,HttpStatus.SC_OK,null);
    }
    @Test(enabled = true)
    public  void eventTest17()throws  Exception{
        //Get Token for User1
        String token1 = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, "22");
        BaseBean customerDetails1 = customerInfo.getCustomerDetails(getExpectedResponse(), token1, HttpStatus.SC_OK);
        //Add two devices to user 1
        AddDevice device1=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW"),token1,customerDetails1.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
         String channelId=device1.getChannelId();
         String installationId=device1.getInstallationId();
         String deviceToken=device1.getDeviceToken();
        GetDevice deviceDetails1=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device1,customerDetails1.getCustomerId()),customerDetails1.getCustomerId(),token1,HttpMethod.GET,HttpStatus.SC_OK);
        AddDevice device2=(AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS","9.1","en","2.8","ALLOW"),token1,customerDetails1.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.PUT);
        GetDevice deviceDetails2=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device1,device2,null,customerDetails1.getCustomerId(),2),customerDetails1.getCustomerId(),token1,HttpMethod.GET,HttpStatus.SC_OK);

        //Get Token for User2
        String token2=gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"23");
        BaseBean customerDetails2=customerInfo.getCustomerDetails(getExpectedResponse(),token2,HttpStatus.SC_OK);
        //Add the same ios Device to user 2
        AddDevice deviceData=apiUtils.getAddDeviceExpectedResponse("IOS","7.1","en","2.8","ALLOW");
        deviceData.setChannelId(channelId);
        deviceData.setDeviceToken(deviceToken);
        deviceData.setInstallationId(installationId);
        AddDevice device01=(AddDevice)deviceOperation.addDevice(deviceData,token2,customerDetails2.getCustomerId(),HttpStatus.SC_OK,null,HttpMethod.POST);
        GetDevice deviceDetail02=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device01,customerDetails2.getCustomerId()),customerDetails2.getCustomerId(),token2,HttpMethod.GET,HttpStatus.SC_OK);

        //after adding same device to user2 Get device for user1 should be only 1
        GetDevice deviceDetails01=(GetDevice)getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device2,customerDetails1.getCustomerId()),customerDetails1.getCustomerId(),token1,HttpMethod.GET,HttpStatus.SC_OK);

        //Create a new location event for user 1
        Event event=(Event) eventOperation.createEvent(apiUtils.getEventData("NEW_LOCATION",customerDetails1.getCustomerId(),"smaragal@gigsky.com"),null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);
        event.setNotificationIds(null);
        Event eventDetails=(Event) eventOperation.getEvent(event,event.getEventId(),HttpStatus.SC_OK);
        //Verify notification status is complete and  delivered
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(eventDetails.getNotificationIds(0), "COMPLETE", eventDetails.getEventId(), "PUSH", "COMPLETE_SENT"), eventDetails.getNotificationIds(0));
        assertTrue(notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, eventDetails.getNotificationIds(0)).equals(getDeviceID(deviceDetails01, 0)), "Device id is not Present in table");

       deleteDeviceOperation.deleteDevice(customerDetails1.getCustomerId(),getDeviceID(deviceDetails2,1),token1,HttpStatus.SC_OK,null);
       deleteDeviceOperation.deleteDevice(customerDetails2.getCustomerId(),getDeviceID(deviceDetail02,0),token2,HttpStatus.SC_OK,null);


    }


}
