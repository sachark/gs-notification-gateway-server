package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.AddDevice;
import com.gigsky.tests.resthandlers.beans.GetCustomerDetails;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.GetDevice;
import com.gigsky.tests.resthandlers.impl.DeleteDeviceOperation;
import com.gigsky.tests.resthandlers.impl.DeviceOperation;
import com.gigsky.tests.resthandlers.impl.GetCustomerDetailsOperation;
import com.gigsky.tests.resthandlers.impl.GetDeviceOperation;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;

import static org.junit.Assert.assertTrue;

public class GetCustomerDetailsTest  extends GSNotificationGatewayBaseTest{
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    GetCustomerDetailsOperation customerInfo = GetCustomerDetailsOperation.getInstance();
    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();




    private String getDeviceID(GetDevice deviceDetails, Integer index) {
        return deviceDetails.getList().get(index).getDeviceId();
    }


    private GetCustomerDetails getExpectedResponse() {
        GetCustomerDetails info = new GetCustomerDetails();
        info.setType("CustomerDetail");
        return info;

    }
    @Test
    public void getCustomerDetailInitialize() throws Exception
    {
        // Verify customer Details when more than one device is present for a customer
        //{getExpectedResponse(),HttpStatus.SC_OK,true,"ANDROID",2,"4",gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"4")},
        String token=gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"4");
        BaseBean baseDetails = (BaseBean) customerInfo.getCustomerDetails(getExpectedResponse(),token,HttpStatus.SC_OK);

        AddDevice device1 = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("ANDROID", "5.1.1", "en", "2.8", "ALLOW"), gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"4"), baseDetails.getCustomerId(), 200, null, HttpMethod.POST);
        GetDevice deviceANDROID=(GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device1, baseDetails.getCustomerId()), baseDetails.getCustomerId(), gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"4"), HttpMethod.GET, HttpStatus.SC_OK);
        Thread.sleep(1000);
        GetCustomerDetails detailsNew = (GetCustomerDetails) customerInfo.getCustomerDetails(getExpectedResponse(), token, HttpStatus.SC_OK);

        AddDevice device2 = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse("IOS", "6.0", "en", "2.8", "ALLOW"), gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"4"), baseDetails.getCustomerId(), 200, null, HttpMethod.POST);
        GetDevice deviceIOS=(GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device1,device2, null, baseDetails.getCustomerId(),2), baseDetails.getCustomerId(), gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"4"), HttpMethod.GET, HttpStatus.SC_OK);
        Thread.sleep(1000);
        GetCustomerDetails detailsNew1 = (GetCustomerDetails) customerInfo.getCustomerDetails(getExpectedResponse(), gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"4"), HttpStatus.SC_OK);
        deleteDeviceOperation.deleteDevice(baseDetails.getCustomerId(), getDeviceID(deviceANDROID, 0), token, HttpStatus.SC_OK, null);
        deleteDeviceOperation.deleteDevice(baseDetails.getCustomerId(), getDeviceID(deviceIOS, 0), token, HttpStatus.SC_OK, null);

    }




    @Test(dataProvider = "GetCustomerDetails_Expected")
    public void getCustomerDetailInitialize(BaseBean info,int responseCode,Boolean addDevice,String deviceType,String mockUserId,String token) throws Exception {


        BaseBean baseDetails = (BaseBean) customerInfo.getCustomerDetails(info,token,responseCode);



        if(info instanceof GetCustomerDetails)
            assertTrue("Mock User UUID is not mapped", gatewayDB.getMockUserUUID(gatewayDBConfigInfo, Integer.parseInt(mockUserId)).equals(baseDetails.getCustomerId()));

        if(addDevice) {

                GetCustomerDetails details = (GetCustomerDetails) customerInfo.getCustomerDetails(info, token, responseCode);
                AddDevice device = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse(deviceType, "6.0", "en", "2.8", "ALLOW"), token, details.getCustomerId(), 200, null, HttpMethod.POST);
                getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(device, details.getCustomerId()), details.getCustomerId(), token, HttpMethod.GET, HttpStatus.SC_OK);
                Thread.sleep(1000);
                GetCustomerDetails detailsNew = (GetCustomerDetails) customerInfo.getCustomerDetails(info, token, responseCode);


        }

    }




    @DataProvider(name = "GetCustomerDetails_Expected")
    public Object[][] createData() throws Exception{

        return new Object[][]{
               // BaseBean info,int responseCode,Boolean addDevice,String deviceType,int numOfDevice,String mockUserId,String token
                //Verify customer Details when customer is present in DB
                {getExpectedResponse(), HttpStatus.SC_OK,false,null,"1",gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"1")},

                //Verify customer Details when customer is not present in DB
                {getExpectedResponse(),HttpStatus.SC_OK,false,null,"2",gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"2")},
                //Verify customer Details when no devices are present for a customer

               // Verify customer Details when only one device is present for a customer
                {getExpectedResponse(),HttpStatus.SC_OK,true,"ANDROID","3",gatewayDB.getMockCustomerToken(gatewayDBConfigInfo,"3")},



               //Verify Customer details can not be obtained with non existant Basic Token
                {apiUtils.getExpectedErrorResponse(11301,"Invalid token"),HttpStatus.SC_UNAUTHORIZED,false,null,"0","bbe54d3e117049f0828f72"},


                //Verify Customer details can not be obtained by not providing Basic token
               {apiUtils.getExpectedErrorResponse(11305,"Token missing"),HttpStatus.SC_UNAUTHORIZED,false,null,null,"empty"},



        };
    }

}




