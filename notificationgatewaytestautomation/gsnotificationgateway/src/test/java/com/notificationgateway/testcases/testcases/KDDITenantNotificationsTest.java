package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBAccessUtils;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.*;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import com.gigsky.tests.resthandlers.utils.Enums.AddDeviceData;
import com.gigsky.tests.resthandlers.utils.Enums.TenantsId;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.HttpMethod;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class KDDITenantNotificationsTest {

    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");

    GetCustomerDetailsOperation customerInfo = GetCustomerDetailsOperation.getInstance();
    DeleteDeviceOperation deleteDeviceOperation = DeleteDeviceOperation.getInstance();
    EventOperation eventOperation = EventOperation.getInstance();
    NotificationStatusOperation notificationStatus = NotificationStatusOperation.getInstance();
    NotificationGatewayDB notificationdb = NotificationGatewayDB.getInstance();
    DeviceOperation deviceOperation = DeviceOperation.getInstance();
    GetDeviceOperation getDeviceOperation = GetDeviceOperation.getInstance();
    APIUtils apiUtils = new APIUtils();


    private GetCustomerDetails getCustomerDetailsExpectedResponse() {
        GetCustomerDetails info = new GetCustomerDetails();
        info.setType("CustomerDetail");
        return info;

    }

    @BeforeClass
    public void beforeclass() throws Exception {

        Map<String, String[]> dbScripts = new LinkedHashMap<String, String[]>();
        dbScripts.put("gigskyNotificationGateway", new String[]{"kddi_Inserts.sql"});
        DBAccessUtils.executeMysqlScripts(dbScripts, false);

    }

    @Test(dataProvider = "pushNotifications")
    //Verify sending push notification for kddi tenant
    public void send_Notification(String eventType,String customerId_kddi, String customerId_gigsky,String userEmail) throws Exception{
        GatewayAPIConfigurator.setTenantID(TenantsId.KDDI.tenantId);
        String token_kddi = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerId_kddi);

        String token_gigsky = gatewayDB.getMockCustomerToken(gatewayDBConfigInfo, customerId_gigsky);

        BaseBean customerDetails_kddi = customerInfo.getCustomerDetails(getCustomerDetailsExpectedResponse(), token_kddi, HttpStatus.SC_OK);
        GatewayAPIConfigurator.setTenantID(TenantsId.GIGSKY.tenantId);
        BaseBean customerDetails_gigsky = customerInfo.getCustomerDetails(getCustomerDetailsExpectedResponse(), token_gigsky, HttpStatus.SC_OK);

        GatewayAPIConfigurator.setTenantID(TenantsId.KDDI.tenantId);
        AddDevice deviceIOS_kddi = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse(AddDeviceData.IOS_DEVICE1), token_kddi, customerDetails_kddi.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS_kddi = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS_kddi, customerDetails_kddi.getCustomerId()), customerDetails_kddi.getCustomerId(), token_kddi, HttpMethod.GET, HttpStatus.SC_OK);

        GatewayAPIConfigurator.setTenantID(TenantsId.GIGSKY.tenantId);
        AddDevice deviceIOS_gigsky = (AddDevice) deviceOperation.addDevice(apiUtils.getAddDeviceExpectedResponse(AddDeviceData.IOS_DEVICE1), token_gigsky, customerDetails_gigsky.getCustomerId(), HttpStatus.SC_OK, null, HttpMethod.POST);
        GetDevice deviceDetailsIOS_gigsky = (GetDevice) getDeviceOperation.getDeviceDetails(apiUtils.getDeviceDetailsExpectedResponse(deviceIOS_gigsky, customerDetails_gigsky.getCustomerId()), customerDetails_gigsky.getCustomerId(), token_gigsky, HttpMethod.GET, HttpStatus.SC_OK);


        GatewayAPIConfigurator.setTenantID(TenantsId.KDDI.tenantId);
        Event getPushNotificationData_kddi = (Event) apiUtils.getEventData(eventType,customerDetails_kddi.getCustomerId(),userEmail);
        Event eventCreated_kddi =  (Event )eventOperation.createEvent(getPushNotificationData_kddi,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);

        GatewayAPIConfigurator.setTenantID(TenantsId.GIGSKY.tenantId);
        Event getPushNotificationData_gigsky = (Event) apiUtils.getEventData(eventType,customerDetails_gigsky.getCustomerId(),userEmail);
        Event eventCreated_gigsky =  (Event )eventOperation.createEvent(getPushNotificationData_gigsky,null,HttpStatus.SC_OK, HttpMethod.POST);
        Thread.sleep(5000);

        GatewayAPIConfigurator.setTenantID(TenantsId.KDDI.tenantId);
        Event getEventLOCATION_kddi = (Event) eventOperation.getEvent(getPushNotificationData_kddi, eventCreated_kddi.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);

        GatewayAPIConfigurator.setTenantID(TenantsId.GIGSKY.tenantId);
        Event getEventLOCATION_gigsky = (Event) eventOperation.getEvent(getPushNotificationData_gigsky, eventCreated_gigsky.getEventId(), HttpStatus.SC_OK);
        Thread.sleep(5000);


        GatewayAPIConfigurator.setTenantID(TenantsId.KDDI.tenantId);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEventLOCATION_kddi.getNotificationIds(0), "COMPLETE", getEventLOCATION_kddi.getEventId(), "PUSH", "COMPLETE_SENT"), getEventLOCATION_kddi.getNotificationIds(0));
        assertEquals(deviceDetailsIOS_kddi.getList().get(0).getDeviceId(), notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEventLOCATION_kddi.getNotificationIds(0)), "Device id is not Present in table");

        GatewayAPIConfigurator.setTenantID(TenantsId.GIGSKY.tenantId);
        notificationStatus.getnotificationStatus(apiUtils.getNotificationStatusResponse(getEventLOCATION_gigsky.getNotificationIds(0), "COMPLETE", getEventLOCATION_gigsky.getEventId(), "PUSH", "COMPLETE_SENT"), getEventLOCATION_gigsky.getNotificationIds(0));
        assertEquals(deviceDetailsIOS_gigsky.getList().get(0).getDeviceId(), notificationdb.validateDeviceIdMockPushNotificationTable(gatewayDBConfigInfo, getEventLOCATION_gigsky.getNotificationIds(0)), "Device id is not Present in table");

    }

    @DataProvider(name = "pushNotifications")
    public Object[][] createData(){
        return new Object[][]{
                {"LOW_BALANCE","31","2","smaragal@gigsky.com"},
                {"SUBSCRIPTION_NEARING_EXPIRY","32","3","jchinta@gigsky.com"},
                {"NEW_LOCATION","33","4","vinay@gigsky.com"},
                {"SUBSCRIPTION_EXPIRED","34","5","subbu@gigsky.com"},
                {"DATA_USED","35","6","pradeep@gigsky.com"}
        };
    }
}
