package com.notificationgateway.testcases.testsetup;

import com.gigsky.tests.common.database.utils.DBAccessUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.LinkedHashMap;
import java.util.Map;

public class CommonSuiteSetup {
    private static final Logger logger = LoggerFactory.getLogger(CommonSuiteSetup.class);


    @BeforeSuite
    public void beforeSuite() throws Exception {
        logger.info("beforeSuite");

        /*Execute PreSuite Script*/
        //TestUtils.executeShellScript("integrated/testsetup/serverStop.sh");


        Map<String, String[]> dbScripts = new LinkedHashMap<String, String[]>();
        dbScripts.put("gigskyNotificationGateway", new String[]{"gigskyNotificationGateway.sql"});

        boolean doTruncate = true;
        DBAccessUtils.executeMysqlScripts(dbScripts, true);

    }

    @AfterSuite
    public void afterSuite() throws Exception {
        logger.info("afterSuite");

    }


}
