package com.notificationgateway.testcases.testsetup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


public class CommonTestSetup {
    private static final Logger logger = LoggerFactory.getLogger(CommonTestSetup.class);

    @BeforeTest
    public void beforeTest() throws Exception {
        logger.info("beforeTest");
    }

    @AfterTest
    public void afterTest() throws Exception {
        logger.info("afterTest");
    }

}
