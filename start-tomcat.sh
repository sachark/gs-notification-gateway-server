#!/bin/sh
counter=0
flag=0
while [ $counter -lt 300 ]
do
let "counter+=1"
if [ -f '/db.txt' ]
then
   /var/lib/tomcat8/bin/catalina.sh run
   echo "db.txt file found"
   flag=1
   break
else
   echo "db.txt not found"
   sleep 5
fi
done
if [ $flag -eq 0 ]
then
  echo "killing the container"
  sleep 5
  kill 1  
fi
